
import React, {useEffect, useState, useCallback} from 'react';
import RouteSectionInfo from '../../App/components/RouteSectionInfo';

const {kakao} = window;
let modalFlag = false;
const KakaoMap = ({mpaId, style}) => {
    const [isOpen, setIsOpen] = useState(false);
    const [modalContent, setModalContent] = useState(null);
    useEffect(()=>{
        kakao.maps.load(() => {
            let container = document.getElementById(mpaId);
            let options = {
                center: new kakao.maps.LatLng(37.506502, 127.053617),
                level: 7
            };
            
            const map = new window.kakao.maps.Map(container, options);
            kakao.maps.event.addListener(map, 'click', function(mouseEvent){
                modalFlag = isOpen;
                modalFlag = !modalFlag;
                setIsOpen(modalFlag);
            });
        });
    }, []);

    useEffect(()=>{
        if(isOpen) setModalContent(<RouteSectionInfo isOpen={isOpen} toggle={toggle}/>);
        else setModalContent(null);
    }, [isOpen]);

    const toggle = (e) => {
        // if(e._dispatchListeners.length==2) return;
        // if(e._dispatchListeners.length>1&&e._dispatchListeners[0].name=="") return;
        // if(e._dispatchListeners.length>1&&e._dispatchListeners[0].name==e._dispatchListeners[1].name) return;
        setIsOpen(!isOpen);
    }
    

    return (
        <>
            <div id={mpaId} className="pcoded-content" style={style} ></div>
            {modalContent}
        </>
    );
}


// /*global kakao*/    
// const InitMap = (initialAppkey, mpaId, openModal) => {
//     const script = document.createElement("script");
//     script.async = true;
//     script.src = "//dapi.kakao.com/v2/maps/sdk.js?appkey="+initialAppkey+"&autoload=false&libraries=services"; //
//     document.head.appendChild(script);

//     let map = null;
//     script.onload = () => {
//         kakao.maps.load(() => {
//             let container = document.getElementById(mpaId);
//             let options = {
//                 center: new kakao.maps.LatLng(37.506502, 127.053617),
//                 level: 7
//             };
  
//             map = new window.kakao.maps.Map(container, options);
//             kakao.maps.event.addListener(map, 'click', function(mouseEvent){
                
//             });
//         });

        

//     };
// }

// const openModal = () => {
//     return <NavItem key="0" title="구간 정보창" component="NodeLinkUpdate" className="window-full" />;
// }

// const KakaoMap = ({mpaId, style}) => {
//     useEffect(() => {
//         InitMap("db7acec995c68e8de2508faf6e9242f7", mpaId, openModal);
//     }, []);

//     return (
//         <div id={mpaId} className="pcoded-content" style={style} ></div>
//     );
// }


export default KakaoMap;