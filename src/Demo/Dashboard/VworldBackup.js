import React, { useEffect, useState } from "react";
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import XYZ from 'ol/source/XYZ';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {Icon, Style} from 'ol/style';
import VectorSource from 'ol/source/Vector';
import {fromLonLat} from 'ol/proj';
import { Vector as VectorLayer} from 'ol/layer';
import MultiPoint from 'ol/geom/MultiPoint';
import marker from './marker.png';
import RouteSectionInfo from '../../App/components/RouteSectionInfo';
let point;
let vectorSource ;
let vectorLayer;    
let map = null;
let view;
let multiPoint;
let modalFlag = false;
const VworldBackup = ({style, pointx, pointy, spotSearch, flag, address}) => {
  const [stateMap, setStateMap] = useState();
  // const [pointXX, setPointX] = useState(pointx);
  // const [pointYY, setPointY] = useState(pointy);
  const [isOpen, setIsOpen] = useState(false);
  const [modalContent, setModalContent] = useState(null);
  useEffect(()=>{
      if(isOpen) setModalContent(<RouteSectionInfo isOpen={isOpen} toggle={toggle}/>);
      else setModalContent(null);
  }, [isOpen]);
  const toggle = (e) => {
      setIsOpen(!isOpen);
  }
    useEffect(()=>{       
             
      point =  new Feature({          
            geometry: new Point(fromLonLat([parseFloat(pointx), parseFloat(pointy)])),            
        });        
        point.setStyle(
          new Style({
            image: new Icon({
              color: '#BADA55',
              crossOrigin: 'anonymous',
              // For Internet Explorer 11
              imgSize: [50,50],
              src: marker,
            }),
          })
        );
        
        vectorSource = (new VectorSource({
          features: [point],
        }));

        vectorLayer = (new VectorLayer({
          source: vectorSource,
        }));

        view = new View({
          center: fromLonLat([parseFloat(pointx), parseFloat(pointy)]), // center 좌표
          zoom: 12, // 초기화면 zoom level
          minZoom: 6, // 최소 zoom level
          maxZoom: 19, // 최대 zoom level
        });

       map = new Map({ 
          target: "_map",
          layers: [
            new TileLayer({
                visible: true,
                type:'base',
                opacity:1,
                source: new XYZ({
                  url: 'http://xdworld.vworld.kr:8080/2d/Base/202002/{z}/{x}/{y}.png',
                }),
            }),
            new TileLayer({
              // extent: [-13884991, 2870341, -7455066, 6338219],
              source: new TileWMS({
                url: 'http://192.168.0.104:8080/geoserver/tpms/wms',
                params: {'LAYERS': 'tpms:tpms_station_master', 'TILED': true},
                serverType: 'geoserver',
                // Countries have transparency, so do not fade tiles:
                transition: 0,
              }),
          }),
            // sidoLayer,
            // sigunLayer,
            // emdLayer,
            vectorLayer
          ],
          view: view
      });
      setStateMap(map);

    },[]);


    useEffect(()=>{
   
      multiPoint = new Feature({
        geometry: new MultiPoint([fromLonLat([parseFloat(pointx), parseFloat(pointy)])], 'XYZ'),  
      })
      
      multiPoint.setStyle(
        new Style({
          image: new Icon({
            color: '#BADA55',
            crossOrigin: 'anonymous',
            // For Internet Explorer 11
            imgSize: [50,50],
            src: marker,
          }),
        })
      );
      
      vectorLayer.setSource(new VectorSource({
        features: [multiPoint],
      }));  

    },[pointx, pointy]);

    const mapOnClick = (evt) => {
      evt.preventDefault();
      modalFlag = isOpen;
      modalFlag = !modalFlag;
      setIsOpen(modalFlag);
    }
    return (
        <>
            <div id="_map" style={style} pointx={pointx} pointy={pointy} onClick={mapOnClick}></div>
            {modalContent}
        </>
    );

}

export default VworldBackup;