import React, { useEffect, useState } from "react";
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import XYZ from 'ol/source/XYZ';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {Icon, Style} from 'ol/style';
import VectorSource from 'ol/source/Vector';
import {fromLonLat} from 'ol/proj';
import { Vector as VectorLayer} from 'ol/layer';
import MultiPoint from 'ol/geom/MultiPoint';
import marker from './marker.png';
import RouteSectionInfo from '../../App/components/RouteSectionInfo';
let point;
let vectorSource ;
let vectorLayer;    
// let map = null;
// let view;
let multiPoint;
let modalFlag = false;
const Vworld = ({style, pointx, pointy, spotSearch, flag, address}) => {
  const [stateMap, setStateMap] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [modalContent, setModalContent] = useState(null);

  const [map, setMap] = useState(null);
  const [view, setView] = useState();

  useEffect(()=>{
      if(isOpen) setModalContent(<RouteSectionInfo isOpen={isOpen} toggle={toggle}/>);
      else setModalContent(null);
  }, [isOpen]);
  const toggle = (e) => {
      setIsOpen(!isOpen);
  }
    useEffect(()=>{       

        // view = new View({
        //   center: fromLonLat([parseFloat(pointx), parseFloat(pointy)]), // center 좌표
        //   zoom: 12, // 초기화면 zoom level
        //   minZoom: 6, // 최소 zoom level
        //   maxZoom: 19, // 최대 zoom level
        // });
        setView(new View({
            center: fromLonLat([parseFloat(pointx), parseFloat(pointy)]), // center 좌표
            zoom: 12, // 초기화면 zoom level
            minZoom: 6, // 최소 zoom level
            maxZoom: 19, // 최대 zoom level
        }));

    },[]);

    useEffect(()=>{
      if(view){
        setMap(new Map({ 
          target: "_map",
          layers: [
            new TileLayer({
                visible: true,
                type:'base',
                opacity:1,
                source: new XYZ({
                  url: 'http://xdworld.vworld.kr:8080/2d/Base/202002/{z}/{x}/{y}.png',
                }),
            }),
            new TileLayer({
              source: new TileWMS({
                url: 'http://192.168.0.104:8080/geoserver/tpms/wms',
                params: {
                          'LAYERS': 'tpms:tpms_sm_master', 
                          'TILED': true,
                          // CQL_FILTER : 'id in (17818, 17819, 17820, 17821)'
                        },
                serverType: 'geoserver',
                transition: 0,
              }),
          }),
            // vectorLayer
          ],
          view: view
        }));
      }
      //   map = new Map({ 
      //     target: "_map",
      //     layers: [
      //       new TileLayer({
      //           visible: true,
      //           type:'base',
      //           opacity:1,
      //           source: new XYZ({
      //             url: 'http://xdworld.vworld.kr:8080/2d/Base/202002/{z}/{x}/{y}.png',
      //           }),
      //       }),
      //       new TileLayer({
      //         source: new TileWMS({
      //           url: 'http://192.168.0.104:8080/geoserver/tpms/wms',
      //           params: {
      //                     'LAYERS': 'tpms:tpms_sm_master', 
      //                     'TILED': true,
      //                     // CQL_FILTER : 'id in (17818, 17819, 17820, 17821)'
      //                   },
      //           serverType: 'geoserver',
      //           transition: 0,
      //         }),
      //     }),
      //       // vectorLayer
      //     ],
      //     view: view
      // });
      // setStateMap(map);
    }, [view]);


    const mapOnClick = (evt) => {
      evt.preventDefault();
      modalFlag = isOpen;
      modalFlag = !modalFlag;
      setIsOpen(modalFlag);
    }
    return (
        <>
            <div id="_map" style={style} pointx={pointx} pointy={pointy} ></div> {/* onClick={mapOnClick} */}
            {modalContent}
        </>
    );

}

export default Vworld;