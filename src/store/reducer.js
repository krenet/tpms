import * as actionTypes from './actions';
import config from './../config';
// import {server_connect} from '../tpms-common';

const initialState = {
    isOpen: [], //for active default menu
    isTrigger: [], //for active default menu, set blank for horizontal
    ...config,
    isFullScreen: false, // static can't change
    isChatOpen: false, // chat open control,
    allRows: null,
    selectedData: null,  // selected rows from a grid
    searchMapList : null,
    chatHeader: [],
    layerCheckList: [],
    cqlFilter: [],
};

const reducer = (state = initialState, action) => {
    let trigger = [];
    let open = [];
    
    switch (action.type) {
        case actionTypes.REOPEN_CHAT:
            return {
                ...state,
                isChatOpen: true
            };
        case actionTypes.OPEN_CHAT:
            if(action.data.action == 'OPEN_CHAT'){
                return {
                    ...state,
                    isChatOpen: true,
                    allRows: action.data.allData,
                    selectedData: action.data.selectedNodes,
                    searchMapList : action.data.selectedNodes.map((obj)=>{return obj['data']}),
                    chatHeader: action.data.header,
                    changeLayer : null
                };
            }else if(action.data.action == 'OPEN_CHAT_TARGET'){
                return {
                    ...state,
                    isChatOpen: true,
                    allRows: action.data.allTarget,
                    selectedData: action.data.selectedNodes,
                    searchMapList : action.data.selectedNodes.map((obj)=>{return obj['data']}),
                    chatHeader: action.data.header,
                    changeLayer : null
                };
            }else if(action.data.action == 'OPEN_CHAT_SCHEDULAR'){
                return {
                    ...state,
                    isChatOpen: true,
                    allRows: action.data.allScRows,
                    selectedData: action.data.scNodes,
                    searchMapList : action.data.scNodes.map((obj)=>{return obj['data']}),
                    chatHeader: action.data.header,
                    changeLayer : null
                };
            }
        case actionTypes.CLOSE_CHAT:
            return {
                ...state,
                isChatOpen: false
            };
        // case actionTypes.REFRESH_CHAT:
        //     return {
        //          ...state,
        //         isChatOpen: false,
        //         allRows: null,
        //         selectedData: null,
        //         chatHeader: []
        //     };
        case actionTypes.COLLAPSE_MENU:
            return {
                ...state,
                collapseMenu: !state.collapseMenu
            };
        case actionTypes.COLLAPSE_TOGGLE:
            if (action.menu.type === 'sub') {
                open = state.isOpen;
                trigger = state.isTrigger;

            const triggerIndex = trigger.indexOf(action.menu.id);
            if (triggerIndex > -1) {
                open = open.filter(item => item !== action.menu.id);
                trigger = trigger.filter(item => item !== action.menu.id);
            }

            if (triggerIndex === -1) {
                open = [...open, action.menu.id];
                trigger = [...trigger, action.menu.id];
            }
        } else {
            open = state.isOpen;
            const triggerIndex = (state.isTrigger).indexOf(action.menu.id);
            trigger = (triggerIndex === -1) ? [action.menu.id] : [];
            open = (triggerIndex === -1) ? [action.menu.id] : [];
        }

        return {
            ...state,
            isOpen: open,
            isTrigger: trigger
        };
    case actionTypes.NAV_CONTENT_LEAVE:
        return {
            ...state,
            isOpen: open,
            isTrigger: trigger,
        };
    case actionTypes.NAV_COLLAPSE_LEAVE:
        if (action.menu.type === 'sub') {
            open = state.isOpen;
            trigger = state.isTrigger;

            const triggerIndex = trigger.indexOf(action.menu.id);
            if (triggerIndex > -1) {
                open = open.filter(item => item !== action.menu.id);
                trigger = trigger.filter(item => item !== action.menu.id);
            }
            return {
                ...state,
                isOpen: open,
                isTrigger: trigger,
            };
        }
        return {...state};
    case actionTypes.FULL_SCREEN :
        return {
            ...state,
            isFullScreen: !state.isFullScreen
        };
    case actionTypes.FULL_SCREEN_EXIT:
        return {
            ...state,
            isFullScreen: false
        };
    case actionTypes.CHANGE_LAYOUT:
        return {
            ...state,
            layout: action.layout
        };
    case actionTypes.CHAGE_SEARCH_LAYER:
        return {
            ...state,
            searchMapList: action.data,
            changeLayer : null
        };
    case actionTypes.LAYER_CHECK:
        return{
            ...state,
            changeLayer: action.changeLayer,
            cqlFilter: action.changeLayer.cqlFilter,
            layerCheckList: action.changeLayer.layerCheckList
        };
    case actionTypes.PAVE_INX_RULE:
        return{
            ...state,
            formula : action.formula
        };
    case actionTypes.GEO_LON_LAT:    
        return{
            ...state,
            geoData : action.data
        };
    case actionTypes.CONNECT_WEB_SOCKET:
        return{
            ...state,
            socketFlag : action.flag
        };
    default:
        return state;
}
};

export default reducer;