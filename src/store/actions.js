export const COLLAPSE_MENU = 'COLLAPSE_MENU';
export const COLLAPSE_TOGGLE = 'COLLAPSE_TOGGLE';
export const FULL_SCREEN = 'FULL_SCREEN';
export const FULL_SCREEN_EXIT = 'FULL_SCREEN_EXIT';
export const CHANGE_LAYOUT = 'CHANGE_LAYOUT';
export const NAV_CONTENT_LEAVE = 'NAV_CONTENT_LEAVE';
export const NAV_COLLAPSE_LEAVE = 'NAV_COLLAPSE_LEAVE';
export const REOPEN_CHAT = 'REOPEN_CHAT';
export const OPEN_CHAT = 'OPEN_CHAT';
export const CLOSE_CHAT = 'CLOSE_CHAT';
export const CHAGE_SEARCH_LAYER = 'CHAGE_SEARCH_LAYER';
export const LAYER_CHECK = 'LAYER_CHECK';
export const GEO_LON_LAT = 'GEO_LON_LAT';
export const PAVE_INX_RULE = 'PAVE_INX_RULE';
export const CONNECT_WEB_SOCKET = 'CONNECT_WEB_SOCKET';
// export const REFRESH_CHAT = 'REFRESH_CHAT';