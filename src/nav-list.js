export default 
    [
        {
            category: "일반",
            menuList: [
                // {
                //     title : "테스트",
                //     component: "Test",
                //     className: "window-full"
                // },
                {
                    title: "포장 인덱스 공식 설정",
                    component: "PaveInxRuleSetting"
                },
                // {
                //     title: "환경설정",
                //     component: "ConfigSetting"
                // },
                // {
                //     title: "메뉴얼",
                //     component: "Manual",
                    
                // }
            ]
        },
        {
            category: "노선관리",
            menuList: [
                { 
                    title: "등록 및 수정",
                    component: "CreateAndUpdate"
                }, 
                { 
                    title: "마스터",
                    component: "RouteMaster", 
                    className: "window-full",
                }, 
                { 
                    title: "위치 검색",
                    component: "PosSearch"
                }
            ]
        },
        {
            category: "조사관리",
            menuList: [
                { 
                    title: "조사 플래너",
                    component: "SurveyPlanner",
                    className: "window-full"
                },
                { 
                    title: "조사대상구간",
                    component: "SurveyTarget",
                    className: "window-full"
                },
                { 
                    title: "상세조사구간",
                    component: "SurveyDetail",
                    className: "window-full"
                },
                { 
                    title: "1차 장비조사 데이터 입력",
                    component: "EquipmentSurvey",
                }, 
                { 
                    title: "2차 장비조사 데이터 입력",
                    component: "EquipmentSurvey2",
                },
                {
                    title : "포장 조사 마스터", 
                    component: "SurveyMaster",
                    className: "window-full"
                }
            ]
        },
        {
            category: "보수관리",
            menuList: [
                { 
                    title: "보수대상구간",
                    component: "RepairTarget",
                    className: "window-full"
                },
                { 
                    title: "보수 이력 마스터",
                    component: "RepairRecord",
                    className: "window-full"
                },
                { 
                    title: "보수 데이터 입력",
                    component: "MthistoryInsert"
                }
            ]
        },
        {
            category: "보고서",
            menuList: [
                {
                    title: "도로 보수 우선순위 보고서",
                    component: "RoadRepairPriorityReport",
                    className: "window-full"
                }
            ]
        },
        // {
        //     category: "DB관리",
        //     menuList: [
        //         { 
        //             title: "노드링크 업데이트",
        //             component: "NodeLinkUpdate"
        //         }, 
        //         { 
        //             title: "노드링크 수정",
        //             component: null
        //         }
        //     ]
        // },
        // {
        //     category: "통계",
        //     menuList: [
        //         { 
        //             title: "통계1",
        //             component: "StatisticsOne",
        //             className: "window-full"
        //         }, 
        //         { 
        //             title: "통계2",
        //             component: "StatisticsTwo",
        //             className: "window-full"
        //         },
        //         { 
        //             title: "통계3",
        //             component: "StatisticsThree",
        //             className: "window-full"
        //         }
        //     ]
        // },
        // {
        //     category: "관리자",
        //     menuList: [
        //         { 
        //             title: "회원 관리",
        //             component: null
        //         }, 
        //         { 
        //             title: "보안 관리",
        //             component: null
        //         }, 
        //         { 
        //             title: "로그 관리",
        //             component: null
        //         }
        //     ]
        // },
    ];
