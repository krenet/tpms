import React, { Component, Suspense } from 'react';
import {connect} from 'react-redux';
import Fullscreen from "react-full-screen";
import windowSize from 'react-window-size';
import Navigation from './Navigation';
import NavBar from './NavBar';
import Aux from "../../../hoc/_Aux";
import * as actionTypes from "../../../store/actions";
import './app.scss';
import TpmsMap from '../../components/TpmsMap';
import Loader from "../Loader";

class AdminLayout extends Component {
    state={
        pointx:"127.1575",
        pointy:"37.6093",
        spotSearch:"",
        flag:false,
        road:[],
    }

    adminParentFunc= (spotSearch, _pointx, _pointy, flag, road) => {
        this.setState({pointx:_pointx, pointy:_pointy, spotSearch:spotSearch, flag:flag, road:road});
    }

    fullScreenExitHandler = () => {
        if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
            this.props.onFullScreenExit();
        }
    };
    
    componentWillMount() {
        if (this.props.windowWidth > 992 && this.props.windowWidth <= 1024 && this.props.layout !== 'horizontal') {
            this.props.onComponentWillMount();
        }
    }
    
    mobileOutClickHandler() {
        if (this.props.windowWidth < 992 && this.props.collapseMenu) {
            this.props.onComponentWillMount();
        }
    }
    
    render() {
        // const mainMapStyle = {
        //     height : "92.5vh",
        //     width : "100%"
        // }
        /* full screen exit call */
        document.addEventListener('fullscreenchange', this.fullScreenExitHandler);
        document.addEventListener('webkitfullscreenchange', this.fullScreenExitHandler);
        document.addEventListener('mozfullscreenchange', this.fullScreenExitHandler);
        document.addEventListener('MSFullscreenChange', this.fullScreenExitHandler);
        
        // const menu = routes.map((route, index) => {
            //     return (route.component) ? (
                //         <Route
                //             key={index}
                //             path={route.path}
                //             exact={route.exact}
                //             name={route.name}
                //             render={props => (
                    //                 <route.component {...props} />
                    //             )} />
                    //     ) : (null);
                    // });
                    
                   
                    return (
                        <Aux>
                <Fullscreen enabled={this.props.isFullScreen}>
                    <Navigation />
                    <NavBar adminParentFunc={this.adminParentFunc}/>
                    <div className="pcoded-main-container" onClick={() => this.mobileOutClickHandler}>
                        <div className="pcoded-wrapper">
                            {/* <KakaoMap mpaId="map" style={mainMapStyle}/> */}
                            {/* <Vworld id="_map"  style={mainMapStyle} pointx={this.state.pointx} pointy={this.state.pointy} spotSearch={this.state.spotSearch} flag={this.state.flag} address={this.state.road}/> */}
                            
                            <Suspense fallback={<Loader/>}>
                                <TpmsMap id="_map" />
                            </Suspense>
                            {/* <div className="pcoded-content">
                                <div className="pcoded-inner-content">
                                    <Breadcrumb />
                                    <div className="main-body">
                                        <div className="page-wrapper">
                                            <Suspense fallback={<Loader/>}>
                                                <Switch>
                                                    {menu}
                                                    <Redirect from="/" to={this.props.defaultPath} />
                                                </Switch>
                                            </Suspense>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Fullscreen>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        defaultPath: state.defaultPath,
        isFullScreen: state.isFullScreen,
        collapseMenu: state.collapseMenu,
        configBlock: state.configBlock,
        layout: state.layout
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFullScreenExit: () => dispatch({type: actionTypes.FULL_SCREEN_EXIT}),
        onComponentWillMount: () => dispatch({type: actionTypes.COLLAPSE_MENU})
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (windowSize(AdminLayout));