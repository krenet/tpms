import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import windowSize from 'react-window-size';
import Aux from "../../../../../../hoc/_Aux";
import NavIcon from "./../NavIcon";
import NavBadge from "./../NavBadge";
import * as actionTypes from "../../../../../../store/actions";
import { Form } from 'react-bootstrap';
import './index.css';

class NavCheckBox extends Component {
    render() {

        const layerCheckEvt = (evt) => {
            var isChecked = evt.target.checked;
            var tagId = evt.target.getAttribute("id");
            var tempParentNode = evt.target.parentNode.parentNode.parentNode.parentNode;
            var parentUlChild = tempParentNode.children;

            var childCheckBox = new Array();
            for(var i=0;i<parentUlChild.length;++i){
                childCheckBox.push(parentUlChild[i].querySelector("input"));
            }
            var parent = tempParentNode.previousSibling;
            var title = parent.getAttribute("data-title");
            
            if(tagId.includes('--1')){
                childCheckBox.map((input, inx)=>{ if(inx>0) input.checked = false; });
            }else{
                var cnt = 0;
                if(childCheckBox.length!=1){
                    childCheckBox.map((input, inx)=>{
                        if(inx>0) {
                            if(input.checked) ++cnt;
                        }
                    });
                    
                    if(cnt==(childCheckBox.length-1)) {
                        childCheckBox[0].checked = true;
                        childCheckBox.map((input, inx)=>{ if(inx>0) input.checked = false; });
                        isChecked = true;
                    }else childCheckBox[0].checked = false;
                }
                
            }
            
            // 현재 클릭한 체크박스의 그룹을 제외한 다른 그룹의 체크박스 전체 해제
            var allCheckBox = document.getElementById("layer-tab-area").querySelectorAll("input");
            allCheckBox.forEach((value)=>{
                var _id = value.getAttribute("id");
                if(!(_id.includes(title))) value.checked = false;
            });
            
            var topParentNode = tempParentNode.parentNode.parentNode.querySelectorAll("input");
            var cqlFilter = new Object();
            var category = [];
            var tempLayer = '';
            var tempStyle = '';
            // 전체 체크박스 중 체크된 항목에 한하여 cql 리스트와 카테고리 리스트 만들기
            topParentNode.forEach((value, inx, list)=>{
                if(value.checked && value.value!="") {
                    // cql.push(value.value);
                    var aTag = value.parentNode.parentNode.parentNode.parentNode.previousSibling;
                    var temp_cate = aTag.getAttribute("data-category");
                    var cql = [];
                    if(cqlFilter.hasOwnProperty(temp_cate)){
                        cql = cqlFilter[temp_cate];
                    }
                    cql.push(value.value);
                    cqlFilter[temp_cate] = cql;
                    tempLayer = aTag.getAttribute("data-layer");
                    tempStyle = aTag.getAttribute("data-style");
                    if(!category.includes(temp_cate)) category.push(temp_cate);
                }
            });

            var cql = [];
            var keys = Object.keys(cqlFilter);
            keys.map((key)=>{
                cql.push("("+cqlFilter[key].join(" or ")+")");
            });
            if(cql.length>1){
                var cqlStr = cql.join(" and ");
                cql = [];
                cql.push(cqlStr);
            }
            // var cql = [];
            // var category = [];
            // var tempLayer = '';
            // var tempStyle = '';
            // // 전체 체크박스 중 체크된 항목에 한하여 cql 리스트와 카테고리 리스트 만들기
            // topParentNode.forEach((value, inx, list)=>{
            //     if(value.checked && value.value!="") {
            //         cql.push(value.value);
            //         var aTag = value.parentNode.parentNode.parentNode.parentNode.previousSibling;
            //         var temp_cate = aTag.getAttribute("data-category");
            //         tempLayer = aTag.getAttribute("data-layer");
            //         tempStyle = aTag.getAttribute("data-style");
            //         if(!category.includes(temp_cate)) category.push(temp_cate);
            //     }
            // });

            var layer='';
            var style='';
            if(category.length>1){
                // data category가 title인 object의 data-layer, data-style 가져오기
                var obj = document.querySelector('a[data-category="'+title+'"]');
                layer = obj.getAttribute("data-layer");
                style = obj.getAttribute("data-style");
            }else{
                if(evt.target.checked){
                    var elStyle = evt.target.getAttribute("data-style");
                    if(elStyle) style = elStyle;
                    else style = parent.getAttribute("data-style");
                    layer = parent.getAttribute("data-layer");
                }else{
                    layer = tempLayer;
                    style = tempStyle;
                }
            }
            this.props.onLayerCheck({checked : isChecked, 
                layer : layer, style : style,
                cql : evt.target.value, cqlFilter: cql, layerCheckList: category});
        }

        let itemTitle = this.props.item.title;
        if (this.props.item.icon) {
            itemTitle = <span className="pcoded-mtext">{this.props.item.title}</span>;
        }

        let itemTarget = '';
        if (this.props.item.target) {
            itemTarget = '_blank';
        }

        let subContent;
        if(this.props.item.external) {
            subContent = (
                <a href={this.props.item.url} target='_blank' rel='noopener noreferrer'>
                    <NavIcon items={this.props.item}/>
                    {itemTitle}
                    <NavBadge layout={this.props.layout} items={this.props.item}/>
                </a>
            );
        } else {
            subContent = (
                <Form.Group className="nav-link" style={{marginBottom: "0", paddingTop:"5px", paddingBottom:"5px", paddingLeft:"70px", paddingRight:"0px"}}>
                    <Form.Check type="checkbox" id={this.props.item.category + '-' + this.props.item.id}>
                        <Form.Check.Input type="checkbox" isValid onChange={layerCheckEvt} value={this.props.item.cql} disabled={this.props.item.disabled} />
                        <Form.Check.Label className="check-label">
                            <span className={`legend ${this.props.item.color}`}></span>
                            {this.props.item.label}
                        </Form.Check.Label>
                    </Form.Check>
                </Form.Group>
            );
        }
        let mainContent = '';
        if (this.props.layout === 'horizontal') {
            mainContent = (
                <li onClick={this.props.onItemLeave}>{subContent}</li>
            );
        } else {
            if (this.props.windowWidth < 992) {
                mainContent = (
                    <li className={this.props.item.classes} onClick={this.props.onItemClick}>{subContent}</li>
                );
            } else {
                mainContent = (
                    <li className={this.props.item.classes}>{subContent}</li>
                );
            }
        }

        return (
            <Aux>
                {mainContent}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        layout: state.layout,
        collapseMenu: state.collapseMenu,
        cqlFilter: state.cqlFilter,
        layerCheckList: state.layerCheckList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onItemClick: () => dispatch({type: actionTypes.COLLAPSE_MENU}),
        onItemLeave: () => dispatch({type: actionTypes.NAV_CONTENT_LEAVE}),
        onLayerCheck: (props) => dispatch({type: actionTypes.LAYER_CHECK, changeLayer: props})
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (windowSize(NavCheckBox)));
