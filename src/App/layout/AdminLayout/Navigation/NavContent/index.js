import React, {Component} from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import windowSize from 'react-window-size';
import Aux from "../../../../../hoc/_Aux";
import NavButton from './NavButton';
import DEMO from "../../../../../store/constant";
import * as actionTypes from "../../../../../store/actions";
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import Draggable from 'react-draggable';
import NavTreeCheckbox from './NavTreeCheckbox';
import { ModalFooter } from 'react-bootstrap';
import NavGroup from './NavGroup';

class NavContent extends Component {
    state = {
        scrollWidth: 0,
        prevDisable: true,
        nextDisable: false,
        modal: false,
        tabId : 'road-layer',
        layerChkList : [],
        layerItems : this.props.navigation,
        navigation:[],
        itemRefresh: false
    };
    
    scrollPrevHandler = () => {
        const wrapperWidth = document.getElementById('sidenav-wrapper').clientWidth;
        
        let scrollWidth = this.state.scrollWidth - wrapperWidth;
        if(scrollWidth < 0) {
            this.setState({scrollWidth: 0, prevDisable: true, nextDisable: false});
        } else {
            this.setState({scrollWidth: scrollWidth, prevDisable: false});
        }
    };
    
    scrollNextHandler = () => {
        const wrapperWidth = document.getElementById('sidenav-wrapper').clientWidth;
        const contentWidth = document.getElementById('sidenav-horizontal').clientWidth;
        
        let scrollWidth = this.state.scrollWidth + (wrapperWidth - 80);
        if (scrollWidth > (contentWidth - wrapperWidth)) {
            scrollWidth = contentWidth - wrapperWidth + 80;
            this.setState({scrollWidth: scrollWidth, prevDisable: false, nextDisable: true});
        } else {
            this.setState({scrollWidth: scrollWidth, prevDisable: false});
        }
    };
    
    toggle = () => {
        this.setState({modal:!(this.state.modal)});
    };

    chekedLayer =()=> {
        //모든 아이템 visible false 로 세팅
        var flag = true;
        var tabId = null;
        var Items = this.state.layerItems;
        if(this._layerChkList.length > 0){
            Items.map(item =>{                                                
                item.visible = false;                
                if(item.children){
                    for(let inx =0; inx<item.children.length;++inx){
                        item.children[inx].visible = false;
                    }
                }
            });
            //체크 박스에 세팅된 항목 visible true로 세팅
            this._layerChkList.map(layerChk => {
                Items.map(item =>{
                    //최상위 아이템일 경우
                    if(layerChk === item.id){
                        if(flag){
                            tabId = item['id'];
                            flag = false;
                        }
                        item.visible = true;
                    }
                    //자식 아이템일 경우
                    if(item.children){
                        for(let inx =0; inx<item.children.length;++inx){
                            if(layerChk === item.children[inx].id){
                                item.children[inx].visible = true;
                            }                            
                        }
                    }
                });
                
            });
            //레이어 버튼및 그룹 다시 그리기
            this.setState({navigation : Items});
        }else{
            alert("하나 이상의 레이어를 선택하셔야 합니다.");
        }        
        this.setState({tabId : tabId,
                    itemRefresh : true});
        
        this.toggle();
    }
    
    _layerChkList = [];
    checkList = (checked) =>{
        this._layerChkList = checked;
    }

    btnclick = (props) =>{
        this.setState({tabId : props.group.id});
    }

    itemRefreshControl = () => {
        this.setState({itemRefresh:false});
    }
    
    render() {
        const navBtns = this.props.navigation.map(item => {
            if(item.visible)  return <NavButton layout={this.props.layout} btnclick={this.btnclick} key={item.id} group={item}/>;
        });
        
        const navItems = this.props.navigation.map(item => {
            if(!item.visible) return;
            if(this.state.tabId === item.id){
                return <NavGroup layout={this.props.layout}  key={item.id} 
                        refresh={this.state.itemRefresh} group={item} formula={this.props.formula} refreshEvt={this.itemRefreshControl} />;
            }
            
        });
        
        let mainContent = '';
        if (this.props.layout === 'horizontal') {
            let prevClass = ['sidenav-horizontal-prev'];
            if (this.state.prevDisable) {
                prevClass = [...prevClass, 'disabled'];
            }
            let nextClass = ['sidenav-horizontal-next'];
            if (this.state.nextDisable) {
                nextClass = [...nextClass, 'disabled'];
            }
                                
            mainContent = (
                <div className="navbar-content sidenav-horizontal" id="layout-sidenav">
                    <a href={DEMO.BLANK_LINK} className={prevClass.join(' ')} onClick={this.scrollPrevHandler}><span/></a>
                    <div id="sidenav-wrapper" className="sidenav-horizontal-wrapper">
                        <ul id="sidenav-horizontal" className="nav pcoded-inner-navbar sidenav-inner" onMouseLeave={this.props.onNavContentLeave} style={{marginLeft: '-'+this.state.scrollWidth+'px'}}>
                            {navBtns}
                        </ul>
                    </div>
                    <a href={DEMO.BLANK_LINK} className={nextClass.join(' ')} onClick={this.scrollNextHandler}><span/></a>
                </div>
            );
        } else {
            mainContent = (
                <div className="navbar-content datta-scroll">
                    <Draggable
                        handle=".handle"
                        defaultPosition={{ x: 0, y: 0 }}
                        grid={[30, 30]}>
                        <Modal isOpen={this.state.modal} toggle={this.toggle} backdrop="static" style={{width:"300px"}}>
                            <ModalHeader toggle={this.toggle}  className="handle">레이어 설정</ModalHeader>
                            <ModalBody>
                                <NavTreeCheckbox node={this.props.navigation} checkList={this.checkList}></NavTreeCheckbox>
                            </ModalBody>
                            <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                                <Button  style={{flex:1}} color="primary" size="sm" onClick={this.chekedLayer}>선택완료</Button>
                                <Button  style={{flex:1}} color="secondary" size="sm" onClick={this.toggle}>닫기</Button>
                            </ModalFooter>
                        </Modal>
                    </Draggable>
                    <PerfectScrollbar>
                        <span onClick={this.toggle} style={{display:"flex", alignItems:"center", width:"32px", height:"32px", justifyContent:"center", position:"absolute", right:"10px", zIndex:"1", cursor:"pointer" }}>
                            <i className="feather icon-settings"></i>
                        </span>
                        <ul className="nav" style={{marginTop:"30px"}}> {/* pcoded-inner-navbar */}
                            {navBtns}
                        </ul>
                        <ul className="nav pcoded-inner-navbar" id="layer-tab-area">
                            {navItems}
                            {/* 초기값 클릭 1. 초기 세팅 (맨 처음 버튼 id 확인) 2. id에 해당하는 칠드런을 출력 */}
                        </ul>
                    </PerfectScrollbar>
                </div>
            );
        }

        return (
            <Aux>
                {mainContent}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        layout: state.layout,
        collapseMenu: state.collapseMenu,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onNavContentLeave: () => dispatch({type: actionTypes.NAV_CONTENT_LEAVE}),
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (windowSize(NavContent)));

