import React, {Component, createRef} from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import Aux from "../../../../../../hoc/_Aux";
import DEMO from "../../../../../../store/constant";
import * as actionTypes from "../../../../../../store/actions";
import NavIcon from './../NavIcon';
import NavBadge from './../NavBadge';
import NavItem from "../NavItem";
import LoopNavCollapse from './index';
import NavCheckbox from './../NavCheckbox';
import NavRadio from './../NavRadio';
import ComboBoxComponent from '../../../../../components/ComboBoxComponent';

class NavCollapse extends Component {

    // constructor(props){
    //     super(props);
    //     this.state = { 
    //         replaceText : 'ooo'
    //     };
    // }

    state = { 
        replaceText : 'ooo'
    };
    
    componentDidMount() {
        const currentIndex = ((document.location.pathname).toString().split('/')).findIndex(id => id === this.props.collapse.id);
        if (currentIndex > -1) {
            this.props.onCollapseToggle(this.props.collapse.id, this.props.type);
        }
    }
    
    radioDisabledChagneEvt = (list, flag, checkObj) => {
        var checkedRadio = null;
        for(var i=0;i<list.length;++i){
            if(list[i].getAttribute("type") != "radio") continue;
            list[i].disabled = flag;
            if(list[i].checked) checkedRadio = list[i];
            if(!flag){
                list[i].setAttribute("data-value", list[i].getAttribute("data-value").replaceAll(this.state.replaceText, checkObj["dateCol"]));
                this.setState({replaceText : checkObj["dateCol"]});
            }
        }

        if(!flag){
            var aTag = list[1].parentNode.parentNode.parentNode.parentNode.previousSibling
            aTag.setAttribute("data-style", checkObj["style"]);
        }

        if(checkedRadio){
            var tempParentNode = checkedRadio.parentNode.parentNode.parentNode.parentNode;
            var parent = tempParentNode.previousSibling;
            var title = parent.getAttribute("data-title");
            var topParentNode = tempParentNode.parentNode.parentNode.querySelectorAll("input");
            
            var cql = [];
            var category = [];
            var layer = '';
            var style = '';
            topParentNode.forEach((value)=>{
                if(value.type!="checkbox"&& value.type!="radio") return;
                var _value = value.getAttribute("data-value");
                if(value.checked && _value!=''){
                    cql.push(_value);
                    var aTag = value.parentNode.parentNode.parentNode.parentNode.previousSibling;
                    var temp_cate = aTag.getAttribute("data-category");
                    layer = aTag.getAttribute("data-layer");
                    style = aTag.getAttribute("data-style");
                    if(!category.includes(temp_cate)) category.push(temp_cate);
                }
            });
            
            this.props.onLayerCheck({checked : true, 
                layer : layer, style : style,
                cql : '', cqlFilter: cql, layerCheckList: category});
        }
    }

    comboBoxChangeEvt = (evt, value) => {
        var parentUl = document.getElementById("combo-box").parentNode.parentNode.parentNode.parentNode;
        var inputList = parentUl.getElementsByTagName("input");
        if(evt){
            this.radioDisabledChagneEvt(inputList, false, value);
        }else{
            this.radioDisabledChagneEvt(inputList, true);
        }
    }
    
    render() {
        
         
        const {isOpen, isTrigger} = this.props;
        let navItems = '';
        if (this.props.collapse.lastChildren) {
            const collapses = this.props.collapse.lastChildren;
            navItems = Object.keys(collapses).map(item => {
                item = collapses[item];
                switch (item.type) {
                    case 'collapse':
                        if(this.props.collapse.flag){
                            var comboList = [];
                            if(this.props.formula){
                                comboList = [
                                    { title: '인덱스', value: this.props.formula, dateCol: 'crack_date', style: 'tpms:tpms_'+this.props.formula},
                                    // { title: '인덱스', value: 'spi', dateCol: 'crack_date', style: 'tpms:tpms_spi'},
                                    // { title: '인덱스', value: 'nhpci', dateCol: 'crack_date', style: 'tpms:tpms_nhpci'},
                                    { title: '균열율', value: 'cr', dateCol: 'crack_date', style: 'tpms:tpms_crack'},
                                    { title: '소성변헝', value: 'survey-rutt', dateCol: 'equip1_date', style: 'tpms:tpms_rutt'},
                                    { title: '평탄성', value: 'survey-iri', dateCol: 'equip1_date', style: 'tpms:tpms_iri'},
                                    { title: '보수이력', value: 'survey-repair', dateCol: 'repair_date', style: 'tpms:tpms_default_line'},
                                ];
                            }
                            return <><ComboBoxComponent combobox={comboList} label="선택" changeEvt={this.comboBoxChangeEvt}/> 
                                     <LoopNavCollapse key={item.id} collapse={item} type="sub" /></>
                        }else{
                            return <LoopNavCollapse key={item.id} collapse={item} type="sub" />;
                        }
                    case 'item':
                        return <NavItem layout={this.props.layout} key={item.id} item={item}/>;
                    case 'checkbox':
                        return <NavCheckbox layout={this.props.layout} key={item.id} item={item} />;
                    case 'radio':
                        return <NavRadio layout={this.props.layout} key={item.id} item={item} />;
                    default:
                        return false;
                }
            });
        }

        let itemTitle = this.props.collapse.label;
        if (this.props.collapse.icon) {
            itemTitle = <span className="pcoded-mtext">{this.props.collapse.label}</span>;
        }

        let navLinkClass = ['nav-link'];

        let navItemClass = ['nav-item', 'pcoded-hasmenu'];
        const openIndex = isOpen.findIndex(id => id === this.props.collapse.id);

        if (openIndex > -1) {
            navItemClass = [...navItemClass, 'active'];
            if (this.props.layout !== 'horizontal') {
                navLinkClass = [...navLinkClass, 'active'];
            }
        }
        const triggerIndex = isTrigger.findIndex(id => id === this.props.collapse.id);
        if (triggerIndex > -1) {
            navItemClass = [...navItemClass, 'pcoded-trigger'];
        }

        const currentIndex = ((document.location.pathname).toString().split('/')).findIndex(id => id === this.props.collapse.id);
        if (currentIndex > -1) {
            navItemClass = [...navItemClass, 'active'];
            if (this.props.layout !== 'horizontal') {
                navLinkClass = [...navLinkClass, 'active'];
            }
        }

        const subContent = (
            <Aux>
                <a href={DEMO.BLANK_LINK} className={navLinkClass.join(' ')} 
                    onClick={() => this.props.onCollapseToggle(this.props.collapse.id, this.props.type)}
                    data-layer={this.props.collapse.layer} data-style={this.props.collapse.style}
                    data-category={this.props.collapse.value} data-title={this.props.collapse.title}>
                    <NavIcon items={this.props.collapse} />
                    {itemTitle}
                    <NavBadge layout={this.props.layout} items={this.props.collapse} />
                </a>
                <ul className="pcoded-submenu">
                    {navItems}
                </ul>
            </Aux>
        );
        let mainContent = '';
        if (this.props.layout === 'horizontal') {
            mainContent = (
                <li className={navItemClass.join(' ')} onMouseLeave={() => this.props.onNavCollapseLeave(this.props.collapse.id, this.props.type)} onMouseEnter={() => this.props.onCollapseToggle(this.props.collapse.id, this.props.type)}>
                    {subContent}
                </li>
            );
        } else {
            mainContent = (
                <li className={navItemClass.join(' ')}>
                    {subContent}
                </li>
            );
        }

        return (
            <Aux>
                { (this.props.collapse.visible)? mainContent : ''}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        layout: state.layout,
        isOpen: state.isOpen,
        isTrigger: state.isTrigger,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onCollapseToggle: (id, type) => dispatch({type: actionTypes.COLLAPSE_TOGGLE, menu: {id: id, type: type}}),
        onNavCollapseLeave: (id, type) => dispatch({type: actionTypes.NAV_COLLAPSE_LEAVE, menu: {id: id, type: type}}),
        onLayerCheck: (props) => dispatch({type: actionTypes.LAYER_CHECK, changeLayer: props})
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavCollapse));
