import React from 'react';
import Aux from "../../../../../../hoc/_Aux";
import { Button } from 'reactstrap';

const NavButton = (props) => {
     const btnstyle = {
        borderRadius: "0.25rem",
        fontSize : "14px",
        marginBottom : "5px",
        width:"85px",
        marginLeft : "2px",
        marginRight : "0px",
        padding:"5px",
    }

    const navBtn=()=>{
       props.btnclick(props);
    }
    
    return (
        <Aux>
            <Button onClick={navBtn} style={btnstyle} key={props.group.id} title={props.group.label}>{props.group.label}</Button>
        </Aux>
     );
}

export default NavButton;