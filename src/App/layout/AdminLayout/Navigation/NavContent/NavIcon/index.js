import React from 'react';

const navIcon = (props) => {
    let navIcons = false;
    // console.log('props: ', props)
    if (props.items.icon) {
        navIcons = <span className="pcoded-micon"><i className={props.items.icon} /></span>;
        // navIcons = <span className="pcoded-micon"><i className='feather icon-box' /></span>;
    }
    return navIcons;
};

export default navIcon;