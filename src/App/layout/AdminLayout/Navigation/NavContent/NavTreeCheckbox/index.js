import React, { useEffect, useState } from 'react';
// import 'react-checkbox-tree/lib/react-checkbox-tree.css';
// import CheckboxTree from 'react-checkbox-tree';
import 'react-checkbox-tree-enhanced/lib/react-checkbox-tree.css';
import CheckboxTree from 'react-checkbox-tree-enhanced';

const NavTreeCheckbox = ({node, checkList}) => {
    const [checked, setChecked] = useState([]);
    const [expanded, setExpanded] = useState([]);


    const chkBox = (checked) =>{
        setChecked(checked);
    }

    const expand = (expanded) => {
        setExpanded(expanded);
    }

    const road = 
    [
        "status",
        "survey",
        "repair",
        // "etcRecord",
        "budget",
        "future",
        "others",
        // "safe"
        // "surveyRecord",
        // "repair-record",
    ];

    const bridge = 
    [
        "bridge-status"
    ];

    useEffect(()=>{
        if(checked.length > 0){
            checked.map((checkeList)=>{
                    var roadSameval = road.includes(checkeList);  
                    if(roadSameval){
                        if(!checked.includes('road-layer')){
                            var roadListChk = checked;
                            roadListChk.push('road-layer');
                            setChecked(roadListChk);
                        }
                    }
                    var bridgeSameval = bridge.includes(checkeList);
                    if(bridgeSameval){
                        if(!checked.includes('bridge-layer')){
                            var bridgeListChk = checked;
                            bridgeListChk.push('bridge-layer');
                            setChecked(bridgeListChk);
                        }
                    }
            });
        }
        var _temp = checked;
        checkList(_temp);
    },[checked]);
    
   
  return (
        <CheckboxTree
            nodes={node}
            checked={checked}
            expanded={expanded}
            onCheck={chkBox}
            onExpand={expand}
            showNodeIcon={false}
        />
  )
}

export default NavTreeCheckbox;