import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import windowSize from 'react-window-size';
import Aux from "../../../../../../hoc/_Aux";

import NavIcon from "./../NavIcon";
import NavBadge from "./../NavBadge";
import * as actionTypes from "../../../../../../store/actions";
import { Form } from 'react-bootstrap';
import '../NavCheckbox/index.css';

class NavRadio extends Component {

    // componentWillUnmount() {
    //     // window.removeEventListener('change', this.radioOnChangeEvt);
    //     this.setState({loading: false});
    // }
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         loading : false
    //     }
    // }
    
    radioOnChangeEvt = (evt) => {
        // 라디오 체크 박스 그룹을 담고있는 Ul 태그
        var tempParentNode = evt.target.parentNode.parentNode.parentNode.parentNode;
        // 카테고리 정보를 담고있는 a 태그 (속성으로 layer, style, category, title 정보를 가지고 있음)
        var parent = tempParentNode.previousSibling;
        // 현재 카테고리 타이틀
        var title = parent.getAttribute("data-title");

        //현재 클릭한 라디오 그룹을 제외한 다른 그룹의 체크 전체 해제
        var allInput = document.getElementById("layer-tab-area").querySelectorAll("input");
        allInput.forEach((value)=>{
            var _id = value.getAttribute("id");
            if(!(_id.includes(title))) value.checked = false;
        });

        var topParentNode = tempParentNode.parentNode.parentNode.querySelectorAll("input");
        var cql = [];
        var category = [];
        var tempLayer = '';
        var tempStyle = '';
        topParentNode.forEach((value)=>{
            if(value.type!="checkbox"&& value.type!="radio") return;
            var _value = value.getAttribute("data-value");
            var _style = value.getAttribute("data-style");
            if(value.checked && _value!=''){
                cql.push(_value);
                var aTag = value.parentNode.parentNode.parentNode.parentNode.previousSibling;
                var temp_cate = aTag.getAttribute("data-category");
                tempLayer = aTag.getAttribute("data-layer");
                tempStyle = aTag.getAttribute("data-style");
                if(_style) tempStyle = _style;
                if(!category.includes(temp_cate)) category.push(temp_cate);
            }
        });

        var layer = '';
        var style = '';
        if(category.length>1){
            // data category가 title인 object의 data-layer, data-style 가져오기
            var obj = document.querySelector('a[data-category="'+title+'"]');
            layer = obj.getAttribute("data-layer");
            style = obj.getAttribute("data-style");
        }else{
            if(evt.target.checked){
                var elStyle = evt.target.getAttribute("data-style");
                if(elStyle) style = elStyle;
                else    style = parent.getAttribute("data-style");

                layer = parent.getAttribute("data-layer");
                
            }else{
                layer = tempLayer;
                style = tempStyle;
            }
        }
        this.props.onLayerCheck({checked : true, 
            layer : layer, style : style,
            cql : evt.target.value, cqlFilter: cql, layerCheckList: category});
        // this.setState({loading : false});
    }

    render() {
        let itemTitle = this.props.item.title;
        if (this.props.item.icon) {
            itemTitle = <span className="pcoded-mtext">{this.props.item.title}</span>;
        }

        let itemTarget = '';
        if (this.props.item.target) {
            itemTarget = '_blank';
        }

        let subContent;
        if(this.props.item.external) {
            subContent = (
                <a href={this.props.item.url} target='_blank' rel='noopener noreferrer'>
                    <NavIcon items={this.props.item}/>
                    {itemTitle}
                    <NavBadge layout={this.props.layout} items={this.props.item}/>
                </a>
            );
        } else {
            subContent = (
                // <Form.Group className="nav-link" style={{marginBottom: "0", paddingTop:"5px", paddingBottom:"5px", paddingLeft:"70px", paddingRight:"0px"}}>
                // <Form.Check
                //         custom
                //         type="radio"
                //         id={this.props.item.category + this.props.item.id}
                //         label={this.props.item.label}
                //         name={this.props.item.category}
                //         disabled={this.props.item.disabled}
                //         data-value={this.props.item.cql}
                //         onChange={this.radioOnChangeEvt}
                //     />
                // </Form.Group>
                <Form.Group className="nav-link" style={{marginBottom: "0", paddingTop:"5px", paddingBottom:"5px", paddingLeft:"70px", paddingRight:"0px"}}>
                    <Form.Check type="radio" id={this.props.item.category + '-' + this.props.item.id}>
                        <Form.Check.Input name={this.props.item.category} type="radio" isValid onChange={this.radioOnChangeEvt} 
                                            data-value={this.props.item.cql} disabled={this.props.item.disabled} data-style={this.props.item.style}/>
                        <Form.Check.Label className="check-label">
                            <span className={`legend ${this.props.item.color}`}></span>
                            {this.props.item.label}
                        </Form.Check.Label>
                    </Form.Check>
                </Form.Group>
            );
        }
        let mainContent = '';
        if (this.props.layout === 'horizontal') {
            mainContent = (
                <li onClick={this.props.onItemLeave}>{subContent}</li>
            );
        } else {
            if (this.props.windowWidth < 992) {
                mainContent = (
                    <li className={this.props.item.classes} onClick={this.props.onItemClick}>{subContent}</li>
                );
            } else {
                mainContent = (
                    <li className={this.props.item.classes}>{subContent}</li>
                );
            }
        }

        return (
            <Aux>
                {mainContent}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        layout: state.layout,
        collapseMenu: state.collapseMenu,
        cqlFilter: state.cqlFilter,
        layerCheckList: state.layerCheckList
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onItemClick: () => dispatch({type: actionTypes.COLLAPSE_MENU}),
        onItemLeave: () => dispatch({type: actionTypes.NAV_CONTENT_LEAVE}),
        onLayerCheck: (props) => dispatch({type: actionTypes.LAYER_CHECK, changeLayer: props})
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (windowSize(NavRadio)));
