import React, { useEffect, useState } from 'react';
import Aux from "../../../../../../hoc/_Aux";
import NavCollapse from './../NavCollapse';
import NavItem from './../NavItem';

const NavGroup = (props) => {
    const [navItems, setNavItems] = useState(null);
    useEffect(()=>{
        
        if(props.refresh){
            makeNavItems();
            props.refreshEvt();
        }
        
    }, [props.refresh]);
    
    const makeNavItems = () => {
        if (props.group.children) {
            const groups = props.group.children;
            if(groups.length>0){
                let _navItems = Object.keys(groups).map((item, index) => {
                    item = groups[item];
                    if(!item.visible) return false;
                    switch (item.type) { 
                        case 'collapse':
                            return <NavCollapse key={item.id} collapse={item} type="sub" formula={props.formula} />;
                        case 'item':
                            return <NavItem layout={props.layout} key={item.id} item={item} />;
                        default:
                            return false;
                    }
                });
                setNavItems(_navItems);
            }
        } 
    }
    
    useEffect(()=>{
        let isMounted = true;

        if(props.formula){
            if(isMounted){
                makeNavItems();
            }
            
        }
        return () => {isMounted = false};
    }, [props.formula]);
   
    return (
        <Aux>
             {/* <li key={props.group.id} className="nav-item pcoded-menu-caption"><label>{props.group.title}</label></li> */}
             {navItems}
        </Aux>
     );
}

export default NavGroup;