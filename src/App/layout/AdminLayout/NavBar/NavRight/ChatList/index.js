import React, { useState, useEffect } from 'react';
import { Button } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {connect} from 'react-redux';
import Friends from './Friends';
import Aux from "../../../../../../hoc/_Aux";
import * as actionTypes from '../../../../../../store/actions';



const ChatList = (props) => {
    let listClass = ['header-user-list'];
    const [isOpen, setIsOpen] = useState(props.listOpen);
    const [gridBtn, setGridBtn] = useState();
    if (isOpen || props.listOpen) { 
        listClass = [...listClass, 'open'];
    }

    useEffect(() => {
        setIsOpen(props.isChatOpen);
        if(props.selectedData!=null) {
            setGridBtn(
                <div style={{position: 'absolute', right: '20px', top: '24%'}}>
                    <Button size="sm" color="secondary" style={{width:"100px", padding: '5px', color: 'white'}} onClick={openGrid}>그리드 열기</Button>
                </div>
            );
        }
    }, [props.isChatOpen]);

    const openGrid = () => {
        props.onCloseChat();
        document.getElementsByClassName('modal')[0].style.display='block';
        document.getElementsByClassName('modal-backdrop')[0].style.display='block';
        document.getElementsByClassName('modal-dialog')[0].style.display='block';
    }

    const chatCloseEvt = () => {
        props.closed();
        props.onCloseChat();
    }

    // const displayData = () => {
    //     var dataList = props.selectedData['ROwNode'];
    // }
    return (
        <Aux>
            <div className={listClass.join(' ')}>
                <div className="h-list-header" style={{position: 'relative'}}>지도확인
                    {gridBtn}
                    {/* <div className="input-group">
                        <input type="text" id="search-friends" className="form-control" placeholder="Search Friend . . ." />
                    </div> */}
                </div>
                <div className="h-list-body">
                    <div className="h-close-text"><i className="feather icon-chevrons-right" style={{cursor: 'pointer'}} onClick={chatCloseEvt}/></div>
                    <div className="main-friend-cont scroll-div">
                        <div className="main-friend-list" style={{height: 'calc(100vh - 85px)'}}>
                            
                            <PerfectScrollbar>
                                {/* <Friends /> */}
                                <Friends listOpen={isOpen} addressList={props.address}/>
                                {/* <div>{props.address}</div> */}
                            </PerfectScrollbar>
                        </div>
                    </div>
                </div>
            </div>
        </Aux>
    );
};

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData,
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onCloseChat: () => dispatch({type: actionTypes.CLOSE_CHAT})
    }
}


export default connect(chatStateToProps, chatDispatchToProps) (ChatList);