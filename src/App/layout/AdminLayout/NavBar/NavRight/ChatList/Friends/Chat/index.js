import React, {useState, useEffect} from 'react';
// import PerfectScrollbar from 'react-perfect-scrollbar'
import './chat.css';
// import chatMsg from './chat';
// import Messages from './Messages';
import Aux from "../../../../../../../../hoc/_Aux";
// import DEMO from "../../../../../../../../store/constant";
// import { useEffect, useState } from 'react';
import {connect} from 'react-redux';
import * as actionTypes from "../../../../../../../../store/actions";
// import * as actionTypes from "../../../../../../../../../store/actions";

const Chat = (props) => {
    const [dataList, setDataList] = useState();
    const [allDataList, setAllDataList] = useState();
    const [infoTable, setInfoTable] = useState("");

    useEffect(()=> {
        if(props.dataList) {
            setAllDataList(props.allList);
            var tempList = [];
            props.dataList.map((data) => {
                 tempList.push(data['data']);
            })
            setDataList(tempList);
        }
    }, [props.dataList])
   
    useEffect(()=> {
        if(dataList) makeTable();
    }, [dataList])
    
    const makeTable = () => {
        var tableHeader = props.headerList.map((obj, index) =>{
            return <th key={index}>{obj['name']}</th>
        });
        if(dataList){
            var tableBody = allDataList.map((obj, inx)=>{
                var trClassName = 'non-selected';
                for(var i=0; i<dataList.length; i++){
                    if(dataList[i]['ts_id']== obj['ts_id']) {
                        trClassName= 'selected-row';
                    }
                    // if(dataList[i].hasOwnProperty('repair_target_id')){
                    //     if(dataList[i]['repair_target_id']== obj['repair_target_id']) {
                    //         trClassName= 'selected-row';
                    //     }
                    // }else{
                    //     if(dataList[i]['ts_id']== obj['ts_id']) {
                    //         trClassName= 'selected-row';
                    //     }
                    // }
                }
                switch (obj['rank']){
                    case '101': 
                        obj['rank'] = '고속국도';
                        break;
                    case '102': 
                        obj['rank'] = '도시고속국도';
                        break;
                    case '103': 
                        obj['rank'] = '일반국도';
                        break;
                    case '104': 
                        obj['rank'] = '특별·광역시도';
                        break;
                    case '105': 
                        obj['rank'] = '국가지원 지방도';
                        break;
                    case '106': 
                        obj['rank'] = '지방도';
                        break;
                    case '107': 
                        obj['rank'] = '시·군도';
                        break;
                    case '108': 
                        obj['rank'] = '기타';
                        break;
                    default:
                        break;
                }
                var dataTsList = null;
                if(obj.hasOwnProperty("tsList")){
                    dataTsList = obj["tsList"];
                }

                return <tr key={inx} className={trClassName} id={obj['ts_id']+""} data-tslist={dataTsList} onMouseDown={rowClick}>
                            {props.headerList.map((h,index)=>{
                                var label = h['label'];           
                                return <td key={index}>{obj[label]}</td>
                            })}
                        </tr>;

            });
            setInfoTable(
                <table className="infoTable">
                    <thead>
                        <tr>
                            {tableHeader}
                        </tr>
                    </thead>
                    <tbody className="infoTbody">
                        {tableBody}
                    </tbody>
                </table>
            );
        }
    }

    const rowClick =(e) =>{
        var tempList = [];
        if(e){
            var chk=0;
            var clickedRowId = e.target.parentNode.getAttribute('id');
            var el = document.getElementById(clickedRowId);
            
            if(el.hasAttribute("data-tslist")){
                if(el.className=="non-selected"){
                    var obj = new Object();
                    obj["tsList"] = el.getAttribute("data-tslist");
                    obj["ts_id"] = parseInt(clickedRowId);
                    
                    tempList.push(obj);
                    dataList.map((obj)=>{ tempList.push(obj); });
                }else{
                    dataList.map((obj)=>{ if(obj["ts_id"]!=clickedRowId) tempList.push(obj); });
                }
            }else{
                for(var i=0; i<dataList.length; i++){
                    if(dataList[i]['ts_id']!= clickedRowId) {
                        tempList.push(dataList[i]);
                        chk += 1;
                    }
                }
                if(dataList.length == chk) {
                    tempList.push({'ts_id': clickedRowId});   
                }
            }
            
            props.changeSearchMapList(tempList);
            setDataList(tempList);
            
        }
    }


    // let message = (
    //     <div className="media chat-messages text-center">
    //         <div className="media-body chat-menu-content">
    //             <div className="">
    //                 <p className="chat-cont">CHAT NOT FOUND</p>
    //             </div>
    //         </div>
    //     </div>
    // );

    // chatMsg.filter(chats => {
    //     if (chats.friend_id === props.user.id) {
    //         message = (chats.messages).map((msg, index) => {
    //             return <Messages key={index} message={msg} name={props.user.name} photo={chats.friend_photo} />;
    //         });
    //     }
    //     return false;
    // });

    return (
        <Aux>
            <div className="info-area">
                {infoTable}
            </div>
            {/* <div className={chatClass.join(' ')}>
                <div className="h-list-header">
                    <h6>{props.user.name}</h6>
                    <a href={DEMO.BLANK_LINK} className="h-back-user-list" onClick={props.closed}><i className="feather icon-chevron-left" /></a>
                </div>
                <div className="h-list-body">
                    <div className="main-chat-cont">
                        <PerfectScrollbar>
                            <div className="main-friend-chat">
                                {message}
                            </div>
                        </PerfectScrollbar>
                    </div>
                </div>
                <div className="h-list-footer">
                    <div className="input-group">
                        <input type="file" className="chat-attach" style={{display: 'none'}} />
                        <a href={DEMO.BLANK_LINK} className="input-group-prepend btn btn-success btn-attach">
                            <i className="feather icon-paperclip" />
                        </a>
                        <input type="text" name="h-chat-text" className="form-control h-send-chat" placeholder="Write hear . . " />
                        <button type="submit" className="input-group-append btn-send btn btn-primary">
                            <i className="feather icon-message-circle" />
                        </button>
                    </div>
                </div>
            </div> */}
        </Aux>
    );
};

const stateToProps = state => {
    return {
        searchMapList : state.searchMapList
    }
}

const dispatchToProps = dispatch =>{
    return {
        changeSearchMapList: (props) => dispatch({type: actionTypes.CHAGE_SEARCH_LAYER, data: props})
    }
}

// export default Chat;
export default connect(stateToProps, dispatchToProps) (Chat);