import React, {Component} from 'react';

import friend from './friends';
import Friend from './Friend';
import Chat from './Chat';
import Aux from "../../../../../../../hoc/_Aux";
import {connect} from 'react-redux';
// import * as actionTypes from '../../../../../../../store/actions';

class Friends extends Component {
    state = {
        chatOpen: false,
        dataList: []
    };
    
    componentWillReceiveProps = (nextProps) => {
        if (!nextProps.listOpen) {
            this.setState({chatOpen: false, dataList: []});
        }
    };

    render() {
        
        return (
            <Aux>
                <Chat allList={this.props.allRows} dataList={this.props.selectedData} headerList={this.props.chatHeader} chatOpen={this.state.chatOpen} listOpen={this.props.listOpen} closed={() => this.setState({chatOpen: false, dataList: []})}/>
            </Aux>
        );
    }
}

const passedDataStateToProps = state => {
    return {
        allRows: state.allRows,
        selectedData: state.selectedData,
        chatHeader: state.chatHeader
    }
}

const passedDataDispatchToProps = () => {
    return {
    }
}

export default connect(passedDataStateToProps, passedDataDispatchToProps) (Friends);
