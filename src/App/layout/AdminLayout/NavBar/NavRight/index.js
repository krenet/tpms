import React, { Component } from 'react';
import {Dropdown} from 'react-bootstrap';
import {createBrowserHistory} from 'history';

import ChatList from './ChatList';
import Aux from "../../../../../hoc/_Aux";
import DEMO from "../../../../../store/constant";


class NavRight extends Component {
 
    state = {
        listOpen: false,
    };
    
    logout = () => {
        const history = createBrowserHistory();
        history.push('/');
    }

    
    render() {
        return (
            <Aux>
                <ul className="navbar-nav ml-auto">
                    <li className={this.props.rtlLayout ? 'm-r-15' : 'm-l-15'}>
                        <a href={DEMO.BLANK_LINK} className="displayChatbox" onClick={() => {this.setState({listOpen: true});}}><i className="icon feather icon-mail"/></a>
                    </li>
                    <li className={this.props.rtlLayout ? 'm-r-15' : 'm-l-15'}>
                        <a href={DEMO.BLANK_LINK} className="" onClick={this.logout} ><i className="icon feather icon-power"/></a>
                    </li>
                </ul>
                <ChatList listOpen={this.state.listOpen} closed={() => {this.setState({listOpen: false});}} address={this.props.address}/>
            </Aux>
        );
    }
}

export default NavRight;
