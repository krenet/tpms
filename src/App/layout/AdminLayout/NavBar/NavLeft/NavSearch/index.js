import React, {Component, useState} from 'react';
import windowSize from 'react-window-size';
import NavLeft from "../../NavLeft";
import Aux from "../../../../../../hoc/_Aux";
import DEMO from "../../../../../../store/constant";
import jsonp from 'jsonp';

class NavSearch extends Component {
    state = {
        searchWidth: (this.props.windowWidth < 992) ? 90 : 0,
        searchString: (this.props.windowWidth < 992) ? '90px' : '',
        isOpen: (this.props.windowWidth < 992),
        pointX : "",
        pointY : "",
        flag:true,
        item : [],
        road : [],
    };

    textval =[
      "김천", 
      "남양주",
      "경북"
    ]

    NavSearchChildFunc = (e) => {
        this.spotSearch = e.target.value;
    }
    
    spotSearchClick = ()=>{
        const spotSear = this.spotSearch;
        jsonp('http://api.vworld.kr/req/search?request=search&crs=EPSG:4326&bbox=127.0904,37.5115,127.4142,37.7531&size=100&query='+spotSear+'&type=address&category=road&format=json&errorformat=json&key=3F837D63-F36D-3E52-BF84-267B5D98E199', null, this.callback);
    
    }
    callback=(err, data)=>{
        
            if(err){
                console.log(err.message);
            }else{
                if(data.response.result == undefined){
                    alert("검색 결과가 없습니다.");
                }else{
                    var item = data.response.result.items;
                    this.setState({item:item});
                }
                this.state.item.map((item)=>{
                    this.setState({road:item.address.road})
                    this.setState({pointX:item.point.x, pointY:item.point.y});
                    
                    this.props.navLeftParentFunc(this.spotSearch, this.state.pointX, this.state.pointY, this.state.flag, this.state.road);
                });

            }
    }
    searchOnHandler = () => {
        if(this.state.isOpen){
            this.spotSearchClick();
        }
        
        this.setState({isOpen: true});
        const searchInterval = setInterval(() => {
            if (this.state.searchWidth >= 91) {
                clearInterval(searchInterval);
                return false;
            }
            this.setState(prevSate => {
                return {
                    searchWidth: prevSate.searchWidth + 15,
                    searchString: prevSate.searchWidth + 'px'
                }
            });
        }, 35);
    };

    searchOffHandler = () => {
        const searchInterval = setInterval(() => {
            if (this.state.searchWidth < 0) {
                this.setState({isOpen: false});
                clearInterval(searchInterval);
                return false;
            }
            this.setState(prevSate => {
                return {
                    searchWidth: prevSate.searchWidth - 15,
                    searchString: prevSate.searchWidth + 'px'
                }
            });
        }, 35);
    };
    
    render() {
        let searchClass = ['main-search'];
        if (this.state.isOpen) {
            searchClass = [...searchClass, 'open'];
        }
    
        return (
            <Aux>
                <div id="main-search" className={searchClass.join(' ')}>
                    <div className="input-group">
                    {/* <NavLeft spotSearch ={this.spotSearch}/> */}
                        <input type="text" id="m-search" className="form-control" placeholder="Search . . ." style={{width: this.state.searchString}} onChange={this.NavSearchChildFunc}/>
                        <a href={DEMO.BLANK_LINK} className="input-group-append search-close" onClick={this.searchOffHandler}>
                            <i className="feather icon-x input-group-text" />
                        </a>
                        <span className="input-group-append search-btn btn btn-primary" onClick={this.searchOnHandler}>
                        <i className="feather icon-search input-group-text" />
                    </span>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default windowSize(NavSearch);