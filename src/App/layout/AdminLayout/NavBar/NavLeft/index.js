import React, {Component} from 'react';
import {connect} from 'react-redux';
import windowSize from 'react-window-size';
import Aux from "../../../../../hoc/_Aux";
import * as actionTypes from "../../../../../store/actions";
import NavDropdown from "./NavDropdown/NavDropdown";

class NavLeft extends Component {
    navLeftParentFunc = (spotSearch, pointX,pointY, flag, road) => {
       this.props.navBarParentFunc(spotSearch, pointX, pointY, flag, road);
    }
    
    render() {
        // let iconFullScreen = ['feather'];
        // iconFullScreen = (this.props.isFullScreen) ? [...iconFullScreen, 'icon-minimize'] : [...iconFullScreen, 'icon-maximize'];

        let navItemClass = ['nav-item'];
        if (this.props.windowWidth <= 575) {
            navItemClass = [...navItemClass, 'd-none'];
        }
        


        return (
            <Aux>
                <ul className="navbar-nav mr-auto">
                    {/* <li><a href={DEMO.BLANK_LINK} className="full-screen" onClick={this.props.onFullScreen}><i className={iconFullScreen.join(' ')} /></a></li> */}
                    <li className={navItemClass.join(' ')}>
                        <NavDropdown/>
                        {/* <Dropdown alignRight={dropdownRightAlign}>
                            <Dropdown.Toggle variant={'link'} id="dropdown-basic">
                                일반
                            </Dropdown.Toggle>
                            <ul>
                                <Dropdown.Menu>
                                    <li><a className="dropdown-item" href={DEMO.BLANK_LINK}>Action</a></li>
                                    <li><a className="dropdown-item" href={DEMO.BLANK_LINK}>Another action</a></li>
                                    <li><a className="dropdown-item" href={DEMO.BLANK_LINK}>Something else here</a></li>
                                </Dropdown.Menu>
                            </ul>
                        </Dropdown> */}
                    </li>
                    {/* <li className="nav-item"><NavSearch navLeftParentFunc={this.navLeftParentFunc}/></li> */}
                </ul>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isFullScreen: state.isFullScreen,
        rtlLayout: state.rtlLayout
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFullScreen: () => dispatch({type: actionTypes.FULL_SCREEN}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(windowSize(NavLeft));
