import React, {useState, useRef} from 'react';
import { Button, ModalBody } from 'reactstrap';

const RouteCreate = React.lazy(() => import('./RouteCreate'));
const RouteUpdate = React.lazy(() => import('./RouteUpdate'));

const CreateAndUpdate = () => {
    const [subModal, setSubModal] = useState(null);
    const create = () => setSubModal(<RouteCreate isOpen={true} onClose={subModalCloseEvt}/>);
    const update = () => setSubModal(<RouteUpdate isOpen={true} onClose={subModalCloseEvt}/>);

    const subModalCloseEvt = () => {
        setSubModal(null);
    }

    return (
        <ModalBody>
            <Button color="primary" size="lg" block onClick={create}>등록</Button>
            {subModal}
            <hr/>
            <Button color="secondary" size="lg" block onClick={update}>수정</Button>
            
        </ModalBody>
    );
}

export default CreateAndUpdate;