import React, {useEffect, useState} from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button} from 'reactstrap';
    const ExportCsv = ({isOpen, onClose}) => {
    const [subModal, setSubModal] = useState(isOpen);
    const toggle = () => setSubModal(!subModal);
    const MapConfirmStyle = {
        height : "150px",
        width : "60%"
    }

    const PlannerCsv = () =>{

    }

    const MasterCsv = () => {

    }
    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" onClosed={onClose}>
            <ModalHeader toggle={toggle}>CSV내보내기</ModalHeader>
                <ModalBody>
                    <Button color="primary" size="lg" block onClick={MasterCsv}>마스터 csv 내보내기</Button>
                    <Button color="primary" size="lg" block onClick={PlannerCsv}>조사플래너 csv 내보내기</Button>
                </ModalBody>
            <ModalFooter style={{justifyContent:"center"}} >
                <Button color="secondary" size="sm" block onClick={toggle}>닫기</Button>
            </ModalFooter>
        </Modal>
    );

}

export default ExportCsv;