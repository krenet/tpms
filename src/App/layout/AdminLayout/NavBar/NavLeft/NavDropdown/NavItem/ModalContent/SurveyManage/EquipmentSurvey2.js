import React, {useState, useRef, useEffect} from 'react';
import { Button, ModalBody, InputGroup, Input, InputGroupAddon, ModalFooter, Form} from 'reactstrap';
import ProgressBarComponent from '../../../../../../../../components/ProgressBarComponent';
import CrackXMl from '../../../../../../../../../resource/settingXml/equip2-crack.xml';
import GprXMl from '../../../../../../../../../resource/settingXml/equip2-gpr.xml';
import HwdXMl from '../../../../../../../../../resource/settingXml/equip2-hwd.xml';
import {XmlParser, server_connect_withfile} from '../../../../../../../../../tpms-common.js';
import serviceUrl from  '../../../../../../../../../request-url-list';

const EquipmentSurvey2 = ({toggle}) => {
    const [crackFolderName, setCrackFolderName] = useState("");
    const [crackFiles, setCrackFiles] = useState("");
    const [gprFolderName, setGprFolderName] = useState("");
    const [gprFiles, setGprFiles] = useState("");
    const [hwdFolderName, setHwdFolderName] = useState("");
    const [hwdFiles, setHwdFiles] = useState("");
    const _crackExcelFolder = useRef();
    const _gprExcelFolder = useRef();
    const _hwdExcelFolder = useRef();
    const [cColumns, setCColumns] = useState();
    const [gColumns, setGColumns] = useState();
    const [hColumns, setHColumns] = useState();
    const [progress, setProgress] = useState({
        open : false,
        title : "PES 데이터 입력 진행상황",
        totalCnt : null,
        completeCnt : null
    });

    const passToggle = () =>{
        toggle();
    }
    //--------crack
    const selectCrackFolder = (e)=>{
        e.preventDefault();
        _crackExcelFolder.current.click();
    }
    const crackFolderChange = (e) => {
        var _crackFolderName = e.target.files[0].webkitRelativePath.split("/");
        setCrackFolderName(_crackFolderName[0]);
        setCrackFiles(e.target.files);
    }
    //crack----------

    //--------gpr
    const selectGprFolder = (e)=>{
        e.preventDefault();
        _gprExcelFolder.current.click();
    }
    const gprFolderChange = (e) => {
        var _gprFolderName = e.target.files[0].webkitRelativePath.split("/");
        setGprFolderName(_gprFolderName[0]);
        setGprFiles(e.target.files);
    }
    //gpr--------

    //----------hwd
    const selectHwdFolder = (e) => {
        e.preventDefault();
        _hwdExcelFolder.current.click();
    }
    const hwdFolderChange = (e) => {
        var _hwdFolderName = e.target.files[0].webkitRelativePath.split("/");
        setHwdFolderName(_hwdFolderName[0]);
        setHwdFiles(e.target.files);
    }
    //hwd----------

    //xmlParser
    useEffect(() => {
        let ignore = false;
        async function fetchData() {
            const result_crack = await XmlParser(CrackXMl);
            const result_gpr = await XmlParser(GprXMl);
            const result_hwd = await XmlParser(HwdXMl);
            if (!ignore) {
                setCColumns(result_crack.getElementsByTagName('Columns')[0]['children']);
                setGColumns(result_gpr.getElementsByTagName('Columns')[0]['children']);
                setHColumns(result_hwd.getElementsByTagName('Columns')[0]['children']);
            }
        }
        fetchData();
        return () => { ignore = true; }
    }, []);

    const insertEquipment2Data = async () =>{
        setProgress({
            open : true,
            title : "데이터 입력 진행 상황",
            totalCnt : crackFiles.length+gprFiles.length+hwdFiles.length,
            completeCnt : 0
        });

        const config = {
            header : {
                'content-type': 'multipart/form-data'
            }
        };
        var data = new FormData();
        // var failList = new Array();
        var failList = [];
        var _complete = 0;
        //crack
        if(crackFiles.length > 0){
            var _columns = cColumns.map((obj)=>{
                return obj['name'];
            });
            data.append('columns', _columns);

            var types = cColumns.map(obj=>{
                return obj['attributes']['data-type'];
            });
            data.append('types', types);

            for(const file of crackFiles){
                data.append('file', file);
                const result = await server_connect_withfile(serviceUrl["pavement"]["insertEquipment2CrackData"], data, config);
                // data.delete("file");
                if(result['status']==200){
                    var keys = Object.keys(result['data']['res']);
                    if(keys.length==0){
                        setProgress({
                            open : true,
                            title : "데이터 입력 진행 상황",
                            totalCnt : crackFiles.length+gprFiles.length+hwdFiles.length,
                            completeCnt : ++_complete
                        });
                        data.delete("file");
                    }else{
                        keys.map((id)=>{
                            failList.push(id);
                        })
                        // failList.push(result['data']['res']['filename']);
                        // failList.join();
                    }
                }else{
                    alert("서버 통신 실패");
                    return;
                }
            }
        }
        
        //gpr
        if(gprFiles.length > 0){
            data = new FormData();
            var _columns = gColumns.map((obj)=>{
                return obj['name'];
            });
            data.append('columns', _columns);

            var types = gColumns.map(obj=>{
                return obj['attributes']['data-type'];
            });
            data.append('types', types);

            for(const file of gprFiles){
                data.append('file', file);
                const result = await server_connect_withfile(serviceUrl["pavement"]["insertEquipment2GprData"], data, config);
                if(result['status']==200){
                    // var keys = Object.keys(result['data']['res']);
                    if(result['data']['res']){
                        setProgress({
                            open : true,
                            title : "데이터 입력 진행 상황",
                            totalCnt : crackFiles.length+gprFiles.length+hwdFiles.length,
                            completeCnt : ++_complete
                        });
                        data.delete("file");
                    }else{
                        // failList.push(result['data']['res']['filename']);
                    }
                }else{
                    alert("서버 통신 실패");
                    return;
                }
            }

        }

        //hwd
        if(hwdFiles.length > 0){
            data = new FormData();
            var _columns = hColumns.map((obj)=>{
                return obj['name'];
            });
            data.append('columns', _columns);

            var types = hColumns.map(obj=>{
                return obj['attributes']['data-type'];
            });
            data.append('types', types);
            
            for(const file of hwdFiles){
                data.append('file', file);
                const result = await server_connect_withfile(serviceUrl["pavement"]["insertEquipment2HwdData"], data, config);
                // data.delete("file");
                if(result['status']==200){
                    var keys = Object.keys(result['data']['res']);
                    if(keys.length==0){
                        setProgress({
                            open : true,
                            title : "데이터 입력 진행 상황",
                            totalCnt : crackFiles.length+gprFiles.length+hwdFiles.length,
                            completeCnt : ++_complete
                        });
                        data.delete("file");
                    }else{
                        
                        // failList.push(result['data']['res']['filename']);
                    }
                }else{
                    alert("서버 통신 실패");
                    return;
                }
            }
        }

        // if(failList.length>0){
        //     var megStr = 'PES Data 입력 실패 : ';
        //     failList.map((filename, inx)=>{
        //         megStr += "(" + (inx+1) + ")";
        //         megStr += filename + "  ";
        //     });
        //     alert(megStr);
        // }
    }

    const getNowPercentage = (percent) => {
        if(percent==100) {
            // setProgress({
            //     open : true,
            //     title : "데이터 입력 진행 상황",
            //     totalCnt : 0,
            //     completeCnt : 0
            // });
            // alert("PES 데이터 입력 완료");
        }
    }

    const progressModal = (pgModal) => {
        setProgress({
            ...progress,
            open : pgModal
        });
    }

    //저장
    const submitForm = (evt) =>{
        evt.preventDefault();
        insertEquipment2Data();
    }



    return (
        <Form onSubmit={submitForm}>
            <ModalBody>
                <p style={{fontWeight:"bold", marginTop:'5px', marginBottom:'0px'}}>균열 분석 폴더 선택</p>
                <InputGroup>
                    <Input value={crackFolderName}  placeholder="폴더를 선택해주세요." readOnly/>
                    <InputGroupAddon addonType="append">
                    <input webkitdirectory="" directory="" type="file" style={{display:"none"}} ref={_crackExcelFolder} onChange={crackFolderChange}  accept=".xlsx, .csv"/>
                    <Button color="primary" onClick={selectCrackFolder}>Folder</Button>
                    </InputGroupAddon>
                </InputGroup>

                <p style={{fontWeight:"bold", marginTop:'10px', marginBottom:'0px'}}>GPR 폴더 선택</p>
                <InputGroup>
                    <Input value={gprFolderName}  placeholder="폴더를 선택해주세요." readOnly/>
                    <InputGroupAddon addonType="append">
                    <input webkitdirectory="" directory="" type="file"  style={{display:"none"}}  ref={_gprExcelFolder} onChange={gprFolderChange}  accept=".xlsx, .csv"/>
                    <Button color="primary" onClick={selectGprFolder}>Folder</Button>
                    </InputGroupAddon>
                </InputGroup>

                <p style={{fontWeight:"bold", marginTop:'10px', marginBottom:'0px'}}>HWD 폴더 선택</p>
                <InputGroup>
                    <Input value={hwdFolderName} placeholder="폴더를 선택해주세요." readOnly/>
                    <InputGroupAddon addonType="append">
                    <input webkitdirectory="" directory="" type="file"  style={{display:"none"}} ref={_hwdExcelFolder} onChange={hwdFolderChange}  accept=".xlsx, .csv"/>
                    <Button color="primary" onClick={selectHwdFolder}>Folder</Button>
                    </InputGroupAddon>
                </InputGroup>
                <ProgressBarComponent open={progress.open} title={progress.title} totalCnt={progress.totalCnt} completeCnt={progress.completeCnt} passPercent={getNowPercentage} modalEvt={progressModal}/>
            </ModalBody>
            <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                <Button style={{flex:1}} color="primary" size="sm" type="submit">저장</Button>
                <Button style={{flex:1}} color="secondary" size="sm" onClick={passToggle}>닫기</Button>
            </ModalFooter>
        </Form>
    );
}
export default EquipmentSurvey2;