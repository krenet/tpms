import React,  {useState} from "react";
import { Button, ModalBody, ModalFooter } from 'reactstrap';
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts';
import GridComponent from '../../../../../../../../components/GridComponent';

const StatisticsTwo = ({toggle}) => {
  const data = [
    {
      "name": "Page A",
      "7-10": 4000,
      "4-6": 2400,
      "1-3": 2400
    },
    {
      "name": "Page B",
      "7-10": 3000,
      "4-6": 1398,
      "1-3": 2400
    },
    {
      "name": "Page C",
      "7-10": 2000,
      "4-6": 9800,
      "1-3": 2400
    },
    {
      "name": "Page D",
      "7-10": 2780,
      "4-6": 3908,
      "1-3": 2400
    },
    {
      "name": "Page E",
      "7-10": 1890,
      "4-6": 4800,
      "1-3": 2400
    },
    {
      "name": "Page F",
      "7-10": 2390,
      "4-6": 3800,
      "1-3": 2400
    },
    {
      "name": "Page G",
      "7-10": 3490,
      "4-6": 4300,
      "1-3": 2400
    }
  ];
  const [stateGrid, setStateGrid] = useState({
    columnDefs: [
        {
            headerName : '노선정보',
            children : [
                {headerName:"노선명", field:"name"},
                {headerName:"행선", field:"dir"},
                {headerName:"차선", field:"lane"},
                {headerName:"구간", field:"section"}    
            ]
        },
        {
            headerName: "연장",
            children : [
                {headerName:"개별", field:"length"},
                {headerName:"누적", field:"lenSum"}
            ]
        },
        {headerName:"중용 분리", field:"overlap"},
        {
            headerName: "주소",
            children : [
                {headerName:"시점", field:"sAddr"},
                {headerName:"종점", field:"eAddr"}
            ]
        },
        {headerName:"차로수", field:"lanes"},
        {
            headerName: "이벤트",
            children : [
                {headerName:"명칭", field:"eventName"},
                {headerName:"종류", field:"eventType"}
            ]
        },
        {headerName:"포장종류", field:"paveType"},
        {
            headerName: "최초포장",
            children : [
                {headerName:"연도", field:"makeYear"},
                {headerName:"AC", field:"makeAC"},
                {headerName:"BB", field:"makeBB"},
                {headerName:"GB", field:"makeGB"},
                {headerName:"SB", field:"makeSB"},
                {headerName:"AFL", field:"makeAFL"}
            ]
        },
        {
            headerName: "최종유지보수",
            children : [
                {headerName:"연도", field:"latestYear"},
                {headerName:"공법", field:"latestMethod"},
                {headerName:"설명", field:"latestDesc"},
                {headerName:"비용", field:"latestPrice"}
            ]
        },
        {
            headerName: "교통량",
            children : [
                {headerName:"연도", field:"trafficYear"},
                {headerName:"지점", field:"trafficPoint"},
                {headerName:"Total", field:"traffic"}
            ]
        }
    ],
    rowData : [
        {name: "", dir: "", lane: "", section: "", length: "", lenSum: "", overlap: "", sAddr: "", eAddr: "", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"", trafficPoint:"", traffic:""}
    ]
});
    const passToggle = () =>{
        toggle();
    }
    return(
        <div>
        <ModalBody>
              <BarChart style={{marginLeft:"23%", borderBottom:" groove"}} width={1300} height={400} data={data}>
                  {/* <CartesianGrid strokeDasharray="3 3" /> */}
                  <XAxis dataKey="name" />
                  <YAxis />
                  <Tooltip />
                  <Legend />
                  <Bar dataKey="7-10" fill="#FF0000" />
                  <Bar dataKey="4-6" fill="#FFCD12" />
                  <Bar dataKey="1-3" fill="#2478FF" />
              </BarChart>
              <GridComponent columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} style={{marginTop: "5px", height:"70vh"}}/>
        </ModalBody>
        <ModalFooter style={{justifyContent:"flex-end", display:"flex"}}>
                     <Button color="secondary" onClick={passToggle}>닫기</Button>
        </ModalFooter>
        {/* <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                <Button color="primary" >확인</Button>
            </span>
        </ModalFooter> */}
        </div>
    );
}

export default StatisticsTwo;