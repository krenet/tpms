import React, { useState, useRef, useEffect } from 'react';
import { ModalBody, ModalHeader, Button, ModalFooter, Modal, Label } from 'reactstrap';
import './CycleSetting.css';
import InputComponent from '../../../../../../../../components/InputComponent';
//import degreeXml from '../../../../../../../../../resource/settingXml/year-degree.xml';
import {XmlParser, server_connect, server_connect_withfile} from '../../../../../../../../../tpms-common.js';
import { Form } from 'react-bootstrap';
import serviceUrl from '../../../../../../../../../request-url-list';

const CycleSetting = ({isOpen, setData, onClose}) => {
    const toggle = () => setSubModal(!subModal);
    const [subModal, setSubModal] = useState(isOpen);
const submitForm = async (evt)=> {
    var params = new Object();

    evt.preventDefault();
    const data = new FormData(evt.target);
    params["degree"] = data.get("degree");
    params["start_year"] = data.get("firstYear");

    alert("시작연도 이후의 연도 주기설정은 자동으로 생성됩니다.");
    const result = await server_connect(serviceUrl["pavement"]["insertRoadSurveyScData"], params);
    if(result['data']['res'] ==1){
        setData(params);
        alert("주기설정이 저장되었습니다.");
        toggle();
    }else{
        alert("주기설정에 실패하였습니다.");
    }

}


const yearComboboxSetData =() =>{
    const thisDate = new Date;
    const thisYear = thisDate.getFullYear();
    let years= new Array();
    for(var i=0; i<=10; ++i){
        years.push(<option value={thisYear+i}>{thisYear+i}</option>);
    }
    
    return years;
}

return (
    <>
    <Modal isOpen={subModal} onClosed={onClose} toggle={toggle} centered={true} backdrop="static" style={{width:"450px"}}>
       <ModalHeader toggle={toggle}>주기 설정</ModalHeader>
        <Form onSubmit={submitForm}>
            <ModalBody>
                    <div  style={{justifyContent:"center", display:"flex"}}>
                            <InputComponent key="startYear" type="dropdown" label="기준연도" name="firstYear" settingDataEvt={yearComboboxSetData}
                                            passLabelWidth="90px"/> 
                    </div>
                    <div>
                            <InputComponent key="degree" type="number" min={1} max={10} name="degree" label="반복주기(년)" passLabelWidth="90px"/> 
                    </div>
                    {/* {
                        years.map((year, index) =>(
                            <div style={{ fontSize: 20,  color:"black"}}>
                            <Label className="myList">
                            <input  ref={chkBox} style={{margin: 5, zoom: 1.8}} key={index} type="checkBox" value={year} onChange={handleChange} />
                            {year}   
                            </Label>
                            </div>
                            ))
                        } */}
            </ModalBody>
            <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                <Button style={{flex:1}} color="info" type="submit">주기생성</Button>
                <Button style={{flex:1}} color="secondary" onClick={toggle}>닫기</Button>
            </ModalFooter>
        </Form>
        </Modal>
    </>
);

}

export default CycleSetting;