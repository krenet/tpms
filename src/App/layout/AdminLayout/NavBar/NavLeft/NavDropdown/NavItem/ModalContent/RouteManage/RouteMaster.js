import React, {useState, useRef, useEffect, useCallback} from 'react';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import { Button, ModalBody, ModalFooter, Label, Input } from 'reactstrap';
import GridComponent from '../../../../../../../../components/GridComponent';
import {typeList} from '../Common/routeInfoList.js';
import MasterXML from '../../../../../../../../../resource/settingXml/road-master.xml';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import { map } from 'jquery';
import e from 'cors';
import {connect} from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import serviceUrl from  '../../../../../../../../../request-url-list';

const BatchModified = React.lazy(() => import('./BatchModified'));
// const MapConfirm = React.lazy(() => import('./MapConfirm'));

const RouteMaster = ({toggle, onOpenChat}) => {
    const [nos, setNos] = useState([]);
    const [subModal, setSubModal] = useState(null);
    const batchModified = () => setSubModal(<BatchModified isOpen={true} onClose={subModalCloseEvt}/>);
    // const mapConfirm = () => setSubModal(<MapConfirm isOpen={true} onClose={subModalCloseEvt}/>);
    // const mapConfirm = () => {
    //     toggle();
    // }
    const subModalCloseEvt = () => {
        setSubModal(null);
    }
    const [names, setNames] = useState([]);
    const [metaData, setMetaData] = useState();
    const [columns, setColumns] = useState([]);
    const [stateGrid, setStateGrid] = useState({});
    const [allRows, setAllRows] = useState({});
    const [selectExt, setSelectExt] = useState('0');
    const [gridApi, setGridApi] = useState(null);
    // const [grid, setGrid] = useState(stateGrid);

const type = useRef();
const no = useRef();
const name = useRef();
const dir = useRef();
const lane = useRef();

const typeChange = async(obj) => {
    var params = new Object();
    params['rank'] = obj.value;
    var result = await server_connect(serviceUrl["pavement"]["getNoList"], params);
    if(result && result['status']==200){
        result = result['data']['res'];
        if(result.length>0) {
            var noList = result.map((obj)=> {
                return {label: obj, value : obj};
            });      
            
            noList.unshift({label: '전체', value: 'all'});
            setNos(noList);
        } 
    }else{
        alert('서버 통신 실패. 관리자에게 문의하세요.');
    }
};

const noChange = async(obj) => {
    var params = new Object();
    params['no'] = obj.value;
    var result = await server_connect(serviceUrl["pavement"]["getNameList"], params);
    result = result['data']['res'];
    if(result.length>0){
        var nameList = result.map((obj)=> {
            return {label: obj['name'], value : obj['id']};
        });      
        nameList.unshift({label: '전체', value: '0'});
        setNames(nameList);
    }else{
        setNames([{label: '전체', value: '0'}]);
    }
};

const handleClick = async(e) => {
    var selectType = type.current.state.value;
    var selectNo = no.current.state.value;
    var selectName = name.current.state.value;
    var selectDir = dir.current.state.value;

    var params = new Object();
    if(selectType !=null) {
        params['rank'] = selectType['value'];
    }else{
        alert('노선 등급을 선택해주세요.');
        return;
    }
    if(selectNo !=null && selectNo['value'] !="-" && selectNo['value'] !="all"){
        params['no'] = selectNo['value'];
    }else params['no'] = "";
    if(selectName !=null){
        params['tri_id'] = selectName['value'];
    }else params['tri_id'] = ""; 
    if(selectDir !=null){
        params['dir'] = selectDir['value']
    }else params['dir'] = "-1";
   
    if(params['rank'] !=null){
        var result = await server_connect(serviceUrl["pavement"]["getSmList"], params);
        if(result && result['status']==200){
            result = result['data']['res'];  
            if(result.length>0){
                var dataList = result.map((obj) => {
                    return  {
                        id: obj['ts_id'], ts_id: obj['ts_id'],
                        no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'],
                        dir: obj['rev_dir'], lanes: obj['lane'], sector: obj['sector'], section: obj['section'], 
                        ext: obj['ext'], ext_sum: obj['ext_sum'], 
                        node_name: obj['node_name'], node_type: obj['node_type'], 
                        main_year: "", method: "", explain: "", cost: "",
                        year: "", s_sm_id: "", total: "", sm_ext: obj['sm_ext']}
                });
                setAllRows(result);
                setStateGrid({
                    ...stateGrid,
                    rowData : dataList,
                    // onRowSelected: function(e) {
                    //     var selectedRows = e.api.getSelectedNodes();
                    
                    // },
                    onSelectionChanged: function(e) {
                        var selectedRows = e.api.getSelectedNodes();
                        // setSelectRowData(selectedRows);
                        var extSum = 0;
                        selectedRows.map((row)=>{
                            var selectedRowExt = row['data']['sm_ext'];
                            extSum = extSum + selectedRowExt;
                        });
                        setSelectExt((extSum/1000).toFixed(2));
                    }
                });
            }else {
                setStateGrid({
                    ...stateGrid,
                    rowData : [
                        {span: 'no-result', no: "검색 결과가 없습니다"}
                    ],
                    onSelectionChanged: function(e) {
                        setSelectExt('-');
                    },
                });
            }           
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    } 
}

const getGridComponentState = (childState) => {
    setGridApi(childState);
}

useEffect(()=>{
    if(gridApi) {
        document.addEventListener('click', handleMouseClick);
    }

    return () => {
        document.removeEventListener('click', handleMouseClick);
    }
}, [gridApi])

   const handleMouseClick = (e) => {
       var eType = e.target.type;
       var eRole = e.target.getAttribute('role');
       if((eRole==null || eRole !='gridcell') && eType!='button'){
           gridApi.deselectAll();
       }
   }

   const viewMap = (obj) => {
       var props = [];
       var selectedNodes = gridApi.getSelectedNodes();
       var header = [
        {
            label : 'type',
            name  : '등급'
        },
        {
            label : 'no',
            name  : '노선번호'
        },
            {
            label : 'name',
            name  : '노선명'
        },
        {
            label : 'rev_dir',
            name  : '행선'
        },
        // {
        //     label : 'extension',
        //     name   : '연장'
        // },
        {
            label : 'node_name',
            name  : '이벤트명'
        },
        {
            label : 'node_type',
            name  : '이벤트 종류'
        }
       ];
       props['allData'] = allRows;
       props['selectedNodes'] = selectedNodes;
       props['header'] = header;
       props['action'] = 'OPEN_CHAT';
       if(selectedNodes.length==0){
           alert("데이터를 먼저 선택해주세요.");
        }else{
            allRows.map((row)=>{
                row['type'] = type.current.state.value['label'];
            });
           onOpenChat(props);
           hideModal();
       }
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display='none';
        document.getElementsByClassName('modal-backdrop')[0].style.display='none';
        document.getElementsByClassName('modal-dialog')[0].style.display='none';
    }

useEffect(()=>{
    let ignore = false;
    async function fetchData(){
        const result = await XmlParser(MasterXML);
        if(!ignore) { setMetaData(result);  }
    }
    fetchData();
    // document.addEventListener('click', handleMouseClick);
    return () => { 
        ignore = true; 
        // document.removeEventListener('click', handleMouseClick);
    }
}, []);

useEffect(()=>{
    if(metaData) setColumns(metaData.getElementsByTagName('Columns')[0]['children']);           
}, [metaData]);

// road-master.xml 변경에 따라, 그리드 컬럼 자동 변경
useEffect(()=>{
    var prev;
    var temp = columns.map((obj, index) =>{
        if(!(prev) || obj['attributes']['className']!=prev){
            prev = obj['attributes']['className'];
            return obj['attributes']['className'];
        } else return undefined;
    });
    temp = temp.filter(function(el){
        return el!=null;
    });

    temp = temp.map((obj)=>{
        // return {
        //     headerName: obj,
        //     children: 
        //         columns.map((col)=>{
        //             var attr = col['attributes'];
        //             if(attr['className']==obj)
        //             return {headerName: attr['title'], field: col['name']}
        //         }).filter(function(el){
        //             return el!=null;
        //         })
        // }

        return {
            headerName: obj,
            children: 
                columns.map((col)=>{
                    var attr = col['attributes'];
                    if(attr['className']==obj && col['name']=="no"){
                        return {
                            headerName: attr['title'], 
                            field: col['name'],
                            width: Number(attr['wd']),
                            colSpan: function(params){
                                if(isSpan(params)){
                                    return 17;
                                }else{
                                    return 1;
                                }
                            },
                            // cellStyle: function(params){
                            //     if(isSpan(params)){
                            //         return {textAlign: 'center'};
                            //     }else {
                            //         // return {textAlign: 'left'};
                            //     }
                            // },
                        }
                    }else if (attr['className']==obj && col['name']!="no"){
                        return {headerName: attr['title'], field: col['name'], width: Number(attr['wd'])}
                    }
                }).filter(function(el){
                    return el!=null;
                })
        }
    });
        
    setStateGrid({
        columnDefs : temp,
        rowSelection: 'multiple',
        rowData : [
            {no: "", name: "", dir: "", lanes: "", sector: "", section: "", 
            ext: "", ext_sum: "", 
            node_name: "", node_type: "", 
            main_year: "", method: "", explain: "", cost: "",
            year: "", s_sm_id: "", total: ""}
        ]
    });
    
    function isSpan(params){
        return params.data.span === 'no-result';
    }

}, [columns]);

const passToggle = () =>{
    toggle();
}
return (
    <div>
        <ModalBody>
            <div>
                <SearchDropdown target={type} options={typeList} placeholder="등급" onChange={typeChange} width="230px"/>
                <SearchDropdown target={no} options={nos} placeholder="번호" onChange={noChange} width="200px"/>
                <SearchDropdown target={name} options={names} placeholder="노선명" width="230px"/>
                <SearchDropdown target={dir} options={[{label: '전체', value: "-1"}, {label: '상행', value:"0"},{label:'하행', value:"1"}]} placeholder="행선" width="200px"/>
                {/* <SearchDropdown target={lane} options={[{label: '1', value:"1"},{label:'2', value:"2"}]} placeholder="차선"/>{' '} */}
                <Button color="secondary" size="sm" style={{width:"100px"}} onClick={handleClick} value="0">검색</Button>
             
            </div>
            <div className="ag-theme-balham" style={{marginTop: "5px", height:"70vh"}}>
                {/* <GridComponent columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} onRowSelected={stateGrid.onRowSelected} onSelectionChanged={stateGrid.onSelectionChanged} style={{width:"100%", height:"70vh"}} /> */}
                <GridComponent columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} onSelectionChanged={stateGrid.onSelectionChanged} style={{width:"100%", height:"70vh", textAlign:'center'}} passStateEvt={getGridComponentState}/>
            </div>

        </ModalBody>
        <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value={selectExt}/>Km
            </span>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                {/* <Button color="primary" onClick={batchModified}>일괄수정</Button> */}
                {subModal}
                <Button className="feather icon-map" color="primary" onClick={viewMap}> 지도확인</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </div>
);
}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({type: actionTypes.OPEN_CHAT, data: props})
    }
}

export default connect(chatStateToProps, chatDispatchToProps) (RouteMaster);