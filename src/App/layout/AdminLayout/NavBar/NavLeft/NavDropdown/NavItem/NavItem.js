import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader } from 'reactstrap';
import Draggable from 'react-draggable';
import {connect} from 'react-redux';

import './NavItem.css';

const PaveInxRuleSetting = React.lazy(() => import('./ModalContent/Default/PaveInxRuleSetting'));
const ConfigSetting = React.lazy(() => import('./ModalContent/Default/ConfigSetting'));
const Manual = React.lazy(() => import('./ModalContent/Default/Manual'));
const CreateAndUpdate = React.lazy(() => import('./ModalContent/RouteManage/CreateAndUpdate'));
const RouteMaster = React.lazy(()=>import('./ModalContent/RouteManage/RouteMaster'));
const EquipmentSurvey = React.lazy(()=>import('./ModalContent/RouteManage/EquipmentSurvey'));
const EquipmentSurvey2 = React.lazy(()=>import('./ModalContent/SurveyManage/EquipmentSurvey2'));
const MthistoryInsert = React.lazy(()=>import('./ModalContent/RepairManage/MthistoryInsert'));
const PosSearch = React.lazy(()=>import('./ModalContent/RouteManage/PosSearch'));
const NodeLinkUpdate = React.lazy(()=>import('./ModalContent/DBManage/NodeLinkUpdate'));
const SurveyPlanner = React.lazy(()=>import('./ModalContent/SurveyManage/Planner'));
const SurveyTarget = React.lazy(()=>import('./ModalContent/SurveyManage/Target'));
const SurveyDetail = React.lazy(()=>import('./ModalContent/SurveyManage/Detail'));
// const SurveyMaster = React.lazy(()=>import('./ModalContent/SurveyManage/Master'));
const SurveyMaster = React.lazy(()=>import('./ModalContent/SurveyManage/SurveyMaster'));
// const RepairPlanner = React.lazy(()=>import('./ModalContent/RepairManage/Planner'));
const RepairTarget = React.lazy(()=>import('./ModalContent/RepairManage/RepairTarget'));
const RepairRecord = React.lazy(()=>import('./ModalContent/RepairManage/Record'));
const StatisticsOne = React.lazy(()=>import('./ModalContent/Statistics/StatisticsOne'));
const StatisticsTwo = React.lazy(()=>import('./ModalContent/Statistics/StatisticsTwo'));
const StatisticsThree = React.lazy(()=>import('./ModalContent/Statistics/StatisticsThree'));
const RoadRepairPriorityReport = React.lazy(()=>import('./ModalContent/Report/RoadRepairPriorityReport'));
// const Test = React.lazy(()=>import('./ModalContent/Default/Test'));

const NavItem = (props) => {
    const {title, className, component} = props;
    const [modal, setModal] = useState(false);
    const [backdrop, setBackdrop] = useState('static');
    // const [nestedModal, setNestedModal] = useState(false);

    const [content, setContent] = useState(null);


    useEffect(() => {
    

        switch(component){
            case "PaveInxRuleSetting":
                // setContent(<PaveInxRuleSetting toggleEvt={toggle}/>);
                setContent(<PaveInxRuleSetting toggle={toggle}/>);
            break;
            case "ConfigSetting":
                setContent(<ConfigSetting toggle={toggle}/>);
            break;
            case "Manual":
                setContent(<Manual toggle={toggle}/>);
            break;
            case "CreateAndUpdate":
                setContent(<CreateAndUpdate />);
            break;
            case "RouteMaster":
                setContent(<RouteMaster toggle={toggle}/>);
            break;
            case "EquipmentSurvey":
                setContent(<EquipmentSurvey toggle={toggle}/>);
            break;
            case "EquipmentSurvey2":
                setContent(<EquipmentSurvey2  toggle={toggle}/>);
            break;
            case "MthistoryInsert":
                setContent(<MthistoryInsert toggle={toggle}/>);
            break;
            case "PosSearch":
                setContent(<PosSearch toggle={toggle} backdropControl={changeBackdrop}/>);
            break;
            case "NodeLinkUpdate":
                setContent(<NodeLinkUpdate toggle={toggle}/>);
            break;
            case "SurveyPlanner":
                setContent(<SurveyPlanner toggle={toggle}/>);
            break;
            case "SurveyTarget":
                setContent(<SurveyTarget toggle={toggle}/>);
            break;
            case "SurveyDetail":
                setContent(<SurveyDetail toggle={toggle}/>);
            break;
            case "SurveyMaster":
                setContent(<SurveyMaster toggle={toggle}/>);
            break;
            case "RepairTarget":
                setContent(<RepairTarget toggle={toggle}/>);
            break;
            case "RepairRecord":
                setContent(<RepairRecord toggle={toggle}/>);
            break;
            case "StatisticsOne":
                setContent(<StatisticsOne toggle={toggle}/>);
            break;
            case "StatisticsTwo":
                setContent(<StatisticsTwo toggle={toggle}/>);
            break;
            case "StatisticsThree":
                setContent(<StatisticsThree toggle={toggle}/>);
            break;
            case "RoadRepairPriorityReport":
                setContent(<RoadRepairPriorityReport toggle={toggle}/>);
            break;
            // case "Test":
            //     setContent(<Test />);
            // break;
        }

    }, [])

    const changeBackdrop = (value) => {
        setBackdrop(value);
    }
    
    const toggle = (e) => {

        //  최상위 모달 닫기 버튼 기능 시작 (ModalFooter)
        if(typeof(e)=='undefined'){
        setModal(false);
        return;
        }
        //  최상위 모달 닫기 버튼 기능 끝 (ModalFooter)

        //  최상위 모달 x 버튼 기능 시작 (ModalHeader)
        if(e._dispatchListeners.length==2) return;
        if(e._dispatchListeners.length>1&&e._dispatchListeners[0].name=="") return;
        if(e._dispatchListeners.length>1&&e._dispatchListeners[0].name==e._dispatchListeners[1].name) return;
        setModal(!modal);

        //  최상위 모달 x 버튼 기능 끝 (ModalHeader)
    }


    // return <li key={inx}><a className="dropdown-item" draggable="false"><span>{item}</span></a></li>; //href={DEMO.BLANK_LINK}
    return (<li onClick={toggle}>
                <a className="dropdown-item" draggable="false">
                    <span>{title}</span>
                </a>
                <Draggable
                    // axis="both"
                    handle=".handle"
                    defaultPosition={{ x: 0, y: 0 }}
                    position={null}
                    grid={[30, 30]}
                    // scale={2}
                    // onStart={this.handleStart}
                    // onDrag={test}
                    // onStop={this.handleStop}
                    // bounds={{left: -600, top: -600, right: 600, bottom: 600}}
                >
                    <Modal isOpen={modal} toggle={toggle} className={className} backdrop={backdrop}>
                        <ModalHeader toggle={toggle}  className="handle">{title}</ModalHeader>
                        {content}
                        {/* <ModalBody>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. */}
                        {/* ---------- subModal ------------
                        <Button color="success" onClick={toggleNested}>Show Nested Modal</Button>
                        <Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
                            <ModalHeader>Nested Modal title</ModalHeader>
                            <ModalBody>Stuff and things</ModalBody>
                            <ModalFooter>
                            <Button color="primary" onClick={toggleNested}>Done</Button>{' '}
                            <Button color="secondary" onClick={toggleAll}>All Done</Button>
                            </ModalFooter>
                        </Modal> */}
                        {/* </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
                            <Button color="secondary" onClick={toggle}>Cancel</Button>
                        </ModalFooter> */}
                    </Modal>
                </Draggable>
            </li>); //href={DEMO.BLANK_LINK}
}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
    }
}

const chatDispatchToProps = dispatch => {
    return {
        // onReOpenChat: () => dispatch({type: actionTypes.REOPEN_CHAT})
    }
}

export default connect(chatStateToProps, chatDispatchToProps) (NavItem);