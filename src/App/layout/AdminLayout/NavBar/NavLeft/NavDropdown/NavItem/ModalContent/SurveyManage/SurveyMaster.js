import React, { useState, useEffect, useRef } from 'react';
import { ModalBody, ModalFooter, Label, Button, Input } from 'reactstrap';
import ToggleButton from 'react-bootstrap/ToggleButton';
import './SurveyMaster.css';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import SwitchButton from '../../../../../../../../components/SwitchButton';
import GridComponent from '../../../../../../../../components/GridComponent';
import { typeList } from '../Common/routeInfoList.js';
import { XmlParser, server_connect } from '../../../../../../../../../tpms-common.js';
import MasterColXML from '../../../../../../../../../resource/settingXml/master.xml';
import CrackXML from '../../../../../../../../../resource/settingXml/equip2-crack.xml';
import GprXML from '../../../../../../../../../resource/settingXml/equip2-gpr.xml';
import HwdXML from '../../../../../../../../../resource/settingXml/equip2-hwd.xml';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import Equip1XML from '../../../../../../../../../resource/settingXml/pavement-equip1.xml';
import { ButtonGroup } from 'react-bootstrap';
import TpmsView from '../../../../../../../../components/TpmsView';
import serviceUrl from  '../../../../../../../../../request-url-list';

const SurveyMaster = ({ toggle, onOpenChat, onCloseChat }) => {
    const [totalExt, setTotalExt] = useState(0);
    const [yearList, setYearList] = useState();
    const [names, setNames] = useState([]);
    const [nos, setNos] = useState([]);
    const [rank, setRank] = useState();
    const [no, setNo] = useState();
    const [name, setName] = useState();
    const [equip1Switch, setEquip1Switch] = useState(false);
    const [equip2Switch, setEquip2Switch] = useState(false);
    const [tableViewSwitch, setTableViewSwitch] = useState(false);
    const [routeMasterCols, setRouteMasterCols] = useState();
    const [routeMaster, setRouteMaster] = useState({});
    const [routeMasterList, setRouteMasterList] = useState();
    const [equip1Master, setEquip1Master] = useState({ style: { display: 'none' } });
    const [equip2Master, setEquip2Master] = useState({ style: { display: 'none' } });
    // const [equip1View, setEquip1View] = useState({ style: { display: 'none' } });
    // const [equip2View, setEquip2View] = useState({ style: { display: 'none' } });
    const [selectedGridType, setSelectedGridType] = useState();
    const [routeMasterGridApi, setRouteMasterGridApi] = useState();
    const [equipGridClass, setEquipGridClass] = useState("");
    const [equipGrid, setEquipGrid] = useState({});
    const [equipGridApi, setEquipGridApi] = useState(null);
    const [equipCols, setEquipCols] = useState();
    const [masterSelectTId, setMasterSelectTId] = useState();
    const [masterIsSelect, setMasterIsSelect] = useState(false);
    const [equip1IsSelect, setEquip1IsSelect] = useState(false);
    const [equip2IsSelect, setEquip2IsSelect] = useState(false);
    const [selEquipData, setSelEquipData] = useState();
    const [btnGroup, setBtnGroup] = useState();
    const [radioValue, setRadioValue] = useState('1');
    const [equip2CrackData, setEquip2CrackData] = useState();
    const [masterSelectTsId, setMasterSelectTsId] = useState();
    const [crackCols, setCrackCols] = useState();
    const [crackTHead, setCrackTHead] = useState();
    const [equip2HwdData, setEquip2HwdData] = useState();
    const [gprCols, setGprCols] = useState();
    const [gprTHead, setGprTHead] = useState();
    const [equip2GprData, setEquip2GprData] = useState();
    const [hwdCols, setHwdCols] = useState();
    const [hwdTHead, setHwdTHead] = useState();
    const [equip2Grid, setEquip2Grid] = useState({});
    const [equip2GridApi, setEquip2GridApi] = useState();
    const [btnType, setBtnType] = useState();
    const year = useRef();
    const dir = useRef();
    const [equip2Btn, setEquip2Btn] = useState('');
    const [equip1Btn, setEquip1Btn] = useState('');
    const [equip1ViewGroup, setEquip1ViewGroup] = useState();
    const [btnEquip1Group, setBtnEquip1Group] = useState();
    const [equip1radioValue, setEquip1radioValue] = useState('rutting');
    const [viewContent, setViewContent] = useState();
    const [view2Content, setView2Content] = useState();

    useEffect(() => {
        yearChangeEvt();
        document.getElementById("bottom-content").style.display = "none";
        let ignore = false;
        async function fetchData() {
            const routeColumn = await XmlParser(MasterColXML);
            const result_equip = await XmlParser(Equip1XML);
            const crackColumn = await XmlParser(CrackXML);
            const gprColumn = await XmlParser(GprXML);
            const hwdColumn = await XmlParser(HwdXML);
            if (!ignore) {
                setRouteMasterCols(routeColumn.getElementsByTagName('Columns')[0]['children']);
                setEquipCols(result_equip.getElementsByTagName('Columns')[0]['children']);
                setCrackCols(crackColumn.getElementsByTagName('Columns')[1]['children']);
                setGprCols(gprColumn.getElementsByTagName('Columns')[1]['children']);
                setHwdCols(hwdColumn.getElementsByTagName('Columns')[1]['children']);
            }
        }
        fetchData();
        return () => {
            ignore = true;
        }
    }, []);
    //1차장비 btn
    const equip1Radios = [
        { name: 'RUTTING', value: 'rutting' },
        { name: 'IRI', value: 'iri' },
    ];

    const euip1ToggleBtns = () => {
        setBtnEquip1Group(
            <>
                <ButtonGroup toggle size="sm">
                    {equip1Radios.map((radio, idx) => (
                        <ToggleButton
                            key={idx}
                            type="radio"
                            variant="secondary"
                            name="equip1Radio"
                            value={radio.value}
                            checked={equip1radioValue === radio.value}
                            onChange={(e) => setEquip1radioValue(e.currentTarget.value)}
                        >
                            {radio.name}
                        </ToggleButton>
                    ))}
                </ButtonGroup>
            </>
        )
    }

    useEffect(() => {
        euip1ToggleBtns();
        if (equip1radioValue && equip1ViewGroup) {
            makeViewList(equip1radioValue);
        }
    }, [equip1radioValue]);

    //2차장비 btn
    const radios = [
        { name: 'CRACK', value: '1' },
        { name: 'GPR', value: '2' },
        { name: 'HWD', value: '3' },
    ];

    const toggleBtns = () => {
        setBtnGroup(
            <>
                <ButtonGroup toggle size="sm">
                    {radios.map((radio, idx) => (
                        <ToggleButton
                            id={radio.name}
                            key={idx}
                            type="radio"
                            variant="secondary"
                            name="radio"
                            value={radio.value}
                            checked={radioValue === radio.value}
                            onChange={(e) => setRadioValue(e.currentTarget.value)}
                        >
                            {radio.name}
                        </ToggleButton>
                    ))}
                </ButtonGroup>
            </>
        )
    }

    useEffect(() => {
        toggleBtns();
        if (equip1IsSelect) {
            equipGridApi.deselectAll();
        }
        if (radioValue == '1') {
            changeEquipGrid(crackTHead, 'crack');
        } else if (radioValue == '2') {
            changeEquipGrid(gprTHead, 'gpr');
            // if(equip2ViewGroup)makeView2List('');
        } else if (radioValue == '3') {
            changeEquipGrid(hwdTHead, 'hwd');
        }
    }, [radioValue]);

    const changeEquipGrid = async (tHeader, equipType) => {
        setBtnType(equipType);
        if (masterSelectTsId) {
            var tsIdList = masterSelectTsId.filter(function (el) {
                return el != null;
            });
            if (equipType == 'crack') {
                var params = new Object();
                params['ts_id'] = tsIdList;
                params['year'] = equip1SelYear;
                var result = await server_connect(serviceUrl["pavement"]["getEquip2Crack"], params);
                var equip2CrackData = result['data']['res'];
                var header = tHeader;
                setCrackTHead(header);
                setEquip2CrackData(equip2CrackData);
            }
            if (equipType == 'gpr') {
                var params = new Object();
                params['ts_id'] = tsIdList;
                params['year'] = equip1SelYear;
                var result = await server_connect(serviceUrl["pavement"]["getEquip2Gpr"], params);
                var equip2GprData = result['data']['res'];
                var header = tHeader;
                setGprTHead(header);
                setEquip2GprData(equip2GprData);
            }
            if (equipType == 'hwd') {
                var params = new Object();
                params['ts_id'] = tsIdList;
                params['year'] = equip1SelYear;
                var result = await server_connect(serviceUrl["pavement"]["getEquip2Hwd"], params);
                var equip2HwdData = result['data']['res'];
                var header = tHeader;
                setHwdTHead(header);
                setEquip2HwdData(equip2HwdData);
            }
        }
        setEquip2Grid({
            ...equip2Grid,
            columnDefs: tHeader,
        });
    }

    useEffect(() => {
        if (crackCols) {
            async function equipCrack() {
                var header = tHeader(crackCols);
                setCrackTHead(header);
                setEquip2Grid({
                    columnDefs: header,
                    rowData: null,
                    style: {
                        display: 'block',
                        width: '100%',
                        height: '23vh',
                        marginTop: '18px'
                    },
                });
            }
            equipCrack();
        }
    }, [crackCols]);

    useEffect(() => {
        if (gprCols) {
            setGprTHead(tHeader(gprCols));
        }
    }, [gprCols]);

    useEffect(() => {
        if (hwdCols) {
            setHwdTHead(tHeader(hwdCols));
        }
    }, [hwdCols]);

    //mster ts_id 
    useEffect(() => {
        if (masterSelectTsId) {
            if (btnType == "crack") changeEquipGrid(crackTHead, 'crack');
            if (btnType == "gpr") changeEquipGrid(gprTHead, 'crack');
            if (btnType == "hwd") changeEquipGrid(hwdTHead, 'hwd');
        }
    }, [masterSelectTsId]);

    useEffect(() => {
        if (equip2CrackData) {
            if (radioValue == 1) {
                if (equip2CrackData.length > 0) {
                    setEquip2Grid({
                        ...equip2Grid,
                        columnDefs: crackTHead,
                        rowData: equip2CrackData,
                        onSelectionChanged: function (e) {
                            var selectedRows = e.api.getSelectedNodes();
                            if (selectedRows.length == 0) return;
                            setMasterIsSelect(false);
                            setEquip2IsSelect(true);
                            setEquip1IsSelect(false);
                            setTotalExt('-');
                        }
                    });
                } else {
                    setEquip2Grid({
                        ...equip2Grid,
                        columnDefs: crackTHead,
                        rowData: [
                            { span: 'no-result', target_id: "검색 결과가 없습니다" }
                        ],
                    });
                }
            }
        }
    }, [equip2CrackData]);

    useEffect(() => {
        if (equip2GprData) {
            if (radioValue == 2) {
                if (equip2GprData.length > 0) {
                    setEquip2Grid({
                        ...equip2Grid,
                        columnDefs: gprTHead,
                        rowData: equip2GprData,
                        onSelectionChanged: function (e) {
                            var selectedRows = e.api.getSelectedNodes();
                            if (selectedRows.length == 0) return;
                            setMasterIsSelect(false);
                            setEquip2IsSelect(true);
                            setEquip1IsSelect(false);
                            setTotalExt('-');
                        }
                    });
                } else {
                    setEquip2Grid({
                        ...equip2Grid,
                        columnDefs: gprTHead,
                        rowData: [
                            { span: 'no-result', target_id: "검색 결과가 없습니다" }
                        ],
                    });
                }
            }
        }
    }, [equip2GprData]);

    useEffect(() => {
        if (equip2HwdData) {
            if (radioValue == 3) {
                if (equip2HwdData.length > 0) {
                    setEquip2Grid({
                        ...equip2Grid,
                        columnDefs: hwdTHead,
                        rowData: equip2HwdData,
                        onSelectionChanged: function (e) {
                            var selectedRows = e.api.getSelectedNodes();
                            if (selectedRows.length == 0) return;
                            setMasterIsSelect(false);
                            setEquip2IsSelect(true);
                            setEquip1IsSelect(false);
                            setTotalExt('-');
                        }
                    });
                } else {
                    setEquip2Grid({
                        ...equip2Grid,
                        columnDefs: hwdTHead,
                        rowData: [
                            { span: 'no-result', target_id: "검색 결과가 없습니다" }
                        ],
                    });
                }
            }
        }
    }, [equip2HwdData]);

    useEffect(() => {
        if (routeMasterCols) {
            async function allMasterList() {
                var result = await server_connect(serviceUrl["pavement"]["getAllMaster"]);
                result = result['data']['res'];
                var header = tHeader(routeMasterCols);
                if (result.length > 0) {
                    setRouteMasterList(result);
                    setRouteMaster({
                        columnDefs: header,
                        rowData: null,
                        style: {
                            // width: '100',
                            // height: '65vh',
                            padding: '3px',
                            display: 'flex',
                            flex: 1,
                            flexDirection: 'column',
                            textAlign:'center'
                        },
                        onSelectionChanged: function (e) {
                            var selectedRows = e.api.getSelectedNodes();
                            if (selectedRows.length == 0) return;
                            setSelectedGridType('master');
                            setEquip1IsSelect(false);
                            setEquip2IsSelect(false);
                            setMasterIsSelect(true);
                            var extSum = 0;
                            var selRows = new Array();
                            var selTsIdList = new Array();
                            selectedRows.map((row) => {
                                var selectedRow = row['data']['target_id'];
                                var selectedTsId = row['data']['ts_id'];
                                var selectedRowExt = row['data']['sm_ext'];
                                extSum = extSum + selectedRowExt;
                                selRows.push(selectedRow);
                                selTsIdList.push(selectedTsId);
                            });
                            setMasterSelectTId(selRows);
                            setMasterSelectTsId(selTsIdList);
                            setTotalExt((extSum / 1000).toFixed(2));
                        },
                        rowClassRules: null
                    });
                }
            }
            allMasterList();
        }
    }, [routeMasterCols]);

    useEffect(() => {
        if (masterSelectTId) {
            var targetList = masterSelectTId.filter(function (el) {
                return el != null;
            });
            async function equip1List() {
                var params = new Object();
                params['target_id'] = targetList;
                var result = await server_connect(serviceUrl["pavement"]["getSelectEquip1"], params);
                var equip1Data = result['data']['res'];
                setSelEquipData(equip1Data);
            }
            equip1List();
        }
    }, [masterSelectTId]);

    useEffect(() => {
        if (selEquipData) {
            var yearfilterData = new Array();
            for (var i = 0; i < selEquipData.length; ++i) {
                if (equip1SelYear == selEquipData[i]['year']) {
                    yearfilterData.push(selEquipData[i]);
                }
            }
            setEquipGrid({
                ...equipGrid,
                rowData: yearfilterData,
                onSelectionChanged: function (e) {
                    var selectedRows = e.api.getSelectedNodes();
                    if (selectedRows.length == 0) return;
                    setMasterIsSelect(false);
                    setEquip2IsSelect(false);
                    setEquip1IsSelect(true);
                    setTotalExt('-');
                }
            });
        } else {
            setEquipGrid({
                ...equipGrid,
                rowData: [
                    { span: 'no-result', target_id: "검색 결과가 없습니다" }
                ],
            });
        }
    }, [selEquipData]);

    useEffect(() => {
        if (masterIsSelect) {
            equipGridApi.deselectAll();
            equip2GridApi.deselectAll();
        }
        if (equip1IsSelect) {
            routeMasterGridApi.deselectAll();
            equip2GridApi.deselectAll();
        }
        if (equip2IsSelect) {
            equipGridApi.deselectAll();
            routeMasterGridApi.deselectAll();
        }
    }, [masterIsSelect, equip1IsSelect, equip2IsSelect]);

    useEffect(() => {
        if (equipCols) {
            var header = tHeader(equipCols);
            async function allEquipList() {
                setEquipGrid({
                    columnDefs: header,
                    rowSelection: 'multiple',
                    rowData: null,
                    style: {
                        width: "100%", height: "25vh", display: "none", padding: "3px"
                    },
                    onSelectionChanged: function (e) {
                        var selectedRows = e.api.getSelectedNodes();
                        if (selectedRows.length == 0) return;
                        setSelectedGridType('equip');
                    }
                });
            }
            allEquipList();
        }
    }, [equipCols]);
    const [equip2ViewGroup, setEquip2ViewGroup] = useState();
    useEffect(() => {
        if (tableViewSwitch) {
            document.getElementById("GPR").style.display = "none";
            document.getElementById("HWD").style.display = "none";
            async function equip1Data() {
                var result = await server_connect(serviceUrl["pavement"]["getEquip1RuttIri"]);
                var equip1RuttIri = result['data']['res'];
                var viewGroup = new Object();
                for (var i = 0; i < equip1RuttIri.length; ++i) {
                    if (equip1RuttIri[i]['iri'] || equip1RuttIri[i]['rutting']) {
                        var iri = Math.round(equip1RuttIri[i]['iri']);
                        var rutting = Math.round(equip1RuttIri[i]['rutting']);
                        if (rutting >= 0 && rutting < 4) {
                            equip1RuttIri[i]['rutting'] = 1;
                        } else if (rutting >= 4 && rutting < 8) {
                            equip1RuttIri[i]['rutting'] = 2;
                        } else if (rutting >= 8 && rutting < 12) {
                            equip1RuttIri[i]['rutting'] = 3;
                        } else if (rutting >= 12 && rutting < 16) {
                            equip1RuttIri[i]['rutting'] = 4;
                        } else if (rutting >= 16) {
                            equip1RuttIri[i]['rutting'] = 5;
                        }

                        if (iri >= 0 && iri < 2) {
                            equip1RuttIri[i]['iri'] = 1;
                        } else if (iri >= 2 && iri < 4) {
                            equip1RuttIri[i]['iri'] = 2;
                        } else if (iri >= 4 && iri < 6) {
                            equip1RuttIri[i]['iri'] = 3;
                        } else if (iri >= 6 && iri < 8) {
                            equip1RuttIri[i]['iri'] = 4;
                        } else if (iri >= 8) {
                            equip1RuttIri[i]['iri'] = 5;
                        }

                    }
                    var key = "" + equip1RuttIri[i]['tri_id'] + equip1RuttIri[i]['main_num'] + equip1RuttIri[i]['sub_num']
                        + equip1RuttIri[i]['dir'] + 1//lane;
                    var viewList = new Array();
                    if (viewGroup.hasOwnProperty(key)) {
                        viewList = viewGroup[key];
                    }
                    viewList.push(equip1RuttIri[i]);
                    viewGroup[key] = viewList;
                }
                setEquip1ViewGroup(viewGroup);
            }
            equip1Data();

            async function equip2ViewCr() {
                var result = await server_connect(serviceUrl["pavement"]["getEquip2ViewCrack"]);
                var equip2ViewCrack = result['data']['res'];
                var viewGroup = new Object();
                for (var i = 0; i < equip2ViewCrack.length; ++i) {
                    if (equip2ViewCrack[i]['cr']) {
                        var crack = Math.round(equip2ViewCrack[i]['cr']);
                        if (crack >= 0 && crack < 5) {
                            equip2ViewCrack[i]['cr'] = 1;
                        } else if (crack >= 5 && crack < 10) {
                            equip2ViewCrack[i]['cr'] = 2;
                        } else if (crack >= 10 && crack < 15) {
                            equip2ViewCrack[i]['cr'] = 3;
                        } else if (crack >= 15 && crack < 25) {
                            equip2ViewCrack[i]['cr'] = 4;
                        } else if (crack >= 25) {
                            equip2ViewCrack[i]['cr'] = 5;
                        }
                    }
                    var key = "" + equip2ViewCrack[i]['tri_id'] + equip2ViewCrack[i]['main_num'] + equip2ViewCrack[i]['sub_num']
                        + equip2ViewCrack[i]['dir'] + 1//lane;
                    var viewList = new Array();
                    if (viewGroup.hasOwnProperty(key)) {
                        viewList = viewGroup[key];
                    }
                    viewList.push(equip2ViewCrack[i]);
                    viewGroup[key] = viewList;
                }
                setEquip2ViewGroup(viewGroup);
            }
            equip2ViewCr();
            setEquip1Switch(false);
            setEquip2Switch(false);
            setEquipGridClass("equip-area");
            setEquip2Btn("equip2-btns");
            setEquip1Btn("equip1-btns");
            document.getElementById('equip1View').style.height = '30vh';
            document.getElementById('equip1View').style.display = 'flex';
            document.getElementById('equip2View').style.height = '30vh';
            document.getElementById('equip2View').style.display = 'flex';
            document.getElementById("bottom-content").style.display = "contents";
        } else {
            if (btnGroup) {
                document.getElementById('GPR').style.display = "inline-block";
                document.getElementById('HWD').style.display = "inline-block";
            }
            document.getElementById('equip1View').style.display = 'none';
            document.getElementById('equip2View').style.display = 'none';
            document.getElementById("bottom-content").style.display = "none";

            if (!(equip1Switch || equip2Switch)) {
                setEquipGridClass("");
                setEquip1Btn({ style: { display: 'none' } });
                document.getElementById("routeMaster").style.width = "100%";
            } else if (equip2Switch) {
                toggleBtns();
                setEquip2Master({
                    style: { display: 'flex', flex: 1, flexDirection: 'column'}
                });
                setEquip2Btn("sm-equip-btns");
                setEquip1Btn('none');
                document.getElementById("bottom-content").style.display = "flex";
                document.getElementById("bottom-content").style.flex = 1;
                
            }
        }
    }, [tableViewSwitch]);

    useEffect(() => {
        if (equip2ViewGroup) {
            makeView2List('cr');
        }
    }, [equip2ViewGroup]);

    const makeView2List = (targetCol) => {
        var keyList = Object.keys(equip2ViewGroup);
        var viewList = new Array();
        var viewrank; var viewno; var viewname;
        for (var i = 0; i < keyList.length; ++i) {
            equip2ViewGroup[keyList[i]].map((obj, inx) => {
                if (inx == 0) {
                    viewrank = obj['rank'];
                    viewno = obj['no'];
                    viewname = obj['name'];
                }
            });
            var addInfo = {
                info: {
                    rank: viewrank,
                    no: viewno,
                    name: viewname,
                    targetCol: targetCol,
                    extCol: 'sm_ext'
                },
                children: equip2ViewGroup[keyList[i]]
            };
            viewList.push(addInfo);
        }
        setView2Content(<TpmsView dataSet={viewList} />);
    }

    useEffect(() => {
        if (equip1ViewGroup) {
            makeViewList('rutting');
        }
    }, [equip1ViewGroup]);

    const makeViewList = (targetCol) => {
        // setViewContent(<TpmsView dataSet=""/>);
        var keyList = Object.keys(equip1ViewGroup);
        var viewList = new Array();
        var viewrank; var viewno; var viewname;
        for (var i = 0; i < keyList.length; ++i) {
            equip1ViewGroup[keyList[i]].map((obj, inx) => {
                if (inx == 0) {
                    viewrank = obj['rank'];
                    viewno = obj['no'];
                    viewname = obj['name'];
                }
            });
            var addInfo = {
                info: {
                    rank: viewrank,
                    no: viewno,
                    name: viewname,
                    targetCol: targetCol,
                    extCol: 'sm_ext'
                },
                children: equip1ViewGroup[keyList[i]]
            };
            viewList.push(addInfo);
        }
        setViewContent(<TpmsView dataSet={viewList} />);
    }

    useEffect(() => {
        if (!(equip1Switch)) {
            setEquip1Master({ style: { display: 'none' } });
            if (routeMasterGridApi) {
                routeMasterGridApi.deselectAll();
            }
        }
        if (!(equip2Switch)) {
            setEquip2Master({ style: { display: 'none' } });
            if (routeMasterGridApi) {
                routeMasterGridApi.deselectAll();
            }
        }
        setEquip2Grid({
            ...equip2Grid,
            rowData: null
        });
        setEquipGrid({
            ...equipGrid,
            rowData: null
        });
        if (equip1Switch || equip2Switch) {
            document.getElementById("equip2View").style.display = "none";
            document.getElementById("equip1View").style.display = "none";
            setEquipGridClass("equip-area");
            setEquip2Btn("equipAll-btns");
            setTableViewSwitch(false);
            setEquip1Btn({ style: { display: 'none' } });
            routeMasterGridApi.deselectAll();
            if (equip1Switch && equip2Switch) {
                setEquip1Btn('none');
                setEquip1Master({
                    style: { display: 'flex', flex: 1, flexDirection: 'column' , textAlign:'center'}
                });
                setEquip2Master({
                    style: { display: 'flex', flex: 1, flexDirection: 'column' , textAlign:'center'}
                });
                document.getElementById("bottom-content").style.display = "contents";
                if (tableViewSwitch) {
                    document.getElementById('equip1View').style.height = '30vh';
                    document.getElementById('equip1View').style.display = 'flex';
                    document.getElementById('equip2View').style.height = '30vh';
                    document.getElementById('equip2View').style.display = 'flex';
                }
            } else {
                if (equip1Switch) {
                    setEquip1Btn({ style: { display: 'none' } });
                    setEquip1Master({
                        style: { display: 'flex', flex: 1, flexDirection: 'column', height: '65vh' , textAlign:'center'}
                    });
                    document.getElementById("bottom-content").style.display = "none";
                } else if (equip2Switch) {
                    toggleBtns();
                    setEquip2Master({
                        style: { display: 'flex', flex: 1, flexDirection: 'column', textAlign:'center'}
                    });
                    setEquip2Btn("sm-equip-btns");
                    setEquip1Btn('none');
                    document.getElementById("bottom-content").style.display = "flex";
                    document.getElementById("bottom-content").style.flex =1;
                }
            }
        } else {
            document.getElementById("bottom-content").style.display = "contents";
            if (!(tableViewSwitch)) {
                setEquipGridClass("");
                document.getElementById("routeMaster").style.width = "100%";
                document.getElementById("bottom-content").style.display = "none";
            }
        }
        // changeFilter();
    }, [equip1Switch, equip2Switch]);

    // 그리드 header 세팅
    const tHeader = (cols) => {
        var prev;
        var colNum = cols.length;
        var firstName = cols[0]['name'];
        var header = cols.map((obj, index) => {
            if (!(prev) || obj['attributes']['className'] != prev) {
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        header = header.filter(function (el) {
            return el != null;
        });

        header = header.map((obj) => {
            return {
                headerName: obj,
                children:
                    cols.map((col) => {
                        var attr = col['attributes'];
                        var width = null;
                        if (attr.hasOwnProperty("wd")) {
                            width = Number(attr['wd']);
                        }
                        if (attr['className'] == obj && col['name'] == firstName) {
                            return {
                                headerName: attr['title'],
                                field: col['name'],
                                width: width,
                                colSpan: function (params) {
                                    if (isSpan(params)) {
                                        return colNum;
                                    } else {
                                        return 1;
                                    }
                                },
                                // cellStyle: function (params) {
                                //     if (isSpan(params)) {
                                //         return { textAlign: 'center' };
                                //     } else {
                                //         return { textAlign: 'left' };
                                //     }
                                // },
                            }
                        } else if (attr['className'] == obj && col['name'] != firstName) {
                            return { headerName: attr['title'], field: col['name'], width: width }
                        }
                    }).filter(function (el) {
                        return el != null;
                    })
            }
        });
        function isSpan(params) {
            return params.data.span === 'no-result';
        }
        return header;
    }

    const viewMap = () => {
        var props = [];
        var masterNodes = routeMasterGridApi.getSelectedNodes();
        if (masterNodes.length > 0) {
            props['allData'] = routeMasterList;
            props['selectedNodes'] = masterNodes;
            props['header'] = [
                {
                    label: 'rank',
                    name: '등급'
                },
                {
                    label: 'no',
                    name: '노선번호'
                },
                {
                    label: 'name',
                    name: '노선명'
                },
                {
                    label: 'rev_dir',
                    name: '행선'
                },
                {
                    label: 'node_name',
                    name: '이벤트명'
                },
                {
                    label: 'node_type',
                    name: '이벤트 종류'
                }
            ];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        } else {
            alert("노선마스터에서 구간을 선택해 주세요.");
        }
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display = 'none';
        document.getElementsByClassName('modal-backdrop')[0].style.display = 'none';
        document.getElementsByClassName('modal-dialog')[0].style.display = 'none';
    }

    const passToggle = () => {
        onCloseChat();
        toggle();
    }

    // 검색필터: 도로등급 선택 시, 해당 도로등급에 속하는 노선번호 불러오기
    const roadRankOnChangeEvt = async (selectedObj) => {
        setRank(selectedObj['value']);
        setNo(null);
        setName(null);
        var params = new Object();
        params['rank'] = selectedObj['value'];
        const noRes = await server_connect(serviceUrl["pavement"]["selectNoListForRank"], params);
        let noResData = noRes['data']['res'];
        setNos(makeOptionsJson(noResData));

        const nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRank"], params);
        let nameResData = nameRes['data']['res'];
        setNames(makeOptionsJson(nameResData));
    }

    const makeOptionsJson = (arr) => {
        let list = arr.map((item) => {
            return { label: item, value: item };
        });
        return list;
    }

    // 검색필터: 노선번호 선택 시, 해당 노선번호에 속하는 노선명 불러오기
    const roadNoOnChangeEvt = async (selectedObj) => {
        setName(null);
        let _no = selectedObj['value'];
        setNo(_no);
        var params = new Object();
        params['rank'] = rank;
        params['no'] = _no;
        const nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRankNo"], params);
        let nameResData = nameRes['data']['res'];
        setNames(makeOptionsJson(nameResData));
    }

    const yearChangeEvt = () => {
        var currentYear = new Date().getFullYear();
        var arr = new Array();
        for (var i = currentYear; i >= (currentYear - 10); --i) {
            arr.push({ label: i, value: i });
        }
        setYearList(arr);
    }

    const [equip1SelYear, setEquip1SelYear] = useState();
    //검색버튼 클릭
    const handleClick = async (e) => {
        setEquipGrid({
            ...equipGrid,
            rowData: [
                { span: 'no-result', target_id: "검색 결과가 없습니다" }
            ],
        });
        routeMasterGridApi.deselectAll();
        var params = new Object();
        var selDir = dir.current.state.value;
        var selectYear = year.current.state.value;

        if (selectYear != null) {
            var selYear = year.current.state.value.value;
            params['year'] = selYear;
        }
        if (rank != null) {
            params['rank'] = rank;
        }
        // 도로번호 (미선택 or '-' or '전체' 선택 시 빈공백("") 처리)
        if (no != null && no != "-" && no != "all") {
            params['no'] = no;
        } else params['no'] = "";
        // 도로명 (해당 도로명에 해당하는 tri_id를 value로 검색 / '전체' 선택 시 tri_id=0 으로 처리)
        if (name != null) {
            params['tri_id'] = name['value'];
        } else params['tri_id'] = "0";
        // 행선 (미선택 시, -1 로 처리)
        if (selDir != null) {
            params['dir'] = selDir['value']
        } else params['dir'] = "-1";

        if (selectYear) {
            setEquip1SelYear(year.current.state.value.value);
            if (rank == null) {
                alert("도로등급을 선택해 주시기 바랍니다.");
            } else {
                if (routeMasterCols) searchMaster(params);
            }

        } else {
            alert("연도를 선택해 주세요.");
        }
    }

    const handleSwitch = (e) => {
        var checkedSwitch = e.target.getAttribute('name');
        // document.getElementById('equip1Master').style.display = 'none';
        // document.getElementById('equip2Master').style.display = 'none';
        switch (checkedSwitch) {
            case 'tableViewSwitch':
                setTableViewSwitch(!tableViewSwitch);
                break;
            case 'equip1Switch':
                setEquip1Switch(!equip1Switch);
                break;
            case 'equip2Switch':
                setEquip2Switch(!equip2Switch);
                break;
        }
    }

    const searchMaster = async (params) => {
        var result = await server_connect(serviceUrl["pavement"]["searchSurveyMaster"], params);
        result = result['data']['res'];
        if (result.length > 0) {
            setRouteMaster({
                ...routeMaster,
                rowData: result
            });
        } else {
            setRouteMaster({
                ...routeMaster,
                rowData: [
                    { span: 'no-result', target_id: "검색 결과가 없습니다" }
                ],
            });
        }
    }

    const getRouteMasterGridApi = (childState) => {
        setRouteMasterGridApi(childState);
    }
    const getEquipGridApi = (childState) => {
        setEquipGridApi(childState);
    }
    const getEquip2GridApi = (childState) => {
        setEquip2GridApi(childState);
    }

    return (
        <>
            <ModalBody style={{ overflow: 'auto' }}>
                <div className="sm-filter-area">
                    <div className="sm-search-area">
                        <div className="sm-search-content">
                            <SearchDropdown target={year} options={yearList} placeholder="연도" width="200px"/>
                            <SearchDropdown options={typeList} placeholder="도로 등급" onChange={roadRankOnChangeEvt} width="230px"/>
                            <SearchDropdown options={nos} placeholder="노선 번호" onChange={roadNoOnChangeEvt} width="200px"/>
                            <SearchDropdown options={names} placeholder="노선명" width="230px"/>
                            <SearchDropdown target={dir} options={[{ label: '전체', value: "-1" }, { label: '상행', value: "0" }, { label: '하행', value: "1" }]} placeholder="행선" width="200px"/>
                            <Button size="sm" color="secondary" style={{ width: "100px" }} onClick={handleClick}>검색</Button>
                        </div>
                        <div className="sm-switch-area">
                            <SwitchButton label="1차장비" isOpen={equip1Switch} onChange={handleSwitch} name="equip1Switch" />
                            <SwitchButton label="2차장비" isOpen={equip2Switch} onChange={handleSwitch} name="equip2Switch" />
                            <SwitchButton label="테이블/뷰" isOpen={tableViewSwitch} onChange={handleSwitch} name="tableViewSwitch" />
                        </div>
                    </div>
                </div>
                <div className="sm-content-area">
                    <GridComponent id="routeMaster" columnDefs={routeMaster.columnDefs} rowData={routeMaster.rowData} onSelectionChanged={routeMaster.onSelectionChanged} style={routeMaster.style} passStateEvt={getRouteMasterGridApi} gridTitle={'노선마스터'} />
                    <div className={equipGridClass}>
                        <GridComponent id="equip1Master" columnDefs={equipGrid.columnDefs} rowData={equipGrid.rowData} onSelectionChanged={equipGrid.onSelectionChanged} style={equip1Master.style} passStateEvt={getEquipGridApi} gridTitle="1차장비" />
                        {/* <GridComponent id="equip1View" style={equip1View.style} gridTitle="1차장비 View"></GridComponent> */}
                        <div id="equip1View" style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
                            <div className="grid-title">1차장비 View</div>
                            <div style={{ overflow: 'auto', border: '1px solid #BDC3C7', padding:10}}>
                                {/* <TpmsView dataSet={equip1List}/> */}
                                {viewContent}
                            </div>
                        </div>
                        <div className="sm-bottom-content" id="bottom-content">
                            <span className={equip2Btn}>
                                {btnGroup}
                            </span>
                            <span className={equip1Btn}>
                                {btnEquip1Group}
                            </span>
                            <GridComponent id="equip2Master" columnDefs={equip2Grid.columnDefs} rowData={equip2Grid.rowData} onSelectionChanged={equip2Grid.onSelectionChanged} passStateEvt={getEquip2GridApi} style={equip2Master.style} gridTitle="2차장비" />
                            {/* <GridComponent id="equip2View" style={equip2View.style} gridTitle="2차장비 View"></GridComponent> */}
                            <div id="equip2View" style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
                                <div className="grid-title">2차장비 View</div>
                                <div style={{ overflow: 'auto', border:'1px solid darkgray', padding:5 }}>
                                    {view2Content}
                                    {/* <TpmsView gridTitle="2차장비 View"/> */}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <GridComponent id="equip1Master" columnDefs={equip1Master.columnDefs} rowData={equip1Master.rowData} onSelectionChanged={equip1Master.onSelectionChanged} style={equip1Master.style} passStateEvt={getRouteMasterGridApi} gridTitle={'1차장비'}/>
                    <GridComponent id="equip2Master" columnDefs={equip2Master.columnDefs} rowData={equip2Master.rowData} onSelectionChanged={equip2Master.onSelectionChanged} style={equip2Master.style} passStateEvt={getRouteMasterGridApi} gridTitle={'2차장비'}/> */}
                </div>
            </ModalBody>
            <ModalFooter style={{ justifyContent: "unset" }}>
                <span style={{ width: "100%", justifyContent: "flex-start", fontSize: "15px", fontWeight: "bold", margin: "5px", color: "#3eb5c1" }}>
                    <Label style={{ margin: "3px" }}>선택연장</Label>
                    <Input type="text" disabled style={{ display: "inline", width: "200px" }} value={totalExt} />Km
                </span>
                <span style={{ width: "100%", justifyContent: "flex-end", textAlign: "right" }}>
                    <Button className="feather icon-map"  color="primary" onClick={viewMap}> 지도확인</Button>
                    <Button color="secondary" onClick={passToggle}>닫기</Button>
                </span>
            </ModalFooter>
        </>
    );
}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({ type: actionTypes.OPEN_CHAT, data: props }),
        onCloseChat: () => dispatch({ type: actionTypes.CLOSE_CHAT })
    }
}

export default connect(chatStateToProps, chatDispatchToProps)(SurveyMaster);