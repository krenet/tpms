import React, {useState} from 'react';
import { Modal, ModalBody, ModalFooter, Button } from 'reactstrap';
import ManualComponent from '../../../../../../../../components/ManualComponent';

const Manual = ({toggle, isOpen}) =>{
    const element = document.getElementsByClassName('modal-dialog')[0];
    element.style.maxWidth="unset";
    element.style.width="1150px";

    const [subModal, setSubModal] = useState(isOpen);
    
    const passToggle = () =>{
        toggle();
    }

    return(
        // <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" style={ManualStyle} className="ManualStyle">
        <div>
            <ModalBody>
                <ManualComponent></ManualComponent>
            </ModalBody>
            <ModalFooter>
                <Button  color="secondary" onClick={passToggle}>닫기</Button>
            </ModalFooter>
     {/* </Modal> */}
        </div>
    )
}

export default Manual;