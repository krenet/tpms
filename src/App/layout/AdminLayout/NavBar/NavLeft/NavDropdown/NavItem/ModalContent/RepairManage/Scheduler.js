import React, {useState, useRef} from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, Button} from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import GridComponent from '../../../../../../../../components/GridComponent';
import {noList, nameList, typeList} from '../Common/routeInfoList.js';

const Scheduler = ({isOpen}) => {
    const [subModal, setSubModal] = useState(isOpen);
    const [nos, setNos] = useState([]);
    const [names, setNames] = useState([]);
    const [infoGrid, setInfoGrid] = useState({
        columnDefs: [
            {
                headerName : '노선정보',
                children : [
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                    {headerName:"구간", field:"section"}    
                ]
            },
            {
                headerName: "연장",
                children : [
                    {headerName:"개별", field:"length"},
                    {headerName:"누적", field:"lenSum"}
                ]
            },
            {headerName:"중용 분리", field:"overlap"},
            {
                headerName: "주소",
                children : [
                    {headerName:"시점", field:"sAddr"},
                    {headerName:"종점", field:"eAddr"}
                ]
            },
            {headerName:"차로수", field:"lanes"},
            {
                headerName: "이벤트",
                children : [
                    {headerName:"명칭", field:"eventName"},
                    {headerName:"종류", field:"eventType"}
                ]
            },
            {headerName:"포장종류", field:"paveType"},
            {
                headerName: "최초포장",
                children : [
                    {headerName:"연도", field:"makeYear"},
                    {headerName:"AC", field:"makeAC"},
                    {headerName:"BB", field:"makeBB"},
                    {headerName:"GB", field:"makeGB"},
                    {headerName:"SB", field:"makeSB"},
                    {headerName:"AFL", field:"makeAFL"}
                ]
            },
            {
                headerName: "최종유지보수",
                children : [
                    {headerName:"연도", field:"latestYear"},
                    {headerName:"공법", field:"latestMethod"},
                    {headerName:"설명", field:"latestDesc"},
                    {headerName:"비용", field:"latestPrice"}
                ]
            },
            {
                headerName: "교통량",
                children : [
                    {headerName:"연도", field:"trafficYear"},
                    {headerName:"지점", field:"trafficPoint"},
                    {headerName:"Total", field:"traffic"}
                ]
            }
        ],
        rowData : [
            {name: "", dir: "", lane: "", section: "", length: "", lenSum: "", overlap: "", sAddr: "", eAddr: "", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"", trafficPoint:"", traffic:""}
        ]
    });
    const [selectGrid, setSelectGrid] = useState({
        columnDefs: [
            {
                headerName : '노선정보',
                children : [
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                    {headerName:"구간", field:"section"}    
                ]
            },
            {
                headerName: "연장",
                children : [
                    {headerName:"개별", field:"length"},
                    {headerName:"누적", field:"lenSum"}
                ]
            },
            {headerName:"중용 분리", field:"overlap"},
            {
                headerName: "주소",
                children : [
                    {headerName:"시점", field:"sAddr"},
                    {headerName:"종점", field:"eAddr"}
                ]
            },
            {headerName:"차로수", field:"lanes"},
            {
                headerName: "이벤트",
                children : [
                    {headerName:"명칭", field:"eventName"},
                    {headerName:"종류", field:"eventType"}
                ]
            },
            {headerName:"포장종류", field:"paveType"},
            {
                headerName: "최초포장",
                children : [
                    {headerName:"연도", field:"makeYear"},
                    {headerName:"AC", field:"makeAC"},
                    {headerName:"BB", field:"makeBB"},
                    {headerName:"GB", field:"makeGB"},
                    {headerName:"SB", field:"makeSB"},
                    {headerName:"AFL", field:"makeAFL"}
                ]
            },
            {
                headerName: "최종유지보수",
                children : [
                    {headerName:"연도", field:"latestYear"},
                    {headerName:"공법", field:"latestMethod"},
                    {headerName:"설명", field:"latestDesc"},
                    {headerName:"비용", field:"latestPrice"}
                ]
            },
            {
                headerName: "교통량",
                children : [
                    {headerName:"연도", field:"trafficYear"},
                    {headerName:"지점", field:"trafficPoint"},
                    {headerName:"Total", field:"traffic"}
                ]
            }
        ],
        rowData : [
            {name: "", dir: "", lane: "", section: "", length: "", lenSum: "", overlap: "", sAddr: "", eAddr: "", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"", trafficPoint:"", traffic:""}
        ]
    });
    const toggle = () => setSubModal(!subModal);

    const type = useRef();
    const no = useRef();
    const name = useRef();
    const dir = useRef();
    const lane = useRef();

    const typeChange = (obj) => {
        setNos(noList[obj.value]);
    };

    const noChange = (obj) => {
        setNames(nameList[type.current.state.value.value+obj.value]);
    }

    const handleBtnClick = () => {
        
        var startDate = document.getElementById("startDate").value;
        var endDate = document.getElementById("endDate").value;
        if(startDate && endDate){
            setInfoGrid({
                ...infoGrid,
                rowData : [
                    { name: "마산로", dir: "상행", lane: "1", section: "2", length: "342", lenSum: "682", overlap: "1", sAddr: "마산로 2구간 시점주소", eAddr: "마산로 2구간 종점주소", lanes: "2", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
                    { name: "황악로", dir: "상행", lane: "1", section: "2", length: "387", lenSum: "727", overlap: "0", sAddr: "황악로 2구간 시점주소", eAddr: "황악로 2구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
                    { name: "부항로", dir: "상행", lane: "1", section: "2", length: "335", lenSum: "1,215", overlap: "3", sAddr: "부항로 2구간 시점주소", eAddr: "부항로 2구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
                    { name: "부항로", dir: "상행", lane: "1", section: "3", length: "420", lenSum: "1,215", overlap: "0", sAddr: "부항로 3구간 시점주소", eAddr: "부항로 3구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
                ]
            });
            setSelectGrid({
                ...selectGrid,
                rowData : [
                    { name: "마산로", dir: "상행", lane: "1", section: "1", length: "340", lenSum: "682", overlap: "0", sAddr: "마산로 1구간 시점주소", eAddr: "마산로 1구간 종점주소", lanes: "2", eventName: "지하차도(북측)4", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
                    { name: "황악로", dir: "상행", lane: "1", section: "1", length: "340", lenSum: "727", overlap: "22", sAddr: "황악로 1구간 시점주소", eAddr: "황악로 1구간 종점주소", lanes: "2", eventName: "일반국도1호선", eventType: "NR48", paveType: "아스팔트", makeYear: "1989", makeAC: "7", makeBB: "", makeGB: "15", makeSB: "40", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"2018", trafficPoint:"0140-001", traffic:"36,407"},
                    { name: "부항로", dir: "상행", lane: "1", section: "1", length: "440", lenSum: "1,215", overlap: "0", sAddr: "부항로 1구간 시점주소", eAddr: "부항로 1구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
                ]
            });
        }else{
            alert('시작/종료일을 선택해주세요.');
            return;
        }
        
    }

    const saveClick = () => {
        alert('저장 완료');
    }

    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" className="window-full">{/*onClosed={closeAll ? toggle : undefined}*/}
                <ModalHeader toggle={toggle}>보수 스케줄러</ModalHeader>
                <ModalBody>
                    <FormGroup style={{display:"inline", alignItems:"center"}}>
                        <Label for="startDate" style={{marginRight:"5px"}}>
                            시작일
                        </Label>
                        <Input type="date" name="startDate" id="startDate" style={{width:'unset', display:"inline"}} />
                    </FormGroup>
                    <FormGroup style={{display:"inline", alignItems:"center"}}>
                        <Label for="endDate" style={{marginLeft:"5px", marginRight:"5px"}}>
                            종료일
                        </Label>
                        <Input type="date" name="endDate" id="endDate" style={{width:'unset', display:"inline"}}/>
                    </FormGroup>
                    <Button color="secondary" size="sm" style={{marginLeft:"5px"}} onClick={handleBtnClick}>보수대상구간</Button>
                    <br/>
                    <div style={{marginTop:"5px", marginLeft:"40px"}}>
                        <SearchDropdown target={type} options={typeList} placeholder="등급" onChange={typeChange}/>
                        <SearchDropdown target={no} options={nos} placeholder="번호" onChange={noChange}/>
                        <SearchDropdown target={name} options={names} placeholder="노선명"/>
                        <SearchDropdown target={dir} options={[{label: '상행', value:"up"},{label:'하행', value:"down"}]} placeholder="행선"/>
                        <SearchDropdown target={lane} options={[{label: '1', value:"1"},{label:'2', value:"2"}]} placeholder="차선"/>{' '}
                        <Button color="info" size="sm" >상세검색</Button>
                    </div>
                    <div>
                        <Label style={{fontWeight:"bold"}}>노선정보</Label>
                        <GridComponent columnDefs={infoGrid.columnDefs} rowData={infoGrid.rowData} style={{width:"100%", marginTop: "5px", height:"30vh", display:"inline-block"}} />
                    </div>
                    <br/>
                    <div style={{display:"flex", justifyContent:"center"}} >
                        <Button outline color="secondary" style={{borderRadius:"50%"}}>▲</Button>
                        <Button outline color="secondary" style={{borderRadius:"50%"}}>▼</Button>
                    </div>
                    <div>
                        <Label style={{fontWeight:"bold"}}>보수대상구간선정</Label>
                        <GridComponent columnDefs={selectGrid.columnDefs} rowData={selectGrid.rowData} style={{width:"100%", marginTop: "5px", height:"30vh", display:"inline-block"}} />
                    </div>
                </ModalBody>
                <ModalFooter style={{justifyContent:"unset"}}>
                    <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                        <Button color="primary"  onClick={saveClick}>저장</Button>
                        <Button color="secondary"  onClick={toggle}>닫기</Button>
                    </span>
                </ModalFooter>
        </Modal>
    );
}

export default Scheduler;