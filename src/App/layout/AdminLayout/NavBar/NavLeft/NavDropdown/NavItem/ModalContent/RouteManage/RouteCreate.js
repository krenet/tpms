import React, {useState, useRef, useEffect} from 'react';
import { Modal, ModalHeader, ModalBody, InputGroup, Input, InputGroupAddon, ModalFooter, FormGroup, Button, Form } from 'reactstrap';
import { Tabs, Tab } from 'react-bootstrap';
import InputComponent from '../../../../../../../../components/InputComponent';
import RoadXML from '../../../../../../../../../resource/settingXml/road-info.xml';
import {XmlParser, server_connect, server_connect_withfile} from '../../../../../../../../../tpms-common.js';
import fileFormat from '../../../../../../../../../resource/fileFormat/신규노선정보등록_양식.xlsx';
import serviceUrl from  '../../../../../../../../../request-url-list';

const RouteCreate = ({isOpen, onClose}) => {
    
    const [subModal, setSubModal] = useState(isOpen);
    const [filename, setFilename] = useState("");
    const [file, setFile] = useState(null);
    const [metaData, setMetaData] = useState();
    const [columns, setColumns] = useState([]);
    const [tabKey, setTabKey] = useState("all");
    const [nodeList, setNodeList] = useState();
    const [optionList, setOptionList] = useState();
    const toggle = () => setSubModal(!subModal);
    
    const _file = useRef();
    
    const allFile = (e) => {
        e.preventDefault();
        _file.current.click();
    };
    const fileChange = (e) => {
        setFilename(e.target.files[0].name);
        setFile(e.target.files[0]);
    }

    const textChange = (e) => {};

    const submitForm = (evt) => {
        evt.preventDefault();
        
        if(tabKey=='all'){
            const data = new FormData();
            data.append('file', file);
            var _columns = columns.map((obj)=>{
                if(obj['attributes']['visible']=='true') return obj['name'];
            });

            _columns = _columns.filter(function (el) {
                return el != null;
            });
            data.append('columns', _columns);

            var _types = columns.map((obj)=>{
                if(obj['attributes']['visible']=='true') return obj['attributes']['data-type'];
            });
            _types = _types.filter(function (el) {
                return el != null;
            });
            data.append('types', _types);

            listInsertNewRoad(data);
        }else{
            const data = new FormData(evt.target);
            eachInsertNewRoad(data);
        }
        
    }
    
    // 신규 노선 리스트 등록
    const listInsertNewRoad = async (data) => {
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        const result = await server_connect_withfile(serviceUrl["pavement"]["insertRoadInfoList"], data, config);
        if(result && result['status']==200){
            if(result['data']['res']==1) alert('노선 리스트 등록 완료');
            else if(result['data']['res']==0){
                alert('엑셀파일의 형식이 아니거나 손상된 파일입니다.');
            }
            else {
                var str = '등록 되어 있는 노선을 제외한 신규 노선 추가 완료\n\n';
                str += '[등록 되어 있는 노선]\n';
                var resList = result['data']['res'];
                resList.map((obj)=> {
                    str += '노선등급 : '+obj['rank']+' / 노선번호 : '+obj['no']+' / 노선명 : '+obj['name']+'\n';
                });
                alert(str);
            }
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    }

    // 신규 노선 개별 등록
    const eachInsertNewRoad = async (data) => {
        var params = new Object();
        columns.map((obj)=>{
            if(typeof obj['attributes']['json']=='undefined') params[obj['name']] = data.get(obj['name']);
        });
        var jsonClassObj = document.getElementsByClassName('json');
        var jsonObj = new Object();
        for(var i=0;i<jsonClassObj.length;++i){
            var key = jsonClassObj[i].getAttribute('name');
            var value = jsonClassObj[i].value;
            jsonObj[key] = value;
        }
        params['etc'] = encodeURIComponent(JSON.stringify(jsonObj));

        // insert road info
        const result = await server_connect(serviceUrl["pavement"]["insertRoadInfo"], params);
        if(result && result['status']==200) {
            if(result['data']['res']==1) alert('노선 추가 완료');
            else alert('등록되어 있는 노선입니다. 데이터를 확인 후 다시 입력해주세요.');
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    }

    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const nlist = await server_connect(serviceUrl["pavement"]["selectNodeList"]);
            if(!ignore) {
                if(nlist && nlist['status']==200) setNodeList(nlist['data']['res']);
                // else alert('서버 통신 실패. 관리자에게 문의하세요.');
            }
            const result = await XmlParser(RoadXML);
            if(!ignore) { setMetaData(result);  }
        }
        fetchData();
        return () => { ignore = true; }
    }, []);
    
    useEffect(()=>{
        if(metaData){
            setColumns(metaData.getElementsByTagName('Columns')[0]['children']);
        }
    }, [metaData]);


    const downloadFileFormat = () => {
        var windowOpen = window.open(fileFormat, 'Download');
        windowOpen.title="신규 노선 정보 등록 양식 다운로드";
    }

    const makeSelectInput = () => {
        if(nodeList){
            let options = nodeList.map((obj)=>{
                return <option value={obj["node_id"]}>{obj["node_name"]}</option>;
            });
            return options;
        }
    }

    const makeRoadRankOptions = () => {
        const roadRanks = [{label: '고속국도', value:"101"}, 
                   {label: '도시고속국도', value:"102"},
                   {label: '일반국도', value:"103"},
                   {label: '특별·광역시도', value:"104"},
                   {label: '국가지원 지방도', value:"105"},
                   {label: '지방도', value:"106"},
                   {label: '시·군도', value:"107"},
                   {label: '기타', value:"108"}
                  ];
        let options = roadRanks.map((obj)=>{
            return <option value={obj["value"]}>{obj["label"]}</option>;
        });
        return options;
    }

    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" onClosed={onClose}>{/*onClosed={closeAll ? toggle : undefined}*/}
            <Form onSubmit={submitForm} >
                <ModalHeader toggle={toggle}>등록 및 수정(신규 등록)</ModalHeader>
                <ModalBody>
                    <Tabs variant="pills" defaultActiveKey="all" className="mb-3" onSelect={key=>setTabKey(key)}>
                        <Tab eventKey="all" title="일괄등록">
                            <p style={{fontWeight:"bold", display:"inline"}}>신규노선파일리스트</p>{' '}
                            <Button onClick={downloadFileFormat} style={{fontSize:"10px", padding:"3px 7px", float:"right", marginRight:"0"}} outline color="secondary" size="sm">
                                양식다운로드
                            </Button>
                            <InputGroup>
                                <Input value={filename} placeholder="파일을 선택해주세요." onChange={textChange}/>
                                <InputGroupAddon addonType="append">
                                <input type="file" ref={_file} style={{display:"none"}} onChange={fileChange} accept=".xlsx, .csv" name="file"/>
                                <Button color="primary" onClick={allFile}>File</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </Tab>
                        <Tab eventKey="each" title="개별등록">
                            <FormGroup check row>
                            {
                                columns.map((obj, inx)=>{
                                    var attributes = obj['attributes'];
                                    return attributes['visible']=='true'?
                                            // <InputComponent key={inx} label={attributes['title']} name={obj['name']} type={attributes['type']} json={attributes['json']}  settingDataEvt={makeSelectInput}/>
                                            <InputComponent key={inx} label={attributes['title']} name={obj['name']} type={attributes['type']} json={attributes['json']}  settingDataEvt={eval(attributes['options'])}/>
                                            :
                                            <input type="hidden" key={inx} name={obj['name']} value="0"/>
                                })
                            }
                            </FormGroup>
                        </Tab>
                    </Tabs>
                </ModalBody>
                <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                    <Button type="submit" style={{flex:1}} color="primary" size="sm" >등록</Button> 
                    <Button style={{flex:1}} color="secondary" size="sm"  onClick={toggle} >닫기</Button>
                </ModalFooter>
            </Form>
        </Modal>
    );
}

export default RouteCreate;