import React, {useState, useRef, useEffect} from 'react';
import { Button, ModalBody, InputGroup, Input, InputGroupAddon, ModalFooter, Form} from 'reactstrap';
import { Tabs, Tab } from 'react-bootstrap';
import PesTableXML from '../../../../../../../../../resource/settingXml/road-pes-table.xml';
import PesExcelXML from '../../../../../../../../../resource/settingXml/road-pes-excel.xml';
import { server_connect_withfile, XmlParser } from '../../../../../../../../../tpms-common.js';
import ProgressBarComponent from '../../../../../../../../components/ProgressBarComponent';
import serviceUrl from  '../../../../../../../../../request-url-list';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";

const EquipmentSurvey = ({toggle, changeSocketFlag}) => {
    const [excelFolderName, setExcelFolderName] = useState("");
    const [excelFiles, setExcelFiles] = useState("");
    const [imgfoldername, setImgfoldername] = useState("");
    const [imgFiles, setImgFiles] = useState("");
    const [pesTableColumns, setPesTableColumns] = useState();
    const [pesExcelColumns, setPesExcelColumns] = useState();
    const _excelFolder = useRef();
    const _imgFolder = useRef();
    const [progress, setProgress] = useState({
        open : false,
        title : "데이터 입력 진행상황",
        totalCnt : null,
        completeCnt : null
    });

    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const result = await XmlParser(PesTableXML);
            const e_result = await XmlParser(PesExcelXML);
            if(!ignore) { 
                setPesTableColumns(result.getElementsByTagName('Columns')[0]['children']);  
                setPesExcelColumns(e_result.getElementsByTagName('Columns')[0]['children']);  
            }
        }
        fetchData();
        return () => { ignore = true; }
    }, []);

    const excelFolder = (e) => {   //1차 엑셀 폴더 선택 버튼 클릭시 호출되는 함수
        e.preventDefault();
        _excelFolder.current.click();
    };

    const imgFolder = (e) => {   //1차 사진 폴더 선택 버튼 클릭시 호출되는 함수
        e.preventDefault();
        _imgFolder.current.click();
    };


    const excelfolderChange = (e) => {
        var _excelFolderName = e.target.files[0].webkitRelativePath.split("/");
        setExcelFolderName(_excelFolderName[0]); //input에 filename
        setExcelFiles(e.target.files); //폴더 내부 파일 저장 -- 사용
    }

    const imgfolderChange = (e) => {
        var imgFolderName = e.target.files[0].webkitRelativePath.split("/");
        setImgfoldername(imgFolderName[0]); //폴더 이름 저장
        setImgFiles(e.target.files); //폴더 내부 파일 저장
    }

    const passToggle = () =>{
        toggle();
    }

    const submitForm = (evt) => {
        evt.preventDefault();
        insertEquipment1Data();
    }

    const insertEquipment1Data = async () => {
        setProgress({
            open : true,
            title : "PES 데이터 입력 진행 상황",
            totalCnt : excelFiles.length+imgFiles.length,
            completeCnt : 0
        });

        const config = {
            header : {
                'content-type': 'multipart/form-data'
            }
        };
        var data = new FormData();
        
        var pesCols = pesTableColumns.map(obj=>{
            return obj['name'];
        });
        var pesTypes = pesTableColumns.map(obj=>{
            return obj['attributes']['data-type'];
        });
        var e_pesCols = pesExcelColumns.map(obj=>{
            return obj['name'];
        });
        var e_pesTypes = pesExcelColumns.map(obj=>{
            return obj['attributes']['data-type'];
        });

        data.append('table_columns', pesCols);
        data.append('table_types', pesTypes);
        data.append('excel_columns', e_pesCols);
        data.append('excel_types', e_pesTypes);
        
        var failList = new Array();
        var _complete = 0;
        for (const efile of excelFiles) {
            data.append("excelFile", efile);
            const result = await server_connect_withfile(serviceUrl["pavement"]["insertEquipment1Data"], data, config);
            
            if(result['status']==200){
                if(result['data']['res']['res']==1){
                    setProgress({
                        open : true,
                        title : "PES 데이터 입력 진행 상황",
                        totalCnt : excelFiles.length+imgFiles.length,
                        completeCnt : ++_complete
                    });
                    data.delete("excelFile");
    
                }else{
                    failList.push(result['data']['res']['filename']);
                }
            }else{
                alert("서버 통신 실패");
                return;
            }
        }
        
        var imgList = Array.from(imgFiles);
        for(const file of imgList){
            data = new FormData();
            data.append('file', file);
            const result = await server_connect_withfile(serviceUrl["pavement"]["buildFileData"], data, config);
            
            if(result['status']==200){
                if(result['data']['res']=1){
                    setProgress({
                        open : true,
                        title : "PES 데이터 입력 진행 상황",
                        totalCnt : excelFiles.length+imgFiles.length,
                        completeCnt : ++_complete
                    });
        
                    data.delete('file');
                }
                else{
                    alert('파일 데이터 입력 중 오류 발생');
                    return;
                }
            }else{
                alert("서버 통신 실패");
                return;
            }
        }

        if(failList.length>0){
            var megStr = 'PES Data 입력 실패 : ';
            failList.map((filename, inx)=>{
                megStr += "(" + (inx+1) + ")";
                megStr += filename + "  ";
            });
            alert(megStr);
        }
        // document.getElementsByClassName('modal-content')[0].style.pointerEvents = "auto";
        // document.documentElement.style.cursor = "unset";
    }

    const getNowPercentage = (percent) => {
        if(percent==100) {
            setProgress({
                open : false,
                title : "PES 데이터 입력 진행 상황",
                totalCnt : 0,
                completeCnt : 0
            });
            changeSocketFlag(true);
            alert("PES 데이터 입력 완료");
        }
    }

    const progressModal = (pgModal) => {
        setProgress({
            ...progress,
            open : pgModal
        });
    }

    return (
        <Form onSubmit={submitForm}>
            <ModalBody>
                <p style={{fontWeight:"bold"}}>엑셀 폴더 선택</p>
                <InputGroup>
                    <Input value={excelFolderName} placeholder="파일을 선택해주세요." readOnly/>
                    <InputGroupAddon addonType="append">
                    <input webkitdirectory="" directory="" type="file" ref={_excelFolder} style={{display:"none"}} onChange={excelfolderChange} accept=".xlsx, .csv"/>
                    <Button color="primary" onClick={excelFolder}>Folder</Button>
                    </InputGroupAddon>
                </InputGroup>
                <hr/>
                <p style={{fontWeight:"bold"}}>사진 폴더 선택</p>
                <InputGroup>
                    <Input value={imgfoldername} placeholder="파일을 선택해주세요." readOnly/>
                        <InputGroupAddon addonType="append">
                            <input webkitdirectory="" directory="" type="file" ref={_imgFolder} style={{display:"none"}} onChange={imgfolderChange} />
                            <Button color="primary"  onClick={imgFolder}>Folder</Button>
                        </InputGroupAddon>
                </InputGroup>
                
                <ProgressBarComponent open={progress.open} title={progress.title} totalCnt={progress.totalCnt} completeCnt={progress.completeCnt} passPercent={getNowPercentage} modalEvt={progressModal}/>
            </ModalBody>
            <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                <Button style={{flex:1}} color="primary" size="sm" type="submit">저장</Button>
                <Button style={{flex:1}} color="secondary" size="sm" onClick={passToggle}>닫기</Button>
            </ModalFooter>
        </Form>
    );
}

// const stateToProps = state => {
//     return {
//         socketFlag: state.socketFlag,
//     }
// }

const dispatchToProps  = dispatch => {
    return {
        changeSocketFlag: (props) => dispatch({ type: actionTypes.CONNECT_WEB_SOCKET, flag: props }),
    }
}

export default connect(null, dispatchToProps)(EquipmentSurvey);
// export default EquipmentSurvey;