import React from 'react';
import PropertyGrid from 'react-property-grid';
import { Button, ModalBody, ModalFooter } from 'reactstrap';

const ConfigSetting = ({toggle}) => {

    const configSave = () => {
        alert('저장 완료');
    }
    const passToggle = () =>{
        toggle();
    }
    const schema = {
        "$schema" : "http://json-schema.org/draft-04/schema#",
        "properties" : {
            "root":{
                "title" : "루트폴더",
                "description" : "",
                "type": "object",
                "properties" :{
                    "fileRoot" : {"type": "string", "title": "베이스경로"}
                }
            },
            "route":{
                "title" : "도로",
                "description" : "",
                "type": "object",
                "properties" :{
                    "pavementImagePath" : {"type": "string", "title": "포장 이미지"}
                }
            },
            "bridge":{
                "title" : "교량",
                "description" : "",
                "type": "object",
                "properties" :{
                    "bridgeImagePath" : {"type": "string", "title": "교량 이미지"},
                    "bridgeData" : {"type": "string", "title": "교량 자료"},
                    "bridgeManager" : {"type": "string", "title": "교량 관리대장"},
                    "bridgeDrawing" : {"type": "string", "title": "교량 설계도면"},
                    "bridgePictureBook" : {"type": "string", "title": "교량 사진대지"},
                    "bridgeStatusEvaluationBook" : {"type": "string", "title": "교량 상태평가"},
                    "bridgeReport" : {"type": "string", "title": "교량 보고서"},
                    "bridgeRepair" : {"type": "string", "title": "교량 순공사비"},
                }
            },
            "slope":{
                "title" : "비탈면",
                "description" : "",
                "type": "object",
                "properties" :{
                    "slopeImagePath" : {"type": "string", "title": "비탈면 이미지"},
                    "slopeData" : {"type": "string", "title": "비탈면 자료"},
                    "slopeReport" : {"type": "string", "title": "비탈면 보고서"},
                    "SlopeEvalData" : {"type": "string", "title": "비탈면 상태평가"},
                }
            },
            "etc":{
                "title" : "기타",
                "description" : "",
                "type": "object",
                "properties" :{
                    "spaceMemoImg" : {"type": "string", "title": "공간 메모"}
                }
            }
        }
    };
 
    const data = {
        "root" :{
            "fileRoot" : "Y:/",
        },
        "route" :{
            "pavementImagePath" : "Y:/도로이미지/"
        },
        "bridge" : {
            "bridgeImagePath" : "Y:/교량이미지/",
            "bridgeData" : "Y:/교량데이터/",
            "bridgeManager" : "Y:/교량관리/",
            "bridgeDrawing" : "Y:/교량설계/",
            "bridgePictureBook" : "Y:/교량사진대지/",
            "bridgeStatusEvaluationBook" : "Y:/교량~/",
            "bridgeReport" : "Y:/교량~/",
            "bridgeRepair" : "Y:/교량~/",
        },
        "slope" :{
            "slopeImagePath" : "Y:/비탈면이미지/",
            "slopeData" : "Y:/비탈면~/",
            "slopeReport" : "Y:/비탈면~/",
            "SlopeEvalData" : "Y:/비탈면~/",
        },
        "etc":{
            "spaceMemoImg" : "Y:/공간메모~/"
        }
    }
   
    return (
        <>
            <ModalBody>
                <PropertyGrid title="시스템 환경설정" schema={schema} data={data}/>
                <br/>
            </ModalBody>
            <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                <Button style={{flex:1}} color="info" size="sm"  onClick={configSave}>저장</Button>
                <Button style={{flex:1}} color="secondary" size="sm"  onClick={passToggle}>닫기</Button>
            </ModalFooter>
        </>
    );
}

export default ConfigSetting;