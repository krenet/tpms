import React, { useState, useEffect, useRef } from 'react';
import { ModalBody, ModalFooter, Label, Input, Button } from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import GridComponent from '../../../../../../../../components/GridComponent';
import SwitchButton from '../../../../../../../../components/SwitchButton';
import {typeList} from '../Common/routeInfoList.js';
import MasterColXML from '../../../../../../../../../resource/settingXml/master.xml';
import Equip1XML from '../../../../../../../../../resource/settingXml/equip1.xml';
import DetailXML from '../../../../../../../../../resource/settingXml/detail.xml';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import '../../../../../../../../components/GridComponent.css';
import './Detail.css';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import TpmsView from '../../../../../../../../components/TpmsView';
import serviceUrl from '../../../../../../../../../request-url-list';

const Detail = ({toggle, onOpenChat, onCloseChat}) => {
    const [planYear, setPlanYear] = useState([]);
    const [names, setNames] = useState([]);
    const [nos, setNos] = useState([]);
    const [masterSwitch, setMasterSwitch] = useState(false);
    const [equipSwitch, setEquipSwitch] = useState(false);
    const [recordSwitch, setRecordSwitch] = useState(false);
    const [tableViewSwitch, setTableViewSwitch] = useState(false);
    const [totalExt, setTotalExt] = useState('0');
    const [filterValues, setFilterValues] = useState(null);
    const [selectedGridType, setSelectedGridType] = useState(null);
    const [contentStyle, setContentStyle] = useState({marginTop: "5px", height:"72vh", flex:1, flexDirection: "column"});
    // 마스터 그리드
    const [mMetaData, setMMetaData] = useState();
    const [mColumns, setMColumns] = useState();
    const [masterGrid, setMasterGrid] = useState({});
    const [masterGridApi, setMasterGridApi] = useState(null);
    const [masterList, setMasterList] = useState(null);  // 마스터 전체 리스트
    const [searchMasterList, setSearchMasterList] = useState(null);  // 마스터 검색 후 리스트 (처음엔 전체로 세팅)
    // 1차장비 그리드
    const [eMetaData, setEMetaData] = useState();
    const [eColumns, setEColumns] = useState();
    const [equipGrid, setEquipGrid] = useState({});
    const [equipGridApi, setEquipGridApi] = useState(null);
    // const [equipRowData, setEquipRowData] = useState(null);
    // 상세조사구간 그리드
    const [dMetaData, setDMetaData] = useState();
    const [dColumns, setDColumns] = useState();
    const [detailGrid, setDetailGrid] = useState({});
    const [detailGridApi, setDetailGridApi] = useState(null);
    const [detailRowData, setDetailRowData] = useState();
    const [tempDetailList, setTempDetailList] = useState(null);  // 상세조사구간 추가 & 삭제에 사용하는 list
    const [beforeAdd, setBeforeAdd] = useState(null);  // 상세조사구간 그리드에 있는 데이터 임시 저장
    // 이력 그리드
    const [recordGrid, setRecordGrid] = useState({});
    const [recordRowData, setRecordRowData] = useState(null);
    const [recordGridApi, setRecordGridApi] = useState(null);
    // 뷰
    // const [viewStyle, setViewStyle] = useState({width:"100%", height:"70vh", display:"inline-grid",textAlign: "center"});
    const [viewDatas, setViewDatas] = useState([]);
    const [viewContent, setViewContent] = useState("");
    const [viewTargetList, setViewTargetList] = useState();
    const viewHeader = {
        detail : [ { label : 'year', name  : '연도' }, { label : 'target_id', name  : '구간ID'},
                    { label : 'detail_target_id', name  : '상세조사구간ID' }, { label : 'spos', name  : '시점' },
                    { label : 'epos', name  : '종점' },  { label : 'rank', name  : '등급' },
                    { label : 'no',  name  : '노선번호' }, { label : 'name', name  : '노선명'},
                    { label : 'rev_dir',  name  : '행선' } ],
        record : [ { label : 'year', name  : '연도' }, { label : 'target_id', name  : '구간ID' },
                    { label : 'detail_target_id', name  : '상세조사구간ID' }, { label : 'spos', name  : '시점' },
                    { label : 'epos', name  : '종점' }, { label : 'rank', name  : '등급' },
                    { label : 'no', name  : '노선번호' }, { label : 'name', name  : '노선명' },
                    { label : 'rev_dir', name  : '행선' }]
                };
    const recordYear = useRef();
    const roadType = useRef();
    const roadNo = useRef();
    const roadName = useRef();
    const roadDir = useRef();

    const passToggle = () => {
        onCloseChat();
        toggle();
    }
   
    useEffect(()=>{
        if(masterList && detailRowData){
            masterList.map((mObj)=>{
                var rank = mObj['rank'];
                switch (rank) {
                    case '101':
                        mObj['rank'] = '고속국도';
                        return;
                    case '102':
                        mObj['rank'] = '도시고속국도';
                        return;
                    case '103':
                        mObj['rank'] = '일반국도';
                        return;
                    case '104':
                        mObj['rank'] = '특별·광역시도';
                        return;
                    case '105':
                        mObj['rank'] = '국가지원 지방도';
                        return;
                    case '106':
                        mObj['rank'] = '지방도';
                        return;
                    case '107':
                        mObj['rank'] = '시·군도';
                        return;
                    case '108':
                        mObj['rank'] = '기타';
                        return;
                }
            });

            var viewGroup = new Object();

            masterList.map((row)=>{
                var key = ""+row['tri_id']+row['main_num']+row['sub_num']+row['dir']+1;
                var viewList = new Array();
                if(viewGroup.hasOwnProperty(key)){
                    viewList = viewGroup[key];
                }
                viewList.push(row);
                viewGroup[key] = viewList;
            });

            var viewList = new Array();
            var keyList = Object.keys(viewGroup);
            var viewrank; var viewno; var viewname;
            for(var i=0;i<keyList.length;++i){
                viewGroup[keyList[i]].map((obj, inx)=>{
                    if(inx == 0){
                        viewrank = obj['rank'];
                        viewno = obj['no'];
                        viewname = obj['name'];
                    }
                    
                    detailRowData.map((tObj)=>{
                        if(tObj['ts_id']==obj['ts_id']) obj['detail_target_id'] = tObj['detail_target_id'];
                    });
                });
                var addInfo = {
                    info : {
                        rank : viewrank,
                        no : viewno,
                        name : viewname,
                        targetCol : 'detail_target_id',
                        extCol : 'sm_ext'
                    },
                    children : viewGroup[keyList[i]]
                };
                viewList.push(addInfo);
            }
            setViewDatas(viewList);
        }
    }, [masterList, detailRowData]);

    useEffect(()=>{
        if(viewDatas.length>0){
            setViewContent(<TpmsView dataSet={viewDatas}/>);
        }
    }, [viewDatas])

    useEffect(() => {
        onCloseChat();
        let ignore = false;
        async function fetchData(){
            const result_master = await XmlParser(MasterColXML);
            const result_equip = await XmlParser(Equip1XML);
            const result_detail = await XmlParser(DetailXML);
            if(!ignore){
                setMMetaData(result_master);
                setEMetaData(result_equip);
                setDMetaData(result_detail);
            }
        }
        fetchData();
        return () => {
            ignore = true; 
        }
    }, []);

    useEffect(() => {
        if(mMetaData) setMColumns(mMetaData.getElementsByTagName('Columns')[0]['children']);    
    }, [mMetaData]);

    useEffect(() => {
        if(eMetaData) setEColumns(eMetaData.getElementsByTagName('Columns')[0]['children']);    
    }, [eMetaData]);

    useEffect(() => {
        if(dMetaData) setDColumns(dMetaData.getElementsByTagName('Columns')[0]['children']);    
    }, [dMetaData]);

    // 마스터 그리드 초기 세팅
    useEffect(() => {
        if(mColumns) {
            var header = tHeader(mColumns);
            setMasterGrid({
                columnDefs : header,
                rowData: null,
                style: {
                    width:"50%", height:"55vh", display:"none", padding:"3px", textAlign:'center'
                },
            });
            async function allMasterList(){
                var result = await server_connect(serviceUrl["pavement"]["getAllMaster"]);
                if(result && result['status']==200){
                    result = result['data']['res'];
                    if(result.length>0){
                        console.log("result: ", result);
                        setMasterList(result);
                        setSearchMasterList(result);
                        setMasterGrid({
                            columnDefs : header,
                            rowData: null,
                            style: {
                                width:"50%", height:"55vh", display:"none", padding:"3px", textAlign:'center'
                            },
                            onSelectionChanged: function(e) {
                                var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length==0) return;
                                setSelectedGridType('master');
                                var extSum = 0;
                                selectedRows.map((row)=>{
                                    var selectedRowExt = row['data']['sm_ext'];
                                    extSum = extSum + selectedRowExt;
                                });
                                setTotalExt((extSum/1000).toFixed(2));
                            },
                            rowClassRules: null
                            // rowClassRules: {
                            //     'included-row-in-target' : function (params) {
                            //         var ts_id = params.data.ts_id;
                            //         var chk = false;
                            //         for(var i=0; i<detailRowData.length; ++i){
                            //             var s_ts_id = detailRowData[i]['s_ts_id'];
                            //             var e_ts_id = detailRowData[i]['e_ts_id'];
                            //             if(ts_id>=s_ts_id && ts_id<=e_ts_id){
                            //                 chk = true;
                            //             }
                            //         }
                            //         return chk;
                            //     }
                            // },
                        });
                    }
                }else{
                    alert('서버 통신 실패. 관리자에게 문의하세요.');
                }
            }
            allMasterList();
        }
    }, [mColumns]);

    // 1차 장비조사 그리드 초기 세팅
    useEffect(() => {
        if(eColumns) {
            var header = tHeader(eColumns);
            setEquipGrid({
                columnDefs : header,
                rowData: null,
                style: {
                    width:"100%", height:"23vh", display:"none", padding:"3px"
                }
            });
            async function allEquipList(){
                var result = await server_connect(serviceUrl["pavement"]["getCurrYearEquip"]);
                if(result&&result['status']==200){
                    result = result['data']['res'];
                    if(result.length>0){
                        // setEquipRowData(result);
                        setEquipGrid({
                            columnDefs : header,
                            rowSelection: 'multiple',
                            rowData: result,
                            style: {
                                width:"100%", height:"25vh", display:"none", padding:"3px", textAlign:'center'
                            },
                            onSelectionChanged: function(e) {
                                var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length==0) return;
                                setSelectedGridType('equip');
                                // setSelectRowData(selectedRows);
                                var extSum = 0;
                                selectedRows.map((row)=>{
                                    var selectedRowExt = row['data']['extension'];
                                    extSum = extSum + selectedRowExt;
                                });
                                setTotalExt((extSum/1000).toFixed(2));
                            }
                        });
                    }else{
                        setEquipGrid({
                            columnDefs : header,
                            rowSelection: 'multiple',
                            rowData: null,
                            style: {
                                width:"100%", height:"23vh", display:"none", padding:"3px"
                            }
                        });
                    }
                }
            }
            allEquipList();
        }
    }, [eColumns]);

    // 상세조사 그리드 & 이력 그리드 초기 세팅
    useEffect(() => {
        if(dColumns) {
            var header = tHeader(dColumns);
            setDetailGrid({
                columnDefs : header,
                rowData: null,
                style: {
                    width:"100%", height:"70vh", display:"inline-block"
                },
            });
            async function currentDetailList(){
                let today = new Date();
                let currYear = today.getFullYear();
                var params = new Object();
                params['year'] = currYear;
                var result = await server_connect(serviceUrl["pavement"]["getCurrYearDetail"], params);
                if(result&&result['status']==200){
                    result = result['data']['res'];
                    if(result.length>0){
                        var detailList = result.map((obj) => {
                            return {
                                id: obj['id'], s_t10m_id: obj['s_t10m_id'], e_t10m_id: obj['e_t10m_id'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'],
                                year: obj['year'], target_id: obj['target_id'], detail_target_id: obj['detail_target_id'], spos: obj['spos'], epos: obj['epos'],
                                no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: obj['lanes'], 
                                total_ext: (obj['total_ext']/1000).toFixed(2), totalExt: obj['total_ext'], ts_id: obj['s_ts_id'], rank: obj['rank'],
                                tri_id: obj['tri_id'], sector: obj['sector'], section: obj['section'], s_node_name: obj['s_node_name'], e_node_name: obj['e_node_name'],
                                s_ts_node_id: obj['s_ts_node_id'], e_ts_node_id: obj['e_ts_node_id']
                            }
                        });
                        setDetailRowData(result);
                        setTempDetailList(result);
                        dataSort(detailList);
                        setDetailGrid({
                            columnDefs : header,
                            rowSelection: 'multiple',
                            rowData: detailList,
                            style: {
                                width:"100%", height:"70vh", display:"inline-block", textAlign:'center'
                            },
                            onSelectionChanged: function(e) {
                                var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length==0) return;
                                setSelectedGridType('detail');
                                // setSelectRowData(selectedRows);
                                var extSum = 0;
                                selectedRows.map((row)=>{
                                    var selectedRowExt = row['data']['totalExt'];
                                    extSum = extSum + selectedRowExt;
                                });
                                setTotalExt((extSum/1000).toFixed(2));
                            }
                        });
                    }else{
                        setDetailRowData(null);
                        setDetailGrid({
                            columnDefs : header,
                            rowSelection: 'multiple',
                            rowData: null,
                            style: {
                                width:"100%", height:"70vh", display:"inline-block"
                            }
                        });
                    }
                }
            }
            currentDetailList();

            async function allDetailList(){

                setRecordGrid({
                    columnDefs : header,
                    rowData: null,
                    style: {
                        width:"100%", height:"70vh", display:"none"
                    }
                });
                var result = await server_connect(serviceUrl["pavement"]["getAllDetail"]);
                if(result&&result['status']==200){
                    var result_detail = result['data']['res'];
                    var result_year = result['data']['yearList'];
                    if(result_detail.length>0){
                        var detailList = result_detail.map((obj) => {
                            return {
                                id: obj['id'], s_t10m_id: obj['s_t10m_id'], e_t10m_id: obj['e_t10m_id'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'],
                                year: obj['year'], target_id: obj['target_id'], detail_target_id: obj['detail_target_id'], spos: obj['spos'], epos: obj['epos'],
                                no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: obj['lanes'], 
                                total_ext: (obj['total_ext']/1000).toFixed(2), totalExt: obj['total_ext'], ts_id: obj['s_ts_id'], rank: obj['rank'],
                                tri_id: obj['tri_id'], sector: obj['sector'], section: obj['section'], s_node_name: obj['s_node_name'], e_node_name: obj['e_node_name'],
                                s_ts_node_id: obj['s_ts_node_id'], e_ts_node_id: obj['e_ts_node_id']
                            }
                        });
                        setRecordRowData(result_detail);
                        dataSort(detailList);
                        setRecordGrid({
                            columnDefs : header,
                            rowSelection: 'multiple',
                            rowData: detailList,
                            style: {
                                width:"100%", height:"70vh", display:"none", textAlign:'center'
                            },
                            onSelectionChanged: function(e) {
                                var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length==0) return;
                                setSelectedGridType('record');
                                var extSum = 0;
                                selectedRows.map((row)=>{
                                    var selectedRowExt = row['data']['totalExt'];
                                    extSum = extSum + selectedRowExt;
                                });
                                setTotalExt((extSum/1000).toFixed(2));
                            }
                        });
                    }else{
                        setRecordGrid({
                            columnDefs : header,
                            rowSelection: 'multiple',
                            rowData: null,
                            style: {
                                width:"100%", height:"70vh", display:"none"
                            }
                        });
                    }

                    recordYearOnChangeEvt();
    
                    // 이력 필터 세팅
                    // if(result_year.length>0){
                    //     var result = result_year.map((obj) => {
                    //         return {label: obj['year'], value : obj['year']};
                    //     });
                    //     result.unshift({label: '전체', value: 'all'});
                    //     setPlanYear(result);
                    // }else {
                    //     setPlanYear([{label: '전체', value: 'all'}]);
                    // }
                }
             }
             allDetailList();
        }        
    }, [dColumns]);

    // 그리드 header 세팅
    const tHeader = (cols) => {
        var prev;
        var colNum = cols.length;
        var firstName = cols[0]['name'];
        var header = cols.map((obj, index) =>{
            if(!(prev) || obj['attributes']['className']!=prev){
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        header = header.filter(function(el){
            return el!=null;
        });

        header = header.map((obj)=>{
            return {
                headerName: obj,
                children: 
                    cols.map((col)=>{
                        var attr = col['attributes'];
                        if(attr['className']==obj && col['name']==firstName){
                            return {
                                headerName: attr['title'], 
                                field: col['name'],
                                width: Number(attr['wd']),
                                colSpan: function(params){
                                    if(isSpan(params)){
                                        return colNum;
                                    }else{
                                        return 1;
                                    }
                                },
                                // cellStyle: function(params){
                                //     if(isSpan(params)){
                                //         return {textAlign: 'center'};
                                //     }else {
                                //         return {textAlign: 'left'};
                                //     }
                                // },
                            }
                        }else if (attr['className']==obj && col['name']!=firstName){
                            return {headerName: attr['title'], field: col['name'], width: Number(attr['wd'])}
                        }
                    }).filter(function(el){
                        return el!=null;
                    })
            }
        });

        function isSpan(params){
            return params.data.span === 'no-result';
        }

        return header;
    }

    // 마스터 그리드 Api
    const getMasterGridComponentState = (childState) => {
        setMasterGridApi(childState);
    }
    // 1차 장비조사 그리드 Api
    const getEquipGridComponentState = (childState) => {
        setEquipGridApi(childState);
    }
    // 상세조사 그리드 Api
    const getDetailGridComponentState = (childState) => {
        setDetailGridApi(childState);
    }

    // 상세조사 그리드 Api
    const getRecordGridComponentState = (childState) => {
        setRecordGridApi(childState);
    }

    const handleMasterSwitch = () => {
        // if(!masterSwitch) {
        //     masterGridApi.redrawRows();
        // }
        setMasterSwitch(!masterSwitch);
    }

    const handleEquipSwitch = () => {
        setEquipSwitch(!equipSwitch);
        // if(masterSwitch) masterGridApi.redrawRows();
    }

    const handleRecordSwitch = () => {
        setRecordSwitch(!recordSwitch);
    }

    const handleTableViewSwitch = () => {
        setTableViewSwitch(!tableViewSwitch);
    }

    const recordYearChange = () => {}
    
    const recordYearOnChangeEvt = () => {
        var currentYear = new Date().getFullYear();
        var arr = new Array();
        arr.push({label: '전체', value: 'all'});
        for(var i=currentYear; i>=(currentYear-10); --i){
            arr.push({label:i, value:i});
        }
        // console.log("arr: ",arr);
        setPlanYear(arr);
    }

    const typeChange = async(obj) => {
        var params = new Object();
        params['rank'] = obj.value;
        var result = await server_connect(serviceUrl["pavement"]["getNoList"], params);
        if(result&&result['status']==200){
            result = result['data']['res'];
            if(result.length>0) {
                var noList = result.map((obj)=> {
                    return {label: obj, value : obj};
                });      
                
                noList.unshift({label: '전체', value: 'all'});
                setNos(noList);
            }
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
        // if(obj.value !="all"){
        // }
    }

    const noChange = async(obj) => {
        var params = new Object();
        var selectType = roadType.current.state.value;
        params['rank'] = selectType['value'];
        params['no'] = obj.value;
        var result = await server_connect(serviceUrl["pavement"]["getNameList"], params);
        result = result['data']['res'];
        if(result.length>0){
            var nameList = result.map((obj)=> {
                return {label: obj['name'], value : obj['id']};
            });      
            nameList.unshift({label: '전체', value: '0'});
            setNames(nameList);
        }else{
            setNames([{label: '전체', value: '0'}]);
        }
    }

    // 검색 버튼 클릭 이벤트
    const handleClick = () => {
       setSelectedGridType('none');

        var params = new Object();
        var selectType = roadType.current.state.value;
        var selectNo = roadNo.current.state.value;
        var selectName = roadName.current.state.value;
        var selectDir = roadDir.current.state.value;
        
        // 도로등급 필수 선택
        if(selectType !=null) {
            params['rank'] = selectType['value'];
        }
        // 도로번호 (미선택 or '-' or '전체' 선택 시 빈공백("") 처리)
        if(selectNo !=null && selectNo['value'] !="-" && selectNo['value'] !="all"){
            params['no'] = selectNo['value'];
        }else params['no'] = "";
        // 도로명 (해당 도로명에 해당하는 tri_id를 value로 검색 / '전체' 선택 시 tri_id=0 으로 처리)
        if(selectName !=null){
            params['tri_id'] = selectName['value'];
        }else params['tri_id'] = "0";
        // 행선 (미선택 시, -1 로 처리)
        if(selectDir !=null){
            params['dir'] = selectDir['value']
        }else params['dir'] = "-1";

        setFilterValues({
            'rank'      : params['rank'],
            'no'        : params['no'],
            'tri_id'    : params['tri_id'],
            'dir'       : params['dir']
        });   
        let today = new Date();
        let currYear = today.getFullYear();
        if(!recordSwitch){
            if(selectType ==null) {
                alert("도로등급을 선택해 주시기 바랍니다.");
            }else{
                searchMaster(params);
                searchEquip(params);
                // if(masterSwitch) searchMaster(params);
                // if(equipSwitch) searchEquip(params);
                params['planYear'] = currYear;
                setFilterValues({
                    'rank'      : params['rank'],
                    'no'        : params['no'],
                    'tri_id'    : params['tri_id'],
                    'dir'       : params['dir'],
                    'planYear'  : params['planYear']
                });   
                searchDetail(params);
            }
        }else{
            var selectYear = recordYear.current.state.value;
            if(selectYear !=null) {
                if(selectType ==null) {
                    params['rank'] = "all";
                }
                setFilterValues({
                    'rank'      : params['rank'],
                    'no'        : params['no'],
                    'tri_id'    : params['tri_id'],
                    'dir'       : params['dir']
                });   
                searchMaster(params);
                searchEquip(params);
                params['planYear'] = selectYear['value'];
                setFilterValues({
                    'rank'      : params['rank'],
                    'no'        : params['no'],
                    'tri_id'    : params['tri_id'],
                    'dir'       : params['dir'],
                    'planYear'  : params['planYear']
                });   
                searchRecord(params);
                params['planYear'] = currYear;
                setFilterValues({
                    'rank'      : params['rank'],
                    'no'        : params['no'],
                    'tri_id'    : params['tri_id'],
                    'dir'       : params['dir'],
                    'planYear'  : params['planYear']
                });  
                searchDetail(params);
                // setFilterValues 해야할지.. 고민
            }else{
                alert("연도를 선택해 주시기 바랍니다.");
            }
        }
    }

    // 마스터 그리드 내 검색
    const searchMaster = async(params) => {
        var result = await server_connect(serviceUrl["pavement"]["searchMaster"], params);
        if(result && result['status']==200){
            result = result['data']['res']; 
            if(result.length>0){
                setSearchMasterList(result);
                setMasterGrid({
                    ...masterGrid,
                    rowData: result
                });
            }else{
                // setTotalExt('0');
                setMasterGrid({
                    ...masterGrid,
                    rowData : [
                        {span: 'no-result', target_id: "검색 결과가 없습니다"}
                    ],
                });
            }
        }
    }

    // 1차 장비조사 내 검색
    const searchEquip = async(params) => {
        var result = await server_connect(serviceUrl["pavement"]["searchEquip"], params);
        if(result && result['status']==200){
            result = result['data']['res']; 
            if(result.length>0){
                setEquipGrid({
                    ...equipGrid,
                    rowData: result
                });
            }else{
                // setTotalExt('0');
                setEquipGrid({
                    ...equipGrid,
                    rowData : [
                        {span: 'no-result', target_id: "검색 결과가 없습니다"}
                    ],
                });
            }
        }
    }

    // 상세조사구간 내 검색
    const searchDetail = (params) => {
        if(detailRowData){
            var searchDetailList = search(params, detailRowData);        
            if(searchDetailList == undefined) return;
            if(searchDetailList.length>0){
                var detailList = searchDetailList.map((obj) => {
                    return {
                        id: obj['id'], s_t10m_id: obj['s_t10m_id'], e_t10m_id: obj['e_t10m_id'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'],
                        year: obj['year'], target_id: obj['target_id'], detail_target_id: obj['detail_target_id'], spos: obj['spos'], epos: obj['epos'],
                        no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: obj['lanes'], 
                        total_ext: (obj['total_ext']/1000).toFixed(2), totalExt: obj['total_ext'], ts_id: obj['s_ts_id'], rank: obj['rank'],
                        tri_id: obj['tri_id'], sector: obj['sector'], section: obj['section']
                    }
                });
    
                dataSort(detailList);
                setDetailGrid({
                    ...detailGrid,
                    rowData: detailList,
                });
            }else{
               setDetailGrid({
                    ...detailGrid,
                    rowData : [
                        {span: 'no-result', year: "검색 결과가 없습니다"}
                    ],
                    // onSelectionChanged: function(e) {
                    //     setMExt('-');
                    // },
                });
            }
        }
    }

    // 이력 내 검색
    const searchRecord = (params) => {
        var searchRecordList = search(params, recordRowData);
        if(searchRecordList.length>0){
            var recordList = searchRecordList.map((obj) => {
                return {
                    id: obj['id'], s_t10m_id: obj['s_t10m_id'], e_t10m_id: obj['e_t10m_id'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'],
                    year: obj['year'], target_id: obj['target_id'], detail_target_id: obj['detail_target_id'], spos: obj['spos'], epos: obj['epos'],
                    no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: obj['lanes'], 
                    total_ext: (obj['total_ext']/1000).toFixed(2), totalExt: obj['total_ext'], ts_id: obj['s_ts_id'], rank: obj['rank'],
                    tri_id: obj['tri_id'], sector: obj['sector'], section: obj['section'], s_node_name: obj['s_node_name'], e_node_name: obj['e_node_name'],
                    s_ts_node_id: obj['s_ts_node_id'], e_ts_node_id: obj['e_ts_node_id']
                }
            });

            dataSort(recordList);
            setRecordGrid({
                ...recordGrid,
                rowData: recordList,
            });
        }else{
            setRecordGrid({
                ...recordGrid,
                rowData : [
                    {span: 'no-result', year: "검색 결과가 없습니다"}
                ],
                // onSelectionChanged: function(e) {
                //     setMExt('-');
                // },
            });
        }
    }

    const search  = (params, dataList) => {
        var resultList = new Array();
        // if(dataList.length)
        if(dataList != undefined){
            for(var i=0; i<dataList.length; ++i){
                var chk=true;
                if(params['planYear'] != 'all'){
                    if((String)(dataList[i]['year']) == params['planYear']){
                        chk = search2(params,dataList, chk, i);
                    } else{
                        chk=false;
                    }
                }else {
                    chk = search2(params,dataList, chk, i);
                }
        
                if(chk) resultList.push(dataList[i]);
            }
            return resultList;
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요');
        }
    }

    const search2  = (params, dataList, chk, i) => {
        if(params['rank'] != 'all'){
            if(dataList[i]['rank'] == params['rank']){
                if(params['no'] != ''){
                    if(dataList[i]['no'] == params['no']){
                        if(params['tri_id'] != '0'){
                            if(dataList[i]['tri_id'] == params['tri_id']){
                                if(params['dir'] != '-1'){                                        
                                    if((String)(dataList[i]['dir']) != params['dir']) chk=false;
                                }
                            }else{
                                chk=false;
                            }
                        }else{
                            if(params['dir'] != '-1'){
                                if((String)(dataList[i]['dir']) != params['dir']) chk=false;
                            }
                        }
                    }else{
                        chk=false;
                    }
                }else{
                    if(params['dir'] != '-1'){
                        if((String)(dataList[i]['dir']) != params['dir']) chk=false;
                    }
                } 
            }else{
                chk=false;
            }
        }else{
            if(params['dir'] != '-1'){
                if((String)(dataList[i]['dir']) != params['dir']) chk=false;
            }
        }
        
        return chk;
    }

    // 이력 스위치 on/off 에 따른 검색 필터 타입 변경
    const changeFilter = () => {
        
        if(recordSwitch){
            document.getElementById("filter1").style.display = 'inline-block';
            // document.getElementById("filter2").style.display = 'none';
        }else {
            document.getElementById("filter1").style.display = 'none';
            // document.getElementById("filter2").style.display = 'inline-block';
        }
    }
    
    // 그리드 내 데이터 선택
    useEffect(() => {

        if(selectedGridType=='master'){
            detailGridApi.deselectAll();
            equipGridApi.deselectAll();
            recordGridApi.deselectAll();
        }else if(selectedGridType=='detail'){
            masterGridApi.deselectAll();
            equipGridApi.deselectAll();
            recordGridApi.deselectAll();
        }else if(selectedGridType=='equip'){
            masterGridApi.deselectAll();
            detailGridApi.deselectAll();
            recordGridApi.deselectAll();
        }else if(selectedGridType=='record'){
            masterGridApi.deselectAll();
            detailGridApi.deselectAll();
            equipGridApi.deselectAll();
        }else if(selectedGridType=='none' && masterGridApi){
            masterGridApi.deselectAll();
            detailGridApi.deselectAll();
            equipGridApi.deselectAll();
            recordGridApi.deselectAll();
        }

    }, [selectedGridType]);

    // 이력 스위치 on/off => 기타 스위치 상태 변경 & 검색 필터 변경
    useEffect(() => {
        setTotalExt('0');
        setSelectedGridType('none');
        if(recordSwitch) {
            setMasterSwitch(false);
            setEquipSwitch(false);
            setTableViewSwitch(false);
        
            document.getElementById('record').style.display='inline-block';
            document.getElementById('detail').style.display='none';
            document.getElementById('viewModel').style.display='none';
        }else {
            document.getElementById('record').style.display='none';
            if(!masterSwitch && !equipSwitch && !tableViewSwitch){
                document.getElementById('detail').style.width ='100%';
                document.getElementById('detail').style.height ='70vh';
                document.getElementById('detail').style.display ='inline-block';
                document.getElementById('detail').style.removeProperty('padding');
            }
        }
        changeFilter();
    }, [recordSwitch])

    // 1차장비 스위치 on/off
    useEffect(() => {
        setTotalExt('0');
        if(equipSwitch) setMasterSwitch(true);
        if(!equipSwitch && masterSwitch){
            setSelectedGridType('none');
            document.getElementById('add_btn').style.top='40%';
            document.getElementById('del_btn').style.top='60%';
            document.getElementById('master').style.height ='72vh';
            document.getElementById('master').style.display ='flex';
            document.getElementById('master').style.flex = 1;
            document.getElementById('master').style.flexDirection = "column";
            document.getElementById('detail').style.height ='72vh';
            document.getElementById('detail').style.width ='50%';
            document.getElementById('detail').style.display ='flex';
            document.getElementById('detail').style.padding ='3px';
            document.getElementById('equip').style.display ='none';
            document.getElementById('viewModel').style.display='none';
            if(tableViewSwitch){
                document.getElementById('detail').style.display='none';
                document.getElementById('viewModel').style.height='72vh';
                document.getElementById('viewModel').style.display='flex';
            }
        }else if(equipSwitch && masterSwitch){
            setSelectedGridType('none');
            setContentStyle({marginTop: "5px", height:"72vh", flex:1, flexDirection: "column"});
            document.getElementById('add_btn').style.top='30%';
            document.getElementById('del_btn').style.top='45%';
            document.getElementById('master').style.height ='42vh';
            document.getElementById('master').style.display ='flex';
            document.getElementById('master').style.flex = 1;
            document.getElementById('master').style.flexDirection = "column";
            document.getElementById('detail').style.height ='42vh';
            document.getElementById('detail').style.display ='flex';
            document.getElementById('detail').style.flex = 1;
            document.getElementById('detail').style.flexDirection = "column";
            document.getElementById('equip').style.width ='100%';
            document.getElementById('equip').style.height ='25vh';
            document.getElementById('equip').style.marginTop ='21px';
            document.getElementById('equip').style.display ='block';
            document.getElementById('viewModel').style.display='none';
            if(tableViewSwitch){
                setContentStyle({marginTop: "5px", height:"72vh", display: 'flex', flex:1, flexDirection: "column"});
                document.getElementById('detail').style.display='none';
                document.getElementById('viewModel').style.display='flex';
                document.getElementById('viewModel').style.height='42vh';
                document.getElementById('viewModel').style.padding ='3px';
            }
        }
    }, [equipSwitch])

    // 마스터 스위치, 뷰 스위치 on/off
    useEffect(() => {     
        setSelectedGridType('none');
        setTotalExt('0');
        document.getElementById('add_btn').style.display='inline-grid';
        document.getElementById('del_btn').style.display='inline-grid';
        if(masterSwitch || equipSwitch || tableViewSwitch) setRecordSwitch(false);
        if(masterSwitch){
            if(equipSwitch){
                document.getElementById('add_btn').style.top='30%';
                document.getElementById('del_btn').style.top='45%';
                document.getElementById('master').style.height ='42vh';
                document.getElementById('master').style.display ='flex';
                document.getElementById('master').style.flex = 1;
                document.getElementById('master').style.flexDirection = "column";
                document.getElementById('equip').style.width ='100%';
                document.getElementById('equip').style.height ='25vh';
                document.getElementById('equip').style.marginTop ='21px';
                document.getElementById('equip').style.display ='block';       
                if(tableViewSwitch){
                    document.getElementById('add_btn').style.display='none';
                    document.getElementById('del_btn').style.display='none';
                    setContentStyle({marginTop: "5px", height:"72vh", display: 'flex', flex:1, flexDirection: "column"});
                    document.getElementById('detail').style.display='none';
                    document.getElementById('viewModel').style.height='42vh';
                    document.getElementById('viewModel').style.display='flex';
                    document.getElementById('viewModel').style.padding ='3px';
                }else{
                    setContentStyle({marginTop: "5px", height:"72vh", flex:1, flexDirection: "column"});
                    document.getElementById('viewModel').style.display='none';
                    document.getElementById('detail').style.height ='42vh';
                    document.getElementById('detail').style.display ='flex';
                    document.getElementById('detail').style.flex = 1;
                    document.getElementById('detail').style.flexDirection = "column";
                    document.getElementById('detail').style.padding ='3px';
                }
            }else{
                document.getElementById('master').style.height ='70vh';
                document.getElementById('master').style.display ='inline-block';
                document.getElementById('master').style.padding ='3px';
                document.getElementById('equip').style.display ='none';
                if(tableViewSwitch){
                    document.getElementById('add_btn').style.display='none';
                    document.getElementById('del_btn').style.display='none';
                    document.getElementById('detail').style.display='none';
                    document.getElementById('master').style.flex='1';
                    document.getElementById('master').style.display ='flex';
                    document.getElementById('master').style.flexDirection = "column";
                    document.getElementById('master').style.height ='72vh';
                    setContentStyle({marginTop: "5px", height:"72vh", display: 'flex', flex:1, flexDirection: "column"});
                    document.getElementById('viewModel').style.height='72vh';
                    document.getElementById('viewModel').style.display='flex';
                    document.getElementById('viewModel').style.flex='1';
                    document.getElementById('viewModel').style.flexDirection = "column";
                    document.getElementById('viewModel').style.padding ='3px';
                }else{
                    document.getElementById('add_btn').style.top='40%';
                    document.getElementById('del_btn').style.top='60%';
                    document.getElementById('viewModel').style.display='none';
                    document.getElementById('detail').style.height ='70vh';
                    document.getElementById('detail').style.width ='50%';
                    document.getElementById('detail').style.display ='inline-block';
                    document.getElementById('detail').style.padding ='3px';
                }
            }
        }else{
            if(equipSwitch) setEquipSwitch(false);
            document.getElementById('equip').style.display ='none';
            document.getElementById('master').style.display ='none';
            document.getElementById('add_btn').style.display='none';
            document.getElementById('del_btn').style.display='none';
            if(tableViewSwitch){
                document.getElementById('detail').style.display='none';
                document.getElementById('viewModel').style.height ='72vh';
                document.getElementById('viewModel').style.display='flex';
            }else if(recordSwitch){
                document.getElementById('viewModel').style.display='none';
                document.getElementById('detail').style.display='none';
                document.getElementById('record').style.width ='100%';
                document.getElementById('record').style.display='inline-block';
            }else{
                document.getElementById('viewModel').style.display='none';
                document.getElementById('record').style.display='none';
                document.getElementById('detail').style.width ='100%';
                document.getElementById('detail').style.height ='70vh';
                document.getElementById('detail').style.display ='inline-block';
                document.getElementById('detail').style.removeProperty('padding');
            }
        }
    }, [masterSwitch, tableViewSwitch]);

    useEffect(() => {
        // if(masterGrid.rowData==null && masterGrid.rowClassRules==null){
        if(masterGrid.rowClassRules==null){
            console.log("detailRowData: ",detailRowData);
            setMasterGrid({
                ...masterGrid,
                rowData: searchMasterList,
                rowClassRules: {
                    'included-row-in-target' : function (params) {
                        var ts_id = params.data.ts_id;
                        var chk = false;
                        if(detailRowData!=null){
                            for(var i=0; i<detailRowData.length; ++i){
                                var s_ts_id = detailRowData[i]['s_ts_id'];
                                var e_ts_id = detailRowData[i]['e_ts_id'];
                                if(ts_id>=s_ts_id && ts_id<=e_ts_id){
                                    if(params.data.type == '0104') chk = true;
                                }
                            }
                            return chk;
                        }
                    }
                }            
            });
        }

    }, [detailRowData, masterSwitch])
    
    const addToDetailGrid = async(e) => {
        var allDetail=tempDetailList;                         // 상세조사 그리드에 있는 리스트 가져오기
        console.log("allDetail: ", allDetail);
        var selectedAllMaster = masterGridApi.getSelectedNodes();      // 마스터에서 선택한 데이터 리스트
     
        if(selectedAllMaster.length>0) {                               // 상세조사 그리에 있는 데이터 제외
            var selectedMaster = new Array(); 
            for(var i=0; i<selectedAllMaster.length; ++i){
                var chk=true;
                var master_obj = selectedAllMaster[i]['data']['ts_id'];
                if(allDetail !=null){
                    for(var j=0; j<allDetail.length; ++j){
                        if(master_obj>= allDetail[j]['s_ts_id'] && master_obj<=allDetail[j]['e_ts_id']){
                            chk=false;
                        }
                    }            
                }
                if(chk) selectedMaster.push(selectedAllMaster[i]);
            }
        
            var params = new Object();
            // list.push(target_id);
            for(var i=0; i<selectedMaster.length; ++i){                     // 1차 그룹핑
                if(selectedMaster[i]['data']['type'] == '0104'){
                    var key = null;
                    if(selectedMaster[i]['data']['target_id'] !=null){
                        key = selectedMaster[i]['data']['target_id'];
                    }else {
                        console.log("selectedMaster: ", selectedMaster[i]['data']);
                        key = String(selectedMaster[i]['data']['tri_id'])+String(selectedMaster[i]['data']['main_num'])
                            +String(selectedMaster[i]['data']['sub_num'])+String(selectedMaster[i]['data']['dir'])
                            +String(selectedMaster[i]['data']['sector']);

                    }
                    var tempArr = new Array();
                    if(params.hasOwnProperty(key)){
                        tempArr = params[key];
                    }
                    // tempArr.push(selectedEquip[i]['data']['t10m_id']);
                    tempArr.push(selectedMaster[i]['data']);
                    params[key] = tempArr;
                }
            }
            
            if(beforeAdd != null){               // 상세조사 그리드에 있는 데이터 중 새로 추가하는 데이터와 동일한 키 값을 갖는 데이터만 params에 추가 (상세조사ID 시퀀스 변경을 위해)
                console.log("beforeAdd: ", beforeAdd);
                for(var i=0; i<Object.keys(beforeAdd).length; ++i){
                    var beforeAdd_key = Object.keys(beforeAdd)[i];
                    console.log("beforeAdd_key: ", beforeAdd_key);
                    var beforeAdd_subKey = beforeAdd[beforeAdd_key]['sub_key'];
                    for(var j=0; j<Object.keys(params).length; ++j){
                        var param_key = Object.keys(params)[j];
                        // if(beforeAdd_key == param_key ) params[beforeAdd_key+"_c"] = beforeAdd[beforeAdd_key];
                        if(beforeAdd_subKey == param_key) {
                            params[beforeAdd_key+"_c"] = beforeAdd[beforeAdd_key];
                        }                   
                    }
                }
            }

            var f_params = new Object();            // 같은 키값의 데이터 리스트 중에서 order가 연속되지 않을 경우 분리 (새로운 키 값 부여)
            for(var i=0; i<Object.keys(params).length; ++i){
                var list = params[Object.keys(params)[i]];
               
                var seq = 1;
                var key = Object.keys(params)[i]+"_"+seq;
                var newList = new Array();
                if(list.length>0){
                    list[0]['group_num'] = key;
                    newList.push(list[0]);
                }else{
                    list['group_num']=key;
                    newList.push(list);
                }
                f_params[key] = newList;

                if(list.length>0){
                    var beforeOrder = list[0]['sm_order'];
                    for(var j=1; j<list.length; ++j){
                        var obj = list[j];
    
                        if((obj['sm_order'] - beforeOrder)==1){
                            newList = f_params[key];
                        }else{
                            ++seq;
                            key = Object.keys(params)[i]+"_"+seq;
                            newList = new Array();
                        }
                        obj['group_num'] = key;
                        newList.push(obj);
                        f_params[key] = newList;
                        beforeOrder = obj['sm_order'];
                    }
                }
            }
            
            console.log("f_params: ",f_params);
            // /encodeURIComponent(JSON.stringify(
            var pKeyList = Object.keys(f_params);       // 서버에 넘거주기위한 encoding 작업
            var data = new Object();
            for(var i=0; i<pKeyList.length; ++i){
                var key = pKeyList[i];
                data[key] = encodeURIComponent(JSON.stringify(f_params[key]));
            }

            var updatedList = new Array();    
            var result = await server_connect(serviceUrl["pavement"]["addToDetail"], data);   // 서버통신
                result = result['data']['res'];
                if(result.length>0){
                    for(var i=0; i<result.length; ++i){            // 결과 리스트를 새로운 arry인 updatedList에 담기
                        updatedList.push(result[i]);
                    }
                    if(allDetail !=null){                       // 기존 상세조사구간 그리드와 비교해서 결과 리스트에 없는 리스트만 updatedList에 추가
                        for(var i=0; i<allDetail.length; ++i){
                            var chk=true;
                            for(var j=0; j<result.length; ++j){
                                if(result[j]['detail_target_id']==allDetail[i]['detail_target_id']) chk=false;
                            }
                            if(chk) updatedList.push(allDetail[i]);
                        }
                    }
                }else {
                    if(allDetail !=null){                     // 결과 리스트가 없는 경우, 기존 리스트를 그대로 updatedList에 담기
                        for(var i=0; i<allDetail.length; ++i){
                            updatedList.push(allDetail[i]);
                        }
                    }
                }

                if(updatedList.length>0){
                    var detailList = new Array();
                    if(filterValues !=null){
                        var filteredList = search(filterValues, updatedList);
                        detailList = filteredList.map((obj) => {
                            return {
                                id: obj['id'], s_t10m_id: obj['s_t10m_id'], e_t10m_id: obj['e_t10m_id'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'],
                                year: obj['year'], target_id: obj['target_id'], detail_target_id: obj['detail_target_id'], spos: obj['spos'], epos: obj['epos'],
                                no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: obj['lanes'], 
                                total_ext: (obj['total_ext']/1000).toFixed(2), totalExt: obj['total_ext'], ts_id: obj['s_ts_id'], rank: obj['rank'],
                                tri_id: obj['tri_id'], sector: obj['sector'], section: obj['section'], s_node_name: obj['s_node_name'], e_node_name: obj['e_node_name'],
                                s_ts_node_id: obj['s_ts_node_id'], e_ts_node_id: obj['e_ts_node_id']
                            }
                        });
                    }else{
                        detailList = updatedList.map((obj) => {
                            return {
                                id: obj['id'], s_t10m_id: obj['s_t10m_id'], e_t10m_id: obj['e_t10m_id'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'],
                                year: obj['year'], target_id: obj['target_id'], detail_target_id: obj['detail_target_id'], spos: obj['spos'], epos: obj['epos'],
                                no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: obj['lanes'], 
                                total_ext: (obj['total_ext']/1000).toFixed(2), totalExt: obj['total_ext'], ts_id: obj['s_ts_id'], rank: obj['rank'],
                                tri_id: obj['tri_id'], sector: obj['sector'], section: obj['section'], s_node_name: obj['s_node_name'], e_node_name: obj['e_node_name'],
                                s_ts_node_id: obj['s_ts_node_id'], e_ts_node_id: obj['e_ts_node_id']
                            }
                        });
                    }

                    var currObj = new Object();
                    for(var i=0; i<updatedList.length; ++i){                 // 추가 작업 이후 상세조사 그리드의 리스트를 key, value로 currObj에 담기
                        // var key = String(updatedList[i]['tri_id'])+String(updatedList[i]['main_num'])
                        //           +String(updatedList[i]['sub_num'])+String(updatedList[i]['dir'])
                        //           +String(updatedList[i]['sector']);
                        var sub_key = String(updatedList[i]['tri_id'])+String(updatedList[i]['main_num'])
                                  +String(updatedList[i]['sub_num'])+String(updatedList[i]['dir'])
                                  +String(updatedList[i]['sector']);
                        updatedList[i]['sub_key'] = sub_key;
                        var key = updatedList[i]['group_num'];
                        if(updatedList[i]['target_id']!=null){
                            key = updatedList[i]['target_id'];
                        }
                        currObj[key] = updatedList[i];
                    }
                    setBeforeAdd(currObj);       // beforeAdd 변수로 관리
                   
                    dataSort(detailList);        // 리스트 sorting
                    
                    setDetailGrid({
                        ...detailGrid,
                        rowData: detailList,
                    });                    
                    
                    setMasterGrid({
                        ...masterGrid,
                        rowData: null,
                        rowClassRules: null
                    });
                    console.log("updatedList: ", updatedList);
                    setTempDetailList(updatedList);  // tempDetailList 업데이트
                    setDetailRowData(updatedList);   // detailRowData 업데이트 (마스터 rowClass에 사용)
                    
                }  
        }
    }

    const dataSort = (data) => {
        data.sort(function (a, b) {
            var before_no = parseInt(a.no);
            var after_no = parseInt(b.no);
            return b.year-a.year || a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
                       || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section;
            // if(gridType=="detail"){
            //     return a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
            //            || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section;
            // }else if(gridType=="record"){
            //     return b.year-a.year || a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
            //            || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section;
            // }
        });
        return data;
    }

    const delFromDetailGrid = () => {
        var allDetail = tempDetailList;
        var updatedDetail = new Array();
        var selectedDetail= detailGridApi.getSelectedNodes();
        if(selectedDetail.length>0){
            for(var i=0; i<allDetail.length; ++i){
                var chk=true;
                for(var j=0; j<selectedDetail.length; ++j){
                    if(allDetail[i]['detail_target_id'] == selectedDetail[j]['data']['detail_target_id']){
                        chk = false;
                    }
                }
                if(chk) updatedDetail.push(allDetail[i]);
            }

            var currObj = new Object();
            for(var i=0; i<updatedDetail.length; ++i){
                var key = String(updatedDetail[i]['tri_id'])+String(updatedDetail[i]['main_num'])
                          +String(updatedDetail[i]['sub_num'])+String(updatedDetail[i]['dir'])
                          +String(updatedDetail[i]['sector'])+String(updatedDetail[i]['section']);
                if(updatedDetail[i]['target_id']!=null){
                    key = updatedDetail[i]['target_id'];
                }
                currObj[key] = updatedDetail[i];
            }
            setBeforeAdd(currObj);

            if(filterValues !=null){
                var filteredList = search(filterValues, updatedDetail);
                dataSort(filteredList);
                setDetailGrid({
                    ...detailGrid,
                    rowData: filteredList
                });
            }else{
                dataSort(updatedDetail);
                setDetailGrid({
                    ...detailGrid,
                    rowData: updatedDetail
                });
            }
      
            setMasterGrid({
                ...masterGrid,
                rowData: null,
                rowClassRules: null
            });

            setTempDetailList(updatedDetail);
            setDetailRowData(updatedDetail);
        }
    }

    const viewMap = () => {
        var props = [];
        var masterNodes = masterGridApi.getSelectedNodes();
        var detailNodes = detailGridApi.getSelectedNodes();
        var recordNodes = recordGridApi.getSelectedNodes();

        if(masterNodes.length>0){
            props['allData'] = masterList;
            props['selectedNodes'] = masterNodes;
            props['header'] = [
             {
                 label : 'rank',
                 name  : '등급'
             },
             {
                 label : 'no',
                 name  : '노선번호'
             },
                 {
                 label : 'name',
                 name  : '노선명'
             },
             {
                 label : 'rev_dir',
                 name  : '행선'
             },
             {
                 label : 'node_name',
                 name  : '이벤트명'
             },
             {
                 label : 'node_type',
                 name  : '이벤트 종류'
             }
            ];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }else if(detailNodes.length>0){
            dataSort(detailRowData);
            getDetailTargetTsList(detailRowData, "detail", detailNodes);
        }else if(recordNodes.length>0){
            dataSort(recordRowData);
            getDetailTargetTsList(recordRowData, "record", recordNodes);
        }else if(masterNodes.length==0 && detailNodes.length==0) {
            alert("확인하실 구간을 먼저 선택해 주시기 바랍니다.");
        }
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display='none';
        document.getElementsByClassName('modal-backdrop')[0].style.display='none';
        document.getElementsByClassName('modal-dialog')[0].style.display='none';
    }

    const saveDetailTarget =async() => {
        if(tempDetailList !=null){
            if(window.confirm("상세조사대상구간을 저장하시겠습니까?") == true) {
                var data = new Object();
                data['list'] = encodeURIComponent(JSON.stringify(tempDetailList));
                var result = await server_connect(serviceUrl["pavement"]["saveDetail"], data);   // 서버통신
                if(result['data']['res']==1){
                    alert("저장이 완료되었습니다.");
                }else{
                    alert("저장이 정상적으로 완료되지 않았습니다. 다시 시도해 주시기 바랍니다.");
                }
            
            }
        }
    }

    useEffect(()=>{
        if(viewTargetList){
            var props = [];
            props['allData'] = viewTargetList["allData"];

            var list = viewTargetList["list"].map((el)=>{
                var obj = el["data"];
                viewTargetList["allData"].map((row)=>{
                    if(obj["detail_target_id"]==row["detail_target_id"]){
                        obj["tsList"] = row["tsList"];
                        el["data"] = obj;
                        return;
                    }
                });
                return el;
            });

            props['selectedNodes'] = list;
            props['header'] = viewHeader[viewTargetList["type"]];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }
    }, [viewTargetList]);

    const getDetailTargetTsList = async (list, type, selectedNodes) => {
        if(list !=null){
            var data = new Object();
            data['list'] = encodeURIComponent(JSON.stringify(list));
            var result = await server_connect(serviceUrl["pavement"]["getTargetTsList"], data);   // 서버통신
            setViewTargetList({ list : selectedNodes, type : type, allData : result['data']['res']});
        }
    }
    
    return <>
        <ModalBody style={{overflow:'auto'}}>
            <span>
            <span id="filter1"> <SearchDropdown target={recordYear} options={planYear} placeholder="연도 선택" onChange={recordYearChange} width="200px"/> </span>
                <span id="filter2">
                    <SearchDropdown target={roadType} options={typeList} placeholder="도로 등급" onChange={typeChange} width="230px"/>
                    <SearchDropdown target={roadNo} options={nos} placeholder="노선 번호" onChange={noChange} width="200px"/>
                    <SearchDropdown target={roadName} options={names} placeholder="노선명" width="230px"/>
                    <SearchDropdown target={roadDir} options={[{label: '전체', value: "-1"}, {label: '상행', value:"0"},{label:'하행', value:"1"}]} placeholder="행선" width="200px"/>
                </span>
                <Button size="sm" color="secondary" style={{width:"100px"}} onClick={handleClick}>검색</Button>
            </span>
            <span>
                <SwitchButton label="마스터" isOpen={masterSwitch} onChange={handleMasterSwitch} name="masterSwitch" />
            </span>
            <span>
                <SwitchButton label="1차장비" isOpen={equipSwitch} onChange={handleEquipSwitch} name="equipSwitch" />
            </span>
            <span>
                <SwitchButton label="이력" isOpen={recordSwitch} onChange={handleRecordSwitch} name="recordSwitch" />
            </span>
            <span>
                <SwitchButton label="테이블/뷰" isOpen={tableViewSwitch} onChange={handleTableViewSwitch} name="tableViewSwitch" />
            </span>
            <div className="ag-theme-balham" style={contentStyle}>
                <div id="add_btn"><input type="button" className="moveBtn" value=">>" onClick={addToDetailGrid}/></div>
                <div id="del_btn"><input type="button" className="moveBtn" value="<<" onClick={delFromDetailGrid}/></div>
                <div style={{display:'block'}}>
                <div id="upperGroup" style={{display: "flex", flex: 1, flexDirection: "row"}}> 
                    <GridComponent id="master" columnDefs={masterGrid.columnDefs} rowData={masterGrid.rowData} onSelectionChanged={masterGrid.onSelectionChanged} 
                                rowClassRules={masterGrid.rowClassRules} style={masterGrid.style} passStateEvt={getMasterGridComponentState} gridTitle={'노선마스터'}/>               
                    <GridComponent id="detail" columnDefs={detailGrid.columnDefs} rowData={detailGrid.rowData} onSelectionChanged={detailGrid.onSelectionChanged} style={detailGrid.style} passStateEvt={getDetailGridComponentState} gridTitle={'상세조사구간'}/>
                    <div id="viewModel" style={{flex:1, display:"flex", flexDirection:"column"}}>
                        <div className="grid-title">View</div>
                        <div style={{overflow:"auto", border: '1px solid #BDC3C7', padding:10}}>
                            {viewContent}
                        </div>
                    </div>
                </div>
                </div>                
                <GridComponent id="equip" columnDefs={equipGrid.columnDefs} rowData={equipGrid.rowData} onSelectionChanged={equipGrid.onSelectionChanged} style={equipGrid.style} passStateEvt={getEquipGridComponentState} gridTitle={'1차장비조사'}/>
                <GridComponent id="record" columnDefs={recordGrid.columnDefs} rowData={recordGrid.rowData} onSelectionChanged={recordGrid.onSelectionChanged} style={recordGrid.style} passStateEvt={getRecordGridComponentState} gridTitle={'상세조사구간 이력'}/>
            </div>
        </ModalBody>
        <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value={totalExt}/>Km
            </span>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                <Button className="feather icon-map" color="primary" onClick={viewMap}> 지도확인</Button>
                <Button className="feather icon-save" color="primary" onClick={saveDetailTarget}> 저장</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </>;

}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({type: actionTypes.OPEN_CHAT, data: props}),
        onCloseChat: () => dispatch({type: actionTypes.CLOSE_CHAT})
    }
}
export default connect(chatStateToProps, chatDispatchToProps) (Detail);