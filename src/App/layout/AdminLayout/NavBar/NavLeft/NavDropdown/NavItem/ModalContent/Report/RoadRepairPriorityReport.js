import React, { useState, useEffect } from 'react';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import {ModalBody, ModalFooter, Button, Label, Input} from 'reactstrap';
import GridComponent from '../../../../../../../../components/GridComponent';
import RepairReportXML from '../../../../../../../../../resource/settingXml/road-repair-report.xml';
import serviceUrl from  '../../../../../../../../../request-url-list';

const RoadRepairPriorityReport = ({toggle}) => {

    const [reportColumns, setReportColumns] = useState();
    const [gridList, setGridList] = useState();
    const [gridProps, setGridProps] = useState({});
    const [totalExt, setTotalExt] = useState();
    const [selectedList, setSelectedList] = useState();
    const passToggle = () => toggle();
    
    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const repairReportXml = await XmlParser(RepairReportXML);
            if(!ignore) { 
                setReportColumns(repairReportXml.getElementsByTagName('Columns')[0]['children']);
            }
        }
        fetchData();
        return () => { ignore = true; }
    }, []);
    
    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const result = await server_connect(serviceUrl["pavement"]["getRoadRepairPriorityList"]);
            if(!ignore){
                var header = tHeader(reportColumns);
                if(result['data']['res'].length>0){
                    setGridList(result['data']['res']);
                    setGridProps({
                        columnDefs : header,
                        rowData: result['data']['res'],
                        style: {
                            display: 'flex',
                            flexDirection: 'column',
                            height: '75vh'
                        },
                        onSelectionChanged: function(e) {
                            var selectedRows = e.api.getSelectedNodes();
                            if(selectedRows.length==0) return;
                            setSelectedList(selectedRows);
                            var totalExt = 0;
                            selectedRows.map((row)=>{
                                totalExt += row['data']['total_ext'];
                            });
                            setTotalExt((totalExt/1000).toFixed(2));
                        }
                    });
                }
                
            }
        }
        
        if(reportColumns) {
            fetchData();
        }

        return () => { ignore = true; }
    }, [reportColumns]);
    
    const tHeader = (cols) => {
        var prev;
        var colNum = cols.length;
        var firstName = cols[0]['name'];
        var header = cols.map((obj, index) =>{
            if(!(prev) || obj['attributes']['className']!=prev){
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        header = header.filter(function(el){
            return el!=null;
        });

        header = header.map((obj)=>{
            return {
                headerName: obj,
                children: 
                    cols.map((col)=>{
                        var attr = col['attributes'];
                        var width = null;
                        if(attr.hasOwnProperty("wd")){
                            width = Number(attr['wd']);
                        }
                        if(attr['className']==obj && col['name']==firstName){
                            return {
                                headerName: attr['title'], 
                                field: col['name'],
                                width: width
                            }
                        }else if (attr['className']==obj && col['name']!=firstName){
                            return {headerName: attr['title'], field: col['name'], width: width}
                        }
                    }).filter(function(el){
                        return el!=null;
                    })
            }
        });

        return header;
    }
    
    
    return (<>
        <ModalBody style={{textAlign: 'center'}}>
            <GridComponent id="repairReportGrid" columnDefs={gridProps.columnDefs} rowData={gridProps.rowData} onSelectionChanged={gridProps.onSelectionChanged} style={gridProps.style} gridTitle="도로 보수 우선순위 보고서" />
        </ModalBody>
        <ModalFooter  style={{justifyContent:"unset"}} >
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value={totalExt}/>Km
            </span>
            <div style={{width:"100%", justifyContent:"right", display:"flex-end", textAlign:"right"}}>
            <Button color="secondary" onClick={passToggle}>닫기</Button>
            </div>
        </ModalFooter>
    </>);
}

export default RoadRepairPriorityReport;