// export const typeList = [
//     {label: '국도', value: '국도'},
//     {label: '지방도', value: '지방도'},
//     {label: '시도', value: '시도'},
//     {label: '군도', value: '군도'},
//     {label: '면도', value: '면도'},
//     {label: '리도', value: '리도'}
// ];

export const typeList = [
    {label: '전체', value: 'all'},
    {label: '고속국도', value: '101'},
    {label: '도시고속국도', value: '102'},
    {label: '일반국도', value: '103'},
    {label: '특별·광역시도', value: '104'},
    {label: '국가지원 지방도', value: '105'},
    {label: '지방도', value: '106'},
    {label: '시·군도', value: '107'},
    {label: '기타', value: '108'}
];

export const noList =  {
        "국도":[
            {label: '1', value :'1'},
            {label: '3', value :'3'},
            {label: '7', value :'7'},
            {label: '8', value :'8'}
        ],
        "지방도":[
            {label: '101', value :'101'},
            {label: '106', value :'106'},
            {label: '107', value :'107'},
            {label: '108', value :'108'}
        ],
        "시도": [
            {label: '11', value :'11'},
            {label: '12', value :'12'},
        ],
        "군도": [
            {label: '21', value :'21'},
            {label: '22', value :'22'},
        ],
        "면도": [
            {label: '32', value :'32'},
        ],
        "리도": [
            {label: '3', value :'3'},
        ]
    };

export const nameList = {
    "국도1":[
        {label: '양정동_경강로', value:'1'}
    ],
    "국도3":[
        {label: '도농동_경춘로', value:'2'}
    ],
    "국도7":[
        {label: '경춘로', value:'3'}
    ],
    "국도8":[
        {label: '경춘로2', value:'4'}
    ],
    "지방도101":[
        {label: '마산로', value:'5'},
        {label: '황악로', value:'6'}
    ],
    "지방도106":[
        {label: '부항로', value:'7'},
        {label: '조마로', value:'8'}
    ],
    "지방도107":[
        {label: '개령로', value:'9'}
    ],
    "지방도108":[
        {label: '어전로', value:'10'},
        {label: '포아로', value:'11'}
    ],
    "시도11":[
        {label: '영남제일로', value:'12'},
        {label: '중앙로', value:'13'}
    ],
    "시도12":[
        {label: '덕곡로', value:'14'}
    ],
    "군도21":[
        {label: '송설로', value:'15'}
    ],
    "군도22":[
        {label: '노실고개길', value:'16'}
    ],
    "면도32":[
        {label: '금송길', value:'17'}
    ],
    "리도3":[
        {label: '신암길', value:'18'}
    ]

};

export const degreeList = [
    {label: '1차', value: '1차'},
    {label: '2차', value: '2차'},
    {label: '3차', value: '3차'},
];