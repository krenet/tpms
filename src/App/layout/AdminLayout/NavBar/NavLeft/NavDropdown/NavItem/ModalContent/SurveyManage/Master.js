import React, { useState, useEffect } from 'react';
import { ModalBody, ModalFooter, Label, Input, Button } from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import GridComponent from '../../../../../../../../components/GridComponent';
import SwitchButton from '../../../../../../../../components/SwitchButton';

const Master = ({toggle}) => {
    const [content, setContent] = useState();

    const [stateGrid, setStateGrid] = useState({
        columnDefs: [
            {
                headerName : '노선정보',
                children : [
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                    {headerName:"구간", field:"section"}    
                ]
            },
            {
                headerName: "연장",
                children : [
                    {headerName:"개별", field:"length"},
                    {headerName:"누적", field:"lenSum"}
                ]
            },
            {headerName:"중용 분리", field:"overlap"},
            {
                headerName: "주소",
                children : [
                    {headerName:"시점", field:"sAddr"},
                    {headerName:"종점", field:"eAddr"}
                ]
            },
            {headerName:"차로수", field:"lanes"},
            {
                headerName: "이벤트",
                children : [
                    {headerName:"명칭", field:"eventName"},
                    {headerName:"종류", field:"eventType"}
                ]
            },
            {headerName:"포장종류", field:"paveType"},
            {
                headerName: "최초포장",
                children : [
                    {headerName:"연도", field:"makeYear"},
                    {headerName:"AC", field:"makeAC"},
                    {headerName:"BB", field:"makeBB"},
                    {headerName:"GB", field:"makeGB"},
                    {headerName:"SB", field:"makeSB"},
                    {headerName:"AFL", field:"makeAFL"}
                ]
            },
            {
                headerName: "최종유지보수",
                children : [
                    {headerName:"연도", field:"latestYear"},
                    {headerName:"공법", field:"latestMethod"},
                    {headerName:"설명", field:"latestDesc"},
                    {headerName:"비용", field:"latestPrice"}
                ]
            },
            {
                headerName: "교통량",
                children : [
                    {headerName:"연도", field:"trafficYear"},
                    {headerName:"지점", field:"trafficPoint"},
                    {headerName:"Total", field:"traffic"}
                ]
            }
        ],
        rowData : [
            { name: "마산로", dir: "상행", lane: "1", section: "1", length: "340", lenSum: "682", overlap: "0", sAddr: "마산로 1구간 시점주소", eAddr: "마산로 1구간 종점주소", lanes: "2", eventName: "지하차도(북측)4", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "마산로", dir: "상행", lane: "1", section: "2", length: "342", lenSum: "682", overlap: "1", sAddr: "마산로 2구간 시점주소", eAddr: "마산로 2구간 종점주소", lanes: "2", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "황악로", dir: "상행", lane: "1", section: "1", length: "340", lenSum: "727", overlap: "22", sAddr: "황악로 1구간 시점주소", eAddr: "황악로 1구간 종점주소", lanes: "2", eventName: "일반국도1호선", eventType: "NR48", paveType: "아스팔트", makeYear: "1989", makeAC: "7", makeBB: "", makeGB: "15", makeSB: "40", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"2018", trafficPoint:"0140-001", traffic:"36,407"},
            { name: "황악로", dir: "상행", lane: "1", section: "2", length: "387", lenSum: "727", overlap: "0", sAddr: "황악로 2구간 시점주소", eAddr: "황악로 2구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "부항로", dir: "상행", lane: "1", section: "1", length: "440", lenSum: "1,215", overlap: "0", sAddr: "부항로 1구간 시점주소", eAddr: "부항로 1구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "부항로", dir: "상행", lane: "1", section: "2", length: "335", lenSum: "1,215", overlap: "3", sAddr: "부항로 2구간 시점주소", eAddr: "부항로 2구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "부항로", dir: "상행", lane: "1", section: "3", length: "420", lenSum: "1,215", overlap: "0", sAddr: "부항로 3구간 시점주소", eAddr: "부항로 3구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""}
        ]
    });

    const [equipGrid, setEquipGrid] = useState({
        columnDefs: [
            {headerName:"ID", field:"id"},
            {
                headerName : '조사 계획',
                children : [
                    {headerName:"연도", field:"year"},
                    {headerName:"차수", field:"order"}    
                ]
            },
            {
                headerName : '노선정보',
                children : [
                    {headerName:"등급", field:"type"},
                    {headerName:"번호", field:"no"},
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                    {headerName:"구간", field:"section"}    
                ]
            },
            {headerName:"균열율", field:"cr"},
            {headerName:"소성변형", field:"rutt"},
            {headerName:"평탄성", field:"iri"},
            {headerName:"인덱스", field:"index"},
            {headerName:"최근보수연도", field:"latestYear"},

        ],
        rowData : [
            {id:"", year:"", order:"", type:"", no:"", name:"", dir:"", lane:"", section:"", cr:"", rutt:"", iri:"", index:"", latestYear:""}
        ]
    });

    const [equipGrid2, setEquipGrid2] = useState({
        columnDefs:[
            {
                headerName : '조사 계획',
                children : [
                    {headerName:"연도", field:"year"},
                    {headerName:"차수", field:"order"}    
                ]
            },
            {headerName:"구간ID", field:"id"},
            {
                headerName : '노선정보',
                children : [
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                ]
            },
            {
                headerName : '시점',
                children : [
                    {headerName:"구간", field:"sSection"},
                    {headerName:"지점", field:"sPoint"},
                    {headerName:"위치", field:"sPos"},
                ]
            },
            {
                headerName : '종점',
                children : [
                    {headerName:"구간", field:"eSection"},
                    {headerName:"지점", field:"ePoint"},
                    {headerName:"위치", field:"ePos"},
                ]
            },
            {headerName:"차로수", field:"lane"},
            {headerName:"연장", field:"length"},
            {
                headerName : '조사등록여부',
                children : [
                    {headerName:"CRACK", field:"_cr"},
                    {headerName:"HWD", field:"hwd"},
                    {headerName:"GPR", field:"gpr"},
                ]
            },
            {headerName:"종단선형", field:"horiLine"},
            {headerName:"평면선형", field:"inLine"},
            {headerName:"균열율", field:"cr"},
            {headerName:"총균열율", field:"totalCr"},
            {headerName:"처짐량", field:"sag"},
            {headerName:"VI", field:"vi"},
            {headerName:"기타발생율", field:"etc"},
            {headerName:"아스콘두께", field:"ascon"},
            {headerName:"인덱스", field:"index"},
            {headerName:"조사대상ID", field:"equip1Id"}
        ],
        rowData:[
            {year:"", order:"", id:"", name:"", dir:"", lane:"", sSection:"", sPoint:"", sPos:"", eSection:"", ePoint:"", ePos:"", lane:"", length:"", _cr:"", hwd:"", gpr:"", horiLine:"", inLine:"", cr:"", totalCr:"", sag:"", vi:"", etc:"", ascon:"", index:"", equip1Id:""}
        ]
    });

    const [grid, setGrid] = useState(equipGrid);
    const [selectYear, setSelectYear] = useState();
    const [masterSwitch, setMasterSwitch] = useState(false);
    const planYear = [{label: '전체', value:"all"},{label:'2021', value:"2021"},{label:'2020', value:"2020"},{label:'2019', value:"2019"}];

    const handleChange = (data) =>{
        setSelectYear(data.value);
    }

    const handleEquipClick = () => {
        switch(selectYear){
            case "all":
                setEquipGrid({
                    ...equipGrid,
                    rowData : [
                        {id:"T211001", year:"2021", order:"1", type:"지방도", no:"903", name:"조마면_조마로", dir:"상행", lane:"1", section:"1", cr:"10.6", rutt:"3.97", iri:"2.6", index:"7.045211028622086", latestYear:"2019"},
                        {id:"T211002", year:"2021", order:"2", type:"시도", no:"4", name:"영남제일로", dir:"상행", lane:"2", section:"2", cr:"11.4884", rutt:"7.44", iri:"12.91", index:"6.124890875937497", latestYear:"2019"},
                        {id:"T211003", year:"2021", order:"3", type:"군도", no:"4", name:"벽봉로", dir:"상행", lane:"3", section:"3", cr:"1.62572", rutt:"4.23", iri:"3.38", index:"4.999832666634824", latestYear:"2018"},
                        {id:"T211004", year:"2021", order:"4", type:"리도", no:"201", name:"수도길", dir:"상행", lane:"4", section:"4", cr:"14.6597", rutt:"2.72", iri:"8.05", index:"6.701932104127495", latestYear:"2018"},
                        {id:"T200401", year:"2020", order:"1", type:"지방도", no:"916", name:"포아로", dir:"하행", lane:"1", section:"1", cr:"1.7", rutt:"3.55", iri:"6.78", index:"6.068256527329737", latestYear:"2019"},
                        {id:"T200501", year:"2020", order:"1", type:"지방도", no:"997", name:"개령로", dir:"상행", lane:"1", section:"1", cr:"0.5", rutt:"2.5", iri:"3.15", index:"9.264009243043564", latestYear:"2018"},
                        {id:"T200601", year:"2020", order:"1", type:"시도", no:"3", name:"황금로", dir:"상행", lane:"1", section:"1", cr:"2.1", rutt:"8.8", iri:"1.17", index:"8.199409760813861", latestYear:"2019"},
                        {id:"T200702", year:"2020", order:"2", type:"시도", no:"3", name:"양천로", dir:"하행", lane:"2", section:"2", cr:"5.9", rutt:"9.2", iri:"1.59", index:"4.433833735399091", latestYear:"2020"},
                        {id:"T190901", year:"2019", order:"1", type:"시도", no:"30", name:"증산로", dir:"상행", lane:"1", section:"1", cr:"10", rutt:"3.8", iri:"9.54", index:"6.064801972262802", latestYear:"2018"},
                        {id:"T191002", year:"2019", order:"2", type:"시도", no:"4", name:"덕곡로", dir:"상행", lane:"2", section:"2", cr:"6.2", rutt:"5.55", iri:"0.06", index:"7.912350410069344", latestYear:"2019"},
                        {id:"T191103", year:"2019", order:"3", type:"시도", no:"59", name:"용암로", dir:"하행", lane:"3", section:"3", cr:"12.9", rutt:"8.29", iri:"1.74", index:"7.330570124932441", latestYear:"2019"},
                        {id:"T191203", year:"2019", order:"3", type:"군도", no:"10", name:"황악로", dir:"하행", lane:"3", section:"3", cr:"1.4", rutt:"7.22", iri:"3.21", index:"6.904941384571362", latestYear:"2018"}
                    ]
                });
                break;
            case "2021":
                setEquipGrid({
                    ...equipGrid,
                    rowData : [
                        {id:"T211001", year:"2021", order:"1", type:"지방도", no:"903", name:"조마면_조마로", dir:"상행", lane:"1", section:"1", cr:"10.6", rutt:"3.97", iri:"2.6", index:"7.045211028622086", latestYear:"2019"},
                        {id:"T211002", year:"2021", order:"2", type:"시도", no:"4", name:"영남제일로", dir:"상행", lane:"2", section:"2", cr:"11.4884", rutt:"7.44", iri:"12.91", index:"6.124890875937497", latestYear:"2019"},
                        {id:"T211003", year:"2021", order:"3", type:"군도", no:"4", name:"벽봉로", dir:"상행", lane:"3", section:"3", cr:"1.62572", rutt:"4.23", iri:"3.38", index:"4.999832666634824", latestYear:"2018"},
                        {id:"T211004", year:"2021", order:"4", type:"리도", no:"201", name:"수도길", dir:"상행", lane:"4", section:"4", cr:"14.6597", rutt:"2.72", iri:"8.05", index:"6.701932104127495", latestYear:"2018"}
                    ]
                });
                break;
            case "2020":
                setEquipGrid({
                    ...equipGrid,
                    rowData : [
                        {id:"T200401", year:"2020", order:"1", type:"지방도", no:"916", name:"포아로", dir:"하행", lane:"1", section:"1", cr:"1.7", rutt:"3.55", iri:"6.78", index:"6.068256527329737", latestYear:"2019"},
                        {id:"T200501", year:"2020", order:"1", type:"지방도", no:"997", name:"개령로", dir:"상행", lane:"1", section:"1", cr:"0.5", rutt:"2.5", iri:"3.15", index:"9.264009243043564", latestYear:"2018"},
                        {id:"T200601", year:"2020", order:"1", type:"시도", no:"3", name:"황금로", dir:"상행", lane:"1", section:"1", cr:"2.1", rutt:"8.8", iri:"1.17", index:"8.199409760813861", latestYear:"2019"},
                        {id:"T200702", year:"2020", order:"2", type:"시도", no:"3", name:"양천로", dir:"하행", lane:"2", section:"2", cr:"5.9", rutt:"9.2", iri:"1.59", index:"4.433833735399091", latestYear:"2020"}
                    ]
                });
                break;
            case "2019":
                setEquipGrid({
                    ...equipGrid,
                    rowData : [
                        {id:"T190901", year:"2019", order:"1", type:"시도", no:"30", name:"증산로", dir:"상행", lane:"1", section:"1", cr:"10", rutt:"3.8", iri:"9.54", index:"6.064801972262802", latestYear:"2018"},
                        {id:"T191002", year:"2019", order:"2", type:"시도", no:"4", name:"덕곡로", dir:"상행", lane:"2", section:"2", cr:"6.2", rutt:"5.55", iri:"0.06", index:"7.912350410069344", latestYear:"2019"},
                        {id:"T191103", year:"2019", order:"3", type:"시도", no:"59", name:"용암로", dir:"하행", lane:"3", section:"3", cr:"12.9", rutt:"8.29", iri:"1.74", index:"7.330570124932441", latestYear:"2019"},
                        {id:"T191203", year:"2019", order:"3", type:"군도", no:"10", name:"황악로", dir:"하행", lane:"3", section:"3", cr:"1.4", rutt:"7.22", iri:"3.21", index:"6.904941384571362", latestYear:"2018"}
                    ]
                });
                break;
        }
    }

    const handleEquip2Click = () => {
        switch(selectYear){
            case "all":
                setEquipGrid2({
                    ...equipGrid2,
                    rowData : [
                        {year:"2021", order:"1", id:"D211130", name:"", dir:"", lane:"", sSection:"26", sPoint:"4.864", sPos:"진암삼거리", eSection:"26", ePoint:"9.956", ePos:"상승대삼거리+0.060", lane:"4", length:"5.092", _cr:"Y", hwd:"Y", gpr:"", horiLine:"23.78", inLine:"91.41", cr:"13.94", totalCr:"21.17", sag:"0.11", vi:"5", etc:"0", ascon:"0", index:"", equip1Id:"T211001"},
                        {year:"2020", order:"1", id:"D201131", name:"", dir:"", lane:"", sSection:"26", sPoint:"10.9", sPos:"나래삼거리+0.470", eSection:"26", ePoint:"13.9", ePos:"훅석삼거리", lane:"4", length:"3.086", _cr:"Y", hwd:"", gpr:"", horiLine:"25", inLine:"177.28", cr:"13.17", totalCr:"20.5", sag:"0.22", vi:"3", etc:"0", ascon:"0", index:"", equip1Id:"T200501"},
                        {year:"2019", order:"1", id:"D191135", name:"", dir:"", lane:"", sSection:"27", sPoint:"2.135", sPos:"동남아파트", eSection:"27", ePoint:"4.645", ePos:"응암휴게소입구+0.590", lane:"4", length:"2.51", _cr:"Y", hwd:"Y", gpr:"", horiLine:"25", inLine:"119.19", cr:"10.32", totalCr:"10.46", sag:"0.08", vi:"2", etc:"0", ascon:"0", index:"", equip1Id:"T191103"}
                    ]
                });
                break;
            case "2021":
                setEquipGrid2({
                    ...equipGrid2,
                    rowData : [
                        {year:"2021", order:"1", id:"D211130", name:"", dir:"", lane:"", sSection:"26", sPoint:"4.864", sPos:"진암삼거리", eSection:"26", ePoint:"9.956", ePos:"상승대삼거리+0.060", lane:"4", length:"5.092", _cr:"Y", hwd:"Y", gpr:"", horiLine:"23.78", inLine:"91.41", cr:"13.94", totalCr:"21.17", sag:"0.11", vi:"5", etc:"0", ascon:"0", index:"", equip1Id:"T211001"}
                    ]
                });
                break;
            case "2020":
                setEquipGrid2({
                    ...equipGrid2,
                    rowData : [
                        {year:"2020", order:"1", id:"D201131", name:"", dir:"", lane:"", sSection:"26", sPoint:"10.9", sPos:"나래삼거리+0.470", eSection:"26", ePoint:"13.9", ePos:"훅석삼거리", lane:"4", length:"3.086", _cr:"Y", hwd:"", gpr:"", horiLine:"25", inLine:"177.28", cr:"13.17", totalCr:"20.5", sag:"0.22", vi:"3", etc:"0", ascon:"0", index:"", equip1Id:"T200501"}
                    ]
                });
                break;
            case "2019":
                setEquipGrid2({
                    ...equipGrid2,
                    rowData : [
                        {year:"2019", order:"1", id:"D191135", name:"", dir:"", lane:"", sSection:"27", sPoint:"2.135", sPos:"동남아파트", eSection:"27", ePoint:"4.645", ePos:"응암휴게소입구+0.590", lane:"4", length:"2.51", _cr:"Y", hwd:"Y", gpr:"", horiLine:"25", inLine:"119.19", cr:"10.32", totalCr:"10.46", sag:"0.08", vi:"2", etc:"0", ascon:"0", index:"", equip1Id:"T191103"}
                    ]
                });
                break;
        }
    }
    
    useEffect(() => {
        setGrid(equipGrid);
    }, [equipGrid]);

    useEffect(() => {
        setGrid(equipGrid2);
    }, [equipGrid2]);

    useEffect(()=>{
        changeGrid();
    }, [grid]);

    const handleSwitch = () => {
        setMasterSwitch(!masterSwitch);
    }

    const passToggle = () =>{
        toggle();
    }

    useEffect(() => {
        changeGrid();
    }, [masterSwitch])
    
    const changeGrid = () => {
        if(masterSwitch){
            setContent(
                <>
                <GridComponent columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} style={{width:"50%", height:"70vh", display:"inline-block", padding:"3px"}} />
                <GridComponent columnDefs={grid.columnDefs} rowData={grid.rowData} style={{width:"50%", height:"70vh", display:"inline-block", padding:"3px"}} />
                </>
        );
        }else{
            setContent(<GridComponent columnDefs={grid.columnDefs} rowData={grid.rowData} style={{width:"100%", height:"70vh", display:"inline-block"}} />);
        }
    }

    return <>
        <ModalBody>
            <span>
                <SearchDropdown options={planYear} placeholder="연도 선택" onChange={handleChange}/>
                <Button size="sm" color="secondary" onClick={handleEquipClick}>1차 조사</Button>
                <Button size="sm" color="secondary" onClick={handleEquip2Click}>2차 조사</Button>
            </span>
            <span>
                <SwitchButton label="마스터" isOpen={masterSwitch} onChange={handleSwitch} name="masterSwitch" />
            </span>
            <div className="ag-theme-balham" style={{marginTop: "5px", height:"70vh"}}>
                {content}
            </div>
        </ModalBody>
        <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value="1,000"/>Km
            </span>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                <Button color="primary">구간확인</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </>;
}

export default Master;