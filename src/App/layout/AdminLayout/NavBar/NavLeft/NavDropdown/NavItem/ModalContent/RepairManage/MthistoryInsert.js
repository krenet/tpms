import React, {useState, useRef, useEffect} from 'react';
import { Button, ModalBody, ModalFooter, InputGroup, Label, Input, InputGroupAddon, Form } from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import "./MthistoryInsert.css";
import PaveMathodXML from '../../../../../../../../../resource/settingXml/repair-pave-method.xml';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import serviceUrl from  '../../../../../../../../../request-url-list';

const MthistoryInsert = ({toggle}) => {

    const [targetList, setTargetList] = useState([]);
    const [paveTypeList, setPaveTypeList] = useState([]);
    const [methodTypeList, setMethodTypeList] = useState([]);
    const [methodList, setMethodList] = useState([]);
    const [materialList, setMaterialList] = useState([]);
    const [paveMethodList, setPaveMathodList] = useState();
    const [selectedOption, setSelectedOption] = useState({ paveType: -1, methodType: -1, method: -1 });
    const [labelResetList, setLabelResetList] = useState({
        paveType: false,
        methodType: false,
        method: false,
        material: false
    });
    const [paveReadOnly, setPaveReadOnly] = useState(true);
    const [selectInxList, setSelectInxList] = useState({ paveType: -1, methodType: -1, method: -1, material: -1 });
    const [settingFlag, setSettingFlag] = useState(false);
    const [targetObj, setTargetObj] = useState({});
    const [paveType, setPaveType] = useState();
    const [methodType, setMethodType] = useState();
    const [method, setMethod] = useState();
    const [material, setMaterial] = useState();

    const save = async(evt) => {
        evt.preventDefault();
        var obj = targetObj;

    if(!obj.hasOwnProperty("date") || obj["date"]==""){
        alert("보수일자를 입력해주세요.");
        document.getElementById("date").focus();
        return;
    }
    obj["pave_type"] = paveType;
    obj["method_type"] = methodType;
    obj["method"] = method;
    obj["pave_meterial"] = material;
    
    var result = await server_connect(serviceUrl["pavement"]["insertRepairData"], obj);
    if(result && result["status"]==200){
        if(result['data']['res']==1) alert("보수 완료 등록 성공");
        else alert("보수 완료 등록 실패");
    }
}
const passToggle = () => toggle();

useEffect(()=>{
    let ignore = false;
    async function fetchData(){
        const parsingXml = await XmlParser(PaveMathodXML);
        var result = await server_connect(serviceUrl["pavement"]["getIncompleteRepairTargetId"]);
        if(result && result['status']==200){
            setTargetList(
                result["data"]["res"].map((targetId)=>{
                    return {label: targetId, value: targetId};
                })
            );
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
        if(!ignore) setPaveMathodList(parsingXml);
    }
    fetchData();
    disabledInputChange(true, true, true, true);
    return () => ignore = true; 
}, []);

useEffect(()=>{
    if(paveMethodList){
        var paveType = paveMethodList.getElementsByTagName("pave_type");
        var paveTypeOptions = makeOptions(paveType);
        setPaveTypeList(paveTypeOptions);
    }
}, [paveMethodList]);

useEffect(()=>{
    if(settingFlag){
        var methodTypeInx;
        methodTypeList.map((obj, inx)=>{
            if(obj["label"]==targetObj["method_type"]){
                methodTypeInx = inx;
                return;
            }
        });
        setSelectInxList({ ...selectInxList, methodType: methodTypeInx });
        methodTypeOnChange({ label: targetObj["method_type"], value: methodTypeInx });
    }
}, [methodTypeList]);

useEffect(()=>{
    if(settingFlag && methodList.length>0){
        var methodInx;
        methodList.map((obj, inx)=>{
            if(obj["label"]==targetObj["method"]){
                methodInx = inx;
                return;
            }
        });
            setSelectInxList({ ...selectInxList, method: methodInx });
            setMaterialList([]);

        methodOnChange({ label: targetObj["method"], value: methodInx });
    }
}, [methodList]);

useEffect(()=>{
    if(settingFlag){
        if(targetObj.hasOwnProperty("split_thin")) document.getElementById("split").value = parseInt(targetObj["split_thin"]);
        else document.getElementById("split").value = 0;
        if(targetObj.hasOwnProperty("layer_reinforce")) document.getElementById("reinforce").value = parseInt(targetObj["layer_reinforce"]);
        else document.getElementById("reinforce").value = 0;
        if(targetObj.hasOwnProperty("surface_thin")) document.getElementById("surface").value = parseInt(targetObj["surface_thin"]);
        else document.getElementById("surface").value = 0;
        if(targetObj.hasOwnProperty("comment")) document.getElementById("comment").value = targetObj["comment"];
        if(targetObj.hasOwnProperty("price")) document.getElementById("price").value = parseInt(targetObj["price"]);
        
        if(materialList.length>0){
            var materialInx;
            materialList.map((obj, inx)=>{
                if(obj["label"]==targetObj["pave_material"]){
                    materialInx = inx;
                    return;
                }
            });

            if(selectInxList["material"]!=materialInx) {
                setSelectInxList({ ...selectInxList, material: materialInx });
            }
        }
        setSettingFlag(false);
    }
}, [materialList]);

const disabledInputChange = (pave, split, reinforce, surface) => {
    setPaveReadOnly(pave);
    document.getElementById("split").disabled = split;
    document.getElementById("reinforce").disabled = reinforce;
    document.getElementById("surface").disabled = surface;
    document.getElementById("split").value = '';
    document.getElementById("reinforce").value = '';
    document.getElementById("surface").value = '';
}

const paveTypeOnChange = (obj) => {
    setPaveType(obj["label"]);
    if(!settingFlag) disabledInputChange(true, true, true, true);
    
    setSelectedOption({ ...selectedOption, paveType: obj["value"] });
    setLabelResetList({ paveType: false, methodType: true, method: true, material: true });
    var methodTypeList = paveMethodList.getElementsByTagName("pave_type")[obj["value"]].getElementsByTagName("type");
    
    var methodTypeOptions = makeOptions(methodTypeList);
    setMethodTypeList(methodTypeOptions);

    setMethodList([]);
    setMaterialList([]);
}

const methodTypeOnChange = (obj) => {
    setMethodType(obj["label"]);

    if(!settingFlag) disabledInputChange(true, true, true, true);
    setSelectedOption({ ...selectedOption, methodType: obj["value"] });
    setLabelResetList({ paveType: false, methodType: false, method: true, material: true });
    var methodTypes = paveMethodList.getElementsByTagName("pave_type")[selectedOption["paveType"]].
                        getElementsByTagName("type")[obj["value"]];

    var methodList;
    if(methodTypes.hasOwnProperty("children")){
        methodList = methodTypes["children"];
        var methodOptions = makeOptions(methodList);
        setMethodList(methodOptions);
    }
    

    
}

const methodOnChange = (obj) => {
    setMethod(obj["label"]);
    // setTargetObj({ ...targetObj, method : obj["label"] });

    setSelectedOption({ ...selectedOption, method: obj["value"] });

    var methodObj = paveMethodList.getElementsByTagName("pave_type")[selectedOption["paveType"]]
    .getElementsByTagName("type")[selectedOption["methodType"]]["children"][obj["value"]];

    var methodAttrs = methodObj["attributes"];
    disabledInputChange((methodAttrs["pave"]!="true"), (methodAttrs["split"]!="true"), (methodAttrs["reinforce"]!="true"), (methodAttrs["surface"]!="true"));

    if(methodObj["children"].length>0){
        var methodOptions = makeOptions(methodObj["children"]);
        setMaterialList(methodOptions);
    }
}

const makeOptions = (list) => {
    return list.map((obj, inx)=>{
        return {label: obj["attributes"]["title"], value: inx};
    });
}

const targetChangeEvt = (obj) => {
    // setTargetObj({ ...targetObj, target_id: obj["label"] });
    getRepairTargetInfo(obj["label"]);
}

const getRepairTargetInfo = async (target_id) => {
    var param = new Object();
    param["target"] = target_id;
    var result = await server_connect(serviceUrl["pavement"]["selectTargetInfoByTargetId"], param);
    if(result["status"]==200){
        setSettingFlag(true);
        result = result["data"]["res"];
        if(targetObj && targetObj["repair_target_id"]==result["repair_target_id"]) return;
        resetInput();
        setTargetObj(result);
        var paveType = result["pave_type"];

        var paveTypeInx;
        paveTypeList.map((obj, inx)=>{
            if(obj["label"]==paveType){
                paveTypeInx = inx;
                return;
            }
        });
        setSelectInxList({ ...selectInxList, paveType: paveTypeInx });
        paveTypeOnChange({ label: paveType, value: paveTypeInx});
    }
}

const resetInput = () => {
    document.getElementById("date").value = '';
    document.getElementById("sigongsa").value = '';
    document.getElementById("sigong_pm").value = '';
    document.getElementById("gamrisa").value = '';
    document.getElementById("gamrisa_pm").value = '';
    document.getElementById("gamriwon").value = '';
    document.getElementById("gamriwon_pm").value = '';
}

const resetLabelStateEvt = () => setLabelResetList({ paveType: false, methodType: false, method: false, material: false });

const dateChangeEvt = (e) => setTargetObj({ ...targetObj, date : e.target.value });

const materialOnChange = (obj) => setMaterial(obj["label"]);
const splitChange = (e) => setTargetObj({ ...targetObj, split_thin : e.target.value });
const reinforceChange = (e) => setTargetObj({ ...targetObj, layer_reinforce : e.target.value });
const surfaceChange = (e) => setTargetObj({ ...targetObj, surface_thin : e.target.value });
const commentChange = (e) => setTargetObj({ ...targetObj, comment : e.target.value });
const priceChange = (e) => setTargetObj({ ...targetObj, price : e.target.value });
const sigongsaChange = (e) => setTargetObj({ ...targetObj, sigongsa : e.target.value });
const sigongPmChange = (e) => setTargetObj({ ...targetObj, sigong_pm : e.target.value });
const gamrisaChange = (e) => setTargetObj({ ...targetObj, gamrisa : e.target.value });
const gamrisaPmChange = (e) => setTargetObj({ ...targetObj, gamrisa_pm : e.target.value });
const gamriwonChange = (e) => setTargetObj({ ...targetObj, gamriwon : e.target.value });
const gamriwonPmChange = (e) => setTargetObj({ ...targetObj, gamriwon_pm : e.target.value });

return (
    <>
        <Form onSubmit={save}>
        <ModalBody>
            <div className="body-container">
                <div className="row-area">
                    <Label className="mthistory-label">구간 ID</Label>
                    <SearchDropdown options={targetList} placeholder="ID 선택" width="375" onChange={targetChangeEvt}/> {/* onChange={roadRankOnChangeEvt} readOnly="true" */}
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">보수일자</Label>
                    <Input type="date" className="mthistory-text-input" id="date" onChange={dateChangeEvt}/>
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">포장종류</Label>
                    <SearchDropdown options={paveTypeList} placeholder="포장종류 선택"  width="375" onChange={paveTypeOnChange} resetLabel={labelResetList["paveType"]} resetStateEvt={resetLabelStateEvt} selectInx={selectInxList["paveType"]} /> 
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">공법구분</Label>
                    <SearchDropdown options={methodTypeList} placeholder="공법종류 선택"  width="185" onChange={methodTypeOnChange} resetLabel={labelResetList["methodType"]} resetStateEvt={resetLabelStateEvt}  selectInx={selectInxList["methodType"]}/> 
                    <SearchDropdown options={methodList} placeholder="공법 선택"  width="185" onChange={methodOnChange} resetLabel={labelResetList["method"]} resetStateEvt={resetLabelStateEvt}  selectInx={selectInxList["method"]}/> 
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">포장재료</Label>
                    <SearchDropdown options={materialList} placeholder="포장재료 선택"  width="375" onChange={materialOnChange} resetLabel={labelResetList["material"]} resetStateEvt={resetLabelStateEvt} readOnly={paveReadOnly}  selectInx={selectInxList["material"]}/> 
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">절삭두께</Label>
                    <Input type="number" className="mthistory-number-input" id="split" onChange={splitChange} /><span className="cm-label">Cm</span>
                    <Label className="mthistory-label" style={{width: "60px"}}>기층보강</Label>
                    <Input type="number" className="mthistory-number-input" id="reinforce" onChange={reinforceChange} /><span className="cm-label">Cm</span>
                    <Label className="mthistory-label" style={{width: "60px"}}>표층두께</Label>
                    <Input type="number" className="mthistory-number-input" id="surface" onChange={surfaceChange} /><span className="cm-label">Cm</span>
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">공법설명</Label>
                    <Input type="text" className="mthistory-text-input" id="comment" onChange={commentChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">비용(백만원)</Label>
                    <Input type="number" className="mthistory-price-input" id="price" onChange={priceChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">시공사</Label>
                    <Input type="text" className="mthistory-text-input" id="sigongsa" onChange={sigongsaChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">시공담당자</Label>
                    <Input type="text" className="mthistory-text-input" id="sigong_pm" onChange={sigongPmChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">감리사</Label>
                    <Input type="text" className="mthistory-text-input" id="gamrisa" onChange={gamrisaChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">감리담당자</Label>
                    <Input type="text" className="mthistory-text-input" id="gamrisa_pm" onChange={gamrisaPmChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">감리원</Label>
                    <Input type="text" className="mthistory-text-input" id="gamriwon" onChange={gamriwonChange} />
                </div>
                <div className="row-area">
                    <Label className="mthistory-label">감리원담당자</Label>
                    <Input type="text" className="mthistory-text-input" id="gamriwon_pm" onChange={gamriwonPmChange} />
                </div>
            </div>
        </ModalBody>
        <ModalFooter style={{justifyContent:"center", display:"flex"}} >
            <Button style={{flex:1}} color="info" type="submit">보수완료</Button>
            <Button style={{flex:1}} color="secondary" onClick={passToggle}>닫기</Button>
        </ModalFooter>
        </Form>
    </>
);
}

export default MthistoryInsert;