import React, { useState, useEffect, useRef } from 'react';
import { ModalBody, ModalFooter, Label, Input, Button } from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import GridComponent from '../../../../../../../../components/GridComponent';
import SwitchButton from '../../../../../../../../components/SwitchButton';
import TpmsView from '../../../../../../../../components/TpmsView';
import '../../../../../../../../components/GridComponent.css';
import { XmlParser, server_connect } from '../../../../../../../../../tpms-common';
import SchedulerXML from '../../../../../../../../../resource/settingXml/road-scheduler.xml';
import DegreeXML from '../../../../../../../../../resource/settingXml/road-sc-degree.xml';
import './Planner.css';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
//import ExportCsv from './ExportCsv';
import serviceUrl from  '../../../../../../../../../request-url-list';

const CycleSetting = React.lazy(() => import('./CycleSetting'));
const MapConfirm = React.lazy(() => import('./MapConfirm'));
const Planner = ({ toggle, onOpenChat, onCloseChat }) => {
    const cycleSetting = () => setSubModal(<CycleSetting isOpen={true} setData={setData}  onClose={subModalCloseEvt}/>);
    // const MasterPlannerCsv = () => setSubModal(<ExportCsv isOpen={true}  />);
    const mapConfirm = (masterNodes, scNodes, header) =>
        setSubModal(<MapConfirm isOpen={true} onClose={subModalCloseEvt} onCloseParent={passToggle} allMaster={allRows} allScRows={allScRows} selectedMaster={masterNodes}
            scNodes={scNodes} header={header} hideModal={hideModal} />);
    const [data, setData] = useState();
    const [subModal, setSubModal] = useState(null);
    const [content, setContent] = useState();
    const [stateGrid, setStateGrid] = useState({});
    const [equipGrid, setEquipGrid] = useState({});
    const [degrees, setDegrees] = useState();
    const [masterSwitch, setMasterSwitch] = useState(false);
    const [tableViewSwitch, setTableViewSwitch] = useState(false);
    const [metaData, setMetaData] = useState();
    const [columns, setColumns] = useState([]);
    const [selectExt, setSelectExt] = useState('0');
    const [dMetaData, setDMetaData] = useState();
    const [dColumns, setdColumns] = useState([]);
    const [viewExtension, setViewExtension] = useState();
    const [allRows, setAllRows] = useState({});
    const [masterGridApi, setMasterGridApi] = useState(null);
    const [allScRows, setAllScRows] = useState(null);
    const [updateList, setUpdateList] = useState();
    const [gridAreaStyle, setGridAreaStyle] = useState(
        { marginTop: "5px", height: "70vh", display:"flex", flex:1, flexDirection:"row"}
    );
    const [server, setServer] = useState(false);

    const subModalCloseEvt = () => {
        setSubModal(null);
    }
    
    const degree = useRef();
    const passToggle = () => {
        onCloseChat();
        toggle();
    }

    const TableViewHandleSwitch = () => {
        setTableViewSwitch(!tableViewSwitch);

    }
    useEffect(() => {
        changeView();
    }, [tableViewSwitch])

    //마스터 스위치 클릭시 실행되는 함수
    const handleSwitch = () => {
        setMasterSwitch(!masterSwitch);
    }
    //마스터 스위치 클릭하고 masterSwitch 변경되면 changeGrid 실행
    useEffect(() => {
        setSelectExt('0');
        changeGrid();
        
    }, [masterSwitch])

    const selSectionConfirm = () => {
        var props = [];
        var header = [
            {
                label: 'rank',
                name: '등급'
            },
            {
                label: 'no',
                name: '노선번호'
            },
            {
                label: 'name',
                name: '노선명'
            },
            {
                label: 'dir',
                name: '행선'
            },
            {
                label: 'node_name',
                name: '이벤트명'
            },
            {
                label: 'node_type',
                name: '이벤트 종류'
            }
        ];

        var masterNodes;
        var scNodes;
        if (schedularGridApi) scNodes = schedularGridApi.getSelectedNodes();
        if (masterGridApi) {
            masterNodes = masterGridApi.getSelectedNodes();
            if (masterNodes.length > 0 && scNodes.length > 0) {
                mapConfirm(masterNodes, scNodes, header);
            } else if (masterNodes.length > 0 && scNodes.length == 0) {
                props['allData'] = allRows;
                props['selectedNodes'] = masterNodes;
                props['header'] = header;
                props['action'] = 'OPEN_CHAT';
                onOpenChat(props);
                hideModal();
            } else if (masterNodes.length == 0 && scNodes.length > 0) {
                props['header'] = header;
                props['allScRows'] = allScRows;
                props['scNodes'] = scNodes;
                props['action'] = 'OPEN_CHAT_SCHEDULAR';
                onOpenChat(props);
                hideModal();
            } else {
                alert('확인하실 구간을 먼저 선택해 주시기 바랍니다.');
            }
        } else {
            if (scNodes.length > 0) {
                props['header'] = header;
                props['allScRows'] = allScRows;
                props['scNodes'] = scNodes;
                props['action'] = 'OPEN_CHAT_SCHEDULAR';
                onOpenChat(props);
                hideModal();
            } else {
                alert("확인하실 구간을 먼저 선택해 주세요.");
            }
        }
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display = 'none';
        document.getElementsByClassName('modal-backdrop')[0].style.display = 'none';
        document.getElementsByClassName('modal-dialog')[0].style.display = 'none';
    }

    const [stateData, setStateData] = useState();
    useEffect(() => {
        changeGrid();
        if (stateGrid.rowData == null && stateData && stateGrid.rowClassRules == null) {
            setStateGrid({
                ...stateGrid,
                rowData: stateData,
                rowClassRules: {
                    'included-row-in-schedular': function (params) {
                        var ts_id = params.data.ts_id;
                        var chk = false;
                        for (var i = 0; i < updateList.length; i++) {
                            var schedule_id = updateList[i]['ts_id'];
                            if (schedule_id == ts_id) {
                                chk = true;
                            }
                        }
                        return chk;
                    }
                }
            });
        }
    }, [equipGrid, stateGrid]);

    //xmlParser
    useEffect(() => {

        let ignore = false;
        async function fetchData() {
            const result_scMaster = await XmlParser(SchedulerXML);
            const result_degreeMaster = await XmlParser(DegreeXML);
            if (!ignore) {
                setMetaData(result_scMaster);
                setDMetaData(result_degreeMaster);
            }
        }
        fetchData();
        return () => {
            ignore = true;
        }
    }, []);

    useEffect(() => {
        if (metaData) setColumns(metaData.getElementsByTagName('Columns')[0]['children']);
    }, [metaData]);

    useEffect(() => {
        if (dMetaData) setdColumns(dMetaData.getElementsByTagName('Columns')[0]['children']);
    }, [dMetaData]);
    //xmlParser
    
    const [masterIsSelect, setMasterIsSelect]=useState(false);
    const [planIsSelect, setPlanIsSelect]=useState(false);
    // road-master.xml 변경에 따라, 그리드 컬럼 자동 변경
    useEffect(() => {
        if(columns.length < 1) return;
        var prev;
        var temp = columns.map((obj, index) => {
            if (!(prev) || obj['attributes']['className'] != prev) {
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        temp = temp.filter(function (el) {
            return el != null;
        });

        temp = temp.map((obj) => {
            return {
                headerName: obj,
                children:
                    columns.map((col) => {
                        var attr = col['attributes'];
                        if (attr['className'] == obj && col['name'] == "no") {
                            return {
                                headerName: attr['title'],
                                field: col['name'],
                                colSpan: function (params) {
                                    if (isSpan(params)) {
                                        return 17;
                                    } else {
                                        return 1;
                                    }
                                },
                                cellStyle: { textAlign: 'center' },
                            }
                        } else if (attr['className'] == obj && col['name'] != "no") {
                            return { headerName: attr['title'], field: col['name'] }
                        }
                    }).filter(function (el) {
                        return el != null;
                    })
            }
        });
        async function list() {
            setStateGrid({
                ...stateGrid,
                columnDefs: temp,
                rowData:null
            });
            const result = await server_connect(serviceUrl["pavement"]["selectSmMaster"]);
            if(result && result['status']==200){
                const list = result['data']['res'];
                if (list.length > 0) {
                    var dataList = list.map((obj) => {
                        return {
                            ts_id: obj['ts_id'], rank: obj['rank'], rank_code: obj['rank_code'],
                            no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'],
                            dir: obj['dir'], lanes: obj['lane'], sector: obj['sector'], section: obj['section'],
                            sm_ext: obj['sm_ext'], ext_sum: obj['ext_sum'],
                            node_name: obj['node_name'], node_type: obj['node_type'],
                        }
                    });
                    var MasterData = planDataSort(dataList);
                    setAllRows(list);
                    setStateData(dataList);
    
                    setStateGrid({
                        columnDefs: temp,
                        rowSelection: 'multiple',
                        rowData: MasterData,
                        onSelectionChanged: function (e) {
                            var selectedRows = e.api.getSelectedNodes();
                            if(selectedRows.length == 0) return;
                            setPlanIsSelect(false);
                            setMasterIsSelect(true);
                            var extSum = 0;
                            selectedRows.map((row) => {
                                var selectedRowExt = row['data']['sm_ext'];
                                extSum = extSum + selectedRowExt;
                            });
                            setSelectExt((extSum / 1000).toFixed(2));
                        },
                    });
                } else {
                    setStateGrid({
                        columnDefs: temp,
                        rowSelection: 'multiple',
                        rowData: [
                            {
                                ts_id: "",
                                rank: "", rank_code: "", no: "", name: "", main_num: "",
                                sub_num: "", dir: "", lanes: "", sector: "", section: "",
                                sm_ext: "", ext_sum: "",
                                node_name: "", node_type: "",
                            }
                        ],
                        onSelectionChanged: function (e) {
                            setSelectExt('-');
                        },
                        rowClassRules: null
                    });
                }
            }else{
                alert('서버 통신 실패. 관리자에게 문의하세요.');
                return;
            }
        }
        
        list();   
        function isSpan(params) {
            return params.data.span === 'no-result';
        }
    }, [columns]);

    useEffect(()=>{
        if(masterIsSelect){
            schedularGridApi.deselectAll();
        }
        if(planIsSelect){
            masterGridApi.deselectAll();
        }
    },[masterIsSelect, planIsSelect]);

    // road-sc-degree.xml 변경에 따라, 그리드 컬럼 자동 변경
    useEffect(() => {
        var prev;
        var temp = dColumns.map((obj, index) => {
            if (!(prev) || obj['attributes']['className'] != prev) {
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        temp = temp.filter(function (el) {
            return el != null;
        });

        temp = temp.map((obj) => {
            return {
                headerName: obj,
                children:
                    dColumns.map((col) => {
                        var attr = col['attributes'];
                        if (attr['className'] == obj && col['name'] == "no") {
                            return {
                                headerName: attr['title'],
                                field: col['name'],
                                colSpan: function (params) {
                                    if (isSpan(params)) {
                                        return 17;
                                    } else {
                                        return 1;
                                    }
                                },
                                cellStyle: { textAlign: 'center' },
                            }
                        } else if (attr['className'] == obj && col['name'] != "no") {
                            return { headerName: attr['title'], field: col['name'] }
                        }
                    }).filter(function (el) {
                        return el != null;
                    })
            }
        });
        setEquipGrid({
            columnDefs: temp,
            rowSelection: 'multiple',
            rowData: [
                {
                    ts_id: "", degree: "",
                    rank: "", rank_code: "", no: "", name: "", main_num: "", sub_num: "",
                    dir: "", lanes: "", sector: "", section: "",
                    extension: "", ext_sum: "",
                    node_name: "", node_type: "",
                }
            ],
            onSelectionChanged: function (e) {
                setSelectExt('-');
            },
        });
        function isSpan(params) {
            return params.data.span === 'no-result';
        }
    }, [dColumns]);


    useEffect(() => {
        async function fetchData() {
            const result = await server_connect(serviceUrl["pavement"]["selectScDgreeNewVersion"]);
            if(result && result['status']==200){
                let degree = new Array();
                degree.push({ label: "전체", value: -1 });
                for (var i = 1; i <= result['data']['res']['degree']; ++i) {
                    degree.push({ "label": i + "차", "value": i });
                }
                setDegrees(degree);
            }
        }
        fetchData();

        if (data) {
            let options = new Array();
            for (var i = 1; i <= data["degree"]; ++i) {
                options.push({ "label": i + "차", "value": i });
            }
            setDegrees(options);
        }
    }, [data]);
    
    const handleEquip2Click = async () => {
        setSelectExt('0');
    
        if (degree.current.state.value == null) {
            alert('차수를 선택하세요.');
        } else {
            setAllScRows(null);
            var params = new Object();
            var data = degree.current.state.value.value;
            params['degree'] = data;
            const result = await server_connect(serviceUrl["pavement"]["selectDegreeMaster"], params);
            const List = result['data']['res'];
            if (List.length > 0) {
                var dataList = List.map((obj) => {
                    return {
                        ts_id: obj['ts_id'], degree: obj['degree'], rank: obj['rank'], rank_code: obj['rank_code'],
                        no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'],
                        dir: obj['dir'], lanes: obj['lane'], sector: obj['sector'], section: obj['section'],
                        extension: obj['sm_ext'], ext_sum: obj['ext_sum'],
                        node_name: obj['node_name'], node_type: obj['node_type'],
                    }
                });
                // var plannerList = planDataSort(dataList);
                setAllScRows(dataList);
                setUpdateList(dataList);
                setEquipGrid({
                    ...equipGrid,
                    rowData: dataList,
                    onSelectionChanged: function (e) {
                        var selectedRows = e.api.getSelectedNodes();
                        if(selectedRows.length == 0) return;
                        setMasterIsSelect(false);
                        setPlanIsSelect(true);
                        var extSum = 0;
                        selectedRows.map((row) => {
                            var selectedRowExt = row['data']['extension'];
                            extSum = extSum + selectedRowExt;
                        });
                        setSelectExt((extSum / 1000).toFixed(2));
                    }
                });
               

            } else {
                setEquipGrid({
                    ...equipGrid,
                    rowData: [
                        { span: 'no-result', no: "검색 결과가 없습니다" }
                    ],
                    onSelectionChanged: function (e) {
                        setSelectExt('-');
                    },
                });
                setUpdateList([]);
            }

            setStateGrid({
                ...stateGrid,
                rowData: null,
                rowClassRules: null
            });
        }
    }

    const getMasterGridComponentState = (childState) => {
        setMasterGridApi(childState);
    }

    const planDataSort = (data) => {
        data.sort(function (a, b) {
            var before_no = parseInt(a.no);
            var after_no = parseInt(b.no);
            return a.rank_code.localeCompare(b.rank_code) || before_no - after_no || a.name.localeCompare(b.name)
                || a.sub_num - b.sub_num || a.main_num - b.main_num || a.dir - b.dir;
        });
        return data;
    }

    const [addRemoveItem, setAddRemoveItem] = useState(null);
    const addToSchedularGrid = (e) => {
        if (degree.current.state.value == null || degree.current.state.value.value == -1) {
            alert('해당 차수를 검색하세요.');
        } else {
            if (masterGridApi) {
                var selectedMasterNodes = masterGridApi.getSelectedNodes();
                var scList = new Array();

                for (var i = 0; i < selectedMasterNodes.length; ++i) {
                    if (allScRows) {
                        for (var j = 0; j < allScRows.length; ++j) {
                            if (allScRows[j]['ts_id'] == selectedMasterNodes[i]['data']['ts_id']) {
                                var idx = allScRows.indexOf(allScRows[j]);
                                allScRows.splice(idx, 1);
                            }
                        }
                    }
                    scList.push(selectedMasterNodes[i]['data']);
                }
                var scRowList = null;
                if (allScRows) scRowList = allScRows;
                else scRowList = new Array();
                for (var j = 0; j < scList.length; ++j) {
                    scList[j]['degree'] = degree.current.state.value.value;
                    scRowList.push(scList[j]);
                }

                if (scRowList.length > 0) {
                    var dataList = scRowList.map((obj) => {
                        return {
                            ts_id: obj['ts_id'], degree: obj['degree'], rank: obj['rank'], rank_code: obj['rank_code'],
                            no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'],
                            dir: obj['dir'], lanes: obj['lane'], sector: obj['sector'], section: obj['section'],
                            extension: obj['sm_ext'], ext_sum: obj['ext_sum'],
                            node_name: obj['node_name'], node_type: obj['node_type'],
                        }
                    });

                    setUpdateList(dataList)
                    setStateGrid({
                        ...stateGrid,
                        rowData: null,
                        rowClassRules: null
                    });
                    setEquipGrid({
                        ...equipGrid,
                        rowData: dataList,

                    });
                }
                setAllScRows(scRowList);
                var scAddList = new Array();
                if (scList.length > 0) {
                    for (var i = 0; i < scList.length; ++i) {
                        scAddList.push(scList[i]);
                        scAddList[i]['type'] = 'insert';
                    }
                }
                if (addRemoveItem == null) {
                    setAddRemoveItem(scAddList);
                } else {
                    for (var i = 0; i < scAddList.length; ++i) {
                        addRemoveItem.push(scAddList[i]);
                    }
                }
            }
        }
    }

    const getSchedularGridComponentState = (childState) => {
        setSchedularGridApi(childState);
    }

    const [schedularGridApi, setSchedularGridApi] = useState();
    const delFromSchedularGrid = () => {
        if (schedularGridApi) {
            var selectedSchedularNodes = schedularGridApi.getSelectedNodes();
            var scDelList = new Array();
            var tempScRows = allScRows;
            if (selectedSchedularNodes.length > 0) {
                for (var i = 0; i < selectedSchedularNodes.length; ++i) {
                    scDelList.push(selectedSchedularNodes[i]['data']);
                    for (var k = 0; k < tempScRows.length; ++k) {
                        if (tempScRows[k]['ts_id'] == selectedSchedularNodes[i]['data']['ts_id']) {
                            var idx = tempScRows.indexOf(tempScRows[k]);
                            tempScRows.splice(idx, 1);
                        }
                    }
                }
                setAllScRows(tempScRows);
         
                if(tempScRows.length>0){
                    var dataList = tempScRows.map((obj) => {
                        return {
                            ts_id: obj['ts_id'], degree: obj['degree'], rank: obj['rank'], rank_code: obj['rank_code'],
                            no: obj['no'], name: obj['name'], main_num: obj['main_num'], sub_num: obj['sub_num'],
                            dir: obj['dir'], lanes: obj['lane'], sector: obj['sector'], section: obj['section'],
                            extension: obj['sm_ext'], ext_sum: obj['ext_sum'],
                            node_name: obj['node_name'], node_type: obj['node_type'],
                        }
                    });
                    setEquipGrid({
                        ...equipGrid,
                        rowData: dataList,
                    });
    
                    setUpdateList(dataList);
                }else{
                    setEquipGrid({
                        ...equipGrid,
                        rowData: null,
                    });
                    setUpdateList([]);
                }
                setStateGrid({
                    ...stateGrid,
                    rowData: null,
                    rowClassRules: null
                });
            }
            if (scDelList.length > 0) {
                for (var i = 0; i < scDelList.length; ++i) {
                    scDelList[i]['type'] = 'delete';
                }
            }
            if (addRemoveItem == null) {
                setAddRemoveItem(scDelList);
            } else {
                for (var i = 0; i < scDelList.length; ++i) {
                    addRemoveItem.push(scDelList[i]);
                }
            }
        }

    }

    const degreeScSave = async () => {
        var nodeNameList = new Array();
        for (var i = 0; i < allScRows.length; ++i) {
            if (allScRows[i]['node_name']) {
                nodeNameList.push(allScRows[i]);
            }
        }
        nodeNameList.map(obj => {
            var inx = allScRows.indexOf(obj);
            if (inx > -1) allScRows.splice(inx, 1);
        });
        var groupList = new Object();
        for (var i = 0; i < allScRows.length; ++i) {
            var key = allScRows[i]['rank_code'] + allScRows[i]['no'] + allScRows[i]['name'] + allScRows[i]['main_num'] + allScRows[i]['sub_num'] + allScRows[i]['dir'] + 1;
            if (groupList.hasOwnProperty(key)) {
                var obj = groupList[key];
                obj.push(allScRows[i]);
                groupList[key] = obj;
            } else {
                var objList = new Array();
                objList.push(allScRows[i]);
                groupList[key] = objList;
            }
        }
        var keys = Object.keys(groupList);
        var group_inx = 1;
        for (var i = 0; i < keys.length; ++i) {
            for (var j = 0; j < groupList[keys[i]].length; ++j) {
                groupList[keys[i]][j]['group_num'] = group_inx;
            }
            ++group_inx;
        }
        var params = new Object();
        params['scList'] = encodeURIComponent(JSON.stringify(allScRows));
        var result = await server_connect(serviceUrl["pavement"]["insertSurveySc"], params);
        if(result){
            alert('선택하신 차수에 스케줄이 저장되었습니다.');
        }
    }

    const changeGrid = () => {
        if (masterSwitch) {
            document.getElementById('add_btn').style.display = 'inline-grid';
            document.getElementById('del_btn').style.display = 'inline-grid';
            document.getElementById('saveBtn').style.display = 'inline';
            document.getElementById('masterGrid').style.display = 'flex';
            document.getElementById('masterGrid').style.flexDirection = 'column';
            document.getElementById('masterGrid').style.padding = '3px';
            // setEquipGrid({
            //     ...equipGrid,
            //     // columnDefs : temp,
            //     rowData: null,
            // });
                setGridAreaStyle({
                    ...gridAreaStyle,
                    display: "flex",
                    padding:3
                });

                setContent(
                    <>
                        <GridComponent columnDefs={equipGrid.columnDefs} rowData={equipGrid.rowData} onSelectionChanged={equipGrid.onSelectionChanged} style={{ width: "50%", height: "70vh", display: "flex", flexDirection:"column", padding:3, textAlign:'center'}} passStateEvt={getSchedularGridComponentState} gridTitle={'조사 플래너'}/>
                    </>
                );
            
            if (tableViewSwitch) {
                document.getElementById('add_btn').style.display = 'none';
                document.getElementById('del_btn').style.display = 'none';
                document.getElementById('saveBtn').style.display = 'none';
                document.getElementById('mapView').style.display = 'inline';
                document.getElementById('masterGrid').style.padding = '3px';
                setGridAreaStyle({
                    ...gridAreaStyle,
                    display: "flex"        
                });
                setContent(
                    <>
                        <div style={{flex:1, display:'flex', flexDirection:'column', padding:3, height:'70vh'}}>
                            <div className="grid-title">View</div>
                            <div style={{overflow:'auto' , border: '1px solid #BDC3C7', padding:10}}>
                                <TpmsView dataSet={viewList}  />
                            </div>
                        </div>
                    </>
                );
            }
        } else {
            document.getElementById('add_btn').style.display = 'none';
            document.getElementById('del_btn').style.display = 'none';
            document.getElementById('saveBtn').style.display = 'none';
            document.getElementById('mapView').style.display = 'inline';
            document.getElementById('masterGrid').style.display = 'none';
            setContent(<GridComponent columnDefs={equipGrid.columnDefs} onSelectionChanged={equipGrid.onSelectionChanged} rowData={equipGrid.rowData} style={{ width: "100%", height: "70vh", display: "flex", flexDirection:'column', textAlign:'center' }} passStateEvt={getSchedularGridComponentState} gridTitle={'조사플래너'}/>);
            if (tableViewSwitch) {
                setGridAreaStyle({
                    ...gridAreaStyle,
                    display: "block"        
                });
                document.getElementById('mapView').style.display = 'none';
                setContent(
                    <>
                    <div className="grid-title">view</div>
                    <TpmsView dataSet={viewList} style={{ width: "100%", height: "70vh", display: "inline-block", textAlign: "center" }}/></>
                );
            }
        }
    }
    const [viewList, setViewList] = useState();
    const changeView =async () => {
       
        if (tableViewSwitch) {
            var params = new Object();
            params['degree'] = -1;
            const result = await server_connect(serviceUrl["pavement"]["selectDegreeMaster"], params);
            if(result && result['status']==200){
                var scAllList = result['data']['res'];
                var masterList = planDataSort(allRows);
    
                var makeLabelExtList = new Object();
                masterList.map((master)=>{
                    var key = master['rank'] + ' / ' + master['no'] + ' / ' + (master['name']?master['name']:'-') + ' / 주로' 
                            + master['main_num'] + ' / 종속로' + master['sub_num'] + ' / ' + (master['dir']==0?'상행':'하행') + ' / 차선' + 1; //키
                    var tempList = new Array();
                    if(makeLabelExtList.hasOwnProperty(key)){
                        tempList = makeLabelExtList[key];
                    }
                    tempList.push(master);
                    makeLabelExtList[key] = tempList;
                });
    
                var viewList = new Array();
                var keyList = Object.keys(makeLabelExtList);
                
                var viewrank; var viewno; var viewname;
                for(var i=0; i<keyList.length; ++i){
                    var totalExt = 0;
                    makeLabelExtList[keyList[i]].map((obj)=>{
                        totalExt += obj['sm_ext'];
                    makeLabelExtList[keyList[i]].map((obj, inx)=>{
                        if(inx == 0){
                            viewrank = obj['rank'];
                            viewno = obj['no'];
                            viewname = obj['name'];
                        }
                        scAllList.map((scObj)=>{
                            if(scObj['ts_id']==obj['ts_id']) obj['degree'] = scObj['degree'];
                            if(obj.hasOwnProperty('node_name')) obj['type'] = '0105';
                            else obj['type'] = '0104';
                        })
                    });
                  });
                    var addObj = {
                        info : {
                            label : keyList[i],
                            totalExt : totalExt,
                            targetCol : 'degree',
                            // label : keyList[i],
                            rank : viewrank,
                            no : viewno,
                            name : viewname,
                            targetCol : 'degree',
                            extCol : 'sm_ext'
                        },
                        children : makeLabelExtList[keyList[i]]
                    };
                    viewList.push(addObj);
                }
    
                setViewList(viewList);
                document.getElementById('saveBtn').style.display = 'none';
                document.getElementById('mapView').style.display = 'none';
                setGridAreaStyle({
                    ...gridAreaStyle,
                    display: "block"        
                });
    
                setContent(
                    <>
                    <div style={{display:'flex', flexDirection:'column' , flex:1, height:'70vh'}}>
                        <div className="grid-title">View</div>
                        <div style={{overflow:'auto', border: '1px solid #BDC3C7', padding:10}}>
                            <TpmsView dataSet={viewList} style={{ width: "100%", height: "70vh",  textAlign: "center" }} />
                        </div>
                    </div>
                    </>
                    );
                if (masterSwitch) {
                    document.getElementById('add_btn').style.display = 'none';
                    document.getElementById('del_btn').style.display = 'none';
                    document.getElementById('mapView').style.display = 'inline';
                    document.getElementById('masterGrid').style.padding ='3px';
                    setGridAreaStyle({
                        ...gridAreaStyle,
                        display: "flex",
                    });
                    setContent(
                        <>
                            <div style={{display:'flex', flexDirection:'column' , flex:1, padding:3, height:'70vh'}}>
                                <div className="grid-title">View</div>
                                <div style={{overflow:'auto', border: '1px solid #BDC3C7', padding:10}}>
                                    <TpmsView dataSet={viewList}/>
                                </div>
                            </div>
                        </>
                    );
                }
            }
        } else {
            document.getElementById('mapView').style.display = 'inline';
            setContent(<GridComponent columnDefs={equipGrid.columnDefs} rowData={equipGrid.rowData} style={{ width: "100%", height: "70vh", display: "flex", flexDirection:"column", padding:3, textAlign:'center'}} passStateEvt={getSchedularGridComponentState} gridTitle={'조사 플래너'}/>);
            if (masterSwitch) {
                document.getElementById('add_btn').style.display = 'inline-grid';
                document.getElementById('del_btn').style.display = 'inline-grid';
                document.getElementById('saveBtn').style.display = 'inline';
                document.getElementById('mapView').style.display = 'inline';
                setContent(
                    <>
                        <GridComponent columnDefs={equipGrid.columnDefs} rowData={equipGrid.rowData} style={{ width: "50%", height: "70vh", display: "flex", flexDirection:"column", padding:3, textAlign:'center' }} passStateEvt={getSchedularGridComponentState} gridTitle={'조사플래너'}/>
                    </>
                );
            }
        }
    }
  
    // const csvExport = () => {
    //         // console.log('masterGridApi : ',masterGridApi);
    //         // console.log('schedularGridApi : ',schedularGridApi);
    //         console.log(allRows);
    //         console.log(allScRows);
    //         if(allScRows == null){
    //             alert('csv 파일로 내려받을 차수를 선택하세요.');
    //         }else{
    //             schedularGridApi.exportDataAsCsv();
    //         }
    //         // if(allRows.length > 0){
    //         //     masterGridApi.exportDataAsCsv();
    //         // }
    //         // if(allRows.length > 0 && allScRows.length > 0 ){
    //         //     MasterPlannerCsv();
    //         // }
    // }
    return <>
        <ModalBody style={{overflow:'auto'}}>
            <span >
                <SearchDropdown options={degrees} target={degree} placeholder="차수" />
                <Button size="sm" color="secondary" onClick={handleEquip2Click}>검색</Button>
            </span>
            <span style={{ float: "right", margin: "8px" }}>
                <i className="feather icon-settings" onClick={cycleSetting} ></i>
            </span>
            <span>
                <SwitchButton label="마스터" isOpen={masterSwitch} onChange={handleSwitch} name="masterSwitch" />
            </span>
            <span>
                <SwitchButton label="테이블/뷰" isOpen={tableViewSwitch} onChange={TableViewHandleSwitch} name="tableViewSwitch" />
            </span>
            <div className="ag-theme-balham" style={gridAreaStyle}>
                <div id="add_btn"><input type="button" className="moveBtn" value=">>" onClick={addToSchedularGrid} /></div>
                <div id="del_btn"><input type="button" className="moveBtn" value="<<" onClick={delFromSchedularGrid} /></div>
                <GridComponent id="masterGrid" columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} 
                style={{ width: "50%", height: "70vh", display: "none", textAlign:'center' }} passStateEvt={getMasterGridComponentState} gridTitle={'노선 마스터'}
                 rowClassRules={(stateGrid.rowClassRules)?stateGrid.rowClassRules:null}/>
                {content}
            </div>
            {subModal}
        </ModalBody>
        <ModalFooter style={{ justifyContent: "unset" }}>
            <span style={{ width: "100%", justifyContent: "flex-start", fontSize: "15px", fontWeight: "bold", margin: "5px", color: "#3eb5c1" }}>
                <Label style={{ margin: "3px" }}>선택연장</Label>
                <Input type="text" disabled style={{ display: "inline", width: "200px" }} value={selectExt} />Km
            </span>
            <span style={{ width: "100%", justifyContent: "flex-end", textAlign: "right" }}>
                {/* <Button className="feather icon-map" id="mapView" color="primary" onClick={csvExport}> csv내보내끼룩끼이룩</Button> */}
                <Button className="feather icon-map" id="mapView" color="primary" onClick={selSectionConfirm}> 지도확인</Button>
                <Button className="feather icon-save" id="saveBtn" color="primary" onClick={degreeScSave}> 저장</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </>;
}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({ type: actionTypes.OPEN_CHAT, data: props }),
        onCloseChat: () => dispatch({ type: actionTypes.CLOSE_CHAT })
    }
}

export default connect(chatStateToProps, chatDispatchToProps)(Planner);