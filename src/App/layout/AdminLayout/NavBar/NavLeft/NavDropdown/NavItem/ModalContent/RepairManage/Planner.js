import React, { useState, useEffect } from 'react';
import { ModalBody, ModalFooter, Label, Input, Button } from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import GridComponent from '../../../../../../../../components/GridComponent';
import SwitchButton from '../../../../../../../../components/SwitchButton';

const Scheduler = React.lazy(()=> import('./Scheduler'));

const Planner = ({toggle}) => {
    const [subModal, setSubModal] = useState(null);
    const handleScheduleClick = () => setSubModal(<Scheduler isOpen={true}/>);
    const [content, setContent] = useState();
    const [stateGrid, setStateGrid] = useState({
        columnDefs: [
            {
                headerName : '노선정보',
                children : [
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                    {headerName:"구간", field:"section"}    
                ]
            },
            {
                headerName: "연장",
                children : [
                    {headerName:"개별", field:"length"},
                    {headerName:"누적", field:"lenSum"}
                ]
            },
            {headerName:"중용 분리", field:"overlap"},
            {
                headerName: "주소",
                children : [
                    {headerName:"시점", field:"sAddr"},
                    {headerName:"종점", field:"eAddr"}
                ]
            },
            {headerName:"차로수", field:"lanes"},
            {
                headerName: "이벤트",
                children : [
                    {headerName:"명칭", field:"eventName"},
                    {headerName:"종류", field:"eventType"}
                ]
            },
            {headerName:"포장종류", field:"paveType"},
            {
                headerName: "최초포장",
                children : [
                    {headerName:"연도", field:"makeYear"},
                    {headerName:"AC", field:"makeAC"},
                    {headerName:"BB", field:"makeBB"},
                    {headerName:"GB", field:"makeGB"},
                    {headerName:"SB", field:"makeSB"},
                    {headerName:"AFL", field:"makeAFL"}
                ]
            },
            {
                headerName: "최종유지보수",
                children : [
                    {headerName:"연도", field:"latestYear"},
                    {headerName:"공법", field:"latestMethod"},
                    {headerName:"설명", field:"latestDesc"},
                    {headerName:"비용", field:"latestPrice"}
                ]
            },
            {
                headerName: "교통량",
                children : [
                    {headerName:"연도", field:"trafficYear"},
                    {headerName:"지점", field:"trafficPoint"},
                    {headerName:"Total", field:"traffic"}
                ]
            }
        ],
        rowData : [
            { name: "마산로", dir: "상행", lane: "1", section: "1", length: "340", lenSum: "682", overlap: "0", sAddr: "마산로 1구간 시점주소", eAddr: "마산로 1구간 종점주소", lanes: "2", eventName: "지하차도(북측)4", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "마산로", dir: "상행", lane: "1", section: "2", length: "342", lenSum: "682", overlap: "1", sAddr: "마산로 2구간 시점주소", eAddr: "마산로 2구간 종점주소", lanes: "2", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "황악로", dir: "상행", lane: "1", section: "1", length: "340", lenSum: "727", overlap: "22", sAddr: "황악로 1구간 시점주소", eAddr: "황악로 1구간 종점주소", lanes: "2", eventName: "일반국도1호선", eventType: "NR48", paveType: "아스팔트", makeYear: "1989", makeAC: "7", makeBB: "", makeGB: "15", makeSB: "40", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"2018", trafficPoint:"0140-001", traffic:"36,407"},
            { name: "황악로", dir: "상행", lane: "1", section: "2", length: "387", lenSum: "727", overlap: "0", sAddr: "황악로 2구간 시점주소", eAddr: "황악로 2구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "부항로", dir: "상행", lane: "1", section: "1", length: "440", lenSum: "1,215", overlap: "0", sAddr: "부항로 1구간 시점주소", eAddr: "부항로 1구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "부항로", dir: "상행", lane: "1", section: "2", length: "335", lenSum: "1,215", overlap: "3", sAddr: "부항로 2구간 시점주소", eAddr: "부항로 2구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""},
            { name: "부항로", dir: "상행", lane: "1", section: "3", length: "420", lenSum: "1,215", overlap: "0", sAddr: "부항로 3구간 시점주소", eAddr: "부항로 3구간 종점주소", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: ""}
        ]
    });
    const [costGrid, setCostGrid] = useState({
        columnDefs: [
            {
                headerName : '보수 계획',
                children : [
                    {headerName:"연도", field:"year"},
                    {headerName:"차수", field:"order"}    
                ]
            },
            {
                headerName : '노선정보',
                children : [
                    {headerName:"등급", field:"type"},
                    {headerName:"번호", field:"no"},
                    {headerName:"노선명", field:"name"},
                    {headerName:"행선", field:"dir"},
                    {headerName:"차선", field:"lane"},
                    {headerName:"구간", field:"section"}    
                ]
            },
            {headerName:"균열율", field:"cr"},
            {headerName:"소성변형", field:"rutt"},
            {headerName:"평탄성", field:"iri"},
            {headerName:"인덱스", field:"index"},
            {headerName:"최근보수연도", field:"latestYear"},

        ],
        rowData : [
            {year:"", order:"", type:"", no:"", name:"", dir:"", lane:"", section:"", cr:"", rutt:"", iri:"", index:"", latestYear:""}
        ]
    })
    const [grid, setGrid] = useState(costGrid);
    const [selectYear, setSelectYear] = useState();
    const [masterSwitch, setMasterSwitch] = useState(false);
    const planYear = [{label: '전체', value:"all"},{label:'2021', value:"2021"},{label:'2020', value:"2020"},{label:'2019', value:"2019"}];

    const handleChange = (data) =>{
        setSelectYear(data.value);
    }

    const handleCostClick = () => {
        switch(selectYear){
            case "all":
                setCostGrid({
                    ...costGrid,
                    rowData : [
                  {year:"2021", order:"1", type:"지방도", no:"913", name:"묘광길", dir:"상행", lane:"1", section:"1", cr:"11.4884", rutt:"3.97", iri:"5.66", index:"7.045211028622086", latestYear:"2020"},
                        {year:"2021", order:"3", type:"시도", no:"913", name:"삼원로", dir:"상행", lane:"3", section:"1", cr:"5.51444", rutt:"15.5", iri:"7.7", index:"6.925557934893186", latestYear:"2021"},
                        {year:"2021", order:"3", type:"군도", no:"15", name:"용시길", dir:"하행", lane:"3", section:"1", cr:"0.547778", rutt:"23.2", iri:"3.4", index:"8.207470825760469", latestYear:"2021"},
                        {year:"2021", order:"4", type:"리도", no:"202", name:"제석길", dir:"하행", lane:"4", section:"1", cr:"4.79828", rutt:"7.64291", iri:"15.53", index:"1.1210145557585331", latestYear:"2020"},
                  {year:"2020", order:"1", type:"지방도", no:"514", name:"김천대로", dir:"하행", lane:"1", section:"1", cr:"2.78571", rutt:"3.777201", iri:"8.46", index:"5.209708565797758", latestYear:"2019"},
                        {year:"2020", order:"1", type:"지방도", no:"514", name:"아포읍_아포대로", dir:"상행", lane:"1", section:"1", cr:"4.8510", rutt:"2.81715", iri:"6.67", index:"7.296049058559776", latestYear:"2019"},
                        {year:"2020", order:"1", type:"시도", no:"15", name:"혁신8로", dir:"하행", lane:"1", section:"1", cr:"2.35714", rutt:"3.850444", iri:"5.12", index:"2.8955799744811026", latestYear:"2020"},
                        {year:"2020", order:"2", type:"군도", no:"5", name:"무안로", dir:"상행", lane:"2", section:"2", cr:"3.57143", rutt:"4.72251", iri:"9.95", index:"6.467552999145978", latestYear:"2020"},
                  {year:"2019", order:"2", type:"군도", no:"3", name:"시민로", dir:"상행", lane:"2", section:"2", cr:"4.07143", rutt:"4.10561", iri:"2.13", index:"7.2031734617062995", latestYear:"2019"},
                        {year:"2019", order:"3", type:"시도", no:"59", name:"환경로", dir:"상행", lane:"3", section:"3", cr:"11.004", rutt:"5.70526", iri:"4.98", index:"4.041601165608287", latestYear:"2019"},
                        {year:"2019", order:"3", type:"시도", no:"4", name:"황산로", dir:"상행", lane:"3", section:"3", cr:"2.57818", rutt:"4.64069", iri:"1.78", index:"8.449517990609781", latestYear:"2018"},
                        {year:"2019", order:"4", type:"군도", no:"3", name:"대학로", dir:"하행", lane:"4", section:"4", cr:"1.50394", rutt:"6.23308", iri:"5.08", index:"9.031864245188817", latestYear:"2019"}
                    ]
                });
                break;
            case "2021":
                setCostGrid({
                    ...costGrid,
                    rowData : [
                        {year:"2021", order:"1", type:"지방도", no:"913", name:"묘광길", dir:"상행", lane:"1", section:"1", cr:"11.4884", rutt:"3.97", iri:"5.66", index:"7.045211028622086", latestYear:"2020"},
                        {year:"2021", order:"3", type:"시도", no:"913", name:"삼원로", dir:"상행", lane:"3", section:"1", cr:"5.51444", rutt:"15.5", iri:"7.7", index:"6.925557934893186", latestYear:"2021"},
                        {year:"2021", order:"3", type:"군도", no:"15", name:"용시길", dir:"하행", lane:"3", section:"1", cr:"0.547778", rutt:"23.2", iri:"3.4", index:"8.207470825760469", latestYear:"2021"},
                        {year:"2021", order:"4", type:"리도", no:"202", name:"제석길", dir:"하행", lane:"4", section:"1", cr:"4.79828", rutt:"7.64291", iri:"15.53", index:"1.1210145557585331", latestYear:"2020"}
                    ]
                });
                break;
            case "2020":
                setCostGrid({
                    ...costGrid,
                    rowData : [
                        {year:"2020", order:"1", type:"지방도", no:"514", name:"김천대로", dir:"하행", lane:"1", section:"1", cr:"2.78571", rutt:"3.777201", iri:"8.46", index:"5.209708565797758", latestYear:"2019"},
                        {year:"2020", order:"1", type:"지방도", no:"514", name:"아포읍_아포대로", dir:"상행", lane:"1", section:"1", cr:"4.8510", rutt:"2.81715", iri:"6.67", index:"7.296049058559776", latestYear:"2019"},
                        {year:"2020", order:"1", type:"시도", no:"15", name:"혁신8로", dir:"하행", lane:"1", section:"1", cr:"2.35714", rutt:"3.850444", iri:"5.12", index:"2.8955799744811026", latestYear:"2020"},
                        {year:"2020", order:"2", type:"군도", no:"5", name:"무안로", dir:"상행", lane:"2", section:"2", cr:"3.57143", rutt:"4.72251", iri:"9.95", index:"6.467552999145978", latestYear:"2020"}
                    ]
                });
                break;
            case "2019":
                setCostGrid({
                    ...costGrid,
                    rowData : [
                        {year:"2019", order:"2", type:"군도", no:"3", name:"시민로", dir:"상행", lane:"2", section:"2", cr:"4.07143", rutt:"4.10561", iri:"2.13", index:"7.2031734617062995", latestYear:"2019"},
                        {year:"2019", order:"3", type:"시도", no:"59", name:"환경로", dir:"상행", lane:"3", section:"3", cr:"11.004", rutt:"5.70526", iri:"4.98", index:"4.041601165608287", latestYear:"2019"},
                        {year:"2019", order:"3", type:"시도", no:"4", name:"황산로", dir:"상행", lane:"3", section:"3", cr:"2.57818", rutt:"4.64069", iri:"1.78", index:"8.449517990609781", latestYear:"2018"},
                        {year:"2019", order:"4", type:"군도", no:"3", name:"대학로", dir:"하행", lane:"4", section:"4", cr:"1.50394", rutt:"6.23308", iri:"5.08", index:"9.031864245188817", latestYear:"2019"}
                    ]
                });
                break;
        }
    }

    useEffect(() => {
        setGrid(costGrid);
    }, [costGrid]);

    useEffect(()=>{
        changeGrid();
    }, [grid]);

    const handleSwitch = () => {
        setMasterSwitch(!masterSwitch);
    }

    const passToggle = () =>{
        toggle();
    }

    useEffect(() => {
        changeGrid();
    }, [masterSwitch])

    const changeGrid = () => {
        if(masterSwitch){
            setContent(
                <>
                <GridComponent columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} style={{width:"50%", height:"70vh", display:"inline-block", padding:"3px"}} />
                <GridComponent columnDefs={grid.columnDefs} rowData={grid.rowData} style={{width:"50%", height:"70vh", display:"inline-block", padding:"3px"}} />
                </>
        );
        }else{
            setContent(<GridComponent columnDefs={grid.columnDefs} rowData={grid.rowData} style={{width:"100%", height:"70vh", display:"inline-block"}} />);
        }
    }

    return <>
        <ModalBody>
            <span>
                <SearchDropdown options={planYear} placeholder="연도 선택" onChange={handleChange}/>
                <Button size="sm" color="secondary" onClick={handleCostClick}>보수대상구간 선정</Button>
            </span>
            <span>
                <SwitchButton label="마스터" isOpen={masterSwitch} onChange={handleSwitch} name="masterSwitch" />
            </span>
            <div className="ag-theme-balham" style={{marginTop: "5px", height:"70vh"}}>
                {content}
            </div>
            {subModal}
        </ModalBody>
        <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value="1,000"/>Km
            </span>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                <Button color="primary" onClick={handleScheduleClick}>스케줄러</Button>
                <Button color="primary">구간확인</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </>;
}

export default Planner;