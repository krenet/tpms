import React, {useState, useRef, useEffect} from 'react';
import { Modal, ModalHeader, Button, Label, Input, ModalBody, ModalFooter} from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import './BatchModified.css';
import PaveMathodXML from '../../../../../../../../../resource/settingXml/repair-pave-method.xml';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import serviceUrl from  '../../../../../../../../../request-url-list';

const BatchModified = ({isOpen, onClose, list, afterSave}) => {
    const [subModal, setSubModal] = useState(isOpen);
    const [selectedList, setSelectedList] = useState(list);
    // const [targetList, setTargetList] = useState([]);
    const [paveTypeList, setPaveTypeList] = useState([]);
    const [methodTypeList, setMethodTypeList] = useState([]);
    const [methodList, setMethodList] = useState([]);
    const [materialList, setMaterialList] = useState([]);
    const [paveMethodList, setPaveMathodList] = useState();
    const [selectedOption, setSelectedOption] = useState({ paveType: -1, methodType: -1, method: -1 });
    const [labelResetList, setLabelResetList] = useState({
        paveType: false,
        methodType: false,
        method: false,
        material: false
    });
    const [targetObj, setTargetObj] = useState({});
    const [paveReadOnly, setPaveReadOnly] = useState(true); 
    const [selectInxList, setSelectInxList] = useState({ paveType: -1, methodType: -1, method: -1, material: -1 });
    const splitChange = (e) => setTargetObj({ ...targetObj, split_thin : e.target.value });
    const reinforceChange = (e) => setTargetObj({ ...targetObj, layer_reinforce : e.target.value });
    const surfaceChange = (e) => setTargetObj({ ...targetObj, surface_thin : e.target.value });
    const [paveType, setPaveType] = useState();
    const [methodType, setMethodType] = useState();
    const [method, setMethod] = useState();
    const [material, setMaterial] = useState();
    
    const materialOnChange = (obj) => setMaterial(obj["label"]);
    
    const toggle = () => setSubModal(!subModal);
    const save = async() => { 

        var input_paveType = pType.current.state.value;
        var input_methodType = mType.current.state.value;
        var input_method = mth.current.state.value;
        var input_material = mat.current.state.value;
        var chk_material = paveReadOnly;
        var split_val = document.getElementById("split").value;
        var reinforce_val = document.getElementById("reinforce").value;
        var surface_val = document.getElementById("surface").value;
        var history_val = document.getElementById("m-history").value;
        var cost_val = document.getElementById("cost").value;

        var chk_split = document.getElementById("split").disabled;
        var chk_reinforce = document.getElementById("reinforce").disabled;
        var chk_surface = document.getElementById("surface").disabled;

        var alert_opt = "";
        if(input_paveType == null) alert_opt +="(포장종류) ";
        if(input_methodType == null) alert_opt +="(공법종류) ";
        if(input_method == null) alert_opt +="(공법) ";
        if(!chk_material && input_material == null) alert_opt +="(포장재료) ";
        if(!chk_split && split_val =='') alert_opt += "(절삭두께) ";
        if(!chk_reinforce && reinforce_val == '') alert_opt += "(기층보강) ";
        if(!chk_surface && surface_val == '') alert_opt +="(표층두께) ";
        if(cost_val == '') alert_opt +="(비용) ";
        
        if(alert_opt != "") {
            alert(alert_opt+"정보를 입력해 주시기 바랍니다");
        }else{
            var params = new Object();
            params['pave_type'] = input_paveType['label'];
            params['method_type'] = input_methodType['label'];
            params['method'] = input_method['label'];
            if(!chk_material) params['pave_material'] = input_material['label'];
            if(!chk_split) {
                params['split_thin'] = split_val;
            }else{
                params['split_thin'] = -9999;
            }
            if(!chk_reinforce) {
                params['layer_reinforce'] = reinforce_val;
            }else{
                params['layer_reinforce'] = -9999;
            }
            if(!chk_surface) {
                params['surface_thin'] = surface_val;
            }else{
                params['surface_thin'] = -9999;
            }
            params['comment'] = history_val;
            params['price'] =cost_val;
            params['list'] = encodeURIComponent(JSON.stringify(selectedList));


            var result = await server_connect(serviceUrl["pavement"]["saveNewRepair"], params);
            result = result['data']['res'];
            if(result == 1) {
                alert("저장 완료");
                afterSave();
            }
    

            // alert("저장 완료");
        }

        

        // document.getElementById("split").disabled
        // document.getElementById("reinforce").disabled
        // document.getElementById("surface").disabled


    }
    const pType = useRef();
    const mType = useRef();
    const mth = useRef();
    const mat = useRef();

    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const parsingXml = await XmlParser(PaveMathodXML);
            if(!ignore){
                setPaveMathodList(parsingXml);
            }
        }
        fetchData();
        return () => ignore = true; 
    }, []);

    useEffect(()=>{
        if(paveMethodList){
            var paveType = paveMethodList.getElementsByTagName("pave_type");
            var paveTypeOptions = paveType.map((obj, inx)=>{
                return {label: obj["attributes"]["title"], value:inx};
            });
            setPaveTypeList(paveTypeOptions);
        }
    }, [paveMethodList]);
    
    const disabledInputChange = (pave, split, reinforce, surface) => {
        setPaveReadOnly(pave);
        document.getElementById("split").disabled = split;
        document.getElementById("reinforce").disabled = reinforce;
        document.getElementById("surface").disabled = surface;
        document.getElementById('split').value = '';
        document.getElementById('reinforce').value = '';
        document.getElementById('surface').value = '';
    }
    
    const paveTypeOnChange = (obj) => {
        setPaveType(obj["label"]);
        disabledInputChange(true, true, true, true);
        
        setSelectedOption({ ...selectedOption, paveType: obj["value"] });
        setLabelResetList({ paveType: false, methodType: true, method: true, material: true });
        var methodTypeList = paveMethodList.getElementsByTagName("pave_type")[obj["value"]].getElementsByTagName("type");
        
        var methodTypeOptions = makeOptions(methodTypeList);
        setMethodTypeList(methodTypeOptions);
    
        setMethodList([]);
        setMaterialList([]);
    }
    
    const methodTypeOnChange = (obj) => {
        setMethodType(obj["label"]);
    
        disabledInputChange(true, true, true, true);
        setSelectedOption({ ...selectedOption, methodType: obj["value"] });
        setLabelResetList({ paveType: false, methodType: false, method: true, material: true });
        var methodTypes = paveMethodList.getElementsByTagName("pave_type")[selectedOption["paveType"]].
                            getElementsByTagName("type")[obj["value"]];
    
        var methodList;
        if(methodTypes.hasOwnProperty("children")){
            methodList = methodTypes["children"];
            var methodOptions = makeOptions(methodList);
            setMethodList(methodOptions);
        }
        
    
        
    }
    
    const methodOnChange = (obj) => {
        setMethod(obj["label"]);
        // setTargetObj({ ...targetObj, method : obj["label"] });
    
        setSelectedOption({ ...selectedOption, method: obj["value"] });
    
        var methodObj = paveMethodList.getElementsByTagName("pave_type")[selectedOption["paveType"]]
        .getElementsByTagName("type")[selectedOption["methodType"]]["children"][obj["value"]];
    
        var methodAttrs = methodObj["attributes"];
        disabledInputChange((methodAttrs["pave"]!="true"), (methodAttrs["split"]!="true"), (methodAttrs["reinforce"]!="true"), (methodAttrs["surface"]!="true"));
    
        if(methodObj["children"].length>0){
            var methodOptions = makeOptions(methodObj["children"]);
            setMaterialList(methodOptions);
        }
    }
    
    const makeOptions = (list) => {
        return list.map((obj, inx)=>{
            return {label: obj["attributes"]["title"], value: inx};
        });
    }

    const resetLabelStateEvt = () => {
        setLabelResetList({ paveType: false, methodType: false, method: false, material: false });
    }

    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" onClosed={onClose}>{/*onClosed={closeAll ? toggle : undefined}*/}
                <ModalHeader toggle={toggle}>보수 공법 입력</ModalHeader>
                <ModalBody>
                    <div className="body-container">
                        {/* <div className="row-area"> */}
                            {/* <Label className="mthistory-label">구간 ID</Label>
                            <SearchDropdown options={targetList} placeholder="ID 선택"  width="380" /> onChange={roadRankOnChangeEvt} readOnly="true" */}
                        {/* </div> */}
                        {/* <div className="row-area">
                            <Label className="mthistory-label">보수일자</Label>
                            <Input type="date" className="mthistory-text-input" />
                        </div> */}
                        <div className="row-area">
                            <Label className="mthistory-label">포장종류</Label>
                            <SearchDropdown target={pType} options={paveTypeList} placeholder="포장종류 선택"  width="375" onChange={paveTypeOnChange} resetLabel={labelResetList["paveType"]} resetStateEvt={resetLabelStateEvt}/> 
                        </div>
                        <div className="row-area">
                            <Label className="mthistory-label">공법구분</Label>
                            <SearchDropdown target={mType} options={methodTypeList} placeholder="공법종류 선택"  width="185" onChange={methodTypeOnChange} resetLabel={labelResetList["methodType"]} resetStateEvt={resetLabelStateEvt}/> 
                            <SearchDropdown target={mth} options={methodList} placeholder="공법 선택"  width="185" onChange={methodOnChange} resetLabel={labelResetList["method"]} resetStateEvt={resetLabelStateEvt}/> 
                        </div>
                        <div className="row-area">
                            <Label className="mthistory-label">포장재료</Label>
                            <SearchDropdown target={mat} options={materialList} placeholder="포장재료 선택"  width="375" onChange={materialOnChange} resetLabel={labelResetList["material"]} resetStateEvt={resetLabelStateEvt} readOnly={paveReadOnly}  selectInx={selectInxList["material"]}/> 
                        </div>
                        <div className="row-area">
                        <Label className="mthistory-label">절삭두께</Label>
                            <Input type="number" className="mthistory-number-input" id="split" onChange={splitChange} /><span className="cm-label">Cm</span>
                            <Label className="mthistory-label" style={{width: "60px"}}>기층보강</Label>
                            <Input type="number" className="mthistory-number-input" id="reinforce" onChange={reinforceChange} /><span className="cm-label">Cm</span>
                            <Label className="mthistory-label" style={{width: "60px"}}>표층두께</Label>
                            <Input type="number" className="mthistory-number-input" id="surface" onChange={surfaceChange} /><span className="cm-label">Cm</span>
                        </div>
                        <div className="row-area">
                            <Label className="mthistory-label">공법설명</Label>
                            <Input type="text" className="mthistory-text-input" id="m-history" />
                        </div>
                        <div className="row-area">
                            <Label className="mthistory-label">비용</Label>
                            <Input type="number" className="mthistory-price-input" id="cost" />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                     <Button style={{flex:1}} color="primary" size="sm" onClick={save}>저장</Button>
                     <Button style={{flex:1}} color="secondary" size="sm" onClick={toggle}>닫기</Button>
                </ModalFooter>
        </Modal>
    );
}

export default BatchModified;