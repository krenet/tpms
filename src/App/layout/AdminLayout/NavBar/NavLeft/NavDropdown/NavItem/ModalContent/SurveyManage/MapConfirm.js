import React, {useEffect, useState} from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button} from 'reactstrap';
import {connect} from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
// import KakaoMap from '../../../../../../../../../Demo/Dashboard/KakaoMap';
// import Vworld from '../../../../../../../../../Demo/Dashboard/Vworld';
    const MapConfirm = ({onOpenChat, isOpen, onClose, allScRows, scNodes, onCloseParent, allMaster, allTarget, selectedMaster, selectedTarget, header, hideModal}) => {
    const [subModal, setSubModal] = useState(isOpen);

    const toggle = () => setSubModal(!subModal);
 

    const MasterConfirm = () => {
        var props = [];
        props['allData'] = allMaster;
        props['selectedNodes'] = selectedMaster;
        props['header'] = header;
        props['action'] = 'OPEN_CHAT';
        onOpenChat(props);
        hideModal();
        toggle();
    }
    
    const TargetConfirm = () => {
        if(header[0]['label']['target_id']){
            var props = [];
            props['allData'] = allMaster;
            header.unshift({label: 'target_id', name: '구간ID'});
            props['header'] = header;
            props['allTarget'] = allTarget;
            props['targetNodes'] = selectedTarget;
            props['action'] = 'OPEN_CHAT_TARGET';
            onOpenChat(props);
            hideModal();
            toggle();
        }else{
            var props = [];
            props['header'] = header;
            props['allScRows'] = allScRows;
            props['scNodes'] = scNodes;
            props['action'] = 'OPEN_CHAT_SCHEDULAR';
            onOpenChat(props);
            hideModal();
            toggle();
        }
    }

    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" onClosed={onClose}>
            <ModalHeader toggle={toggle}>지도확인</ModalHeader>
                <ModalBody>
                    <Button color="primary" size="lg" block onClick={MasterConfirm}>마스터구간 확인</Button>
                    <Button color="primary" size="lg" block onClick={TargetConfirm}>대상구간 확인</Button>
                </ModalBody>
            <ModalFooter style={{justifyContent:"center"}} >
                <Button color="secondary" size="sm" block onClick={toggle}>닫기</Button>
            </ModalFooter>
        </Modal>
    );

}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({type: actionTypes.OPEN_CHAT, data: props})
    }
}

export default connect(chatStateToProps, chatDispatchToProps) (MapConfirm);