import React, { useState, useEffect, useRef } from 'react';
import { ModalBody, ModalFooter, Label, Input, Button } from 'reactstrap';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import GridComponent from '../../../../../../../../components/GridComponent';
import SwitchButton from '../../../../../../../../components/SwitchButton';
import {typeList} from '../Common/routeInfoList.js';
import MasterXML from '../../../../../../../../../resource/settingXml/road-master.xml';
import TargetXML from '../../../../../../../../../resource/settingXml/target-grid.xml';
import RecordTargetXML from '../../../../../../../../../resource/settingXml/record-traget.xml';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import '../../../../../../../../components/GridComponent.css';
import './Target.css';
import {connect} from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import TpmsView from '../../../../../../../../components/TpmsView';
import serviceUrl from  '../../../../../../../../../request-url-list';


const Target = ({toggle, onOpenChat, onCloseChat}) => {
    const [selectedGridType, setSelectedGridType] = useState(null);
    const [totalExt, setTotalExt] = useState('0');
    // 조사대상구간
    const [targetCols, setTargetCols] = useState();
    const [targetGrid, setTargetGrid] = useState({});
    const [targetGridApi, setTargetGridApi] = useState();
    const [targetList, setTargetList] = useState();
    const [targetCnt, setTargetCnt] = useState();
    const [searchT_List, setSearchT_List] = useState();
    const [updateList, setUpdateList] = useState();     // 데이터 add 또는 delete한 후
    // 노선마스터
    const [masterCols, setMasterCols] = useState();
    const [masterGrid, setMasterGrid] = useState({});
    const [masterGridApi, setMasterGridApi] = useState();
    const [masterList, setMasterList] = useState();
    const [searchM_List, setSearchM_List] = useState();
    // 이력
    const [recordCols, setRecordCols] = useState();
    const [recordGrid, setRecordGrid] = useState({});
    const [recordGridApi, setRecordGridApi] = useState();
    const [recordList, setRecordList] = useState()
    // 뷰
    const [viewDatas, setViewDatas] = useState([]);
    const [viewContent, setViewContent] = useState("");
    // 스위치
    const [masterSwitch, setMasterSwitch] = useState(false);
    const [tableViewSwitch, setTableViewSwitch] = useState(false);
    const [recordSwitch, setRecordSwitch] =useState(false);
    // 검색
    const [rankReadOnly, setRankReadOnly] = useState(false);
    const [noReadOnly, setNoReadOnly] = useState(false);
    const [nameReadOnly, setNameReadOnly] = useState(false);
    const [dirReadOnly, setDirReadOnly] = useState(false);

    const [yearList, setYearList] = useState();
    const [names, setNames] = useState([]);
    const [nos, setNos] = useState([]);
    const dirList = [
        {label: '전체', value: "-1"}, 
        {label: '상행', value:"0"},
        {label:'하행', value:"1"}
    ];
    const [year, setYear] = useState();
    const [rank, setRank] = useState();
    const [no, setNo] = useState();
    const [name, setName] = useState();
    const [dir, setDir] = useState();
    const recordYear = useRef();
    const roadRank = useRef();
    const roadNo = useRef();
    const roadName = useRef();
    const roadDir = useRef();
    const [labelResetList, setLabelResetList] = useState({
        year: false,
        rank: false,
        no: false,
        name: false,
        dir: false
    });

    // 저장 chk
    const [saveChk, setSaveChk] = useState(false);
    // const [groupNum, setGroupNum] = useState(0);
    const [scheduleOn, setScheduleOn] = useState(false);

    useEffect(()=>{
        yearChangeEvt();

        document.getElementById("masterGrid").style.display = "none";
        
        let ignore = false;
        async function fetchData(){
            const masterColumn = await XmlParser(MasterXML);
            const targetColumn = await XmlParser(TargetXML);
            const recordColumn = await XmlParser(RecordTargetXML);
            if(!ignore){
                setMasterCols(masterColumn.getElementsByTagName('Columns')[1]['children']);
                setTargetCols(targetColumn.getElementsByTagName('Columns')[0]['children']);
                setRecordCols(recordColumn.getElementsByTagName('Columns')[0]['children']);
            }
        }
        fetchData();
        return () => {
            ignore = true; 
        }

    }, []);

    useEffect(()=>{
        if(masterCols){
            var header = tHeader(masterCols);
            setMasterGrid({
                columnDefs: header,
                rowData: null,
                style: {
                    display: 'none',
                    flex: 1,
                    flexDirection: 'column',
                    padding: '3px',
                    height: '72vh',
                    textAlign:'center'
                },
            });
            async function getMaster(){
                var result = await server_connect(serviceUrl["pavement"]["getMasterForTarget"]);
                if(result && result['status']===200){
                result = result['data']['res'];
                var header = tHeader(masterCols);
                if(result.length>0){
                    setMasterList(result);
                    setMasterGrid({
                        columnDefs: header,
                        rowData: null,
                        style: {
                            display: 'none',
                            flex: 1,
                            flexDirection: 'column',
                            padding: '3px',
                            height: '72vh',
                            textAlign:'center'
                        },
                        onSelectionChanged: function(e) {
                            var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length===0) return;
                            setSelectedGridType('master');
                            var extSum = 0;
                            selectedRows.map((row)=>{
                                var selectedRowExt = row['data']['sm_ext'];
                                extSum = extSum + selectedRowExt;
                            });
    
                            setTotalExt((extSum/1000).toFixed(2));
                        },
                        rowClassRules: null
                    });
                }
                }else{
                    alert('서버 통신 실패. 관리자에게 문의하세요.');
            }
            }
            getMaster();
        }

    }, [masterCols]);

    useEffect(()=>{
      if(targetCols){
        var header = tHeader(targetCols);
        setTargetGrid({
            columnDefs : header,
            rowData:null,
            style: {
                display: "felx",
                flex: 1,
                flexDirection: "column",
                padding: '3px',
                height: '72vh',
                textAlign:'center'
            },
        });
        async function currTargetList(){
            var header = tHeader(targetCols);
            let today = new Date();
            let currYear = today.getFullYear();
            var params = {};
            params['year'] = currYear; 
            var result = await server_connect(serviceUrl["pavement"]["getCurrentYearTarget"], params);
            if(result && result['status']===200){
            if(result['data'].hasOwnProperty('res')){
                result = result['data']['res'];
                if(result.length>0){
                    setTargetCnt(result.length);
                    setTargetList(result);
                    dataSort(result, 'target');
                    setTargetGrid({
                        columnDefs: header,
                        rowData: result,
                        style: {
                            display: "felx",
                            flex: 1,
                            flexDirection: "column",
                            padding: '3px',
                            height: '72vh',
                            textAlign:'center'
                        },
                        onSelectionChanged: function(e) {
                            var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length===0) return;
                            setSelectedGridType('target');
                            var extSum = 0;
                            selectedRows.map((row)=>{
                                var selectedRowExt = row['data']['sm_ext'];
                                extSum = extSum + selectedRowExt;
                            });
                            setTotalExt((extSum/1000).toFixed(2));
                        }
                    });
                }else{
                    setTargetCnt(0);
                    setTargetList(null);
                    setTargetGrid({
                        columnDefs: header,
                        rowData: null,
                        style: {
                            display: "felx",
                            flex: 1,
                            flexDirection: "column",
                            padding: '3px',
                            height: '72vh',
                            
                        },
                        onSelectionChanged: function(e) {
                            var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length===0) return;
                            setSelectedGridType('target');
                            var extSum = 0;
                            selectedRows.map((row)=>{
                                var selectedRowExt = row['data']['sm_ext'];
                                extSum = extSum + selectedRowExt;
                            });
                            setTotalExt((extSum/1000).toFixed(2));
                        }
                    });
                }
            }
        }
        }
        currTargetList();
      }

    }, [targetCols]);

    useEffect(()=>{
        if(recordCols){
            var header = tHeader(recordCols);
            setRecordGrid({
                columnDefs: header,
                rowData: null,
                style: {
                    display: 'none',
                    flex: 1,
                    flexDirection: 'column',
                    padding: '3px',
                    height: '72vh',
                    textAlign:'center'
                },
                onSelectionChanged: function(e) {
                    var selectedRows = e.api.getSelectedNodes();
                    if(selectedRows.length===0) return;
                    setSelectedGridType('record');
                    var extSum = 0;
                    selectedRows.map((row)=>{
                        var selectedRowExt = row['data']['sm_ext'];
                        extSum = extSum + selectedRowExt;
                    });

                    setTotalExt((extSum/1000).toFixed(2));
                }
            });
        }

    }, [recordCols]);

    const dataSort = (data, gridType) => {
        data.sort(function (a, b) {
            var before_no = parseInt(a.no);
            var after_no = parseInt(b.no);
            if(gridType ==='master' || gridType ==='target'){
                return a.rank.localeCompare(b.rank) || before_no - after_no || a.name.localeCompare(b.name) || a.sub_num - b.sub_num 
                       || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section || a.order - b.order;
            }else if(gridType ==='record'){
                return b.year-a.year || a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
                       || a.main_num - b.main_num || a.dir - b.dir;
            }
           
        });
        return data;
    }

    // 그리드 header 세팅
    const tHeader = (cols) => {
        var prev;
        var colNum = cols.length;
        var firstName = cols[0]['name'];
        var header = cols.map((obj, index) =>{
            if(!(prev) || obj['attributes']['className']!==prev){
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        header = header.filter(function(el){
            return el!=null;
        });

        header = header.map((obj)=>{
            return {
                headerName: obj,
                children: 
                    cols.map((col)=>{
                        var attr = col['attributes'];
                        var width = null;
                        if(attr.hasOwnProperty("wd")){
                            width = Number(attr['wd']);
                        }
                        if(attr['className']===obj && col['name']===firstName){
                            return {
                                headerName: attr['title'], 
                                field: col['name'],
                                width: width,
                                colSpan: function(params){
                                    if(isSpan(params)){
                                        return colNum;
                                    }else{
                                        return 1;
                                    }
                                },
                                // cellStyle: function(params){
                                //     if(isSpan(params)){
                                //         return {textAlign: 'center'};
                                //     }else {
                                //         return {textAlign: 'left'};
                                //     }
                                // },
                            }
                        }else if (attr['className']===obj && col['name']!==firstName){
                            return {headerName: attr['title'], field: col['name'], width: width}
                        }
                    }).filter(function(el){
                        return el!=null;
                    })
            }
        });

        function isSpan(params){
            return params.data.span === 'no-result';
        }

        return header;
    }
        
    useEffect(()=>{
        if(masterList && targetList){
            var viewGroup = {};

            dataSort(masterList, "master");
            // masterList.map((mObj)=>{
            //     var rank = mObj['rank'];
            //     switch (rank) {
            //         case '101':
            //             mObj['rank'] = '고속국도';
            //             return;
            //         case '102':
            //             mObj['rank'] = '도시고속국도';
            //             return;
            //         case '103':
            //             mObj['rank'] = '일반국도';
            //             return;
            //         case '104':
            //             mObj['rank'] = '특별·광역시도';
            //             return;
            //         case '105':
            //             mObj['rank'] = '국가지원 지방도';
            //             return;
            //         case '106':
            //             mObj['rank'] = '지방도';
            //             return;
            //         case '107':
            //             mObj['rank'] = '시·군도';
            //             return;
            //         case '108':
            //             mObj['rank'] = '기타';
            //             return;
            //     }
            // });
    
            masterList.map((row)=>{
                var key = ""+row['tri_id']+row['main_num']+row['sub_num']+row['dir']+1;
                var viewList = [];
                if(viewGroup.hasOwnProperty(key)){
                    viewList = viewGroup[key];
                }
                viewList.push(row);
                viewGroup[key] = viewList;
            });
    
            var viewList = [];
            var keyList = Object.keys(viewGroup);
            var viewrank; var viewno; var viewname;
            for(var i=0;i<keyList.length;++i){
                viewGroup[keyList[i]].map((obj, inx)=>{
                    if(inx === 0){
                        viewrank = obj['rank'];
                        viewno = obj['no'];
                        viewname = obj['name'];
                    }
                    if(typeof obj['target_id'] === 'undefined') {
                        obj['target_id'] = null;
                        delete obj['target_id'];
                    }
                    targetList.map((tObj)=>{
                        if(tObj['ts_id']===obj['ts_id']) obj['target_id'] = tObj['target_id'];
                    });
                });
                var addInfo = {
                    info : {
                        rank : viewrank,
                        no : viewno,
                        name : viewname,
                        targetCol : 'target_id',
                        extCol : 'sm_ext'
                    },
                    children : viewGroup[keyList[i]]
                };
                viewList.push(addInfo);
            }
            setViewDatas(viewList);
        }

    }, [masterList, targetList])

    useEffect(()=>{
        if(viewDatas.length>0){
            setViewContent(<TpmsView dataSet={viewDatas}/>);
        }
    }, [viewDatas]);

    const handleSwitch = (e) => {
        var checkedSwitch = e.target.getAttribute('name');

        switch(checkedSwitch){
            case 'tableViewSwitch':
                setTableViewSwitch(!tableViewSwitch);
                break;
            case 'masterSwitch':
                setMasterSwitch(!masterSwitch);
                break;
            case 'recordSwitch':
                setRecordSwitch(!recordSwitch);
                break;
        }       
    }

    const getMasterGridApi = (childState) => {
        setMasterGridApi(childState);
    }

    const getTargetGridApi = (childState) => {
        setTargetGridApi(childState);
    }

    const getRecordGridApi = (childState) => {
        setRecordGridApi(childState);
    }

    useEffect(() => {
        setSelectedGridType('none');
        setLabelResetList({ year: true, rank: true, no: true, name: true, dir: true});
        setNos([]); setNames([]);
        setYear(); setRank(); setNo(); setName(); setDir();
        setTotalExt('0');
        if(recordSwitch) {
            document.getElementById("yearFilter").style.display = "block";
            document.getElementById("targetGrid").style.display = "none";
            document.getElementById("recordGrid").style.display = "flex";
            if(tableViewSwitch) setTableViewSwitch(false);
            if(masterSwitch) {
                resetLabelStateEvt();
                setMasterSwitch(false);
            }
        }else{
            setRecordGrid({
                ...recordGrid,
                rowData: null
            });
            document.getElementById("yearFilter").style.display = "none";
            document.getElementById("recordGrid").style.display = "none";
            if(!tableViewSwitch)document.getElementById("targetGrid").style.display = "flex";
        }

    }, [recordSwitch]);

    useEffect(() => {
        setSelectedGridType('none');
        setLabelResetList({ year: true, rank: true, no: true, name: true, dir: true, lane: true });
        setNos([]); setNames([]);
        setYear(); setRank(); setNo(); setName(); setDir();
        setTotalExt('0');
        if(masterSwitch) {
            setRankReadOnly(false);
            setNoReadOnly(false);
            setNameReadOnly(false);
            setDirReadOnly(false);
            document.getElementById('add_btn').style.display='inline-grid';    
            document.getElementById('del_btn').style.display='inline-grid';
            document.getElementById('masterGrid').style.display='flex';
            if(recordSwitch) {
                resetLabelStateEvt();
                setRecordSwitch(false);        
            }
            if(tableViewSwitch){
                document.getElementById('add_btn').style.display='none';    
                document.getElementById('del_btn').style.display='none';
            }  
        }else{
            if(targetList){
                if(targetList.length>0) {
                    dataSort(targetList, "target");
                    setTargetGrid({
                        ...targetGrid,
                        rowData: targetList
                    });
                }else{
                    setTargetGrid({
                        ...targetGrid,
                        rowData: null
                    });
                }
            }
            
            if(tableViewSwitch){
                setRankReadOnly(true);
                setNoReadOnly(true);
                setNameReadOnly(true);
                setDirReadOnly(true);
            }
            document.getElementById('add_btn').style.display='none';    
            document.getElementById('del_btn').style.display='none';
            document.getElementById('masterGrid').style.display='none';
        }
    }, [masterSwitch]);

    useEffect(() => {
        setSelectedGridType('none');
        setTotalExt('0');
        if(tableViewSwitch) {
            if(!masterSwitch){
                setRankReadOnly(true);
                setNoReadOnly(true);
                setNameReadOnly(true);
                setDirReadOnly(true);
            }else{
                document.getElementById('add_btn').style.display='none';    
                document.getElementById('del_btn').style.display='none';
            }
            document.getElementById('targetGrid').style.display='none';
            document.getElementById("viewModel").style.display = 'flex';
            if(recordSwitch) setRecordSwitch(false);
        }else{
            setRankReadOnly(false);
            setNoReadOnly(false);
            setNameReadOnly(false);
            setDirReadOnly(false);
            if(!recordSwitch)document.getElementById('targetGrid').style.display='flex';
            document.getElementById("viewModel").style.display = 'none';
            if(masterSwitch){
                document.getElementById('add_btn').style.display='inline-grid';    
                document.getElementById('del_btn').style.display='inline-grid';
            }
        }
    }, [tableViewSwitch])

    const yearChangeEvt = () => {
        var currentYear = new Date().getFullYear();
        var arr = new Array();
        arr.push({label: '전체', value: 'all'});
        for(var i=currentYear; i>=(currentYear-10); --i){
            arr.push({label:i, value:i});
        }
        setYearList(arr);
    }

    // 검색필터: 연도 변경 시, 다른 검색 필터 value를 null로 변경
    const recordYearOnChangeEvt = (selectedObj) => {
        setLabelResetList({ year: false, rank: true, no: true, name: true, dir: true, lane: true });
        setYear(selectedObj['value']);
        setRank(null); setNo(null);  setName(null); setDir(null);
    }
    
    // 검색필터: 도로등급 선택 시, 해당 도로등급에 속하는 노선번호 불러오기
    const roadRankOnChangeEvt = async (selectedObj) => {
        if(!recordSwitch){
            setLabelResetList({ year: true, rank: false, no: true, name: true, dir: true, lane: true });
            setYear(null);
        }else {
            setLabelResetList({ year: false, rank: false, no: true, name: true, dir: true, lane: true });
        }
        setRank(selectedObj['value']);
        setNo(null);  setName(null); setDir(null);

        var params = new Object();
        params['rank'] = selectedObj['value'];
        const noRes = await server_connect(serviceUrl["pavement"]["selectNoListForRank"], params);
        if(noRes && noRes['status']==200){
        let noResData = noRes['data']['res'];
        var noList = makeOptionsJson(noResData);
        if(selectedObj['value']=='all') noList.unshift({label: '전체', value: 'all'});
        setNos(noList);
        setNames([]);
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    };

   // 검색필터: 노선번호 선택 시, 해당 노선번호에 속하는 노선명 불러오기
   const roadNoOnChangeEvt = async (selectedObj) => {
       setName(null);
       if(!recordSwitch){
           setLabelResetList({ year: true, rank: false, no: false, name: true, dir: true, lane: true });
           setYear(null);
       }else {
           setLabelResetList({ year: false, rank: false, no: false, name: true, dir: true, lane: true });
       }
       setName(null); setDir(null);
       let _no = selectedObj['value'];
       setNo(_no);
       var params = new Object();
       params['rank'] = rank;
       var nameRes;
       if(_no=='all'){
           nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRank"], params);
       }else{
           params['no'] = _no;
           nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRankNo"], params);
       }
       let nameResData = nameRes['data']['res'];
       var nameList = makeOptionsJson(nameResData);
    //    if(nameList.length==0) nameList.unshift({label: '-', value: '-'});
       setNames(nameList);
}

    const roadNameOnChangeEvt = (selectedObj) => {
        setName(selectedObj['value']);
    }

    const roadDirOnChangeEvt = (selectedObj) => {
        setDir(selectedObj['value']);
    }

    const makeOptionsJson = (arr) => {
        let list = arr.map((item)=>{
            if(item=="") {
                return {label: "-", value: "-"}
            }
            return {label:item, value:item};
        });
        return list;
    }

    // 검색 버튼 클릭 이벤트
    const handleClick = async(e) => {
        setTotalExt('0');
        var params = new Object();
        params['year'] = year;
        params['rank'] = rank;
        params['no'] = no;
        params['name'] = name;
        params['dir'] = dir;

        if(recordSwitch){
            searchRecord(params);
        }else{
            params['year'] = null;
            searchTarget(params);
            if(masterSwitch) searchMaster(params);
        }
    }

    const searchTarget = (params) => {
      
        if(params['rank'] !=null){
            if(targetList !=null){
                if(targetList.length>0){
                    for(var i=0; i<targetList.length; ++i){
                        var t_rank = targetList[i]['rank'];
                        switch (t_rank){
                            case '고속국도': 
                                targetList[i]['rank'] = '101';
                                break;
                            case '도시고속국도': 
                                targetList[i]['rank'] = '102';
                                break;       
                            case '일반국도': 
                                targetList[i]['rank'] = '103';
                                break;   
                            case '특별·광역시도': 
                                targetList[i]['rank'] = '104';
                                break;  
                            case '국가지원 지방도': 
                                targetList[i]['rank'] = '105';
                                break;   
                            case '지방도': 
                                targetList[i]['rank'] = '106';
                                break;   
                            case '시·군도': 
                                targetList[i]['rank'] = '107';
                                break;      
                            case '기타': 
                                targetList[i]['rank'] = '108';
                                break;                      
                        }
                    }
                    console.log("targetList: ", targetList);
                    var searchTargetList = search(params, targetList);
                    if(searchTargetList.length>0){
                        setSearchT_List(searchTargetList);
                        dataSort(searchTargetList, "target");
                        setTargetGrid({
                            ...targetGrid,
                            rowData: searchTargetList
                        });
                    }else{
                        setSearchT_List(null);
                        setTargetGrid({
                            ...targetGrid,
                            rowData: [
                                {span: 'no-result', year: "검색 결과가 없습니다"}
                            ]
                        });
                    }
                }
            }
        }else{
            alert("도로등급을 선택해 주시기 바랍니다");
        } 
    }

    const searchMaster = (params) => {
        if(params['rank'] !=null){
            var searchMasterList = search(params, masterList);
            if(searchMasterList.length>0){
                setSearchM_List(searchMasterList);
                dataSort(searchMasterList, "master");
                setMasterGrid({
                    ...masterGrid,
                    rowData: searchMasterList,
                    rowClassRules: {
                        'included-row-in-target' : function (params) {
                            var ts_id = params.data.ts_id;
                            var chk = false;
                            if(targetList !=null){
                                for(var i=0; i<targetList.length; ++i){
                                    if(ts_id == targetList[i]['ts_id']){
                                        chk = true;
                                    }
                                }
                                return chk;
                            }
                        }   
                    }
                });
            }else{
                setMasterGrid({
                    ...masterGrid,
                    rowData:[
                        {span: 'no-result', no: "검색 결과가 없습니다"}
                    ]
                });
            }
        }else{
            alert("도로등급을 선택해 주시기 바랍니다");
        }
    }

    const searchRecord = async(params) => {
        if(params['year'] !=null){            
            async function recordTargetList(){
                var param = new Object();
                param['year'] = year;    
                var recordTargetList = await server_connect(serviceUrl["pavement"]["getAllTarget"], param);
                recordTargetList = recordTargetList['data']['res'];  // 검색한 연도 리스트만 가져오기
                if(recordTargetList != null){
                    var searchRecordList = search(params, recordTargetList);   // 세부 검색 필터 적용
                    if(searchRecordList.length>0){
                        var dataList = searchRecordList.map((obj) => {
                            return  {
                                year: obj['year'], target_id: obj['target_id'], spos: obj['spos'], epos: obj['epos'], 
                                total_ext: (obj['total_ext']/1000).toFixed(2), no: obj['no'], name: obj['name'],
                                main_num: obj['main_num'], sub_num: obj['sub_num'], dir: obj['dir'], lanes: "",
                                totalExt: obj['total_ext'], s_ts_id: obj['s_ts_id'], e_ts_id: obj['e_ts_id'], ts_id: obj['s_ts_id'], rank: obj['rank']
                            }
                        });
                        setRecordList(dataList);
                        dataSort(dataList, 'record');
                        setRecordGrid({
                            ...recordGrid,
                            rowData : dataList,
                            onSelectionChanged: function(e) {
                                var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length==0) return;
                                setSelectedGridType('record');
                                var extSum = 0;
                                selectedRows.map((row)=>{
                                    var selectedRowExt = row['data']['totalExt'];
                                    extSum = extSum + selectedRowExt;
                                });
                                setTotalExt((extSum/1000).toFixed(2));
                            }
                        });
                    }else {
                        setTotalExt('-');
                        setRecordGrid({
                            ...recordGrid,
                            rowData : [
                                {span: 'no-result', year: "검색 결과가 없습니다"}
                            ]
                        });
                    }    
                }else{
                    setTotalExt('-');
                    setRecordGrid({
                        ...recordGrid,
                        rowData : [
                            {span: 'no-result', year: "검색 결과가 없습니다"}
                        ]
                    });
                }
            }
            recordTargetList();       
        }else{
            alert("연도를 선택해 주시기 바랍니다");
        }
    }

    const search  = (params, dataList) => {
        console.log("params: ", params);
        var resultList = new Array();
        for(var i=0; i<dataList.length; ++i){
            var chk=true;
            if(params['year'] != 'all' && params['year'] !=null){
                if((String)(dataList[i]['year']) == params['year']){
                    chk = search2(params,dataList, chk, i);
                } else{
                    chk=false;
                }
            }else {
                chk = search2(params,dataList, chk, i);
            }

            if(chk) resultList.push(dataList[i]);
        }

        return resultList;
    }

    const search2  = (params, dataList, chk, i) => {
        if(params['rank'] !=null){
            if(params['rank'] != 'all'){
                if(dataList[i]['rank'] != params['rank']) chk=false; 
            }
            if(params['no'] != null){
                if(params['no'] != 'all'){
                    if(dataList[i]['no'] != params['no']) chk=false;
                }
                if(params['name'] !=null){
                    if(params['name'] !='all'){
                        if(dataList[i]['name'] != params['name']) chk=false;
                    }
                }   
            }
        }

        if(params['dir'] != '-1' && params['dir'] !=null){
            if(dataList[i]['dir'] != params['dir']) chk=false;
        }
        
        return chk;
    }

    useEffect(() => {
       if(selectedGridType=='master'){
           targetGridApi.deselectAll();
           recordGridApi.deselectAll();
       }
       if(selectedGridType=='target'){
           masterGridApi.deselectAll();
           recordGridApi.deselectAll();
       }
       if(selectedGridType=='record'){
           masterGridApi.deselectAll(); 
           targetGridApi.deselectAll();
       }
       if(selectedGridType=='none' && masterGridApi){
           masterGridApi.deselectAll(); 
           targetGridApi.deselectAll();
           recordGridApi.deselectAll();
       }

    }, [selectedGridType]);


    useEffect(()=>{
        console.log("응????");
        if(masterGrid.rowData==null && updateList){
            console.log("updateList: ", updateList);

            setMasterGrid({
                ...masterGrid,
                rowData: searchM_List,
                rowClassRules: {
                    'included-row-in-target' : function (params) {
                        var ts_id = params.data.ts_id;
                        var chk = false;
                        if(updateList.length>0){
                            for(var i=0; i<updateList.length; ++i){
                                if(ts_id == updateList[i]['ts_id']){
                                    chk = true;
                                }
                            }
                            return chk;
                        }
                    }   
                }
            });

            setTargetGrid({
                ...targetGrid,
                rowData: searchT_List
            });

            setUpdateList(null);
        }

    }, [updateList]);

// }, [masterGrid]);


    const addToTargetGrid = (e) => {
        var selectedMaster = masterGridApi.getSelectedNodes();
        // console.log("selectedMaster: ",selectedMaster);
        var addList = new Array();
        if(targetList) addList = targetList;
        // var addList = targetList;
        var addSearchList = searchT_List;
        if(searchT_List == null) addSearchList = new Array();
        if(addSearchList && addSearchList.length>0){
            for(var i=0; i<selectedMaster.length; ++i){
                if(selectedMaster[i]['data']['type']=='0104'){
                    selectedMaster[i]['data']['target_id'] = undefined;
                    var chk=true;
                    for(var j=0; j<addSearchList.length; ++j){
                        if(addSearchList[j]['ts_id'] == selectedMaster[i]['data']['ts_id'])  chk=false; 
                    }
                    if(chk){
                       var dir = selectedMaster[i]['data']['dir'];
                       if(dir==0){
                           selectedMaster[i]['data']['roadDir'] = "상행";
                       }else{
                           selectedMaster[i]['data']['roadDir'] = "하행";
                       }
                       selectedMaster[i]['data']['year'] = 2021;
                       addList.push(selectedMaster[i]['data']);
                       addSearchList.push(selectedMaster[i]['data']);
                    }
                }
            }

            // for(var i=0; i<targetList.length; ++i){
            //     addList.push(targetList[i]);
            // }
        }else{
            for(var i=0; i<selectedMaster.length; i++){
                if(selectedMaster[i]['data']['type']=='0104') {
                    var dir = selectedMaster[i]['data']['dir'];
                    if(dir==0){
                        selectedMaster[i]['data']['roadDir'] = "상행";
                    }else{
                        selectedMaster[i]['data']['roadDir'] = "하행";
                    }
                    selectedMaster[i]['data']['year'] = 2021;
                    addList.push(selectedMaster[i]['data']);
                    addSearchList.push(selectedMaster[i]['data']);
                }
            }
        }

        setTargetList(addList);
        setSearchT_List(addSearchList);
        dataSort(addSearchList, 'target');
        
        setTargetGrid({
            ...targetGrid,
            rowData: null
        });

        // console.log("addList: ", addList);
        // setUpdateList(addList);
        
        setMasterGrid({
            ...masterGrid,
            rowData: null,
            rowClassRules: null
        });
        
        console.log("addList: ", addList);
        setUpdateList(addList);
    }

    const delFromTargetGrid = (e) => {
        var selectedTarget = targetGridApi.getSelectedNodes();
        var allList = new Array();  // 전체 타겟 리스트
        var list = new Array();     // 검색 타겟 리스트
        var a_list = targetList;
        var s_list = searchT_List;
        console.log("s_list:", s_list);        if(selectedTarget.length>0){
            for(var i=0; i<a_list.length; ++i){
                var chk=true;
                for(var j=0; j<selectedTarget.length; ++j){
                    if(selectedTarget[j]['data']['ts_id'] == a_list[i]['ts_id']) chk=false;
                }
                if(chk) allList.push(a_list[i]);
            }

            for(var i=0; i<s_list.length; ++i){
                var chk=true;
                for(var j=0; j<selectedTarget.length; ++j){
                    if(selectedTarget[j]['data']['ts_id'] == s_list[i]['ts_id'])  chk=false;
                }
                if(chk) list.push(s_list[i]);
            }          
        }

        setTargetList(allList);
        setSearchT_List(list);
        dataSort(list, 'target');

        setTargetGrid({
            ...targetGrid,
            rowData: null
        });

        setMasterGrid({
            ...masterGrid,
            rowData: null,
            rowClassRules: null
        });

        setUpdateList(allList);

    }

    const passToggle = () =>{
        onCloseChat();
        toggle();
    }

    const getSchedule = async(e) => {
        let today = new Date();
        let currYear = today.getFullYear();
        var params = new Object();
        params['year'] = currYear; 

        if(!scheduleOn){
            if(masterSwitch) setMasterSwitch(false);
            if(recordSwitch) setRecordSwitch(false);
            if(tableViewSwitch) setTableViewSwitch(false);
            setScheduleOn(true);
            var result = await server_connect(serviceUrl["pavement"]["getSchedule"], params);
            if(result && result['status']==200){
                // setGroupNum(result['data']['maxNum']);
            result = result['data']['res']; 
            if(result.length>0){
                var list = new Array();
                if(targetList !=null){
                    if(targetList.length>0){
                        for(var i=0; i<targetList.length; ++i){
                            list.push(targetList[i]);
                        }
                        for(var i=0; i<result.length; ++i){
                            var chk = true;
                            for(var j=0; j<list.length; ++j){
                                if(result[i]["ts_id"] == list[j]["ts_id"]) chk=false;
                            }
                            if(chk) list.push(result[i]);
                        }
                    }
                }else{
                    list = result;
                }
                console.log("불러오기 체크 후: ", list);                setTargetList(list);
                dataSort(list,'target');
                setTargetGrid({
                    ...targetGrid,
                    rowData : list
                });
            }
            }else{
                alert('서버 통신 실패. 관리자에게 문의하세요.');
            }
            // }else {
            //     setTotalExt('-');
            //     setTargetGrid({
            //         ...targetGrid,
            //         rowData : [
            //             {span: 'no-result', target_year: "결과가 없습니다"}
            //         ],
            //     });
            // }
        }else{
            alert("이미 스케줄을 불러오기를 진행하셨습니다.");
        } 
    } 

    const viewMap = () => {
        var props =[];
        var masterNodes = masterGridApi.getSelectedNodes();
        var targetNodes = targetGridApi.getSelectedNodes();
        var recordNodes = recordGridApi.getSelectedNodes();
        var header = [
            {
                label : 'rank',
                name  : '등급'
            },
            {
                label : 'no',
                name  : '노선번호'
            },
                {
                label : 'name',
                name  : '노선명'
            },
            {
                label : 'rev_dir',
                name  : '행선'
            },
            {
                label : 'node_name',
                name  : '이벤트명'
            },
            {
                label : 'node_type',
                name  : '이벤트 종류'
            }
           ];
        if(masterNodes.length>0){
            var all = dataSort(masterList, 'master');
            props['header'] = header;
            props['allData'] = all;
            props['selectedNodes'] = masterNodes;
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }else if(targetNodes.length>0){
            var all = dataSort(targetList, 'target');
            header.unshift({label: 'target_id', name: '구간ID'});
            props['header'] = header;
            props['allData'] = all;
            props['selectedNodes'] = targetNodes;
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }else if(recordNodes.length>0){
            dataSort(recordList, 'record');
            console.log("recordList: ",recordList);            getTargetTsList(recordList, recordNodes);
        }
    }

    const getTargetTsList = async (list, selectedNodes) => {
        if(list !=null){
            var data = new Object();
            data['list'] = encodeURIComponent(JSON.stringify(list));
            var result = await server_connect(serviceUrl["pavement"]["getTargetTsList"], data);   // 서버통신

            var props = [];
            props['allData'] = result['data']['res'];

            var list = selectedNodes.map((el)=>{
                var obj = el["data"];
                props["allData"].map((row)=>{
                    if(obj["target_id"]==row["target_id"]){
                        obj["tsList"] = row["tsList"];
                        el["data"] = obj;
                        return;
                    }
                });
                return el;
            });

            props['selectedNodes'] = list;
            props['header'] = [ { label : 'target_id', name  : '구간ID' }, { label : 'spos', name  : '시점' },
                                { label : 'epos', name  : '종점' }, { label : 'rank', name  : '등급' },
                                { label : 'no', name  : '노선번호' }, { label : 'name', name  : '노선명' },
                                { label : 'rev_dir', name  : '행선' } ];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display='none';
        document.getElementsByClassName('modal-backdrop')[0].style.display='none';
        document.getElementsByClassName('modal-dialog')[0].style.display='none';
    }

    useEffect(()=>{
        if(saveChk){
            if(targetList.length>0){
                var params = new Object();
                params['year'] = year;
                params['rank'] = rank;
                params['no'] = no;
                params['name'] = name;
                params['dir'] = dir;
                
                var searchList = search(params, targetList);
                console.log("searchList: ",searchList);                setSearchT_List(searchList);
                dataSort(searchList, "target");
                setTargetGrid({
                    ...targetGrid,
                    rowData: searchList
                });
            }else{
                setTargetGrid({
                    ...targetGrid,
                    rowData: null
                });
            }
            setSaveChk(false);
        }
    }, [saveChk])

    const saveBtn = async() => {
        if(window.confirm("조사대상구간을 저장하시겠습니까?") == true) {
            var data = new Object();
            let today = new Date();
            let currYear = today.getFullYear();
            data['cnt'] = targetCnt;
            data['year'] = currYear;

            // console.log("targetList: ", targetList);

            if(targetList){
                for(var i=0; i<targetList.length; ++i){
                    if(targetList[i]['target_id'] == undefined){
                        var no = targetList[i]['no'];
                        var dir = targetList[i]['dir'];
                        var main_num = targetList[i]['main_num'];
                        var sub_num = targetList[i]['sub_num'];
                        var sector = targetList[i]['sector'];
                        var section = targetList[i]['section'];
                        // var sm_order = targetList[i]['sm_order'];
                        var temp_key = no + dir + main_num + sub_num + sector;
                        targetList[i]['temp_key'] = temp_key;
                        // console.log("key: ", key + section);
                        if(i==0) targetList[i]['group_num'] = temp_key + section;
                        if(i>0){
                            if(targetList[i-1]['temp_key'] == targetList[i]['temp_key']){
                                var diff = targetList[i]['sm_order']-targetList[i-1]['sm_order'];
                                if(diff == 1) {
                                    targetList[i]['group_num'] = targetList[i-1]['group_num'];
                                }else{
                                    targetList[i]['group_num'] = temp_key + section;
                                }
                            }else{
                                targetList[i]['group_num'] = temp_key + section;
                            }
                        }
                    }
                }
                for(var i=0; i<targetList.length; ++i){
                    targetList[i]['temp_key'] =undefined;
                }
                // console.log("그룹핑 후 targetList: ", targetList);
            }
            data['list'] = encodeURIComponent(JSON.stringify(targetList));
           
            var save_result = await server_connect(serviceUrl["pavement"]["saveTargetList"], data);
            if(save_result && save_result['status']==200){
                save_result = save_result['data']['res'];
                
                if(save_result=="success"){
                    var params = new Object();
                    params['year'] = currYear;
                    var result = await server_connect(serviceUrl["pavement"]["getCurrentYearTarget"], params);
                    if(result['data'].hasOwnProperty('res')){
                        result = result['data']['res'];
                        console.log("불러오기: ", result);                    
                        if(result.length>0){
                            setTargetCnt(result.length);
                            setTargetList(result);
                            setTargetGrid({
                                ...targetGrid,
                                rowData: null
                            });
                        }else{
                            setTargetList(null);
                            setTargetGrid({
                                ...targetGrid,
                                rowData: null
                            });
                        }
                    }
                    setSaveChk(true);                    alert("저장이 완료되었습니다.");
                }
            }else{
                alert("서버통신 실패. 관리자에게 문의하세요.");
            }
        }     
    }

    const resetLabelStateEvt = () => {
        setLabelResetList({ year: false, rank: false, no: false, name: false, dir: false, lane: false });
    }
    
    return <>
        <ModalBody style={{overflow:'auto'}}>
            <div className="filter-area">
                <div className="sTarget-search-area">
                    <div className="sTarget-search-content">
                        <div id="yearFilter">
                            <SearchDropdown target={recordYear} options={yearList} placeholder="연도" onChange={recordYearOnChangeEvt} resetLabel={labelResetList["year"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                        </div>
                        <div id="restFilter">
                            <SearchDropdown target={roadRank} options={typeList} placeholder="도로 등급" onChange={roadRankOnChangeEvt} resetLabel={labelResetList["rank"]} resetStateEvt={resetLabelStateEvt} readOnly={rankReadOnly} width="230px"/> 
                            <SearchDropdown target={roadNo} options={nos} placeholder="노선 번호" onChange={roadNoOnChangeEvt} resetLabel={labelResetList["no"]} resetStateEvt={resetLabelStateEvt} readOnly={noReadOnly} width="200px"/> 
                            <SearchDropdown target={roadName} options={names} placeholder="노선명" onChange={roadNameOnChangeEvt} resetLabel={labelResetList["name"]} resetStateEvt={resetLabelStateEvt} readOnly={nameReadOnly} width="230px"/> 
                            <SearchDropdown target={roadDir} options={dirList} placeholder="행선" onChange={roadDirOnChangeEvt} resetLabel={labelResetList["dir"]} resetStateEvt={resetLabelStateEvt} readOnly={dirReadOnly} width="200px"/> 
                        </div>
                        <Button size="sm" color="secondary" style={{width:"100px"}} onClick={handleClick}>검색</Button>
                    </div>
                    <div className="sTarget-switch-area">
                        <SwitchButton label="테이블/뷰" isOpen={tableViewSwitch} onChange={handleSwitch} name="tableViewSwitch" />        
                        <SwitchButton label="이력" isOpen={recordSwitch} onChange={handleSwitch} name="recordSwitch" />        
                        <SwitchButton label="마스터" isOpen={masterSwitch} onChange={handleSwitch} name="masterSwitch" />
                    </div>
                </div>
            </div>
            <div className="sTarget-content-area">
                <div id="add_btn"><input type="button" className="moveBtn" value=">>" onClick={addToTargetGrid}/></div>
                <div id="del_btn"><input type="button" className="moveBtn" value="<<" onClick={delFromTargetGrid}/></div>
                <GridComponent id="masterGrid" columnDefs={masterGrid.columnDefs} rowData={masterGrid.rowData} rowClassRules={masterGrid.rowClassRules} onSelectionChanged={masterGrid.onSelectionChanged} style={masterGrid.style} passStateEvt={getMasterGridApi} gridTitle="노선마스터" />
                <GridComponent id="targetGrid" columnDefs={targetGrid.columnDefs} rowData={targetGrid.rowData} onSelectionChanged={targetGrid.onSelectionChanged} style={targetGrid.style} passStateEvt={getTargetGridApi} gridTitle="조사대상구간" />
                <GridComponent id="recordGrid" columnDefs={recordGrid.columnDefs} rowData={recordGrid.rowData} onSelectionChanged={recordGrid.onSelectionChanged} style={recordGrid.style} passStateEvt={getRecordGridApi} gridTitle="조사대상구간 이력" />
                <div id="viewModel" style={{flex:1, display:"none", flexDirection:"column",  height: '72vh', padding: '3px'}}>
                    <div className="grid-title">View</div>
                    <div id="view" style={{overflow:"auto", border: "1px solid #BDC3C7", padding:10}}>
                        {viewContent}
                    </div>
                </div>
            </div>
        </ModalBody>
        <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value={totalExt}/>Km
            </span>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                <Button className="feather icon-corner-right-down" color="primary" onClick={getSchedule}> 불러오기</Button>
                <Button className="feather icon-map" color="primary" onClick={viewMap}> 지도확인</Button>
                <Button className="feather icon-save" color="primary" onClick={saveBtn}> 저장</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </>;
}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({type: actionTypes.OPEN_CHAT, data: props}),
        onCloseChat: () => dispatch({type: actionTypes.CLOSE_CHAT})
    }
}

export default connect(chatStateToProps, chatDispatchToProps) (Target);