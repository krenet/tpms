import React, { useState, useRef } from 'react';
import { ModalBody, InputGroup, Input, InputGroupAddon, Button, ModalFooter } from 'reactstrap';
import { Tabs, Tab } from 'react-bootstrap';

const NodeLinkUpdate = ({toggle}) => {
    const [nodeFile, setNodeFile] = useState("");
    const [linkFile, setLinkFile] = useState("");

    const _nodeFile = useRef();
    const _linkFile = useRef();

    const textChange = () => {};

    const nodeFileClick = (e) => {
        e.preventDefault();
        _nodeFile.current.click();
    }

    const linkFileClick = (e) => {
        e.preventDefault();
        _linkFile.current.click();
    }

    const nodeFileChange = (e) => {
        setNodeFile(e.target.files[0].name);
    }

    const linkFileChange = (e) => {
        setLinkFile(e.target.files[0].name);
    }

    const insert = () => alert("업데이트 완료");
    const passToggle =() => toggle()

    return (
        <>
            <ModalBody>
                <Tabs variant="pills" defaultActiveKey="node" className="mb-3">
                        <Tab eventKey="node" title="노드">
                            <p style={{fontWeight:"bold"}}>DB파일</p>
                            <InputGroup>
                                <Input value={nodeFile} placeholder="파일을 선택해주세요." onChange={textChange}/>
                                <InputGroupAddon addonType="append">
                                <input type="file" ref={_nodeFile} style={{display:"none"}} onChange={nodeFileChange} accept=".dbf, .sql"/>
                                <Button color="primary" onClick={nodeFileClick}>File</Button>
                                </InputGroupAddon>
                            </InputGroup>
                            {/* <hr/>
                            <Button color="primary" size="lg" block onClick={insert} >업데이트</Button> */}
                        </Tab>
                        <Tab eventKey="link" title="링크">
                            <p style={{fontWeight:"bold"}}>DB파일</p>
                            <InputGroup>
                                <Input value={linkFile} placeholder="파일을 선택해주세요." onChange={textChange}/>
                                <InputGroupAddon addonType="append">
                                <input type="file" ref={_linkFile} style={{display:"none"}} onChange={linkFileChange} accept=".dbf, .sql"/>
                                <Button color="primary" onClick={linkFileClick}>File</Button>
                                </InputGroupAddon>
                            </InputGroup>
                            {/* <hr/>
                            <Button color="primary" size="lg" block onClick={insert} >업데이트</Button> */}
                        </Tab>
                    </Tabs>
            </ModalBody>
            <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                <Button style={{flex:1}} color="primary" size="sm"  onClick={insert} >업데이트</Button>
                <Button style={{flex:1}} color="secondary" size="sm"  onClick={passToggle} >닫기</Button>
            </ModalFooter>
        </>
    )
}

export default NodeLinkUpdate;