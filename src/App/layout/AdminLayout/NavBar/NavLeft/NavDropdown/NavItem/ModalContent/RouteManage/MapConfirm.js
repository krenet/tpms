import React, {useState} from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button} from 'reactstrap';
import KakaoMap from '../../../../../../../../../Demo/Dashboard/KakaoMap';
import Vworld from '../../../../../../../../../Demo/Dashboard/Vworld';
    const MapConfirm = ({isOpen, onClose}) => {
    const [subModal, setSubModal] = useState(isOpen);
    const toggle = () => setSubModal(!subModal);
    const MapConfirmStyle = {
        height : "800px",
        width : "100%"
    }
    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" onClosed={onClose}>
            <ModalHeader toggle={toggle}>지도확인</ModalHeader>
                <ModalBody>
                    <Vworld id="map" style={MapConfirmStyle} />
                </ModalBody>
            <ModalFooter style={{justifyContent:"center"}} >
                <Button color="secondary" size="sm" block onClick={toggle}>닫기</Button>
            </ModalFooter>
        </Modal>
    );

}

export default MapConfirm;