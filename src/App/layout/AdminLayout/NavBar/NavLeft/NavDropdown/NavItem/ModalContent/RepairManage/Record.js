import React, {useState, useRef, useEffect} from 'react';
import {ModalBody, ModalFooter, Button, Label, Input} from 'reactstrap';
import "./Record.css";
import GridComponent from '../../../../../../../../components/GridComponent';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import SwitchButton from '../../../../../../../../components/SwitchButton';
import {typeList} from '../Common/routeInfoList.js';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import RecordXml from '../../../../../../../../../resource/settingXml/repair-record.xml';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import TpmsView from '../../../../../../../../components/TpmsView';
import serviceUrl from  '../../../../../../../../../request-url-list';

const Record = ({toggle,onOpenChat, onCloseChat}) => {
    //선택연장
    const [totalExt, setTotalExt] = useState(0);
    // 검색
    const [searchChk, setSearchChk] = useState(false);
    const [yearList, setYearList] = useState();
    // const [rankList, setRankList] = useState(typeList);
    const [names, setNames] = useState([]);
    const [nos, setNos] = useState([]);
    const [dirList, setDirList] = useState([
        {label: '전체', value: "-1"}, 
        {label: '상행', value:"0"},
        {label:'하행', value:"1"}
    ]);
    const [laneList, setLaneList] = useState([
        {label: '1차로', value: "1" }
        // {label: '1차선', value: "-" },
        // {label: '2차선', value: "" },
        // {label: '3차선', value: "" },
        // {label: '4차선', value: "" }
    ]);
    const [year, setYear] = useState();
    const [rank, setRank] = useState();
    const [no, setNo] = useState();
    const [name, setName] = useState();
    const [dir, setDir] = useState();
    const [lane, setLane] = useState();

    const recordYear = useRef();
    const roadRank = useRef();
    const roadNo = useRef();
    const roadName = useRef();
    const roadDir = useRef();
    const roadLane = useRef();
    const [labelResetList, setLabelResetList] = useState({
        year: false,
        rank: false,
        no: false,
        name: false,
        dir: false
    });
    // 보수 이력 그리드
    const [recordCols, setRecordCols] = useState();
    const [recordGrid, setRecordGrid] = useState({});
    const [recordGridApi, setRecordGridApi] = useState();
    const [recordList, setRecordList] = useState(null); 
    const [searchList, setSearchList] = useState(null); 
    // 스위치
    const [tableViewSwitch, setTableViewSwitch] = useState(false);
    // 뷰 그리드
    const [masterList, setMasterList] = useState(null);
    const [viewDatas, setViewDatas] = useState([]);
    const [viewContent, setViewContent] = useState("");
    // 지도확인
    const [viewTargetList, setViewTargetList] = useState();
    const [viewHeader, setViewHeader] = useState({
        record : [ { label : 'year', name  : '연도' }, { label : 'repair_target_id', name  : '보수대상구간ID' },
        { label : 'spos', name  : '시점' }, { label : 'epos', name  : '종점' }, 
        { label : 'rank', name  : '등급' }, { label : 'no', name  : '노선번호' }, { label : 'name', name  : '노선명' },
        { label : 'roadDir', name  : '행선' }, { label : 'lane', name  : '차로' }, { label : 'totalExt',  name  : '총연장(km)' }]
    });

    // 서버통신체크
    const [serverChk, setServerChk] = useState(true);


    useEffect(()=>{
        yearChangeEvt();
        
        let ignore = false;
        async function fetchData(){
            const recordColumn = await XmlParser(RecordXml);

            if(!ignore){
                setRecordCols(recordColumn.getElementsByTagName('Columns')[0]['children']);
            }
        }
        fetchData();
        return () => {
            ignore = true; 
        }

    }, []);


    useEffect(()=>{
        document.getElementById("viewModel").style.display = 'none';
        if(recordCols){
            var header = tHeader(recordCols);
            setRecordGrid({
                columnDefs: header,
                rowData: null,
                style: {
                    display: "flex",
                    flex: 1,
                    flexDirection: "column",
                    padding: "3px",
                    textAlign: 'center'
                }
            });
            async function allRecordList(){
                var result = await server_connect(serviceUrl["pavement"]["getAllRepairRecordList"]);
                if(result && result['status']==200){
                    result = result['data']['res'];
                    if(result.length>0){
                        setRecordList(result);
                        setRecordGrid({
                            columnDefs: header,
                            rowData: null,
                            style: {
                                display: "flex",
                                flex: 1,
                                flexDirection: "column",
                                padding: "3px",
                                textAlign: 'center'
                            },
                            onSelectionChanged: function(e){
                                var selectedRows = e.api.getSelectedNodes();
                                var extSum = 0;
                                selectedRows.map((row)=>{
                                    var selectedRowExt = row['data']['total_ext'];
                                    extSum = extSum + selectedRowExt;
                                });
                                setTotalExt((extSum/1000).toFixed(2));
                            }
                        });
                    }
                }else{
                    setServerChk(false);
                }
            }
            allRecordList();
        }
    }, [recordCols])

    // 그리드 header 세팅
    const tHeader = (cols) => {
        var prev;
        var colNum = cols.length;
        var firstName = cols[0]['name'];
        var header = cols.map((obj, index) =>{
            if(!(prev) || obj['attributes']['className']!=prev){
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        header = header.filter(function(el){
            return el!=null;
        });

        header = header.map((obj)=>{
            return {
                headerName: obj,
                children: 
                    cols.map((col)=>{
                        var attr = col['attributes'];
                        var width = null;
                        if(attr.hasOwnProperty("wd")){
                            width = Number(attr['wd']);
                        }
                        if(attr['className']==obj && col['name']==firstName){
                            return {
                                headerName: attr['title'], 
                                field: col['name'],
                                width: width,
                                colSpan: function(params){
                                    if(isSpan(params)){
                                        return colNum;
                                    }else{
                                        return 1;
                                    }
                                },
                                // cellStyle: function(params){
                                //     if(isSpan(params)){
                                //         return {textAlign: 'center'};
                                //     }else {
                                //         return {textAlign: 'left'};
                                //     }
                                // },
                            }
                        }else if (attr['className']==obj && col['name']!=firstName){
                            return {headerName: attr['title'], field: col['name'], width: width}
                        }
                    }).filter(function(el){
                        return el!=null;
                    })
            }
        });

        function isSpan(params){
            return params.data.span === 'no-result';
        }

        return header;
    }

    useEffect(()=>{
        if(searchList!=null && masterList !=null){
            setViewContent("");
            async function getTsList(){
                var data = new Object();
                data['list'] = encodeURIComponent(JSON.stringify(searchList));
                var result = await server_connect(serviceUrl["pavement"]["getTargetTsList"], data);
                result = result['data']['res'];
                
                var viewGroup = new Object();

                masterList.map((mObj)=>{
                    var rank = mObj['rank'];
                    switch (rank) {
                        case '101':
                            mObj['rank'] = '고속국도';
                            return;
                        case '102':
                            mObj['rank'] = '도시고속국도';
                            return;
                        case '103':
                            mObj['rank'] = '일반국도';
                            return;
                        case '104':
                            mObj['rank'] = '특별·광역시도';
                            return;
                        case '105':
                            mObj['rank'] = '국가지원 지방도';
                            return;
                        case '106':
                            mObj['rank'] = '지방도';
                            return;
                        case '107':
                            mObj['rank'] = '시·군도';
                            return;
                        case '108':
                            mObj['rank'] = '기타';
                            return;
                    }
                });
    
                masterList.map((row)=>{
                    var key = ""+row['tri_id']+row['main_num']+row['sub_num']+row['dir']+1;
                    var viewList = new Array();
                    if(viewGroup.hasOwnProperty(key)){
                        viewList = viewGroup[key];
                    }
                    viewList.push(row);
                    viewGroup[key] = viewList;
                });
    
                var viewList = new Array();
                var keyList = Object.keys(viewGroup);
                var viewrank; var viewno; var viewname;
                for(var i=0;i<keyList.length;++i){
                    viewGroup[keyList[i]].map((obj, inx)=>{
                        if(inx ==0){
                            viewrank = obj['rank'];
                            viewno = obj['no'];
                            viewname = obj['name'];
                        }

                        result.map((rObj)=>{
                            var tsList = rObj['tsList'];
                            for(var i=0; i<tsList.length; ++i){
                                if(tsList[i]==obj['ts_id']) obj['repair_target_id'] = rObj['repair_target_id'];    
                            }
                        });
                    });
                        
                    var addInfo = {
                        info : {
                            rank : viewrank,
                            no : viewno,
                            name : viewname,
                            targetCol : 'repair_target_id',
                            extCol : 'sm_ext'
                        },
                        children : viewGroup[keyList[i]]
                    };
                    viewList.push(addInfo);
                }
                setViewDatas(viewList);
            }
            getTsList();
        }else{
            setViewDatas([]);
        }

    }, [masterList])

    useEffect(()=>{
        if(viewDatas.length>0){
            document.getElementById("noResult").style.display = 'none';
            setViewContent(<TpmsView dataSet={viewDatas}/>);
        }else{
            document.getElementById("noResult").style.display = 'block';
            setViewContent("");
        }
    }, [viewDatas])

    useEffect(()=>{
        setTotalExt('0');
        if(tableViewSwitch){
            document.getElementById("recordGrid").style.display = 'none';
            document.getElementById("viewModel").style.display = 'flex';
        }else{
            document.getElementById("recordGrid").style.display = 'flex';
            document.getElementById("viewModel").style.display = 'none';
        }
        
    }, [tableViewSwitch])

    const passToggle = () => {
        // onCloseChat();
        toggle();
    }

    const handleSwitch = () => {
        // setTableViewSwitch(!tableViewSwitch);

        if(year==undefined || year==null || !searchChk){
            if(!serverChk) alert('서버 통신 실패. 관리자에게 문의하세요.');
            else alert("연도를 선택하여 검색하신 후, 뷰 스위치를 켜주시기 바랍니다.");
        }else{
            setTableViewSwitch(!tableViewSwitch);
        }
    }

    const dataSort = (data) => {
        data.sort(function (a, b) {
            var before_no = parseInt(a.no);
            var after_no = parseInt(b.no);
            return b.year-a.year || a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
                   || a.main_num - b.main_num || a.dir - b.dir;
        });
        return data;
    }
    
    const yearChangeEvt = () => {
        var currentYear = new Date().getFullYear();
        var arr = new Array();
        // arr.push({label: '전체', value: 'all'});
        for(var i=currentYear; i>=(currentYear-10); --i){
            arr.push({label:i, value:i});
        }
        setYearList(arr);
    }

    // 검색필터: 연도 변경 시, 다른 검색 필터 value를 null로 변경
    const recordYearOnChangeEvt = (selectedObj) => {
        setLabelResetList({ year: false, rank: true, no: true, name: true, dir: true, lane: true });
        setYear(selectedObj['value']);
        setRank(null); setNo(null);  setName(null); setDir(null); setLane(null);

    }

    // 검색필터: 도로등급 선택 시, 해당 도로등급에 속하는 노선번호 불러오기
    const roadRankOnChangeEvt = async (selectedObj) => {
        setLabelResetList({ year: false, rank: false, no: true, name: true, dir: true, lane: true });
        setRank(selectedObj['value']);
        setNo(null);  setName(null); setDir(null); setLane(null);

        var params = new Object();
        params['rank'] = selectedObj['value'];
        const noRes = await server_connect(serviceUrl["pavement"]["selectNoListForRank"], params);
        if(noRes && noRes['status']==200){
            let noResData = noRes['data']['res'];
            var noList = makeOptionsJson(noResData);
            if(selectedObj['value']=='all') noList.unshift({label: '전체', value: 'all'});
            setNos(noList);
            setNames([]);
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    }

     // 검색필터: 노선번호 선택 시, 해당 노선번호에 속하는 노선명 불러오기
     const roadNoOnChangeEvt = async (selectedObj) => {
        setLabelResetList({ year: false, rank: false, no: false, name: true, dir: true, lane: true });
        setName(null); setDir(null); setLane(null);

        let _no = selectedObj['value'];
        setNo(_no);
        var params = new Object();
        params['rank'] = rank;
        var nameRes;
        if(_no=='all'){
            nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRank"], params);
        }else{
            params['no'] = _no;
            nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRankNo"], params);
        }
        let nameResData = nameRes['data']['res'];
        var nameList = makeOptionsJson(nameResData);
        // if(_no=='all') nameList.unshift({label: '-', value: '-'});
        if(nameList.length==0) nameList.unshift({label: '-', value: '-'});
        setNames(nameList);
    }

    const roadNameOnChangeEvt = (selectedObj) => {
        setName(selectedObj['value']);
    }

    const roadDirOnChangeEvt = (selectedObj) => {
        setDir(selectedObj['value']);
    }

    const roadLaneOnChangeEvt = (selectedObj) => {
        setLane(selectedObj['value']);
    }

    const makeOptionsJson = (arr) => {
        let list = arr.map((item)=>{
            if(item=="") {
                return {label: "-", value: "-"}
            }
            return {label:item, value:item};
        });
        return list;
    }

    const resetLabelStateEvt = () => {
        setLabelResetList({ year: false, rank: false, no: false, name: false, dir: false, lane: false });
    }

    const handleClick = (e) => {
        setTotalExt('0');
        var params = new Object();
        params['year'] = year;
        params['rank'] = rank;
        params['no'] = no;
        params['name'] = name;
        params['dir'] = dir;
        params['lane'] = lane;

        if(year==undefined && rank==undefined){
            alert("연도 또는 도로등급을 반드시 선택해 주시기 바랍니다.");
        }else{
            if(!serverChk) alert('서버 통신 실패. 관리자에게 문의하세요.');
            if(year==undefined && rank != undefined) params['year'] = null;
            
            if(recordList !=null){
                if(recordList.length>0) var searchRecordList = search(params, recordList);
                if(searchRecordList.length>0){
                    setSearchList(searchRecordList);
                    dataSort(searchRecordList);
                    setRecordGrid({
                        ...recordGrid,
                        rowData: searchRecordList
                    });
                }else{
                    setSearchList(null);
                    setRecordGrid({
                        ...recordGrid,
                        rowData: [
                            {span: 'no-result', year: "검색 결과가 없습니다"}
                        ]
                    });
                    setMasterList(null);
                }

                setSearchChk(true);

                if(searchRecordList.length>0){
                    async function getMaster(){
                        var data = new Object();
                        var list = new Array();
                        if(searchRecordList.length<2) {
                            list.push(searchRecordList[0]['no']);   
                        }else{
                            for(var i=0; i<searchRecordList.length-1; ++i){
                                if(i==0)list.push(searchRecordList[i]['no']);                            
                                if(searchRecordList[i+1]['no'] != searchRecordList[i]['no']) {
                                    list.push(searchRecordList[i+1]['no']);   
                                }
                            }
                        }
                        if(list.length>0){
                            data['no'] = list;
                            var result = await server_connect(serviceUrl["pavement"]["getSearchMaster"], data);
                            result = result['data']['res'];
                            if(result.length>0){
                                setMasterList(result);
                            }
                        }
                    }
                    getMaster();
                }else{
                    // setMasterList(null);
                    // document.getElementById("noResult").style.display = 'block';
                }
            }           
        }
    }

    const search  = (params, dataList) => {
        var resultList = new Array();
        for(var i=0; i<dataList.length; ++i){
            var chk=true;
            if(params['year'] != 'all' && params['year'] !=null){
                if((String)(dataList[i]['year']) == params['year']){
                    chk = search2(params, dataList, chk, i);
                } else{
                    chk=false;
                }
            }else {
                chk = search2(params, dataList, chk, i);
            }

            if(chk) resultList.push(dataList[i]);
        }

        return resultList;
    }

    const search2  = (params, dataList, chk, i) => {
        if(params['rank'] !=null){
            if(params['rank'] != 'all'){
                if(dataList[i]['rank'] != params['rank']) chk=false; 
            }
            if(params['no'] != null){
                if(params['no'] != 'all'){
                    if(dataList[i]['no'] != params['no']) chk=false;
                }
                if(params['name'] !=null){
                    if(params['name'] !='all'){
                        if(dataList[i]['name'] != params['name']) chk=false;
                    }
                }   
            }
        }

        if(params['dir'] != '-1' && params['dir'] !=null){
            if(dataList[i]['dir'] != params['dir']) chk=false;
        }

        if(params['lane'] !=null){
            if(dataList[i]['lane'] != params['lane']) chk=false;
        }

        return chk;
    }


    const viewMap = () => {
        var props =[];
        var recordNodes = recordGridApi.getSelectedNodes();
        if(recordNodes.length>0){
            // dataSort(recordList);
            dataSort(searchList);
            getRepairRecordTsList(searchList, "record", recordNodes);
        }else{
            alert("확인하실 구간을 먼저 선택해 주시기 바랍니다");
        }
    }

    const getRepairRecordTsList = async (list, type, selectedNodes) => {
        if(list !=null){
            var data = new Object();
            data['list'] = encodeURIComponent(JSON.stringify(list));
            var result = await server_connect(serviceUrl["pavement"]["getTargetTsList"], data);   // 서버통신
            setViewTargetList({ list : selectedNodes, type : type, allData : result['data']['res']});
        }
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display='none';
        document.getElementsByClassName('modal-backdrop')[0].style.display='none';
        document.getElementsByClassName('modal-dialog')[0].style.display='none';
    }

    useEffect(()=>{
        if(viewTargetList){
            var props = [];
            props['allData'] = viewTargetList["allData"];

            var list = viewTargetList["list"].map((el)=>{
                var obj = el["data"];
                viewTargetList["allData"].map((row)=>{
                    if(obj["repair_target_id"]==row["repair_target_id"]){
                        obj["tsList"] = row["tsList"];
                        el["data"] = obj;
                        return;
                    }
                });
                return el;
            });

            props['selectedNodes'] = list;
            props['header'] = viewHeader[viewTargetList["type"]];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }
    }, [viewTargetList]);


    const getRecordGridApi = (childState) => {
        setRecordGridApi(childState);
    }

    return (
        <>
            <ModalBody style={{overflow: 'auto'}}>
                <div className="record-filter-area">
                    <div className="record-search-area">
                        <div className="record-search-content">
                            <div id="filter">
                                <SearchDropdown target={recordYear} options={yearList} placeholder="연도" onChange={recordYearOnChangeEvt} resetLabel={labelResetList["year"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                                <SearchDropdown target={roadRank} options={typeList} placeholder="도로 등급" onChange={roadRankOnChangeEvt} resetLabel={labelResetList["rank"]} resetStateEvt={resetLabelStateEvt} width="230px"/> 
                                <SearchDropdown target={roadNo} options={nos} placeholder="노선 번호" onChange={roadNoOnChangeEvt} resetLabel={labelResetList["no"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                                <SearchDropdown target={roadName} options={names} placeholder="노선명" onChange={roadNameOnChangeEvt} resetLabel={labelResetList["name"]} resetStateEvt={resetLabelStateEvt} width="230px"/> 
                                <SearchDropdown target={roadDir} options={dirList} placeholder="행선" onChange={roadDirOnChangeEvt} resetLabel={labelResetList["dir"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                                <SearchDropdown target={roadLane} options={laneList} placeholder="차로" onChange={roadLaneOnChangeEvt} resetLabel={labelResetList["lane"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                            </div>
                            <Button size="sm" color="secondary" style={{width:"100px"}} onClick={handleClick}>검색</Button>
                        </div>
                        <div className="record-switch-area">
                            <SwitchButton label="테이블/뷰" isOpen={tableViewSwitch} onChange={handleSwitch} name="tableViewSwitch" />        
                        </div>
                    </div>
                </div>
                <div className="record-content-area">
                    <GridComponent id="recordGrid" columnDefs={recordGrid.columnDefs} rowData={recordGrid.rowData} onSelectionChanged={recordGrid.onSelectionChanged} style={recordGrid.style} passStateEvt={getRecordGridApi} gridTitle="보수 이력" />
                    <div id="viewModel" style={{flex:1, display:"flex", flexDirection:"column", padding: "3px", position: 'relative'}}>
                        <div className="grid-title">View</div>
                        <div id="noResult">검색 결과에 해당하는 데이터가 없습니다</div>
                        <div style={{overflow:"auto", border: '1px solid #BDC3C7', padding:10}}>
                            {viewContent}
                        </div>
                    </div>
                </div>
            </ModalBody>
            <ModalFooter style={{justifyContent:"unset"}}>
                <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                    <Label style={{margin:"3px"}}>선택연장</Label>
                    <Input type="text" disabled style={{display:"inline", width:"200px"}} value={totalExt}/>Km
                </span>
                <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                    {/* <Button color="primary">일괄수정</Button> */}
                    <Button className="feather icon-map" color="primary" onClick={viewMap}> 지도확인</Button>
                    <Button color="secondary" onClick={passToggle}>닫기</Button>
                </span>
            </ModalFooter>
        </>
    );
}

const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({type: actionTypes.OPEN_CHAT, data: props}),
        onCloseChat: () => dispatch({type: actionTypes.CLOSE_CHAT})
    }
}

export default connect(chatStateToProps, chatDispatchToProps) (Record);