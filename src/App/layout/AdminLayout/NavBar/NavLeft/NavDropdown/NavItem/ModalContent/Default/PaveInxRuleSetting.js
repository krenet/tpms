import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import * as actionTypes from '../../../../../../../../../store/actions';
import { Form, FormGroup, Input, Button, ModalBody, ModalFooter, Label } from 'reactstrap';
import { server_connect} from '../../../../../../../../../tpms-common.js';
import serviceUrl from  '../../../../../../../../../request-url-list';

const PaveInxRuleSetting = ({toggle, onChangeFormula}) => {

    const [rule, setRule] = useState();
    
    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            var result = await server_connect(serviceUrl["pavement"]["getIndexFormula"]);
            if(!ignore) {
                if(result && result['status']==200) setRule(result['data']['res']);
                else alert('서버 통신 실패. 관리자에게 문의하세요.');
            }
        }
        fetchData();
        return () => { ignore = true; }
    }, []);

    const save = async () => {
        var params = new Object();
        params['formula'] = rule;
        var result = await server_connect(serviceUrl["pavement"]["setIndexFormula"], params);
        if(result && result['status']==200) {
            if(result['data']['res']) alert("설정 완료");
            onChangeFormula(rule);
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    }

    const radioBtnChangeEvt = (evt) => setRule(evt.target.value);
   
    const passToggle = () =>{
        toggle();
    }

    return (
        <>
            <Form>
            <ModalBody>
                {/* <FormGroup> */}
                    {/* <Input type="textarea" name="rule" id="rule" value={rule} onChange={ruleChange}/> */}
                {/* </FormGroup> */}
                <FormGroup tag="fieldset" style={{marginBottom: "unset", textAlign: "center" }}>
                    <FormGroup check style={{marginRight:"10px", fontSize:"medium", display:"inline"}}>
                    <Input id="spi" type="radio" name="rule" value="spi" onChange={radioBtnChangeEvt} checked={rule === 'spi'}/>
                    <Label check for="spi">
                        SPI
                    </Label>
                    </FormGroup>
                    <FormGroup check style={{marginRight:"10px", fontSize:"medium", display:"inline"}}>
                    <Input type="radio" name="rule" id="nhpci" value="nhpci" onChange={radioBtnChangeEvt} checked={rule === 'nhpci'}/>
                    <Label check for="nhpci">
                        NHPCI
                    </Label>
                    </FormGroup>
                    {/* <FormGroup check style={{marginRight:"10px", fontSize:"medium", display:"inline"}}>
                    <Input type="radio" name="rule" id="hpci" value="hpci" onChange={radioBtnChangeEvt} checked={rule === 'hpci'}/>
                    <Label check for="hpci">
                        HPCI
                    </Label>
                    </FormGroup> */}
                    <FormGroup check style={{marginRight:"10px", fontSize:"medium", display:"inline"}}>
                    <Input type="radio" name="rule" id="mpci" value="mpci" onChange={radioBtnChangeEvt} checked={rule === 'mpci'}/>
                    <Label check for="mpci">
                        MPCI
                    </Label>
                    </FormGroup>
                </FormGroup>
            </ModalBody>
            <ModalFooter style={{justifyContent:"center", display:"flex"}} >
                <Button style={{flex:1}} color="info"  onClick={save}>저장</Button>
                <Button style={{flex:1}} color="secondary" onClick={passToggle}>닫기</Button>
            </ModalFooter>
            </Form>
        </>
    );
}

const dispatchToProps = dispatch => {
    return{
        onChangeFormula: (formula) => dispatch({type: actionTypes.PAVE_INX_RULE, formula: formula})
    }
}

export default connect(null, dispatchToProps) (PaveInxRuleSetting);