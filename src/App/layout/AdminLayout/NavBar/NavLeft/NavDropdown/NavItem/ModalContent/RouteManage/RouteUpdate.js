import React, {useState, useRef, useEffect} from 'react';
import { Modal, ModalHeader, Button, ModalBody, InputGroup, Input, InputGroupAddon, InputGroupText, ModalFooter, Form } from 'reactstrap';
import { Row, Col, Tabs, Tab, Nav } from 'react-bootstrap';
import InputComponent from '../../../../../../../../components/InputComponent';
import fileFormat from '../../../../../../../../../resource/fileFormat/노선정보수정_양식.xlsx';
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import RoadXML from '../../../../../../../../../resource/settingXml/road-info.xml';
import {XmlParser, server_connect, server_connect_withfile} from '../../../../../../../../../tpms-common.js';
import serviceUrl from  '../../../../../../../../../request-url-list';

const roadRanks = [{label: '고속국도', value:"101"}, 
                   {label: '도시고속국도', value:"102"},
                   {label: '일반국도', value:"103"},
                   {label: '특별·광역시도', value:"104"},
                   {label: '국가지원 지방도', value:"105"},
                   {label: '지방도', value:"106"},
                   {label: '시·군도', value:"107"},
                   {label: '기타', value:"108"}
                  ];
const RouteUpdate = ({isOpen, onClose}) => {
    const [subModal, setSubModal] = useState(isOpen);
    const [noList, setNoList] = useState([]);
    const [nameList, setNameList] = useState([]);
    const [rank, setRank] = useState(null);
    const [no, setNo] = useState(null);
    const [name, setName] = useState(null);
    const [columns, setColumns] = useState();
    const [nodeList, setNodeList] = useState();
    const [content, setContent] = useState();

    const toggle = () => setSubModal(!subModal);

    const roadRankOnChangeEvt = async (selectedObj) => {
        setRank(selectedObj['value']);
        setNo(null);
        setName(null);
        var params = new Object();
        params['rank'] = selectedObj['value'];
        const noRes = await server_connect(serviceUrl["pavement"]["selectNoListForRank"], params);
        if(noRes && noRes['status']==200) {
            let noResData = noRes['data']['res'];
            setNoList(makeOptionsJson(noResData));
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
            return;
        }

        const nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRank"], params);
        let nameResData = nameRes['data']['res'];
        setNameList(makeOptionsJson(nameResData));
    }

    const makeOptionsJson = (arr) => {
        let list = arr.map((item)=>{
            return {label:item, value:item};
        });
        return list;
    }

    const roadNoOnChangeEvt = async (selectedObj) => {
        setName(null);
        let _no = selectedObj['value'];
        setNo(_no);

        var params = new Object();
        params['rank'] = rank;
        params['no'] = _no;
        const nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRankNo"], params);
        let nameResData = nameRes['data']['res'];
        setNameList(makeOptionsJson(nameResData));
    }
    
    const roadNameOnChangeEvt = async (selectedObj) => {
        let _name = selectedObj['value'];
        setName(_name);
    }

    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const nlist = await server_connect(serviceUrl["pavement"]["selectNodeList"]);
            const noRes = await server_connect(serviceUrl["pavement"]["selectNoList"]);
            const nameRes = await server_connect(serviceUrl["pavement"]["selectNameList"]);
            const xml = await XmlParser(RoadXML);
            if(!ignore) {
                if(nlist && nlist['status']==200){
                    setNodeList(nlist['data']['res']);
                    let noResData = noRes['data']['res'];
                    setNoList(makeOptionsJson(noResData));
    
                    let nameResData = nameRes['data']['res'];
                    setNameList(makeOptionsJson(nameResData));                    
                }

                setColumns(xml.getElementsByTagName('Columns')[0]['children']);
            }
        }
        fetchData();
        return ()=>{ignore=true;}
    }, []);

    useEffect(()=>{
        if(columns){
            setContent(columns.map((obj, inx)=>{
                var attributes = obj['attributes'];
                return attributes['visible']=='true'?
                        <InputComponent key={inx} label={attributes['title']} name={obj['name']} type={attributes['type']} json={attributes['json']}  settingDataEvt={eval(attributes['options'])}/>
                        :
                        <input type="hidden" key={inx} name={obj['name']} value="0"/>
            }));
        }
    }, [columns]);

    useEffect(()=>{
        async function getRoadInfo(){
            if((rank && no && no!='-') || (rank && name)){
                
                var params = new Object();
                params['rank'] = rank;
                params['no'] = no;
                params['name'] = name;
    
                const res = await server_connect(serviceUrl["pavement"]["selectOneRoadInfo"], params);
                const roadInfo = res['data']['res'];

                var jsonList = null;
                if(roadInfo['etc']!=null){
                    jsonList = JSON.parse(roadInfo['etc']);
                }
                

                var inputNames = columns.map((obj)=>{
                    return obj['name'];
                });

                var fieldTypes = columns.map((obj)=>{
                    return obj['attributes']['data-type'];
                });

                inputNames.map((name, inx)=> {
                    var el = document.getElementsByName(name)[0];

                    if(fieldTypes[inx]=='json'){
                        if(jsonList){el.value = jsonList[name];}
                        else{el.value = '';}
                    }else{
                        el.value = roadInfo[name];
                    }
                });
            }
        }
        getRoadInfo();
    }, [rank, no, name]);

    const makeSelectInput = () => {
        if(nodeList){
            let options = nodeList.map((obj)=>{
                return <option value={obj["node_id"]}>{obj["node_name"]}</option>;
            });
            return options;
        }
    }

    const submitForm = (evt) => {
        evt.preventDefault();
        const data = new FormData(evt.target);
        eachUpdateRoad(data);
    }

    const eachUpdateRoad = async (data) => {
        var params = new Object();
        columns.map((obj)=>{
            if(typeof obj['attributes']['json']=='undefined') params[obj['name']] = data.get(obj['name']);
        });
        var jsonClassObj = document.getElementsByClassName('json');
        var jsonObj = new Object();
        for(var i=0;i<jsonClassObj.length;++i){
            var key = jsonClassObj[i].getAttribute('name');
            var value = jsonClassObj[i].value;
            jsonObj[key] = value;
        }
        params['etc'] = encodeURIComponent(JSON.stringify(jsonObj));

        var originalObj = new Object();
        originalObj['rank'] = rank;
        originalObj['no'] = no;
        originalObj['name'] = name;

        var paramList = new Object();
        paramList['updateObj'] = encodeURIComponent(JSON.stringify(params));
        paramList['originalObj'] = encodeURIComponent(JSON.stringify(originalObj));

        const result = await server_connect(serviceUrl["pavement"]["updateEachRoadInfo"], paramList);
        if(result && result['status']==200) {
            if(result['data']['res']==1) alert('수정이 완료되었습니다.');
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    }

    const makeRoadRankOptions = () => {
        let options = roadRanks.map((obj)=>{
            return <option value={obj["value"]}>{obj["label"]}</option>;
        });
        return options;
    }
    
    return (
        <Modal isOpen={subModal} toggle={toggle} centered={true} backdrop="static" unmountOnClose={false} onClosed={onClose} style={{width:"800px", maxWidth:"800px"}}>{/*onClosed={closeAll ? toggle : undefined}*/}
            <Form onSubmit={submitForm} >
                <ModalHeader toggle={toggle}>등록 및 수정(노선 수정)</ModalHeader>
                <ModalBody>
                    {/* <Tabs variant="pills" defaultActiveKey="all" className="mb-3" onSelect={key=>setTabKey(key)}>
                        <Tab eventKey="all" title="일괄수정">
                            <p style={{fontWeight:"bold", display:"inline"}}>수정노선파일리스트</p>
                            <Button onClick={downloadFileFormat} style={{fontSize:"10px", padding:"3px 7px", float:"right", marginRight:"0"}} outline color="secondary" size="sm">
                                양식다운로드
                            </Button>
                            <InputGroup>
                                <Input value={filename} placeholder="파일을 선택해주세요." onChange={textChange}/>
                                <InputGroupAddon addonType="append">
                                <input type="file" ref={_file} style={{display:"none"}} onChange={fileChange} accept=".xlsx, .csv"/>
                                <Button color="primary" onClick={allFile}>File</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </Tab>
                        <Tab eventKey="each" title="개별수정"> */}
                            <span style={{marginBottom:"5px"}}>
                                <SearchDropdown options={roadRanks} placeholder="등급" onChange={roadRankOnChangeEvt} width="150" label="노선등급" />
                                <SearchDropdown options={noList} placeholder="번호" onChange={roadNoOnChangeEvt} width="150" label="노선번호" />
                                <SearchDropdown options={nameList} placeholder="노선명" onChange={roadNameOnChangeEvt} width="200" label="노선명" />
                                {/* <SearchDropdown options={} placeholder="노선번호" onChange={}/>
                                <SearchDropdown options={} placeholder="노선명" onChange={}/> */}
                            </span>
                            <br/><br/>
                            {/* {
                                columns.map((obj, inx)=>{
                                    var attributes = obj['attributes'];
                                    return attributes['visible']=='true'?
                                            <InputComponent key={inx} label={attributes['title']} name={obj['name']} type={attributes['type']} json={attributes['json']}  settingDataEvt={eval(attributes['options'])}/>
                                            :
                                            <input type="hidden" key={inx} name={obj['name']} value="0"/>
                                })
                            } */}
                            {content}
                            {/* { RouteCol.map((label, index) => { return <InputComponent label={label} val={RouteVal[index]}/>}) } */}
                        {/* </Tab>
                    </Tabs> */}
                </ModalBody>
                <ModalFooter style={{justifyContent:"center", display:"flex"}}>
                    {/* <Button style={{flex:1}} color="primary" size="sm" block onClick={update} >수정</Button> */}
                    <Button type='submit' style={{flex:1}} color="primary" size="sm" block >수정</Button>
                    <Button style={{flex:1}} color="secondary" size="sm"  onClick={toggle} >닫기</Button>
                </ModalFooter>
            </Form>
        </Modal>
    );
}

export default RouteUpdate;