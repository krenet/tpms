import React, { useState, useEffect, useRef } from 'react';
import { ModalBody, ModalFooter, Label, Button, Input } from 'reactstrap';
import ToggleButton from 'react-bootstrap/ToggleButton';
import "./RepairTarget.css";
import SearchDropdown from '../../../../../../../../components/SearchDropdown';
import SwitchButton from '../../../../../../../../components/SwitchButton';
import GridComponent from '../../../../../../../../components/GridComponent';
import {typeList} from '../Common/routeInfoList.js';
import {XmlParser, server_connect} from '../../../../../../../../../tpms-common.js';
import MasterColXML from '../../../../../../../../../resource/settingXml/master.xml';
import RepairXml from '../../../../../../../../../resource/settingXml/repair-target.xml';
import Equip1XML from '../../../../../../../../../resource/settingXml/equip1.xml';
import CrackXML from '../../../../../../../../../resource/settingXml/equip2-crack.xml';
import GprXML from '../../../../../../../../../resource/settingXml/equip2-gpr.xml';
import HwdXML from '../../../../../../../../../resource/settingXml/equip2-hwd.xml';
import { ButtonGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import TpmsView from '../../../../../../../../components/TpmsView';
import serviceUrl from  '../../../../../../../../../request-url-list';

const BatchModified = React.lazy(() => import('../RouteManage/BatchModified.js'));

// 보수대상구간 Modal Content
const RepairTarget = ({toggle, onOpenChat, onCloseChat}) => {
    //선택연장
    const [totalExt, setTotalExt] = useState(0);
    // 검색
    const [yearList, setYearList] = useState();
    const [rankList, setRankList] = useState(typeList);
    const [names, setNames] = useState([]);
    const [nos, setNos] = useState([]);
    const [dirList, setDirList] = useState([
        {label: '전체', value: "-1"}, 
        {label: '상행', value:"0"},
        {label:'하행', value:"1"}
    ]);
    const [laneList, setLaneList] = useState([
        {label: '1차선', value: "-" }
        // {label: '1차선', value: "-" },
        // {label: '2차선', value: "" },
        // {label: '3차선', value: "" },
        // {label: '4차선', value: "" }
    ]);
    const [year, setYear] = useState();
    const [rank, setRank] = useState();
    const [no, setNo] = useState();
    const [name, setName] = useState();
    const [dir, setDir] = useState();
    const [lane, setLane] = useState();
    const recordYear = useRef();
    const roadRank = useRef();
    const roadNo = useRef();
    const roadName = useRef();
    const roadDir = useRef();
    const roadLane = useRef();
    const [labelResetList, setLabelResetList] = useState({
        year: false,
        rank: false,
        no: false,
        name: false,
        dir: false
    });
    // 스위치
    const [masterSwitch, setMasterSwitch] = useState(false);
    const [equipSwitch, setEquipSwitch] = useState(false);
    const [tableViewSwitch, setTableViewSwitch] = useState(false);
    const [recordSwitch, setRecordSwitch] = useState(false);
    // 마스터
    const [routeMasterCols, setRouteMasterCols] = useState();
    const [routeMaster, setRouteMaster] = useState({});
    const [routeMasterGridApi, setRouteMasterGridApi] = useState();
    const [routeMasterList, setRouteMasterList] = useState(null);  // 마스터 전체 리스트
    const [masterSelectTs, setMasterSelectTs] = useState(); // 마스터에서 선택된 row 내 ts_id 리스트
    const [masterSelectTId, setMasterSelectTId] = useState();  // 마스터에서 선택된 row 내 target_id 리스트 
    const [searchMList, setSearchMList] = useState(null);  // 마스터 검색 후 리스트
    // 보수대상
    const [repairCols, setRepairCols] = useState();
    const [repairGrid, setRepairGrid] = useState({});
    const [repairGridApi, setRepairGridApi] = useState();
    const [repairList, setRepariList] = useState(null);  // 당해 연도 리스트
    const [repairTsIds, setRepairTsIds] = useState(null);  // 당해 연도 보수구간 ts_id 리스트
    // 보수대상 이력
    const [recordGrid, setRecordGrid] = useState({});
    const [recordGridApi, setRecordGridApi] = useState();
    const [recordList, setRecordList] = useState(null);  // 보수대상 이력 리스트
    // 1/2차 장비조사
    const [equipGrid, setEquipGrid] = useState({});
    const [equipGridApi, setEquipGridApi] = useState();
    const [pesCols, setPesCols] = useState();
    const [pesTHead, setPesTHead] = useState();
    const [pesList, setPesList] = useState(null); // PES 리스트
    const [crackCols, setCrackCols] = useState();
    const [crackTHead, setCrackTHead] = useState();
    const [crackList, setCrackList] = useState(null); // CRACK 리스트
    const [gprCols, setGprCols] = useState();
    const [gprTHead, setGprTHead] = useState();
    const [gprList, setGprList] = useState(null); // GPR 리스트
    const [hwdCols, setHwdCols] = useState();
    const [hwdTHead, setHwdTHead] = useState();
    const [hwdList, setHwdList] = useState(null); // HWD 리스트
    // 뷰
    const [viewDatas, setViewDatas] = useState([]);
    const [viewContent, setViewContent] = useState("");
    
    // 지도확인
    const [viewTargetList, setViewTargetList] = useState();
    const [viewHeader, setViewHeader] = useState({
        repair : [  { label : 'repair_target_id', name  : '보수대상구간ID' }, { label : 'spos', name  : '시점' },
                    { label : 'epos', name  : '종점' },  { label : 'rank', name  : '등급' },
                    { label : 'no',  name  : '노선번호' }, { label : 'name', name  : '노선명'},
                    { label : 'roadDir',  name  : '행선' }, { label : 'totalExt',  name  : '총연장(km)' } ],
        record : [ { label : 'year', name  : '연도' }, { label : 'repair_target_id', name  : '보수대상구간ID' },
                    { label : 'detail_target_id', name  : '상세조사구간ID' }, { label : 'spos', name  : '시점' },
                    { label : 'epos', name  : '종점' }, { label : 'rank', name  : '등급' },
                    { label : 'no', name  : '노선번호' }, { label : 'name', name  : '노선명' },
                    { label : 'roadDir', name  : '행선' }, { label : 'totalExt',  name  : '총연장(km)' }]
                });
    // 보수대상구간 신규 등록 여부 
    const [saveStatus, setSaveStatus] = useState(false);
    // 버튼 그룹
    const [btnGroup, setBtnGroup] = useState();
    // 서브모달
    const [subModal, setSubModal] = useState(null);
    const batchModified = (dataList) => setSubModal(<BatchModified isOpen={true} onClose={subModalCloseEvt} list={dataList} afterSave={saveSuccess}/>);
    const subModalCloseEvt = () => {
        setSubModal(null);
    }

    const [selectedGridType, setSelectedGridType] = useState();
 
    // 서버통신체크
    const [serverChk, setServerChk] = useState(false);


    useEffect(()=>{
        yearChangeEvt();

        document.getElementById("bottom-content").style.display = "none";
        document.getElementById("routeMaster").style.display = "none";
        
        let ignore = false;
        async function fetchData(){
            const routeColumn = await XmlParser(MasterColXML);
            const repairColumn = await XmlParser(RepairXml);
            const pesColumn = await XmlParser(Equip1XML);
            const crackColumn = await XmlParser(CrackXML);
            const gprColumn = await XmlParser(GprXML);
            const hwdColumn = await XmlParser(HwdXML);
            if(!ignore){
                setRouteMasterCols(routeColumn.getElementsByTagName('Columns')[0]['children']);
                setRepairCols(repairColumn.getElementsByTagName('Columns')[0]['children']);
                setPesCols(pesColumn.getElementsByTagName('Columns')[0]['children']);
                setCrackCols(crackColumn.getElementsByTagName('Columns')[1]['children']);
                setGprCols(gprColumn.getElementsByTagName('Columns')[1]['children']);
                setHwdCols(hwdColumn.getElementsByTagName('Columns')[1]['children']);
            }
        }
        fetchData();
        return () => {
            ignore = true; 
        }

    }, []);

   
    useEffect(()=>{
        if(routeMasterCols) {
            var header = tHeader(routeMasterCols);
            setRouteMaster({
                columnDefs : header,
                rowData: null,
                style: {
                    padding: '3px',
                    display: 'none',
                    flex: 1,
                    flexDirection: 'column',
                    heigh: '72vh',
                    textAlign: 'center'
                }
            });
            async function allMasterList(){
                var result = await server_connect(serviceUrl["pavement"]["getAllMaster"]);
                if(result && result['status']==200){
                    setServerChk(true);
                    result = result['data']['res'];
                    if(result.length>0){
                        setRouteMasterList(result);
                        setRouteMaster({
                            columnDefs : header,
                            rowData: null,
                            style: {
                                padding: '3px',
                                display: 'none',
                                flex: 1,
                                flexDirection: 'column',
                                heigh: '72vh',
                                textAlign: 'center'
                            },
                            onSelectionChanged: function(e) {
                                var selectedRows = e.api.getSelectedNodes();
                                if(selectedRows.length==0) return;
                                setSelectedGridType('master');
                                var extSum = 0;
                                var selRows_ts = new Array();
                                var selRows_target = new Array();
                                selectedRows.map((row)=>{
                                    var selectedRow_ts = row['data']['ts_id'];
                                    var selectedRow_target = row['data']['target_id'];
                                    var selectedRowExt = row['data']['sm_ext'];
                                    extSum = extSum + selectedRowExt;
                                    selRows_ts.push(selectedRow_ts);
                                    selRows_target.push(selectedRow_target);
                                });
                                setMasterSelectTId(selRows_target);
                                setMasterSelectTs(selRows_ts);
                                setTotalExt((extSum/1000).toFixed(2));
                            },
                            rowClassRules: null
                        });
                    }
                }else{
                    setServerChk(false);
                }
            }
            allMasterList();
        }
    }, [routeMasterCols]);

    useEffect(()=>{
        if(masterSelectTId){
            var targetList = masterSelectTId.filter(function(el){
                return el !=null;
            });
            async function equip1List(){
                var params = new Object();
                params['target_id'] = targetList;
                var result = await server_connect(serviceUrl["pavement"]["getSelectEquip1Data"], params);
                result = result['data']['res'];
                setPesList(result);
            }
            equip1List();
        }
    }, [masterSelectTId]);

    useEffect(()=>{
        if(masterSelectTs){
            var tsList = masterSelectTs.filter(function(el){
                return el !=null;
            });
            async function equip2List(){
                var params = new Object();
                params['ts_id'] = tsList;
                var result = await server_connect(serviceUrl["pavement"]["getSelectEquip2Data"], params);
                var result_crack = result['data']['res_crack'];
                var result_gpr = result['data']['res_gpr'];
                var result_hwd = result['data']['res_hwd'];
                setCrackList(result_crack);
                setGprList(result_gpr);
                setHwdList(result_hwd);
            }
            equip2List();
        }
    }, [masterSelectTs]);

    useEffect(()=>{
        if(pesList && radioValue=='1'){
            setEquipGrid({
                ...equipGrid,
                columnDefs: pesTHead,
                rowData: pesList
            });
            // changeEquipGrid(pesTHead, 'pes', pesList);
        }
        if(crackList && radioValue=='2'){
            setEquipGrid({
                ...equipGrid,
                columnDefs: crackTHead,
                rowData: crackList
            });
        }

        if(gprList && radioValue=='3'){
            setEquipGrid({
                ...equipGrid,
                columnDefs: gprTHead,
                rowData: gprList
            });
        }

        if(hwdList && radioValue=='4'){
            setEquipGrid({
                ...equipGrid,
                columnDefs: hwdTHead,
                rowData: hwdList
            });
        }

    },[pesList, crackList, gprList, hwdList]);

    useEffect(()=>{
        if(repairCols){
            var header = tHeader(repairCols);
            setRepairGrid({
                columnDefs: header,
                rowData: null,
                style: {display: "flex", flex:1, flexDirection: "column", padding: '3px', height: '72vh', textAlign: 'center'}
            });
            // 당해연도 보수대상구간
            async function currYearMaint(){
                // 이미 보수대상구간으로 선정된 구간의 ts_id 리스트 불러오기
                var tsId_list = await server_connect(serviceUrl["pavement"]["getCurrMaintTs_Id"]);
                var repairTsId_list = new Array();  
                if(tsId_list && tsId_list['status']==200){
                    setServerChk(true);
                    if(tsId_list['data'].hasOwnProperty("res")){
                        repairTsId_list = tsId_list['data']['res'];
                        setRepairTsIds(repairTsId_list);
                    }
                }else{
                    // alert('서버 통신 실패. 관리자에게 문의하세요.');
                    setServerChk(false);
                    return;
                }
                // 당해 선정된 보수대상구간 불러오기
                let today = new Date();
                let currYear = today.getFullYear();
                var params = new Object();
                params['year'] = currYear;
                var result = await server_connect(serviceUrl["pavement"]["getCurrYearMaint"], params);
                if(result['data'].hasOwnProperty('res')){
                    result = result['data']['res'];
                    if(result.length>0){
                        setRepariList(result);
                        dataSort(result, "repair");       
                    }else{
                        result= null;
                    }
                }else{
                    result = null;
                }
                setRepairGrid({
                    ...repairGrid,
                    rowData: result,
                    columnDefs: header,
                    rowData: result,
                    style: {display: "flex", flex:1, flexDirection: "column", padding: '3px', height: '72vh', textAlign: 'center'},
                    onSelectionChanged: function(e){
                        var selectedRows = e.api.getSelectedNodes();
                        if(selectedRows.length==0) return;
                        setSelectedGridType('repair');
                        var extSum = 0;
                        selectedRows.map((row)=>{
                            var selectedRowExt = row['data']['total_ext'];
                            extSum = extSum + selectedRowExt;
                        });
                        setTotalExt((extSum/1000).toFixed(2));
                    },
                });
            }
            currYearMaint();
            
            // 보수대상구간 이력
            async function allMaint(){
                var result = await server_connect(serviceUrl["pavement"]["getAllMaint"]);
                if(result && result['status']==200){
                    if(result['data'].hasOwnProperty('res')){
                        result = result['data']['res'];
                        if(result.length>0) {
                            setRecordList(result);
                        }
                    } 
                }else{
                    return;
                }
            }
            allMaint();

            setRecordGrid({
                columnDefs: header,
                rowData: null,
                style: {display: "none", flex:1, flexDirection: "column", padding: '3px', height: '72vh'},
                onSelectionChanged: function(e){
                    var selectedRows = e.api.getSelectedNodes();
                    if(selectedRows.length==0) return;
                    setSelectedGridType('record');
                    var extSum = 0;
                    selectedRows.map((row)=>{
                        var selectedRowExt = row['data']['total_ext'];
                        extSum = extSum + selectedRowExt;
                    });
                    setTotalExt((extSum/1000).toFixed(2));
                },
            });             
            
            document.getElementById("yearFilter").style.display = "none";
            document.getElementById("laneFilter").style.display = "none";
        }
    }, [repairCols]);

    useEffect(()=>{
        if(pesCols){
            var header = tHeader(pesCols);
            setPesTHead(header);
            setEquipGrid({
                columnDefs : header,
                rowData : null,
                style : {
                    display : 'block',
                    width : '100%',
                    height : '23vh',
                    marginTop : '18px',
                    textAlign: 'center'
                },
                onSelectionChanged: function(e){
                    var selectedRows = e.api.getSelectedNodes();
                    if(selectedRows.length==0) return;
                    setSelectedGridType('pes');
                    // var extSum = 0;
                    // selectedRows.map((row)=>{
                    //     var selectedRowExt = row['data']['sm_ext'];
                    //     extSum = extSum + selectedRowExt;
                    // });
                    // setTotalExt((extSum/1000).toFixed(2));
                }
            });
        }
    }, [pesCols])

    useEffect(()=>{
        if(crackCols){
            var header = tHeader(crackCols);
            setCrackTHead(header);
        }
    }, [crackCols])

    useEffect(()=>{
        if(gprCols){
            var header = tHeader(gprCols);
            setGprTHead(header);
        }
    }, [gprCols])

    useEffect(()=>{
        if(hwdCols){
            var header = tHeader(hwdCols);
            setHwdTHead(header);
        }
    }, [hwdCols])

    useEffect(()=>{
        setLabelResetList({ year: true, rank: true, no: true, name: true, dir: true, lane: true });
        setNos([]); setNames([]);
        setYear(); setRank(); setNo(); setName(); setDir(); setLane();
        setTotalExt('0');
        setSelectedGridType('none');
        setRepairGrid({
            ...repairGrid,
            rowData: repairList
        });
        
        if(masterSwitch){
            setRouteMaster({
                ...routeMaster,
                rowData: null,
                style: {
                    padding: '3px',
                    display: 'flex',
                    flex: 1,
                    flexDirection: 'column',
                    textAlign: 'center'
                }
            });
            setRankList([
                {label: '고속국도', value: '101'},
                {label: '도시고속국도', value: '102'},
                {label: '일반국도', value: '103'},
                {label: '특별·광역시도', value: '104'},
                {label: '국가지원 지방도', value: '105'},
                {label: '지방도', value: '106'},
                {label: '시·군도', value: '107'},
                {label: '기타', value: '108'}
            ]);
            setDirList([
                {label: '상행', value:"0"},
                {label:'하행', value:"1"}
            ]);
            
            document.getElementById("laneFilter").style.display = "block";
            if(!tableViewSwitch){
                document.getElementById('add_btn').style.display='inline-grid';
                document.getElementById('del_btn').style.display='inline-grid';
            }
            if(equipSwitch){
                document.getElementById('add_btn').style.top='30%';
                document.getElementById('del_btn').style.top='45%';
                document.getElementById('routeMaster').style.height='42vh';    
            }else{
                document.getElementById('add_btn').style.top='40%';
                document.getElementById('del_btn').style.top='60%';
                document.getElementById('routeMaster').style.height='72vh'; 
            }

            if(recordSwitch){
                resetLabelStateEvt();
                setRecordSwitch(false);
            }
        }else{
            setRankList(typeList);
            setDirList([
                {label: '전체', value: "-1"}, 
                {label: '상행', value:"0"},
                {label:'하행', value:"1"}
            ]);
            document.getElementById("laneFilter").style.display = "none";
            document.getElementById('add_btn').style.display='none';
            document.getElementById('del_btn').style.display='none';
            setRouteMaster({
                ...routeMaster,
                style: { display:"none" }
            });
            if(equipSwitch) setEquipSwitch(false);
        }
    }, [masterSwitch]);

    useEffect(()=>{
        if(equipSwitch){
            document.getElementById('repairGrid').style.height = '42vh';
            document.getElementById('routeMaster').style.height = '42vh';
            document.getElementById('viewModel').style.height = '42vh';
            document.getElementById('add_btn').style.top='30%';
            document.getElementById('del_btn').style.top='45%';
            setMasterSwitch(true);
            // setEquipGrid({
            //     ...equipGrid,
            //     rowData: pesList
            // });
            toggleBtns();
            document.getElementById("bottom-content").style.display = "flex";
        }else{
            document.getElementById('add_btn').style.top='40%';
            document.getElementById('del_btn').style.top='60%';
            document.getElementById("bottom-content").style.display = "none";
            document.getElementById('repairGrid').style.height = '72vh';
            document.getElementById('routeMaster').style.height = '72vh';
            document.getElementById('viewModel').style.height = '72vh';
            
            setPesList(null); setCrackList(null); setGprList(null); setHwdList(null);
           
            setEquipGrid({
                ...equipGrid,
                rowData: null
            });
        }
    }, [equipSwitch])


    useEffect(()=>{
        setLabelResetList({ year: true, rank: true, no: true, name: true, dir: true, lane: true });
        setNos([]); setNames([]);
        setYear(); setRank(); setNo(); setName(); setDir(); setLane();
        setTotalExt('0');
        setSelectedGridType('none');
        if(repairList !=null) dataSort(repairList, "repair");

        if(recordSwitch){
            if(masterSwitch){
                resetLabelStateEvt();
                setMasterSwitch(false);
            }
            setEquipSwitch(false);
            setTableViewSwitch(false);   

            document.getElementById("yearFilter").style.display = "block";
   
            setRepairGrid({
                ...repairGrid,
                rowData: repairList,
                style: {display: "none", flex:1, flexDirection: "column", padding: '3px', textAlign: 'center'}
            });

            if(recordList !=null) dataSort(recordList, "record");
            setRecordGrid({
                ...recordGrid,
                rowData: recordList,
                style: {display: "flex", flex:1, flexDirection: "column", padding: '3px', height: '72vh', textAlign: 'center'}
            });
        }else{
            document.getElementById("yearFilter").style.display = "none";
            if(tableViewSwitch){
                document.getElementById('repairGrid').style.display='none';
                document.getElementById('viewModel').style.display='flex';  
            }else{
                setRepairGrid({
                    ...repairGrid,
                    rowData: repairList,
                    style: {display: "flex", flex:1, flexDirection: "column", padding: '3px', heigh: '72vh', textAlign: 'center'}
                });
            }

            setRecordGrid({
                ...recordGrid,
                style: {display: "none", flex:1, flexDirection: "column", padding: '3px', textAlign: 'center'}
            });
        }
    }, [recordSwitch])

    useEffect(()=>{
        if(tableViewSwitch){
            if(recordSwitch) {
                setRecordSwitch(false);
            }else{
                document.getElementById('repairGrid').style.display='none';
                document.getElementById('viewModel').style.display='flex';  
                if(equipSwitch) document.getElementById('viewModel').style.height='42vh';  
            }  
            document.getElementById('add_btn').style.display='none';
            document.getElementById('del_btn').style.display='none';        
        }else{
            document.getElementById('viewModel').style.display='none';
            if(!recordSwitch) document.getElementById('repairGrid').style.display='flex';    
            if(masterSwitch){
                document.getElementById('add_btn').style.display='inline-grid';
                document.getElementById('del_btn').style.display='inline-grid';
                if(equipSwitch){
                    document.getElementById('add_btn').style.top='30%';
                    document.getElementById('del_btn').style.top='45%';
                }else{
                    document.getElementById('add_btn').style.top='40%';
                    document.getElementById('del_btn').style.top='60%';
                }
            }
        }
    }, [tableViewSwitch])


    // 그리드 header 세팅
    const tHeader = (cols) => {
        var prev;
        var colNum = cols.length;
        var firstName = cols[0]['name'];
        var header = cols.map((obj, index) =>{
            if(!(prev) || obj['attributes']['className']!=prev){
                prev = obj['attributes']['className'];
                return obj['attributes']['className'];
            } else return undefined;
        });
        header = header.filter(function(el){
            return el!=null;
        });

        header = header.map((obj)=>{
            return {
                headerName: obj,
                children: 
                    cols.map((col)=>{
                        var attr = col['attributes'];
                        var width = null;
                        if(attr.hasOwnProperty("wd")){
                            width = Number(attr['wd']);
                        }
                        if(attr['className']==obj && col['name']==firstName){
                            return {
                                headerName: attr['title'], 
                                field: col['name'],
                                width: width,
                                colSpan: function(params){
                                    if(isSpan(params)){
                                        return colNum;
                                    }else{
                                        return 1;
                                    }
                                },
                                // cellStyle: function(params){
                                //     if(isSpan(params)){
                                //         return {textAlign: 'center'};
                                //     }else {
                                //         return {textAlign: 'left'};
                                //     }
                                // },
                            }
                        }else if (attr['className']==obj && col['name']!=firstName){
                            return {headerName: attr['title'], field: col['name'], width: width}
                        }
                    }).filter(function(el){
                        return el!=null;
                    })
            }
        });

        function isSpan(params){
            return params.data.span === 'no-result';
        }

        return header;
    }

    // 뷰 세팅
    useEffect(()=>{
        if(routeMasterList && repairList){
            async function getView(){
                var data = new Object();
                data['list'] = encodeURIComponent(JSON.stringify(repairList));
                var result = await server_connect(serviceUrl["pavement"]["getTargetTsList"], data);
                result = result['data']['res'];
    
                var viewGroup = new Object();
                dataSort(routeMasterList, 'master');
                routeMasterList.map((row)=>{
                    var key = ""+row['tri_id']+row['main_num']+row['sub_num']+row['dir']+1;
                    var viewList = new Array();
                    if(viewGroup.hasOwnProperty(key)){
                        viewList = viewGroup[key];
                    }
                    viewList.push(row);
                    viewGroup[key] = viewList;
                });
                
                var viewList = new Array();
                var keyList = Object.keys(viewGroup);
                var viewrank; var viewno; var viewname;
                for(var i=0;i<keyList.length;++i){
                    viewGroup[keyList[i]].map((obj, inx)=>{
                        if(inx == 0){
                            viewrank = obj['rank'];
                            viewno = obj['no'];
                            viewname = obj['name'];
                        }
                        
                        result.map((rObj)=>{
                            var tsList = rObj['tsList'];
                            for(var i=0; i<tsList.length; ++i){
                                if(tsList[i]==obj['ts_id']) obj['repair_target_id'] = rObj['repair_target_id'];    
                            }
                        });
                    });
                    var addInfo = {
                        info : {
                            rank : viewrank,
                            no : viewno,
                            name : viewname,
                            targetCol : 'repair_target_id',
                            extCol : 'sm_ext'
                        },
                        children : viewGroup[keyList[i]]
                    };
                    viewList.push(addInfo);
                }
                setViewDatas(viewList);
            }
            getView();
        }
    }, [routeMasterList, repairList]);

    useEffect(()=>{
        if(viewDatas.length>0){
            setViewContent(<TpmsView dataSet={viewDatas}/>);
        }
    }, [viewDatas])

    useEffect(()=>{
        if(selectedGridType=='master'){
            repairGridApi.deselectAll();
            equipGridApi.deselectAll();
            recordGridApi.deselectAll();
        }else if(selectedGridType=='repair'){
            routeMasterGridApi.deselectAll();
            equipGridApi.deselectAll();
            recordGridApi.deselectAll();
        }else if(selectedGridType=='pes' || selectedGridType=='crack' || selectedGridType=='gpr' || selectedGridType=='hwd'){
            routeMasterGridApi.deselectAll();
            repairGridApi.deselectAll();
            recordGridApi.deselectAll();
        }else if(selectedGridType=='record'){
            routeMasterGridApi.deselectAll();
            repairGridApi.deselectAll();
            equipGridApi.deselectAll();
        }else if(selectedGridType=='none' && routeMasterGridApi){
            routeMasterGridApi.deselectAll();
            repairGridApi.deselectAll();
            equipGridApi.deselectAll();
            recordGridApi.deselectAll();
        }

    }, [selectedGridType])

    const viewMap = () => {
        var props =[];
        var masterNodes = routeMasterGridApi.getSelectedNodes();
        var repairNodes = repairGridApi.getSelectedNodes();
        var recordNodes = recordGridApi.getSelectedNodes();

        if(masterNodes.length>0){
            props['allData'] = searchMList;
            props['selectedNodes'] = masterNodes;
            props['header'] = [
                {
                    label : 'target_id',
                    name  : '구간ID'
                },
                {
                    label : 'rank',
                    name  : '등급'
                },
                {
                    label : 'no',
                    name  : '노선번호'
                },
                    {
                    label : 'name',
                    name  : '노선명'
                },
                {
                    label : 'rev_dir',
                    name  : '행선'
                },
                {
                    label : 'node_name',
                    name  : '이벤트명'
                },
                {
                    label : 'node_type',
                    name  : '이벤트 종류'
                }
            ];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }else if(repairNodes.length>0){
            dataSort(repairList, 'repair');
            getRepairTargetTsList(repairList, "repair", repairNodes);
        }else if(recordNodes.length>0){
            dataSort(recordList, 'record');
            getRepairTargetTsList(recordList, "record", recordNodes);
        }else if(masterNodes.length==0 && repairNodes.length==0 && recordNodes.length==0){
            alert("확인하실 구간을 먼저 선택해 주시기 바랍니다");
        }
    }

    useEffect(()=>{
        if(viewTargetList){
            var props = [];
            props['allData'] = viewTargetList["allData"];

            var list = viewTargetList["list"].map((el)=>{
                var obj = el["data"];
                viewTargetList["allData"].map((row)=>{
                    if(obj["repair_target_id"]==row["repair_target_id"]){
                        obj["tsList"] = row["tsList"];
                        el["data"] = obj;
                        return;
                    }
                });
                return el;
            });

            props['selectedNodes'] = list;
            props['header'] = viewHeader[viewTargetList["type"]];
            props['action'] = 'OPEN_CHAT';
            onOpenChat(props);
            hideModal();
        }
    }, [viewTargetList]);

    const getRepairTargetTsList = async (list, type, selectedNodes) => {
        if(list !=null){
            var data = new Object();
            data['list'] = encodeURIComponent(JSON.stringify(list));
            var result = await server_connect(serviceUrl["pavement"]["getTargetTsList"], data);   // 서버통신
            setViewTargetList({ list : selectedNodes, type : type, allData : result['data']['res']});
        }
    }

    const passToggle = () =>{
        // onCloseChat();
        toggle();
    }

    const hideModal = () => {
        document.getElementsByClassName('modal')[0].style.display='none';
        document.getElementsByClassName('modal-backdrop')[0].style.display='none';
        document.getElementsByClassName('modal-dialog')[0].style.display='none';
    }

    const yearChangeEvt = () => {
        var currentYear = new Date().getFullYear();
        var arr = new Array();
        arr.push({label: '전체', value: 'all'});
        for(var i=currentYear; i>=(currentYear-10); --i){
            arr.push({label:i, value:i});
        }
        setYearList(arr);
    }

    // 검색필터: 연도 변경 시, 다른 검색 필터 value를 null로 변경
    const recordYearOnChangeEvt = (selectedObj) => {
        setLabelResetList({ year: false, rank: true, no: true, name: true, dir: true, lane: true });
        setYear(selectedObj['value']);
        setRank(null); setNo(null);  setName(null); setDir(null); setLane(null);
    }

    // 검색필터: 도로등급 선택 시, 해당 도로등급에 속하는 노선번호 불러오기
    const roadRankOnChangeEvt = async (selectedObj) => {
        if(!recordSwitch){
            setLabelResetList({ year: true, rank: false, no: true, name: true, dir: true, lane: true });
            setYear(null);
        }else {
            setLabelResetList({ year: false, rank: false, no: true, name: true, dir: true, lane: true });
        }
        setRank(selectedObj['value']);
        setNo(null);  setName(null); setDir(null); setLane(null);

        var params = new Object();
        params['rank'] = selectedObj['value'];
        const noRes = await server_connect(serviceUrl["pavement"]["selectNoListForRank"], params);
        if(noRes && noRes['status']==200){
            let noResData = noRes['data']['res'];
            var noList = makeOptionsJson(noResData);
            if(!masterSwitch && selectedObj['value']=='all') noList.unshift({label: '전체', value: 'all'});
            setNos(noList);
            setNames([]);
        }else{
            alert('서버 통신 실패. 관리자에게 문의하세요.');
        }
    }

     // 검색필터: 노선번호 선택 시, 해당 노선번호에 속하는 노선명 불러오기
     const roadNoOnChangeEvt = async (selectedObj) => {
        setName(null);
        if(!recordSwitch){
            setLabelResetList({ year: true, rank: false, no: false, name: true, dir: true, lane: true });
            setYear(null);
        }else {
            setLabelResetList({ year: false, rank: false, no: false, name: true, dir: true, lane: true });
        }
        setName(null); setDir(null); setLane(null);
        let _no = selectedObj['value'];
        setNo(_no);
        var params = new Object();
        params['rank'] = rank;
        var nameRes;
        if(_no=='all'){
            nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRank"], params);
        }else{
            params['no'] = _no;
            nameRes = await server_connect(serviceUrl["pavement"]["selectNameListForRankNo"], params);
        }
        let nameResData = nameRes['data']['res'];
        var nameList = makeOptionsJson(nameResData);
        // if(_no=='all') nameList.unshift({label: '-', value: '-'});
        if(nameList.length==0) nameList.unshift({label: '-', value: '-'});
        setNames(nameList);
    }
    
    const roadNameOnChangeEvt = (selectedObj) => {
        setName(selectedObj['value']);
    }

    const roadDirOnChangeEvt = (selectedObj) => {
        setDir(selectedObj['value']);
    }

    const roadLaneOnChangeEvt = (selectedObj) => {
        setLane(selectedObj['value']);
    }

    const makeOptionsJson = (arr) => {
        let list = arr.map((item)=>{
            if(item=="") {
                return {label: "-", value: "-"}
            }
            return {label:item, value:item};
        });
        return list;
    }

    const handleClick = async(e) => {
        if(serverChk) {
            var params = new Object();
            params['year'] = year;
            params['rank'] = rank;
            params['no'] = no;
            params['name'] = name;
            params['dir'] = dir;
            params['lane'] = lane;
            if(recordSwitch){
                params['lane'] = null;
                if(year == null){
                    alert("연도를 선택해 주시기 바랍니다.");
                }else{
                    // setYear(selectYear);
                    if(recordList !=null){
                        if(recordList.length>0) var searchRecordList = search(params, recordList);
                        if(searchRecordList.length>0){
                            dataSort(searchRecordList, "record");
                            setRecordGrid({
                                ...recordGrid,
                                rowData: searchRecordList
                            });
                        }else{
                            setRecordGrid({
                                ...recordGrid,
                                rowData: [
                                    {span: 'no-result', year: "검색 결과가 없습니다"}
                                ],
                            });
                        }
                    }
                }
            }else{
                params['year'] = null;
                setRank(params['rank']); setNo(params['no']); setName(params['name']); setDir(params['dir']); setLane(params['lane']);
                if(masterSwitch){
                    if(params['rank']==null || params['no']==null || params['name']==null || params['dir']==null || params['lane']==null){
                        if(params['rank']==null){
                            alert("도로등급을 선택해 주시기 바랍니다");
                        }else {
                            if( params['no']==null){
                                alert("노선번호를 선택해 주시기 바랍니다");
                            }else{
                                if(params['name']==null){
                                    alert("노선명을 선택해 주시기 바랍니다");
                                }else{
                                    if(params['dir']==null){
                                        alert("행선을 선택해 주시기 바랍니다");
                                    }else{
                                        if(params['lane']==null) alert("차로를 선택해 주시기 바랍니다");
                                    }
                                }
                            }
                        }
                    }else{
                        var searchMasterList = search(params, routeMasterList);
                        if(searchMasterList.length>0){
                            dataSort(searchMasterList, "master");
                            setSearchMList(searchMasterList);
                            setRouteMaster({
                                ...routeMaster,
                                rowData: searchMasterList,
                                rowClassRules: {
                                    'included-row-in-target' : function (params) {
                                        var ts_id = params.data.ts_id;
                                        var chk = false; 
                                        if(repairTsIds !=null){
                                            for(var i=0; i<repairTsIds.length; ++i){
                                                if(ts_id == repairTsIds[i]['ts_id']){
                                                    chk = true;
                                                }
                                            }
                                            return chk;
                                        }
                                    }   
                                },
                            });
                        }else{
                            setSearchMList([
                                {span: 'no-result', no: "검색 결과가 없습니다"}
                            ]);
                            setRouteMaster({
                                ...routeMaster,
                                rowData: [
                                    {span: 'no-result', no: "검색 결과가 없습니다"}
                                ],
                            });
                        }
                    }
                    searchRepair(params);
                }else{
                    searchRepair(params);
                }
            }
        }
    }

    const searchRepair =(params) =>{
        params['lane'] = null;
        var searchRepairList = search(params, repairList);
        if(searchRepairList.length>0){
            dataSort(searchRepairList, "repair");
            setRepairGrid({
                ...repairGrid,
                rowData: searchRepairList
            });
        }else{
            setRepairGrid({
                ...repairGrid,
                rowData: [
                    {span: 'no-result', year: "검색 결과가 없습니다"}
                ],
            });
        }
    }

    const search  = (params, dataList) => {
        var resultList = new Array();
        if(dataList !=null){
            for(var i=0; i<dataList.length; ++i){
                var chk=true;
                if(params['year'] != 'all' && params['year'] !=null){
                    if((String)(dataList[i]['year']) == params['year']){
                        chk = search2(params, dataList, chk, i);
                    } else{
                        chk=false;
                    }
                }else {
                    chk = search2(params, dataList, chk, i);
                }
    
                if(chk) resultList.push(dataList[i]);
            }
            
        }
        return resultList;
    }

    const search2  = (params, dataList, chk, i) => {
        if(params['rank'] !=null){
            if(params['rank'] != 'all'){
                if(dataList[i]['rank'] != params['rank']) chk=false; 
            }
            if(params['no'] != null){
                if(params['no'] != 'all'){
                    if(dataList[i]['no'] != params['no']) chk=false;
                }
                if(params['name'] !=null){
                    if(params['name'] !='all'){
                        if(dataList[i]['name'] != params['name']) chk=false;
                    }
                    if(params['lane'] !=null){
                        if(dataList[i]['lane'] != params['lane']) chk=false;
                    }
                }   
            }
        }
        if(params['dir'] !=null && params['dir'] != '-1'){
            if(dataList[i]['dir'] != params['dir']) chk=false;
        }

        return chk;
    }

    const dataSort = (data, gridType) => {
        data.sort(function (a, b) {
            var before_no = parseInt(a.no);
            var after_no = parseInt(b.no);
            if(gridType =='master'){
                return a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
                       || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section || a.order - b.order;
            }else if(gridType=="repair"){
                return a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
                       || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section;
            }
            else if(gridType =='record'){
                return b.year-a.year || a.rank.localeCompare(b.rank) || before_no - after_no || a.sub_num - b.sub_num 
                       || a.main_num - b.main_num || a.dir - b.dir || a.section - b.section;
            }
           
        });
        return data;
    }

    const resetLabelStateEvt = () => {
        setLabelResetList({ year: false, rank: false, no: false, name: false, dir: false, lane: false });
    }

    const handleSwitch = (e) => {
        var checkedSwitch = e.target.getAttribute('name');
        // document.getElementById('equip1Master').style.display = 'none';
        // document.getElementById('equip2Master').style.display = 'none';

        switch(checkedSwitch){
            case 'tableViewSwitch':
                if(serverChk){
                    setTableViewSwitch(!tableViewSwitch);
                    break;
                }else{
                    alert('서버 통신 실패. 관리자에게 문의하세요.');
                    break;
                }
            case 'masterSwitch':
                setMasterSwitch(!masterSwitch);
                break;
            case 'equipSwitch':
                setEquipSwitch(!equipSwitch);
                break;
            case 'recordSwitch':
                setRecordSwitch(!recordSwitch);
                break;       
        }       
    }

    const getRouteMasterGridApi = (childState) => {
        setRouteMasterGridApi(childState);
    }

    const getRepairGridApi = (childState) => {
        setRepairGridApi(childState);
    }

    const getRecordGridApi = (childState) => {
        setRecordGridApi(childState);
    }

    const getEquipGridApi = (childState) => {
        setEquipGridApi(childState);
    }


    useEffect(()=>{
        if(saveStatus){
            setRouteMaster({
                ...routeMaster,
                rowData: searchMList,
                rowClassRules: {
                    'included-row-in-target' : function (params) {
                        var ts_id = params.data.ts_id;
                        var chk = false; 
                        if(repairTsIds !=null){
                            for(var i=0; i<repairTsIds.length; ++i){
                                if(ts_id == repairTsIds[i]['ts_id']){
                                    chk = true;
                                }
                            }
                            return chk;
                        }
                    }   
                },
            });
        }
    },[saveStatus])


    const addToRepairGrid = async(e) => {
        var selectedAllMaster = routeMasterGridApi.getSelectedNodes();
        if(selectedAllMaster.length>0){
            var tempList = new Array();
            var tempList2 = new Array();
            var chk_list = true;
            // 이미 선정된 구간을 제외
            if(repairTsIds.length>0){
                for(var i=0; i<selectedAllMaster.length; ++i){
                    var chk=true;
                    for(var j=0; j<repairTsIds.length; ++j){
                        if(repairTsIds[j]['ts_id'] == selectedAllMaster[i]['data']['ts_id']) chk=false;
                    }
                    if(chk) tempList.push(selectedAllMaster[i]);
                }
            }else{
                tempList = selectedAllMaster;
            }

            // 주로, 종속로 변화 확인
            var chk_main_sub = true;
            if(tempList.length>0){
                for(var i=0; i<tempList.length-1; ++i){
                    var curr_mainNum = tempList[i]['data']['main_num'];
                    var next_mainNum = tempList[i+1]['data']['main_num'];
                    var curr_subNum = tempList[i]['data']['sub_num'];
                    var next_subNum = tempList[i+1]['data']['sub_num'];

                    if(curr_mainNum != next_mainNum) {
                        chk_main_sub =false;
                        alert("주로가 다른 구간은 함께 선택될 수 없습니다. 다시 선택해 주시기 바랍니다.");
                        return;
                    }else {
                        if(curr_subNum != next_subNum) {
                            chk_main_sub=false;
                            alert("종속로가 다른 구간은 함께 선택될 수 없습니다. 다시 선택해 주시기 바랍니다.");
                            return;
                        }
                    }
                }
            }

            // 선택한 구간이 연속된 구간인지 확인
            if(tempList.length>0 && chk_main_sub){
                var chk_seq = true;
                for(var i=0; i<tempList.length-1; ++i){
                    var curr_id = parseInt(tempList[i]['id']);
                    var next_id = parseInt(tempList[i+1]['id']);
                    var diff = next_id - curr_id;
                    if(diff !=1) {
                        chk_seq=false;
                        alert("구간 선택이 연속되지 않습니다. 연속된 구간으로 선택해 주시기 바랍니다.");
                        routeMasterGridApi.deselectAll();
                        return;
                    }
                }
                // 노드를 제외한 링크 리스트 수 확인
                if(chk_seq){
                    for(var i=0; i<tempList.length; ++i){
                        if(tempList[i]['data']['type'] == "0104") tempList2.push(tempList[i]['data']);
                    }
                    if(tempList2.length==0) {
                        chk_list = false;
                        alert("선택하신 구간은 보수대상구간 선정에 적합하지 않습니다. 새로운 구간을 다시 선택해 주시기 바랍니다.");
                    }
                }
            }else{
                chk_list = false;
                alert("선택하신 구간은 이미 보수대상구간으로 선정된 구간입니다. 새로운 구간을 다시 선택해 주시기 바랍니다.");
            }

            if(chk_list){
                setSaveStatus(false);
                var data = new Object();
                data['list'] = encodeURIComponent(JSON.stringify(tempList2));
                var result = await server_connect(serviceUrl["pavement"]["getRepairId"], data);
                if(result['data'].hasOwnProperty('res')){
                    if(result['data']['res'].length>0){
                        batchModified(result['data']['res']);
                    }
                }               
            }
            
        }
    }

    const saveSuccess =async() => {
        var tsId_list = await server_connect(serviceUrl["pavement"]["getCurrMaintTs_Id"]);
        var repairTsId_list = new Array();  
        if(tsId_list['data'].hasOwnProperty("res")){
            repairTsId_list = tsId_list['data']['res'];
            setRepairTsIds(repairTsId_list);
        }
        // 당해 선정된 보수대상구간 불러오기
        let today = new Date();
        let currYear = today.getFullYear();
        var params = new Object();
        params['year'] = currYear;
        var result = await server_connect(serviceUrl["pavement"]["getCurrYearMaint"], params);
        if(result['data'].hasOwnProperty('res')){
            result = result['data']['res'];
            if(result.length>0){
                setRepariList(result);
                var filter = new Object();
                filter['year'] = null;
                filter['rank'] = rank;
                filter['no'] = no;
                filter['name'] = name;
                filter['dir'] = dir;
                filter['lane'] = lane;
                if(lane=="-") filter['lane'] = "1";          // lane 정보가 없을 경우 "-" 로 할지 빈칸으로 둘지 협의 필요
                result = search(filter, result);
                if(result.length>1) dataSort(result, "repair");       
            }else{
                result= null;
            }
        }else{
            result = null;
        }

        setRepairGrid({
            ...repairGrid,
            rowData: result
        });

        setRouteMaster({
            ...routeMaster,
            rowData: null,
            rowClassRules: null
        });

        setSaveStatus(true);
    }

    const delFromRepairGrid = async(e) => {
        var selectedRepairRows = repairGridApi.getSelectedNodes();
        if(selectedRepairRows.length>0){
            if(window.confirm("선택하신 보수대상구간을 삭제하시겠습니까?") == true) {
                setSaveStatus(false);
                var list = new Array();
                var data = new Object();
                for(var i=0; i<selectedRepairRows.length; ++i){
                    list.push(selectedRepairRows[i]['data']['repair_target_id']);
                }
                data['repair_target_id'] = list;
                var result = await server_connect(serviceUrl["pavement"]["deleteRepairData"], data);
                if(result['data'].hasOwnProperty('res')){
                    result = result['data']['res'];
                    if(result=='success'){
                        alert("삭제가 완료되었습니다.");
                        saveSuccess();
                    }else{
                        alert("선택하신 구간이 정상적으로 삭제되지 않았습니다. 다시 시도해 주시기 바랍니다.");
                    }
                }
            }
        }
    }
 

    const [radioValue, setRadioValue] = useState('1');
    const radios = [
        {name: 'PES', value: '1'},
        {name: 'CRACK', value: '2'},
        {name: 'GPR', value: '3'},
        {name: 'HWD', value: '4'}
    ];
    const toggleBtns = () => {

        setBtnGroup(
            <>
                <ButtonGroup toggle>
                {radios.map((radio, idx) => (
                    <ToggleButton
                    key={idx}
                    type="radio"
                    variant="secondary"
                    name="radio"
                    value={radio.value}
                    checked={radioValue === radio.value}
                    onChange={(e) => setRadioValue(e.currentTarget.value)}
                    className="repair-toggle-btn"
                    >
                    {radio.name}
                    </ToggleButton>
                ))}
                </ButtonGroup>
            </>
         )
    }

    const changeEquipGrid = (tHeader, equipType, equipData) => {
        setEquipGrid({
            ...equipGrid,
            columnDefs : tHeader,
            rowData : equipData,
            onSelectionChanged: function(e){
                var selectedRows = e.api.getSelectedNodes();
                if(selectedRows.length==0) return;
                setSelectedGridType(equipType);
                // var extSum = 0;
                // selectedRows.map((row)=>{
                //     var selectedRowExt = row['data']['sm_ext'];
                //     extSum = extSum + selectedRowExt;
                // });
                // setTotalExt((extSum/1000).toFixed(2));
            }
        });
    }
    
    useEffect(() => {
        toggleBtns();
        if(radioValue=='1'){
            changeEquipGrid(pesTHead, 'pes', pesList);
        }else if(radioValue=='2'){
            changeEquipGrid(crackTHead, 'crack', crackList);
        }else if(radioValue=='3'){
            changeEquipGrid(gprTHead, 'gpr', gprList);
        }else if(radioValue=='4'){
            changeEquipGrid(hwdTHead, 'hwd', hwdList);
        }

    }, [radioValue])

    return (<>
        <ModalBody style={{overflow:'auto'}}>
            <div className="rTarget-filter-area">
                <div className="rTarget-search-area">
                    <div className="rTarget-search-content">
                        <div id="yearFilter">
                            <SearchDropdown target={recordYear} options={yearList} placeholder="연도" onChange={recordYearOnChangeEvt} resetLabel={labelResetList["year"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                        </div>
                        <div id="restFilter">
                            <SearchDropdown target={roadRank} options={rankList} placeholder="도로 등급" onChange={roadRankOnChangeEvt} resetLabel={labelResetList["rank"]} resetStateEvt={resetLabelStateEvt} width="230px"/> 
                            <SearchDropdown target={roadNo} options={nos} placeholder="노선 번호" onChange={roadNoOnChangeEvt} resetLabel={labelResetList["no"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                            <SearchDropdown target={roadName} options={names} placeholder="노선명" onChange={roadNameOnChangeEvt} resetLabel={labelResetList["name"]} resetStateEvt={resetLabelStateEvt} width="230px"/> 
                            <SearchDropdown target={roadDir} options={dirList} placeholder="행선" onChange={roadDirOnChangeEvt} resetLabel={labelResetList["dir"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                        </div>
                        <div id="laneFilter">
                            <SearchDropdown target={roadLane} options={laneList} placeholder="차로" onChange={roadLaneOnChangeEvt} resetLabel={labelResetList["lane"]} resetStateEvt={resetLabelStateEvt} width="200px"/> 
                        </div>
                        <Button size="sm" color="secondary" style={{width:"100px"}} onClick={handleClick}>검색</Button>
                    </div>
                    <div className="rTarget-switch-area">
                        <SwitchButton label="테이블/뷰" isOpen={tableViewSwitch} onChange={handleSwitch} name="tableViewSwitch" />        
                        <SwitchButton label="이력" isOpen={recordSwitch} onChange={handleSwitch} name="recordSwitch" />        
                        <SwitchButton label="1/2차장비" isOpen={equipSwitch} onChange={handleSwitch} name="equipSwitch" />
                        <SwitchButton label="마스터" isOpen={masterSwitch} onChange={handleSwitch} name="masterSwitch" />
                    </div>
                </div>
            </div>
            <div className="rTarget-content-area">
                <div className="top-content">
                    {subModal}
                    <div id="add_btn"><input type="button" className="moveBtn" value=">>" onClick={addToRepairGrid}/></div>
                    <div id="del_btn"><input type="button" className="moveBtn" value="<<" onClick={delFromRepairGrid}/></div>
                    <GridComponent id="routeMaster" columnDefs={routeMaster.columnDefs} rowData={routeMaster.rowData} onSelectionChanged={routeMaster.onSelectionChanged} rowClassRules={routeMaster.rowClassRules} style={routeMaster.style} passStateEvt={getRouteMasterGridApi} gridTitle='노선마스터'/>
                    <GridComponent id="recordGrid" columnDefs={recordGrid.columnDefs} rowData={recordGrid.rowData} onSelectionChanged={recordGrid.onSelectionChanged} style={recordGrid.style} passStateEvt={getRecordGridApi} gridTitle="보수대상구간선정 이력" />
                    <GridComponent id="repairGrid" columnDefs={repairGrid.columnDefs} rowData={repairGrid.rowData} onSelectionChanged={repairGrid.onSelectionChanged} style={repairGrid.style} passStateEvt={getRepairGridApi} gridTitle="보수대상구간선정" />
                    <div id="viewModel" style={{flex:1, display:"none", flexDirection:"column",  height: '72vh', padding: '3px'}}>
                        <div className="grid-title">View</div>
                        <div style={{overflow:"auto", border: '1px solid #BDC3C7', padding:10}}>
                            {viewContent}
                            {/* <TpmsView dataSet={viewDatas} style={viewStyle}/> */}
                        </div>
                    </div>
                </div>
                <div className="bottom-content" id="bottom-content">
                    <span className="equip-btns">
                        {btnGroup}
                        {/* <Button size="sm" color="light" onClick={equipChange} name="pesBtn">PES</Button>
                        <Button size="sm" color="light" onClick={equipChange} name="crackBtn">CRACK</Button>
                        <Button size="sm" color="light" onClick={equipChange} name="gprBtn">GPR</Button>
                        <Button size="sm" color="light" onClick={equipChange} name="hwdBtn">HWD</Button> */}
                    </span>
                    <GridComponent id="equipGrid" columnDefs={equipGrid.columnDefs} rowData={equipGrid.rowData} onSelectionChanged={equipGrid.onSelectionChanged} style={equipGrid.style} passStateEvt={getEquipGridApi} gridTitle="1/2차 장비조사" /> 
                </div>
            </div>
        </ModalBody>
        <ModalFooter style={{justifyContent:"unset"}}>
            <span style={{width:"100%", justifyContent:"flex-start", fontSize:"15px", fontWeight:"bold", margin:"5px", color:"#3eb5c1"}}>
                <Label style={{margin:"3px"}}>선택연장</Label>
                <Input type="text" disabled style={{display:"inline", width:"200px"}} value={totalExt}/>Km
            </span>
            <span style={{width:"100%", justifyContent:"flex-end", textAlign:"right"}}>
                <Button className="feather icon-map" color="primary" onClick={viewMap}> 지도확인</Button>
                <Button color="secondary" onClick={passToggle}>닫기</Button>
            </span>
        </ModalFooter>
    </>);
}


const chatStateToProps = state => {
    return {
        isChatOpen: state.isChatOpen,
        selectedData: state.selectedData
    }
}

const chatDispatchToProps = dispatch => {
    return {
        onOpenChat: (props) => dispatch({type: actionTypes.OPEN_CHAT, data: props}),
        onCloseChat: () => dispatch({type: actionTypes.CLOSE_CHAT})
    }
}

export default connect(chatStateToProps, chatDispatchToProps) (RepairTarget);