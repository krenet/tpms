import React, { useEffect, useState } from 'react';
import { NavItem } from 'react-bootstrap';
import { ModalBody, Input, Button, FormGroup, ModalFooter } from 'reactstrap';
import { server_connect } from '../../../../../../../../../tpms-common';
import { connect } from 'react-redux';
import * as actionTypes from "../../../../../../../../../store/actions";
import serviceUrl from  '../../../../../../../../../request-url-list';

const { kakao } = window;
const PosSearch = ({ toggle , geoXYData, backdropControl}) => {
    const [routeList, setRouteList] = useState();
    const [geoData, setGeoData] = useState();
    const [spotSeach, setSpotSearch] = useState();

    useEffect(()=>{
    backdropControl(false);
    }, []);
    
    const passToggle = () => {
    toggle();
    }
    const handleClick = async () => {
        var params = new Object();
        var name = spotSeach;
        params['node_name'] = name;
    const result = await server_connect(serviceUrl["pavement"]["geoXYData"], params);
        if(result && result['status']==200){
            const nodeName = result['data']['res'];
            setGeoData(nodeName);
            var geoList = Array();
            for (var i = 0; i < nodeName.length; ++i) {
                geoList.push({ name: nodeName[i]['node_name'], geoX: nodeName[i]['geo_x'], geoY: nodeName[i]['geo_y'] });
            }
        
            kakao.maps.load(function () {
                var ps = new kakao.maps.services.Places();
                var keyword = spotSeach;
                ps.keywordSearch(keyword, function (data, status) {
                    if (status === kakao.maps.services.Status.OK) {
                        // 데이터 확인
                        for (var i = 0; i < data.length; ++i) {
                            geoList.push({ name: data[i]['address_name'], geoX: Number(data[i]['x']), geoY: Number(data[i]['y']) });
                        }
                        setRouteList(geoList);
                    } else if (status === kakao.maps.services.Status.ZERO_RESULT) {
                        alert('검색 결과가 존재하지 않습니다.');
                        return;
                    } else if (status === kakao.maps.services.Status.ERROR) {
                        alert('검색 결과 중 오류가 발생했습니다.');
                        return;
                    }
                });
            });
        }
    }
    const handleChange = (e) => {
    setSpotSearch(e.target.value);
    }

    const spotSelect = (e) => {
    if(routeList){
        var props = [];
        var x = e.target.getAttribute("data-x");
        var y = e.target.getAttribute("data-y");
        props['geo_x'] = x;
        props['geo_y'] = y;
        geoXYData(props);
    }
    }

    const enterKey = ()=>{
    if(window.event.keyCode == 13){
        handleClick();
    }
    }
    
    return (
    <>
        <ModalBody>
            <div>
                <Input id="spotSeach" bsSize="sm" placeholder="텍스트 검색 . . ." style={{ width: "85%", display: "inline-block" }} onKeyUp={enterKey} onChange={handleChange} />
                <Button size="sm" color="info" style={{ marginLeft: "3px" }} onClick={handleClick}>검색</Button>
            </div>
            <FormGroup>
                <Input type="select" name="selectMulti" multiple onClick={spotSelect}>
                    {(routeList) ? routeList.map((route, index) => {
                        return <option id="option" key={index} data-x={route.geoX} data-y={route.geoY}>{route.name}</option>
                    }) : ""}
                </Input>
            </FormGroup>
        </ModalBody>
        <ModalFooter>
            <Button color="secondary" size="sm" onClick={passToggle}>닫기</Button>
        </ModalFooter>
    </>
    );
}

const geoDataDispatchToProps  = dispatch => {
    return {
        geoXYData: (props) => dispatch({ type: actionTypes.GEO_LON_LAT, data: props }),
    }
}

export default connect(null, geoDataDispatchToProps)(PosSearch);