import React,  {useState} from "react";
import { ModalBody, ModalFooter, Button } from 'reactstrap';
import { BarChart, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts';
// import { BarChart, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts';
import GridComponent from '../../../../../../../../components/GridComponent';

const StatisticsOne = ({toggle}) => {

  const data = [
    {
      "name": "Page A",
      "uv": 4000,
      "pv": 2400
    },
    {
      "name": "Page B",
      "uv": 3000,
      "pv": 1398
    },
    {
      "name": "Page C",
      "uv": 2000,
      "pv": 9800
    },
    {
      "name": "Page D",
      "uv": 2780,
      "pv": 3908
    },
    {
      "name": "Page E",
      "uv": 1890,
      "pv": 4800
    },
    {
      "name": "Page F",
      "uv": 2390,
      "pv": 3800
    },
    {
      "name": "Page G",
      "uv": 3490,
      "pv": 4300
    }
  ];
  const [stateGrid, setStateGrid] = useState({
    columnDefs: [
        {
            headerName : '노선정보',
            children : [
                {headerName:"노선명", field:"name"},
                {headerName:"행선", field:"dir"},
                {headerName:"차선", field:"lane"},
                {headerName:"구간", field:"section"}    
            ]
        },
        {
            headerName: "연장",
            children : [
                {headerName:"개별", field:"length"},
                {headerName:"누적", field:"lenSum"}
            ]
        },
        {headerName:"중용 분리", field:"overlap"},
        {
            headerName: "주소",
            children : [
                {headerName:"시점", field:"sAddr"},
                {headerName:"종점", field:"eAddr"}
            ]
        },
        {headerName:"차로수", field:"lanes"},
        {
            headerName: "이벤트",
            children : [
                {headerName:"명칭", field:"eventName"},
                {headerName:"종류", field:"eventType"}
            ]
        },
        {headerName:"포장종류", field:"paveType"},
        {
            headerName: "최초포장",
            children : [
                {headerName:"연도", field:"makeYear"},
                {headerName:"AC", field:"makeAC"},
                {headerName:"BB", field:"makeBB"},
                {headerName:"GB", field:"makeGB"},
                {headerName:"SB", field:"makeSB"},
                {headerName:"AFL", field:"makeAFL"}
            ]
        },
        {
            headerName: "최종유지보수",
            children : [
                {headerName:"연도", field:"latestYear"},
                {headerName:"공법", field:"latestMethod"},
                {headerName:"설명", field:"latestDesc"},
                {headerName:"비용", field:"latestPrice"}
            ]
        },
        {
            headerName: "교통량",
            children : [
                {headerName:"연도", field:"trafficYear"},
                {headerName:"지점", field:"trafficPoint"},
                {headerName:"Total", field:"traffic"}
            ]
        }
    ],
    rowData : [
        {name: "", dir: "", lane: "", section: "", length: "", lenSum: "", overlap: "", sAddr: "", eAddr: "", lanes: "", eventName: "", eventType: "", paveType: "", makeYear: "", makeAC: "", makeBB: "", makeGB: "", makeSB: "", makeAFL: "", latestYear: "", latestMethod: "", latestDesc: "", latestPrice: "", trafficYear:"", trafficPoint:"", traffic:""}
    ]
});
    const passToggle = () =>{
        toggle();
    }
    return(
        <div>
        <ModalBody>
            <div style={{display:"flex"}}>
                <BarChart style={{borderBottom:"groove", textAlign: "center", flex:1}} width={1300} height={400} data={data}>
                    {/* <CartesianGrid strokeDasharray="3 3" /> */}
                    <XAxis dataKey="name"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend />
                    <Bar dataKey="pv" fill="#8884d8" />
                    <Bar dataKey="uv" fill="#82ca9d" />
                </BarChart>
            </div>
            <GridComponent columnDefs={stateGrid.columnDefs} rowData={stateGrid.rowData} style={{marginTop: "5px", height:"70vh"}}/>
        </ModalBody>
        <ModalFooter style={{justifyContent:"flex-end", display:"flex"}}>
                     <Button color="secondary"onClick={passToggle}>닫기</Button>
        </ModalFooter>
        </div>
    );
}

export default StatisticsOne;