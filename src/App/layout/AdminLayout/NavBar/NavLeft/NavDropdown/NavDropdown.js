import React, { useState } from 'react';
import { Dropdown } from 'react-bootstrap';
// import DEMO from '../../../../../../store/constant';
import NavItem from './NavItem/NavItem';
import './NavDropdown.css';
import navList from '../../../../../../nav-list';

const itemRender = (index) => {

    return (
        navList[index].menuList.map((item, inx) => {
            // return <li key={inx}><a className="dropdown-item" draggable="false"><span>{item}</span></a></li>; //href={DEMO.BLANK_LINK}
            return <NavItem key={inx} title={item.title} component={item.component} className={item.className}/>
        })
    );
}

const NavDropdown = () => {
    // let dropdownRightAlign = false;
    // if (this.props.rtlLayout) {
    //     dropdownRightAlign = true;
    // }

    return (

        navList.map((listItem, index) => {
            return (
                <Dropdown alignRight={false} key={index}>
                    <Dropdown.Toggle variant={'link'} id="dropdown-basic">
                        {listItem.category}
                    </Dropdown.Toggle>
                    <ul>
                        <Dropdown.Menu>
                            {itemRender(index)}
                        </Dropdown.Menu>
                    </ul>
                </Dropdown>
            );
        })
    );

}

export default NavDropdown;