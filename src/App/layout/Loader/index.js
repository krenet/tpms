import React from 'react';

const loader = (props) => {
    if(props.error){
        return <div>Error!<button onClick={ props.retry }>Retry</button></div>;
    }else{
        return (
            <div className="loader-bg">
                <div className="loader-track">
                    <div className="loader-fill"/>
                </div>
            </div>
        );
    }
    // return (
    //     <div className="loader-bg">
    //         <div className="loader-track">
    //             <div className="loader-fill"/>
    //         </div>
    //     </div>
    // );
};

export default loader;