import React, { Component, Suspense } from 'react';
import { Router, Route } from 'react-router-dom';
import Loadable from 'react-loadable';
// import {createBrowserHistory} from 'history';

import '../../node_modules/font-awesome/scss/font-awesome.scss';

import Loader from './layout/Loader';
import Aux from "../hoc/_Aux";
import ScrollToTop from './layout/ScrollToTop';

// import Login from './login/Login';

const AdminLayout = Loadable({
    loader: () => import('./layout/AdminLayout'),
    loading: Loader
});

const Login = Loadable({
    loader: () => import('../login/Login'),
    loading: Loader
});
// const cors = require('cors');
// App.use(cors());
class App extends Component {

    render() {
        return (
            <Aux>
                <ScrollToTop>
                    <Suspense fallback={<Loader/>}>
                            {/* <Route path="/" component={AdminLayout} /> */}
                            {/* <Router history={createBrowserHistory}> */}
                                {/* <Route path="/" component={Login} exact={true}/> */}
                                {/* <Route path="/main" component={AdminLayout}/> */}
                                <Route path="/" component={AdminLayout}/>
                            {/* </Router> */}
                    </Suspense>
                </ScrollToTop>
            </Aux>
        );
    }
}

export default App;
