import React, { useEffect, useState } from 'react';
import { Table } from 'reactstrap';

const PropertiGrid = ({header, rows}) => {
    const [propertyRows, setPropertyRows] = useState();
    useEffect(()=>{
        if(rows) setPropertyRows(rows);
    }, [rows]);
    
    return (
        <Table hover bordered style={{textAlign:"center"}}>
            <tbody>
                {
                    (propertyRows)? propertyRows.map((row, index)=>{
                        return (
                            <tr key={index}>
                                <th scope="row" style={{width:"30%"}}>{row.title}</th>
                                <td style={{width:"70%"}}>{row.value}</td>
                            </tr>
                        );
                    })
                    :
                    <tr></tr>
                }
            </tbody>
        </Table>
    );
}

export default PropertiGrid;