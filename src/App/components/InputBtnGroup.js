import React, { useState, useRef } from 'react';
import { Button, InputGroup, Input } from 'reactstrap';

const InputBtnGroup = ({placeholder, buttonLabel, textVal}) => {
    const [textChange, setTextChange] = useState(true);
    const [textValue, setTextValue] = useState(textVal);
    const revert = useRef();

    const handleChange = (e) => {
        var textLength = e.target.value.length;
        setTextValue(e.target.value);
        if(textLength > 0){
            setTextChange(false);
        }else{
            setTextChange(true);
        }
    }

    const revertText = () =>{
        setTextValue(textVal);
    }

    return(
        <>
        <label size="sm" className={"label "+(textChange?"":"selectLabel")}>{buttonLabel}</label>
        <InputGroup>
            <Input ref={revert} placeholder={placeholder} value={textValue} onChange={handleChange}/>
            <Button color="primary" onClick={revertText}>되돌리기</Button>
        </InputGroup>
        <hr/>
        </>
    );
}


export default InputBtnGroup;