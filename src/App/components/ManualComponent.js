import { CenterFocusStrong } from '@material-ui/icons';
import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import { Button, ModalBody } from 'reactstrap';
// import { pdfjs } from 'react-pdf';
import myPdf from './react.pdf';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

function ManualComponent() {
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    const pdf =  myPdf 
    const page ={
      textAlign:"center",
      marginTop: "30px"
    }
    function onDocumentLoadSuccess({ numPages }) {
      setNumPages(numPages);
    }

    const pavePage = () => {
      if(pageNumber > 1) setPageNumber(pageNumber-1);
    }

    const nextPage = () =>{
      if(pageNumber < numPages) setPageNumber(pageNumber+1);
    }

    return (
      <div>
            <Document
              file={pdf}
              onLoadSuccess={onDocumentLoadSuccess}>
              <Page pageNumber={pageNumber} />
            </Document>
            <div style={page}>
              <span>
                <Button color="primary" onClick={pavePage}>이전 페이지</Button>
              </span>
              
              <span> (Page {pageNumber} of {numPages})</span>

              <span>
                <Button style={{marginLeft:10}} color="primary" onClick={nextPage}>다음 페이지</Button>
              </span>
              </div>
      </div>
    );
  }

  export default ManualComponent;