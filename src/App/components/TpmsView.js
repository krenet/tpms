import React, { useEffect, useState } from 'react';
import MakeView from './MakeView';
import ViewLabel from './ViewLabel';
import './TpmsView.css';

const TpmsView = ({dataSet, style}) => {
    const [content, setContent] = useState("");
    const [tags, setTags] = useState([]);

    useEffect(()=>{
        if(dataSet){
            setContent(null);
            makeTagHandler(dataSet);
        }
    }, [dataSet]);
    
    useEffect(()=>{
        if(tags.length>0) setContent(tags);
    }, [tags]);
    
    const makeTagHandler = (list) =>{
        var groupList = new Object();
        list.map((obj)=>{
            var info = obj['info'];
            var list = obj['children'];
            list.map((row, index)=>{
                var key = info['rank']+info['no']+info['name']; //+row['main_num']+row['sub_num']+row['dir'];
                var tempArr = new Array();
                var tempObj = new Object();
                if(index==0) tempObj['info'] = info;
                if(groupList.hasOwnProperty(key)){
                    tempObj['info'] = groupList[key]['info'];
                    tempArr = groupList[key]['children'];
                }
                tempArr.push(row);
                tempObj['children'] = tempArr;
                groupList[key] = tempObj;
            });
        });


        var keys = Object.keys(groupList);
        var makeTag = keys.map((key, inx)=>{
            var info = groupList[key]['info'];
            var tags = new Array();
            tags.push(<ViewLabel key={"label"+inx} rank={info['rank']} no={info['no']} name={info['name']}/>);
            tags.push(<MakeView key={"makeView"+inx} setData={groupList[key]} />);
            return tags;
        });
        setTags(makeTag);
        
    }

    return (        
        <>  
            <div>
                { content }
            </div>
        </>
    );

}

export default TpmsView;