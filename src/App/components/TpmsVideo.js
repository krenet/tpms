import React, { useEffect, useRef, useState } from 'react';

const TpmsVideo = ({url, width, height, speed, play, html, callback, allData, currentInx, mode}) => {
    const videoRef = useRef();
    const previousUrl = useRef(url);
    const [drawCrack, setDrawCrack] = useState('');
    const [prevFrame, setPrevFrame] = useState(0);
    const [playInx, setPlayInx] = useState(currentInx);
    
    useEffect(()=>{
        if (previousUrl.current !== url && videoRef.current) {
            var video = document.getElementById("tpms-video");
            video.playbackRate = speed;
            videoRef.current.load();
            previousUrl.current = url;
        }
    }, [url]);

    useEffect(()=>{
        if(speed){
            var video = document.getElementById("tpms-video");
            video.playbackRate = speed;
        }
    }, [speed]);

    useEffect(()=>{
        var video = document.getElementById("tpms-video");
        video.playbackRate = speed;
        if(play && video.paused) video.play();
        else video.pause();
    }, [play]);

    useEffect(()=>{
        if(html!=null) setDrawCrack(html);
    }, [html]);
    
    const videoTimeUpdate = () => {
        var inx = playInx;
        if(mode === "pave" && allData[inx].hasOwnProperty("res_json")&&allData[inx]["res_json"].length>0) makeAIDetectArea(allData[inx]["res_json"]);
        if(callback) callback(allData[inx], inx);
        setPlayInx(++inx);
    }

    const makeAIDetectArea = (resJson) => {
        if(resJson==="[]") return;
        var objList = JSON.parse(resJson.replace(/\'/g,"\""));
        var item;
        var html = [];
        var style;
        
        var videoWidth = 201;
        var videoHeight = 520;
        var orgWidth = 3872.0;
        var orgHeight = 10000.0;
        var rx = videoWidth / orgWidth;
        var ry = videoHeight / orgHeight;
        var bgColor;

        for(var inx = 0;inx<objList.length;++inx){
          item = objList[inx];
          switch(item.class){
            case "aligator":
            case "vertical":
            case "horizontal":
            case "danbu":
            case "charo":
            case "sigong":
            case "bansa":
            case "millim":
              bgColor="red";
              break;
            case "sigong joint":
            case "sinchuk joint":
              bgColor = "blue";
              break;
            case "manhole":
            case "baesu":
              bgColor = "green";
              break;
            case "lane":
            case "crosswalk":
            case "road sign":
              bgColor = "yellow";
              break;
            case "pothole":
            case "sonsang repair":
            case "gulchak repair":
              bgColor = "orange";
              break;
          }

          style = { 
              left : (item.left_x * rx )+"px", 
              top : (item.top_y * rx )+"px",
              width : (item.width * rx )+"px",
              height : (item.height * rx )+"px",
              backgroundColor : bgColor
          };

          html.push(<div className="tpms-ai-element" style={style} ></div>);
        }
        setDrawCrack(html);
    }

    const timeUpdateEvt = (evt) => {
        if(callback && play){
            var video = document.getElementById("tpms-video");
            var frame = Math.floor(video.currentTime * 10.0);
            if(prevFrame === frame) return;
            setPrevFrame(frame);
            videoTimeUpdate();
        }
    }
    
    return (
        <>
        <video id="tpms-video" width={width} height={height} preload="metadata" ref={videoRef} onTimeUpdate={timeUpdateEvt}>
            <source src={url} type="video/mp4"/>
        </video>
        <div className="tpms-ai-canvas">{drawCrack}</div>
        </>
    )
}

export default TpmsVideo;