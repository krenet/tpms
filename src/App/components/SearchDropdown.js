import { useScrollTrigger } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import Select from 'react-select';
import { Label } from 'reactstrap';
// import SelectSearch from 'react-select-search';
import './SelectSearch.css';

const styles = {
    container: (styles, {selectProps: {width, display}})=>({
        ...styles,
        width : width,
        marginRight : "5px",
        display : display
    }),
    menu: (provided, state) =>({
        ...provided,
        marginRight : "5px",
        width : state.selectProps.width
    }),
    control: (styles, {selectProps: {width}}) =>({
        ...styles,
        marginRight : "5px",
        width : width
    })
}

const SearchDropdown = ({label, options, placeholder, onChange, target, width, display, readOnly, resetLabel, resetStateEvt, selectInx}) => {
    const [disabled, setDisabled] = useState(false);
    const [value, setValue] = useState();

const changeHandler = (value) => {
    setValue(value);
    if(onChange) onChange(value);
}

useEffect(()=>{
    if(readOnly || readOnly==false) setDisabled(readOnly);
}, [readOnly]); 

useEffect(()=>{
    if(options && selectInx>-1){
        setValue(options[selectInx]);
    }
}, [selectInx]);

useEffect(()=>{
    if(options && selectInx>-1){
        setValue(options[selectInx]);
    }
}, [options]);

useEffect(()=>{
    if(resetLabel) {
        setValue(null);
        if(resetStateEvt) resetStateEvt();
    }
}, [resetLabel]);

return (
    <span className="sd-span">
        {(label)?(<Label style={{marginRight:"3px"}} className="search-dropdown-label">{label}</Label>):""}
        <Select 
            value={value}
            ref={target} 
            options={options} 
            styles={styles} 
            width={(width)?width:250}
            placeholder={placeholder} 
            onChange={changeHandler} 
            display={(display)?display:"inline-block"}
            isDisabled={disabled}
        />
    </span>
);
}

export default SearchDropdown;