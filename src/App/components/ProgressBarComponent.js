import React, {useState, useEffect} from 'react';
import { ProgressBar } from 'react-bootstrap';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

const ProgressBarComponent = ({open, title, totalCnt, completeCnt, passPercent, modalEvt}) => {
    const [modal, setModal] = useState(open);
    const toggle = () => setModal(!modal);
    const [percent, setPercent] = useState(0);

    useEffect(()=>{
        setModal(open);
    }, [open]);

    useEffect(()=>{
        modalEvt(modal);
    }, [modal]);

    useEffect(()=>{
        if(totalCnt && completeCnt) setPercent(parseInt(completeCnt/totalCnt*100));
    }, [totalCnt, completeCnt]);

    useEffect(()=>{
        if(percent) passPercent(percent);
    }, [percent])

    return (
        <Modal isOpen={modal} toggle={toggle} centered={true} backdrop="false" keyboard={false} >{/*onClosed={closeAll ? toggle : undefined}*/}
                <ModalHeader toggle={toggle}>{title}</ModalHeader>

                <ModalBody>
                    <ProgressBar  now={percent} label={`${percent}%`} />
                </ModalBody>
        </Modal>
    );
}

export default ProgressBarComponent;