import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import './ComboBoxComponent.css';

const ComboBoxComponent = ({combobox, label, changeEvt}) => {
    const [comboState, setComboState]= useState(combobox);
    
    const changeEvtHandler = (evt, value) =>{
        if(changeEvt && value) changeEvt(evt, value);
        else changeEvt(null, null);
    }
    
    return(
        <>
         <Autocomplete
            id="combo-box"
            options={comboState}
            getOptionLabel={(option) => option.title} 
            style={{ width: 200, margin:"0px auto"}} onChange={changeEvtHandler}
            renderInput={(params) => <TextField {...params}  size="small"  label={label} variant="outlined" />}
        />
        </>
    )

}


export default ComboBoxComponent;