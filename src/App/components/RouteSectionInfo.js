import React, { useState, useEffect, useRef } from 'react';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import { Tabs, Tab, Image } from 'react-bootstrap';
import Draggable from 'react-draggable';
import './RouteSectionInfo.css';
import { Player } from 'video-react';
import "../../../node_modules/video-react/dist/video-react.css";
import PropertiGrid from "./PropertiGrid";
import { server_connect } from "../../tpms-common";
import MakeDirUnitLine from "./MakeDirUnitLine";
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';
import serviceUrl from '../../request-url-list';
import SearchDropdown from "./SearchDropdown";
import TpmsVideo from './TpmsVideo';

const movieSelectBox = [{label: "전방 영상", value: "front"}, {label: "포장 영상", value: "pave"}];
const movieSpeedSelectBox = [{label : "0.5배속 (1FPS)", value: 0.1},
                             {label : "1배속 (2FPS)", value: 0.2},
                             {label : "2배속 (4FPS)", value: 0.4},
                             {label : "3배속 (6FPS)", value: 0.6},
                             {label : "5배속 (10FPS)", value: 1}];
const RouteSectionInfo = ({isOpen, toggle, info, geoXYData}) => {
    const [sectionInfo, setSectionInfo] = useState({
        frontImg : "", paveImg : "", rank : "", no : "", name : "", dir : "", lane : 1
    })
    const [modal, setModal] = useState(isOpen);
    const rowField = [{ id: 'target_id', title: "조사구간ID"}, { id: 'detail_target_id', title: "상세조사구간ID"},{ id: 'rutting', title: "소성변형(MM)"},
                      { id: 'avg_iri', title: "평탄성(M/KM)"}, { id: 'profile_slope', title: "종단경사(%)"}, { id: 'curve_radius', title: "곡선반경(M)"},
                      { id: 'speed', title: "속도(KM/H)"}, { id: 'line_cr', title: "선형균열(M)"}, { id: 'fetching', title: "패칭(M2)"}, 
                      { id: 'turtle_lamp_cr', title: "거북등균열(M2)"}, { id: 'pothole', title: "포트홀(M2)"}, { id: 'cr', title: "균열율(%)"},
                      { id: 'index', title: "포장상태지수", value: "" }];
    const [rows, setRows] = useState();

    const repairField = [{ id: 'repair_target_id', title: "보수구간ID"},{ id: 'date', title: "보수일자"},{ id: 'price', title: "보수비용(백만원)"},{ id: 'pave_type', title: "포장종류"},
                         { id: 'method_type', title: "공법종류"}, { id: 'method', title: "공법"}, { id: 'pave_meterial', title: "포장재료"}, { id: 'split_thin', title: "절삭두께(CM)"},
                         { id: 'layer_reinforce', title: "기층보강(CM)"}, { id: 'comment', title: "공법설명"}, { id: 'sigongsa', title: "시공사"}, { id: 'sigong_pm', title: "시공담당자"},
                         { id: 'gamrisa', title: "감리사"}, { id: 'gamrisa_pm', title: "감리사담당자"}, { id: 'gamriwon', title: "감리원"}, { id: 'gamriwon_pm', title: "감리원담당자"}];
    const [repairRows, setRepairRows] = useState();

    const aiField = [{id: "target_id", title: "조사구간ID"}, {id: "total_cr", title: "균열율(%)"}, {id: "ai_index", title: "포장상태지수"},
                     {id: "v_line_cr", title: "종방향 선형균열(M)"}, {id: "h_line_cr", title: "횡방향 선형균열(M)"}, {id: "aligator_cr", title: "거북등균열(M2)"},
                     {id: "ai_fetching", title: "패칭(M2)"}, {id: "ai_pothole", title: "포트홀(M2)"}, {id: "etc", title: "기타(M2)"}];
    const [aiRows, setAiRows] = useState();

    const [lineContent, setLineContent] = useState("");
    const [pervLink, setPrevLink] = useState(null);
    const [nextLink, setNextLink] = useState(null);
    const [status, setStatus] = useState(null);
    const [currentLaneList, setCurrentLaneList] = useState();
    const [state10m, setState10m] = useState({id: null, index: null});
    const [videoMode, setVideoMode] = useState("pave");
    const [videoData, setVideoData] = useState();
    const [videoSpeed, setVideoSpeed] = useState(0.2);
    const [videoVisible, setVideoVisible] = useState(true);
    const [videoPlay, setVideoPlay] = useState(false);
    const [playText, setPlayText] = useState("▶");
    
    
    const modalOpenControl = () => {
        setModal(!modal);
        resetData();
        toggle();
    }

    const resetData = () => {
        setSectionInfo({
            ...sectionInfo,
            frontImg : "",
            paveImg : ""
        });

        if(rows){
            var _rows = rows;
            _rows.map((row)=>{
                row['value'] = "";
            });
            setRows(_rows);
        }
        if(repairRows){
            _rows = repairRows;
            _rows.map((row)=>{
                row['value'] = "";
            });
            setRepairRows(_rows);
        }
        

        if(aiRows){
            _rows = aiRows;
            _rows.map((row)=>{
                row['value'] = "";
            });
            setAiRows(_rows);
        }
        
        setLineContent("");
        setVideoData({width: 0, height: 0, url : '', speed: 0});
    }

    useEffect(()=>{
        setState10m({ ...state10m, id : info['properties']['t10m_id']});
        var _info = {};
        _info["t10m_id"] = info['properties']['t10m_id'];
        _info["tri_id"] = info['properties']['tri_id'];
        _info["main_num"] = info['properties']['main_num'];
        _info["sub_num"] = info['properties']['sub_num'];
        _info["dir"] = info['properties']['dir'];
        _info["ts_id"] = info['properties']['ts_id'];
        getLaneDataList(_info);
    }, []);
    
    useEffect(()=>{
        setModal(isOpen);
    }, [isOpen]);
    
    useEffect(()=>{
        if(currentLaneList && state10m["id"]){
            for(var i=0; i<currentLaneList.length; ++i){
                if(currentLaneList[i]["t10m_id"]===state10m["id"]) setState10m({...state10m, index : i});
            }
        }
    }, [currentLaneList]);

    useEffect(()=>{
        if(!videoPlay&&state10m["id"]&&(state10m["index"]||state10m["index"]===0)){
            settingData(currentLaneList[state10m["index"]]);
        }
    }, [state10m]);

    const getLaneDataList = async (info) => {
        var result = await server_connect(serviceUrl["pavement"]["selectRoadSectionList"], info);
        if(result && result["status"]===200) setCurrentLaneList(result["data"]["res"]);
    }
    
    const callbackVideo = (row, inx) => {
        var props = {'geo_x' : row['geox'], 'geo_y' : row['geoy'] };
        geoXYData(props);
        setLineContent("");
        settingData(row);
        if(videoPlay) setState10m({id:row["t10m_id"], index:inx});
    }
    
    const settingData = async (row) => {
        if(row.hasOwnProperty("target_id")) setVideoVisible(true);
        else setVideoVisible(false);
        setStatus(row);
        
        var param = {};
        param['tri_id'] = row['tri_id'];
        param['main_num'] = row['main_num'];
        param['sub_num'] = row['sub_num'];
        if(row["dir"]=="상행"||row["dir"]=="하행") param["dir"] = (row["dir"]=="상행")? 0 : 1;
        else param['dir'] = row['dir'];
        param['ts_id'] = row['ts_id'];
        param['t10m_id'] = row['t10m_id'];

        var lineRes = await server_connect(serviceUrl["pavement"]["selectRoadSectionLineExt"], param);
        if(lineRes && lineRes["status"]===200){
            lineRes = lineRes["data"]["res"];
            setNextLink(lineRes["next"]);
            setPrevLink(lineRes["prev"]);
            makeLine(lineRes["tsList"], row['ts_id'], lineRes["t10mList"], row['t10m_id']);
        }

        setSectionInfo({
            ...sectionInfo,
            frontImg : row["frontImgPath"],
            paveImg : row["paveImgPath"],
            rank : (row.hasOwnProperty("rankstr")) ? row["rankstr"] : "",
            no : (row.hasOwnProperty("no")) ? row["no"] : "",
            name : (row.hasOwnProperty("name")) ? row["name"] : "",
            dir : (row.hasOwnProperty("dir")) ? row["dir"] : "",
        });

        var rowList = [];
        rowField.map((obj)=>{
            if(row.hasOwnProperty(obj['id'])) obj['value'] = row[obj['id']];
            else obj['value'] = '';
            rowList.push(obj);
        });
        setRows(rowList);

        var repairList = [];
        repairField.map((obj)=> {
            if(row.hasOwnProperty(obj['id'])) obj['value'] = row[obj['id']];
            else obj['value'] = '';
            repairList.push(obj);
        });
        setRepairRows(repairList);

        var aiList = [];
        aiField.map((obj)=>{
            if(row.hasOwnProperty(obj['id'])) obj['value'] = row[obj['id']];
            else obj['value'] = '';
            aiList.push(obj);
        });
        setAiRows(aiList);

        if(!videoPlay) initMovie(row);
    }
    
    const makeAIDetectArea = (resJson) => {
        if(videoMode != "pave") {
          return "";
        }
        var objList = JSON.parse(resJson.replace(/\'/g,"\""));
        var item;
        var html = [];
        var style;
        
        var videoWidth = 201;
        var videoHeight = 520;
        var orgWidth = 3872.0;
        var orgHeight = 10000.0;
        var rx = videoWidth / orgWidth;
        var ry = videoHeight / orgHeight;
        var bgColor;

        for(var inx = 0;inx<objList.length;++inx){
          item = objList[inx];
          switch(item.class){
            case "aligator":
            case "vertical":
            case "horizontal":
            case "danbu":
            case "charo":
            case "sigong":
            case "bansa":
            case "millim":
              bgColor="red";
              break;
            case "sigong joint":
            case "sinchuk joint":
              bgColor = "blue";
              break;
            case "manhole":
            case "baesu":
              bgColor = "green";
              break;
            case "lane":
            case "crosswalk":
            case "road sign":
              bgColor = "yellow";
              break;
            case "pothole":
            case "sonsang repair":
            case "gulchak repair":
              bgColor = "orange";
              break;
          }

          style = { 
              left : (item.left_x * rx )+"px", 
              top : (item.top_y * rx )+"px",
              width : (item.width * rx )+"px",
              height : (item.height * rx )+"px",
              backgroundColor : bgColor
          };
          html.push(<div className="tpms-ai-element" style={style} ></div>);
        }
        return html;    
    }

    const initMovie = (row) => {
        var html = '';
        if(row.hasOwnProperty("res_json")&&row["res_json"].length>0){
            if(row["res_json"]!=="[]") {
                html = makeAIDetectArea(row["res_json"]);
            }
        }

        if(videoMode==="front" && row.hasOwnProperty("frontVideo")){
            var url = row["frontVideo"];
            if(row.hasOwnProperty("seq") && row["seq"] > -1) url += '#t='+(row["seq"]/10.0);
            // if(row.hasOwnProperty("resJ"))
            setVideoData({width: 924, height: 520, url : url, html : html});
        }else if (videoMode==="pave" && row.hasOwnProperty("paveVideo")){
            var url = row["paveVideo"];
            if(row.hasOwnProperty("seq") && row["seq"] > -1) url += '#t='+(row["seq"]/10.0);

            setVideoData({width: 201, height: 520, url : url, html : html});
        }
    }

    useEffect(()=>{
        if(status) {initMovie(status);}
    }, [videoMode]);
 
    const makeLine = (list, ts_id, _t10mList, t10m_id) => {
        var info = {
            targetCol : "target",
            extCol : "sm_ext"
        };
        var tsList = list.map((row)=>{
            if(row["ts_id"]===ts_id)  row["target"] = 1;
            return row;
        });

        var t10mList = _t10mList.map((row)=>{
            if(row["id"]===t10m_id) row["target"] = 1;
            return row;
        });
        setLineContent(<MakeDirUnitLine key="routeSectionDirLine" dir='C' list={tsList} 
                        unique='routeSectionDirLine' info={info} end={true} standardDir="U" type="line" t10mList={t10mList}/>);
    }

    const btnClickEvt = (type) => {
        var t10mId = null;
        var props = [];
        var inx = -1;
        
        if(type==='prev'){
            if(pervLink==null){
                alert("시점입니다.");
                return;
            }
            t10mId = pervLink['id'];
            props['geo_x'] = pervLink['geox'];
            props['geo_y'] = pervLink['geoy'];
            inx = state10m["index"] -1;
        }else if(type==='next'){
            if(nextLink==null){
                alert("종점입니다.");
                return;
            }
            t10mId = nextLink['id'];
            props['geo_x'] = nextLink['geox'];
            props['geo_y'] = nextLink['geoy'];
            inx = state10m["index"] +1;
        }else if(type==='first'){
            if(!status.hasOwnProperty("target_id")){
                alert("장비 조사 데이터 미입력 구간입니다.");
                return;
            }
            for( var i=0; i<currentLaneList.length; ++i ){
                if(currentLaneList[i].hasOwnProperty("target_id")&&status.hasOwnProperty("target_id")){
                    if(currentLaneList[i]["target_id"]===status["target_id"]) { 
                        inx = i; 
                        t10mId = currentLaneList[i]["t10m_id"]; 
                        props['geo_x'] = currentLaneList[i]['geox'];
                        props['geo_y'] = currentLaneList[i]['geoy'];
                        break;
                    }
                }
            }
        }else if(type==='last'){
            if(!status.hasOwnProperty("target_id")){
                alert("장비 조사 데이터 미입력 구간입니다.");
                return;
            }
            for( var i=(currentLaneList.length-1); i>-1; --i ){
                if(currentLaneList[i].hasOwnProperty("target_id")&&status.hasOwnProperty("target_id")){
                    if(currentLaneList[i]["target_id"]===status["target_id"]) { 
                        inx = i; 
                        t10mId = currentLaneList[i]["t10m_id"];
                        props['geo_x'] = currentLaneList[i]['geox'];
                        props['geo_y'] = currentLaneList[i]['geoy'];
                        break;
                    }
                }
            }
        }
        
        geoXYData(props);
        resetData();
        setState10m({id : t10mId, index: inx});
    }
    
    const movieChangeEvt = (option) => {
        setVideoMode(option['value']);
    }

    const movieSpeedChangeEvt = (option) => {
        setVideoSpeed(option['value']);
    }
    
    const playBtnEvt = (evt) => {
        if(playText==="▶") setPlayText("||");
        else setPlayText("▶");
        setVideoPlay(!videoPlay);
    }
    
   
    
    
    return (
        <Draggable
            handle=".handle"
            defaultPosition={{ x: 0, y: 0 }}
            position={null}
            grid={[30, 30]}
        >
            <Modal isOpen={modal} toggle={modalOpenControl}  backdrop="static" style={{maxWidth:'unset'}} >
                <ModalHeader toggle={modalOpenControl}  className="handle">
                    <span style={{fontWeight:"bold", fontFamily:"emoji"}}>
                    {"  "+sectionInfo["rank"]+
                    ((sectionInfo["no"])? " "+sectionInfo["no"]+"호선":"") +
                    ((sectionInfo["name"])? " "+sectionInfo["name"]:"") +
                    " "+sectionInfo["dir"]+" "} 
                    </span>
                    <span style={{fontSize:"15px"}}>구간 정보창</span>
                </ModalHeader>
                
                <ModalBody>
                    <div style={{display:"flex", padding: "10px 20px", alignItems: "center"}} >
                        <Button className="leftBtn" onClick={()=>btnClickEvt('prev')} >◀</Button>
                        <div style={{width:"100%", height:"25px"}} >
                            <div style={{display:"contents"}} > 
                                {lineContent}
                            </div>
                        </div>
                        <Button className="rightBtn" onClick={()=>btnClickEvt('next')} >▶</Button>
                    </div>
                    <div className="container"> 
                        <div className="tab-div left">
                            <Tabs variant="pills" defaultActiveKey="movie" className="mb-3">
                                
                                {/* <Tab  eventKey="movie" title="동영상" >
                                    <Player
                                        playsInline
                                        // poster=""
                                        src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                                    />
                                </Tab> */}
                                <Tab eventKey="movie" title="동영상" style={{display: videoVisible ? 'block' : 'none' }}>
                                    <div className="movie-container">
                                        <div className="movie-btn-area">
                                            <SearchDropdown options={movieSelectBox} placeholder="선택(전방/포장)" onChange={movieChangeEvt} width="140" selectInx="1"/>
                                            <Button color="info" className="movie-play-btn" onClick={()=>btnClickEvt('first')} >{"<<"}</Button>
                                            <Button color="info" className="movie-play-btn" onClick={()=>btnClickEvt('prev')}>{"<"}</Button>
                                            <Button color="info" className="movie-play-btn" onClick={playBtnEvt} >{playText}</Button>
                                            <Button color="info" className="movie-play-btn" onClick={()=>btnClickEvt('next')} >{">"}</Button>
                                            <Button color="info" className="movie-play-btn"  onClick={()=>btnClickEvt('last')} >{">>"}</Button>
                                            <SearchDropdown options={movieSpeedSelectBox} placeholder="영상 속도 선택" onChange={movieSpeedChangeEvt} width="140" selectInx="1" />
                                        </div>
                                        <div className="movie-area">
                                            { (videoData)? <TpmsVideo url={videoData["url"]} width={videoData["width"]} height={videoData["height"]} speed={videoSpeed} 
                                                                        play={videoPlay} html={videoData["html"]} allData={currentLaneList} currentInx={state10m["index"]}
                                                                        callback={callbackVideo} mode={videoMode} /> : ''}
                                        </div>
                                    </div>
                                </Tab>
                                {/* <Tab eventKey="frontImg" title="전방이미지" >
                                    <Image src={sectionInfo["frontImg"]} fluid/>
                                </Tab>
                                <Tab eventKey="paveImg" title="포장이미지" style={{textAlign: "center"}} >
                                    <Image src={sectionInfo["paveImg"]} fluid style={{objectFit: "cover", height: "500px"}} />
                                </Tab> */}
                            </Tabs>
                        </div>
                        <div className="vr"></div>
                        <div className="tab-div right">
                            <Tabs variant="pills" defaultActiveKey="paveState" className="mb-3">
                                <Tab eventKey="paveState" title="포장현황" className="route-section-info">
                                    <PropertiGrid key="1" rows={rows}/>
                                </Tab>
                                <Tab eventKey="aiState" title="균열율AI분석" className="route-section-info">
                                    <PropertiGrid key="2" rows={aiRows} />
                                </Tab>
                                <Tab eventKey="repairRecord" title="보수이력" className="route-section-info" >
                                    <PropertiGrid key="3" rows={repairRows}/>
                                </Tab>
                                {/* <Tab eventKey="traffic" title="교통량" >
                                    <PropertiGrid key="4" rows={rows}/>
                                </Tab> */}
                            </Tabs>
                        </div>
                    </div>

               
                </ModalBody>
                {/* <ModalFooter>
                    <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
                    <Button color="secondary" onClick={toggle}>닫기</Button>
                </ModalFooter> */}
            </Modal>
        </Draggable>
    );
}

// export default RouteSectionInfo;
const geoDataDispatchToProps  = dispatch => {
    return {
        geoXYData: (props) => dispatch({ type: actionTypes.GEO_LON_LAT, data: props }),
    }
}

export default connect(null, geoDataDispatchToProps)(RouteSectionInfo);