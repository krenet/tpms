import React, {useState, useRef, useEffect} from 'react';
import {Button, Label, Input, InputGroup, InputGroupAddon, Form} from 'reactstrap';
import SearchDropdown from './SearchDropdown';
import './Memo.css';
import { server_connect, server_connect_withfile } from '../../tpms-common';
import serviceUrl from '../../request-url-list';


const Memo = ({coord, removeLayer, mapParam, layerParam, afterSaveLayer}) => {
    // const [subModal, setSubModal] = useState(isOpen);
    const [formObj, setFormObj] = useState({});
    const [categoryList, setCategoryList]= useState([
        {label: '파손', value: '파손'},
        {label: '균열', value: '균열'},
        {label: '공사', value: '공사'},
        {label: '민원', value: '민원'},
        {label: '기타', value: '기타'},
        {label: '확인완료', value: '확인완료'},
        {label: '처리중', value: '처리중'},
        {label: '처리완료', value: '처리완료'}
    ]);
    const [category, setCategory] = useState();
    const [titleText, setTitleText] = useState();
    const [contentText, setContentText] = useState();
    const [alermUse, setAlermUse] = useState();

    const [filename, setFilename] = useState("");
    const [file, setFile] = useState(null);
    const _file = useRef();


    const categoryOnChange = (e) => {
        setCategory(e["label"]);
        setFormObj({ ...formObj, category : e['value']});
    }

    const titleOnChange = (e) => setFormObj({ ...formObj, title : e.target.value });

    const contentOnChange = (e) => setFormObj({ ...formObj, content : e.target.value });

    const radioBtnChangeEvt = (e) => {
        setAlermUse(e.target.value);
        setFormObj({...formObj, alerm_yn: e.target.value});
        if(e.target.value==='0'){
            document.getElementById('memo-date').setAttribute('disabled', true);
            document.getElementById('memo-time').setAttribute('disabled', true);
        }else{
            document.getElementById('memo-date').removeAttribute("disabled");
            document.getElementById('memo-time').removeAttribute("disabled");
        }
    }

    const dateChangeEvt = (e) => setFormObj({ ...formObj, date : e.target.value });

    const timeChangeEvt = (e) => setFormObj({ ...formObj, time : e.target.value });
    
    const allFile = (e) => {
        e.preventDefault();
        _file.current.click();
    };

    const fileChange = (e) => {
        setFilename(e.target.files[0].name);
        setFile(e.target.files[0]);
        setFormObj({ ...formObj, file : e.target.files[0].name});
    }
    const textChange = (e) => {};

    const closeMemo = () => {
        document.getElementById('memo-modal').style.display='none';
        removeLayer();
    }
    
    const submitForm = (evt) => {
        evt.preventDefault();
        insertMemo();
    }

    const insertMemo = async() => {
        var data = new FormData();
        var obj = formObj; 
        var message = "";
        if(obj['category']==undefined) message += "(카테고리) ";
        if(obj['title']==undefined) message += "(제목) ";
        if(obj['content']==undefined) message += "(내용) ";
        if(obj['alerm_yn']==undefined) message += "(알람사용여부) ";
        
        if(obj['category']==undefined || obj['title']==undefined || obj['content']==undefined || obj['alerm_yn']==undefined){
            message += "을 선택/입력해 주시기 바랍니다.";
            alert(message);
        }
        if(obj['alerm_yn']=="1" && (obj['date']==undefined || obj['time']==undefined)){
            message += "알람시간을 설정해 주시기 바랍니다.";
            alert(message);
        }

        if(message==""){
            data.append('coord_X', coord[0]);
            data.append('coord_Y', coord[1])
            data.append('category', obj['category']);
            data.append('title', obj['title']);
            data.append('content', obj['content']);
            data.append('alerm_yn', obj['alerm_yn']);
            data.append('date', obj['date']);
            data.append('time', obj['time']);
            if(obj['file']!=undefined){
                data.append('file', obj['file']);
                data.append('filePath', file);

                const config = {
                    header : {
                        'content-type': 'multipart/form-data'
                    }
                };
                     
                // const result = await server_connect_withfile("/tpms/tpms/insertMemo.do", data, config);
                const result = await server_connect_withfile(serviceUrl["pavement"]["insertMemo"], data, config);
                if(result['status']==200){
                    if(result['data']['res']==1){
                        alert("저장이 완료되었습니다.");
                        afterSave();
                    }else{
                        alert("저장이 정상적으로 완료되지 않았습니다. 다시 시도해 주시기 바랍니다");
                    }
                }

            }else{
                obj['coord_X'] = coord[0];
                obj['coord_Y'] = coord[1];
                // const result_noFile = await server_connect("/tpms/tpms/insertMemoWithoutImg.do", obj);
                const result_noFile = await server_connect(serviceUrl["pavement"]["insertMemoWithoutImg"], obj);
                if(result_noFile['status']==200){
                    if(result_noFile['data']['res']==1){
                        alert("저장이 완료되었습니다.");
                        afterSave();
                    }else{
                        alert("저장이 정상적으로 완료되지 않았습니다. 다시 시도해 주시기 바랍니다");
                    }
                }
            }     
        }
    }

    const afterSave = () => {
        document.getElementById('memo-modal').style.display='none';
        // var layer = 'tpms:tpms_memo'; var style = 'tpms:tpms_memo_marker'; //var cql = ['(1=1)'];
        // afterSaveLayer(mapParam, layerParam, layer, style);
        afterSaveLayer();
    }

    // useEffect(()=>{
    //     if(formObj){
    //         console.log("formObj: ", formObj);
    //     }
    // }, [formObj])

    // const toggle = () => setSubModal(!subModal);

    return (
         <Form onSubmit={submitForm} id="memo-modal">
            <div>
                <div className="memo-header"><h5 id="memo-title">공간 메모 작성</h5></div>
                <div className="memo-body">
                    <div className="memo-row">
                        <Label className="memo-label">카테고리</Label>
                        <SearchDropdown options={categoryList} onChange={categoryOnChange} placeholder="카테고리 선택"  width="380" /> 
                    </div>
                    <div className="memo-row">
                        <Label className="memo-label">제목</Label>
                        <Input type="text" className="memo-text-input" id="m-title" onChange={titleOnChange} placeholder="제목을 입력하세요"/>
                    </div>
                    <div className="memo-row">
                        <Label className="memo-label">내용</Label>
                        <Input type="text" className="memo-text-input" id="m-content" onChange={contentOnChange} placeholder="내용을 입력하세요"/>
                    </div>
                    <div className="memo-row">
                        <Label className="memo-label">알람사용여부</Label>
                        <div className="radio-group">
                            <Input type="radio" id="alerm-y" name="alermUse" value="1" onChange={radioBtnChangeEvt} checked={alermUse === '1'}/>
                            <Label className="radio-label" check for="alerm-y">사용</Label>
                            <Input type="radio" id="alerm-n" name="alermUse" value="0" onChange={radioBtnChangeEvt} checked={alermUse === '0'}/>
                            <Label className="radio-label" check for="alerm-n">사용안함</Label>                            
                        </div>
                    </div>
                    <div className="memo-row">
                        <Label className="memo-label">알람시간설정</Label>
                        <Input type="date" className="memo-alerm" id="memo-date" onChange={dateChangeEvt}/>
                        <Input type="time" className="memo-alerm" id="memo-time" onChange={timeChangeEvt}/>
                    </div>
                    <div className="memo-row">
                        <Label className="memo-label">사진등록</Label>
                        <InputGroup id="file-input-group">
                            <Input className="file-name" value={filename} placeholder="파일을 선택해주세요." onChange={textChange}/>
                            <InputGroupAddon addonType="append">
                                <input type="file" ref={_file} style={{display:"none"}} onChange={fileChange} accept=".jpg, .png" name="file"/>
                                <Button color="primary" onClick={allFile}>File</Button>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                </div>
                <div className="memo-footer">
                    <Button style={{flex:1}} color="primary" size="sm" type="submit">저장</Button>
                    <Button style={{flex:1}} color="secondary" size="sm" onClick={closeMemo}>닫기</Button>
                </div>
            </div>
         </Form>
    );
}

export default Memo;