import React from 'react';
import {Col, Row, Form, FormGroup, Label, Input} from 'reactstrap';

const ViewLabel = (props) => {
    return (
    <Form>
        <Row form>
            <Col md={2}>
                <FormGroup>
                    {
                        (props.rank)?
                            <>
                                <Label>노선등급</Label>
                                <Input type="text" value={props.rank} readOnly/>
                            </>
                        :
                            ""
                    }
                    
                </FormGroup>
            </Col>
            <Col md={2}>
                <FormGroup>
                    {
                        (props.no)?
                            <>
                                <Label>노선번호</Label>
                                <Input type="text" value={props.no} readOnly/>
                            </>
                        :
                            ""
                    }
                </FormGroup>
            </Col>
            <Col md={2}>
                <FormGroup>
                    {
                        (props.name)?
                            <>
                                <Label>노선명</Label>
                                <Input type="text" value={props.name} readOnly/>
                            </>
                        :
                            ""
                    }
                </FormGroup>
            </Col>
        </Row>
    
    </Form>
    );
}

export default ViewLabel;