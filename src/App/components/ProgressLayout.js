import React, { useEffect, useState } from 'react';
import { Label } from 'reactstrap';
import {CircleProgress} from 'react-gradient-progress'

const ProgressLayout = ({target_id, dataSet}) => {
    const [progressObj, setProgressObj] = useState({ai: 0, pave: 0, front: 0});
    const [targetLabel, setTargetLabel] = useState();

    useEffect(()=>{
        if(dataSet){
            setTargetLabel(target_id);
            var tempObj = new Object();
            dataSet.map((obj)=> {
                tempObj[obj["type"]] = obj["progress"];
            });
            setProgressObj(tempObj);
        }
    }, [dataSet]);

    const changeLayout = (hidden, visible) => {
        document.getElementById(hidden).style.display = 'none';
        document.getElementById(visible).style.display = 'flex';
    }
    
    useEffect(()=>{
        if(progressObj["ai"]===100) changeLayout(targetLabel+"-ai-progress", targetLabel+"-ai-progress-complete");
        if(progressObj["pave"]===100) changeLayout(targetLabel+"-pave-progress", targetLabel+"-pave-progress-complete");
        if(progressObj["front"]===100) changeLayout(targetLabel+"-front-progress", targetLabel+"-front-progress-complete");
    }, [progressObj]);

    
    return <div className="progress-panel">
            <Label className="progress-target-label" >
                <span className="progress-target">
                {targetLabel}
                </span>
            </Label>
            <div className="circle-progress-set">
                <span id={`${targetLabel}-ai-progress`}>
                    <CircleProgress percentage={progressObj["ai"]} strokeWidth={10} secondaryColor="#f0f0f0" width={120}/>
                    <Label className="progress-label">균열율 AI 분석</Label>
                </span>
                <span id={`${targetLabel}-ai-progress-complete`} style={{display:'none', alignItems: 'center'}}>
                    <Label className="progress-label complete-label">균열율 AI 분석 완료</Label>
                </span>
                <span id={`${targetLabel}-pave-progress`}>
                    <CircleProgress percentage={progressObj["pave"]} strokeWidth={10} secondaryColor="#f0f0f0" width={120}/>
                    <Label className="progress-label">포장 이미지 동영상 변환</Label>
                </span>
                <span id={`${targetLabel}-pave-progress-complete`} style={{display:'none', alignItems: 'center'}}>
                    <Label className="progress-label complete-label">포장 이미지 동영상 변환 완료</Label>
                </span>
                <span id={`${targetLabel}-front-progress`}>
                    <CircleProgress percentage={progressObj["front"]} strokeWidth={10} secondaryColor="#f0f0f0" width={120}/>
                    <Label className="progress-label">전방 이미지 동영상 변환</Label>
                </span>
                <span id={`${targetLabel}-front-progress-complete`} style={{display:'none', alignItems: 'center'}}>
                    <Label className="progress-label complete-label">전방 이미지 동영상 변환 완료</Label>
                </span>
            </div>
           </div>;
}

export default ProgressLayout;