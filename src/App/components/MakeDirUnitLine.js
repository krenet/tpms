import React, { useEffect, useRef, useState } from 'react';
import { Label, Button } from 'reactstrap';
import Tooltip from '@material-ui/core/Tooltip';

const MakeDirUnitLine = ({dir, list, unique, info, end, standardDir, type, t10mList}) =>{
    const [dirBtn, setDirBtn] = useState();
    const [contents, setContents] = useState();
    const [areaClass, setAreaClass] = useState();

    useEffect(()=>{
        if(dir=='U'){
            setAreaClass('dir-up-area');
        }else if(dir=='D'){
            setAreaClass('dir-down-area');
        }else{
            setAreaClass('center-area');
        }

        var lines = new Array();  
        var is_start = true;
        var totalExt = 0;

        list.map((obj, inx)=>{
            if(dir!='C'){ // center line이 아닐 때
                if(obj['type']=='0104'){ // link
                    totalExt += obj[info['extCol']];
                    if(end && is_start){ 
                        is_start = false;
                        lines.push(<div key={`start${dir+inx+""}`} className="start-label">0km</div>); 
                    }
                    if(obj.hasOwnProperty(info['targetCol'])){
                        var targetVal = obj[info['targetCol']];
                        var ext = (obj[info['extCol']]*0.001).toFixed(2);
                        if(typeof targetVal == 'number'){
                            lines.push(<div key={`link${inx}`} className={`target${obj[info['targetCol']]} lane-horizontal ${(dir=='U')?'dir-up-div':'dir-down-div'}`}  style={{flex:obj[info['extCol']]}}>
                                            <div className="target-label">{ext}km</div>
                                        </div>);
                        }else if(typeof targetVal == 'string'){
                            lines.push(<div key={`link${inx}`} className={`target1 lane-horizontal ${(dir=='U')?'dir-up-div':'dir-down-div'}`} style={{flex:obj[info['extCol']]}}>
                                            <div className="target-label">{ext}km</div>
                                        </div>);
                        }else{
                            lines.push(<div key={`link${inx}`} className={`default-color lane-horizontal ${(dir=='U')?'dir-up-div':'dir-down-div'}`} style={{flex:obj[info['extCol']]}}>
                                            <div className="target-label">{ext}km</div>
                                        </div>);
                        }
                    }else{
                        lines.push(<div key={`link${inx}`} className={`default-color lane-horizontal ${(dir=='U')?'dir-up-div':'dir-down-div'}`} style={{flex:obj[info['extCol']]}}></div>);
                    }
                }
                if(end && inx==list.length-1){ 
                    lines.push(<div key={`end${dir+inx+""}`} className="end-label">{(totalExt*0.001).toFixed(2)}km</div>); 
                }
            }else{
                var labelClass = (inx/2==0)? "event-node-label-top":"event-node-label-bottom";
                if(type){
                    if(obj['type']=='0104'){
                        if(end && is_start){ 
                            is_start = false;
                            lines.push(<div key={`start${dir+inx+""}`} className="start-label">0km</div>); 
                        }
                        totalExt += obj[info['extCol']];
                        if(obj.hasOwnProperty(info['targetCol'])){
                            t10mList.map((t10m, t10Inx)=>{
                                if(t10m.hasOwnProperty(info['targetCol'])){
                                    // lines.push(<div key={`link${''+inx+t10Inx}`} className='target-10m center-line' style={{flex:t10m[info['extCol']]}}></div>);
                                    lines.push(<div key={`eventArea${inx}`} className="rs-target-node-area">
                                    <div key={`targetNode${inx}`} className="target-10m-node" >
                                        <span className="target-node-label-bottom">Target</span>
                                    </div>
                                   </div>); 
                                }else{
                                    lines.push(<div key={`link${inx+'-'+t10Inx}`} className='ts-section-color center-line' style={{flex:t10m[info['extCol']]}}></div>);
                                }
                            })
                            
                        }else{
                            lines.push(<div key={`link${inx}`} className='road-section-line-color center-line' style={{flex:obj[info['extCol']]}}></div>);
                        }
                        
                    }else{
                        // lines.push(<div key={`node${inx}`} className="event-node" ><span className={labelClass}>{obj['node_name']}</span></div>); 
                        lines.push(<div key={`eventArea${inx}`} className="event-node-area">
                                    <div key={`node${inx}`} className="event-node" >
                                        <span className="rs-event-node-label-top">{obj['node_name']}</span>
                                    </div>
                                   </div>); 
                    }
                    if(end && inx==list.length-1){ 
                        lines.push(<div key={`end${dir+inx+""}`} className="end-label">{(totalExt*0.001).toFixed(2)}km</div>); 
                    }
                }else{
                    if(obj['type']=='0104'){
                        // totalExt += obj[info['extCol']];
                        lines.push(<div key={`link${inx}`} className='center-line-color center-line' style={{flex:obj[info['extCol']]}}></div>);
                    }else{
                        // lines.push(<div key={`node${inx}`} className="event-node" ><span className={labelClass}>{obj['node_name']}</span></div>); 
                        lines.push(<div key={`eventArea${inx}`} className="event-node-area">
                                    <div key={`node${inx}`} className="event-node" >
                                        <span className={labelClass}>{obj['node_name']}</span>
                                    </div>
                                   </div>); 
                    }
                }
                
            }
  
        });
        
        
        var tags = new Array();
        
        if(dir=='U'){
            tags.push(<span key={`${unique}up`} className="dir-text">상</span>);
        }else if(dir=='D'){
            tags.push(<span key={`${unique}down`} className="dir-text">하</span>);
            lines.reverse();
        }else{
            if(!type) tags.push(<span key={`${unique}hidden`} className="dir-text dir-btn-hidden">상</span>);
        }

        if(dir=='C'&&standardDir=='D') lines.reverse();

        setDirBtn(tags);
        setContents(lines);

    }, []);

    return (
        <div className={areaClass}>
            {dirBtn}
            <div className="line-container">
                {contents}
            </div>
        </div>
    );
}

export default MakeDirUnitLine;