import React from 'react';
import {Switch, FormGroup, FormControlLabel} from '@material-ui/core';
import  './SwitchButton.css';

const SwitchButton = ({isOpen, onChange, name, label}) => {
    return (
        <FormGroup>
            <FormControlLabel
            control={
                <Switch
                checked={isOpen}
                onChange={onChange}
                name={name}
                color="primary"/>
            }
            label={label} />
        </FormGroup>
        // <Switch color="primary" 
    );
}

export default SwitchButton;