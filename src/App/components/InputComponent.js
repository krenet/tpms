import React, { useEffect, useState } from 'react';
import {InputGroup, InputGroupAddon, InputGroupText, Input} from 'reactstrap';

const InputComponent = ({label, name, type, json, settingDataEvt, data, min, max, passLabelWidth}) => {

    const [inputTag, setInputTag] = useState("");
    const [value, setValue] = useState(data);
    const [labelWidth, setLabelWidth] = useState("60px");
    useEffect(()=>{
        let _input;
        
        if(type==='string'){
            _input = <Input name={name} className={(json==='true')?"json":""} value={value}/>;
        }else if(type==='number'){
            _input = <Input type="number" min={(min)?min:0} max={(max)?max:999999} step="1" name={name} value={value} className={(json==='true')?"json":""} />;
        }else if(type==='dropdown'){
            _input = <Input type="select" name={name} className={(json==='true')?"json":""} value={value}>
                        <option></option>
                        {settingDataEvt()}
                    </Input>;
        }
        setInputTag(_input);
    }, []);

    useEffect(()=>{
        if(passLabelWidth) setLabelWidth(passLabelWidth);
    }, [passLabelWidth]);
    
    

    return (
        <>
            <InputGroup size="sm" style={{marginBottom: "5px"}}>
                <InputGroupAddon addonType="prepend">
                <InputGroupText style={{width:labelWidth, justifyContent: "center"}}>{label}</InputGroupText>
                </InputGroupAddon>
                {inputTag}
            </InputGroup>
            
        </>
        
        // (type=='string')?
        // <InputGroup size="sm" style={{marginBottom: "5px"}}>
        //     <InputGroupAddon addonType="prepend">
        //     <InputGroupText style={{width:"60px", justifyContent: "center"}}>{label}</InputGroupText>
        //     </InputGroupAddon>
        //     <Input name={name} className={(json=='true')?"json":""}/>
        // </InputGroup>
        // :
        // <InputGroup size="sm" style={{marginBottom: "5px"}}>
        //     <InputGroupAddon addonType="prepend">
        //     <InputGroupText style={{width:"60px", justifyContent: "center"}}>{label}</InputGroupText>
        //     </InputGroupAddon>
        //     <Input type="number" min={0} max={100} step="1" name={name} className={(json=='true')?"json":""} />
        // </InputGroup>
    );

}

export default InputComponent;