import React, { useEffect, useRef, useState } from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import { Label, Button } from 'reactstrap';
import MakeDirUnitLine from './MakeDirUnitLine';

const MakeView = ({setData}) => {
    const [lineList, setLineList] = useState("");
    const areaRef = useRef();

    useEffect(()=>{
        if(setData) {
            var laneUnitList = new Object();
            setData['children'].map((row, index)=>{
                var key = ''+row['main_num']+row['sub_num']; //+row['dir']+1;
                var tempArr = new Array();
                var tempObj = new Object();
                tempObj['info'] = {'main_num':row['main_num'], 'sub_num':row['sub_num']};
                if(laneUnitList.hasOwnProperty(key)){
                    tempArr = laneUnitList[key]['list'];
                }
                
                tempArr.push(row);
                tempObj['list'] = tempArr;
                laneUnitList[key] = tempObj;
            });
            makeMSUnitLayout(laneUnitList, setData['info']);
        }
    }, [setData]);

    const makeMSUnitLayout = (list, info) => {

        var keys = Object.keys(list);
        var tags = new Array();
        keys.map((key, inx)=>{
            var _info = list[key]['info'];
            var classList = (inx!=0)? "ms-label label-top-margin": "ms-label";
            if(_info['main_num']>0){
                tags.push(<Label key={`mslabel${key}${inx}`} className={classList}>주로{_info['main_num']}</Label>);
            }else{
                tags.push(<Label key={`mslabel${key}${inx}`} className={classList}>종속로{_info['sub_num']}</Label>);
            }

            var dirLinkList = makeDirUnitLayout(list[key]['list']);
            dirLinkList = Object.keys(dirLinkList).sort().reduce(
                (obj, key) => {
                    obj[key] = dirLinkList[key];
                    return obj;
                }, {}
            );

            // target Column 같은것끼리 연장 합치기
            var targetCol = info['targetCol'];
            var extCol = info['extCol'];
            
            var dirKeys = Object.keys(dirLinkList);
            var mergeLinkList = new Object();
            var before_target = null;
            dirKeys.map((dir)=>{
                var list = dirLinkList[dir];
                mergeLinkList[dir] = null;
                var tempTarget = '';
                var sumExt = 0;
                var arr = new Array();
                list.map((obj)=>{
                    if(obj['type']=='0105') return;
                    if(obj.hasOwnProperty(targetCol)){
                        if(obj[targetCol]==tempTarget){
                            sumExt += obj[extCol];//obj['sm_ext'];
                        }else{
                            if(sumExt>0){
                                arr = mergeLinkList[dir];
                                var mergeObj = new Object();
                                mergeObj['type'] = '0104';
                                mergeObj[targetCol] = tempTarget;
                                mergeObj[extCol] = sumExt;
                                arr.push(mergeObj);
                                mergeLinkList[dir] = arr;
                            }
                            sumExt = obj[extCol];
                        }
                        tempTarget = obj[targetCol];
                    }else{
                        if(sumExt>0){
                            if(mergeLinkList[dir]==null) arr = new Array();
                            else arr = mergeLinkList[dir];
                            var mergeObj = new Object();
                            mergeObj['type'] = '0104';
                            mergeObj[targetCol] = tempTarget;
                            mergeObj[extCol] = sumExt;
                            arr.push(mergeObj);
                            mergeLinkList[dir] = arr;
                            sumExt=0;
                            tempTarget = '';
                        }
                        arr.push(obj);
                        mergeLinkList[dir] = arr;
                    }
                    
                });
                if(sumExt>0){
                    if(mergeLinkList[dir]==null) arr = new Array();
                    else arr = mergeLinkList[dir];
                    var mergeObj = new Object();
                    mergeObj['type'] = '0104';
                    mergeObj[targetCol] = tempTarget;
                    mergeObj[extCol] = sumExt;
                    arr.push(mergeObj);
                    mergeLinkList[dir] = arr;
                }
            });

            dirKeys = Object.keys(mergeLinkList);
            var centerFlag = true;
            var end = false;
            dirKeys.map((dir, inx)=>{
                if(inx==dirKeys.length-1) {
                    end = true;
                }
                if(dirKeys.length==1){
                    if(dir=='U'){
                        tags.push(<MakeDirUnitLine key={`cline${key+inx+dir}`} dir='C' list={dirLinkList[dirKeys[0]]} unique={'c'+key+inx} info={info} end={end} standardDir="U"/>);
                        tags.push(<MakeDirUnitLine key={`line${key+inx+dir}`} dir={dir} list={mergeLinkList[dir]} unique={key+inx} info={info} end={end}/>); 
                    }else{
                        tags.push(<MakeDirUnitLine key={`line${key+inx+dir}`} dir={dir} list={mergeLinkList[dir]} unique={key+inx} info={info} end={end}/>); 
                        tags.push(<MakeDirUnitLine key={`cline${key+inx+dir}`} dir='C' list={dirLinkList[dirKeys[0]]} unique={'c'+key+inx} info={info} end={end}  standardDir="D"/>);
                    }
                    
                }else{
                    if(centerFlag && dir=='U'){
                        tags.push(<MakeDirUnitLine key={`cline${key+inx+dir}`} dir='C' list={dirLinkList[dirKeys[0]]} unique={'c'+key+inx} info={info} end={end}  standardDir="D"/>);
                        centerFlag = false;
                    }
                    tags.push(<MakeDirUnitLine key={`line${key+inx+dir}`} dir={dir} list={mergeLinkList[dir]} unique={key+inx} info={info} end={end}/>); 
                }
            })

        });
        setLineList(tags);

    }

    const makeDirUnitLayout = (list) => {
        var linkList = new Object(); // up / down 

        list.map((obj)=>{
            var arr = new Array();
            var key = (obj['dir'])==0?'U':'D';
            if(linkList.hasOwnProperty(key)) arr = linkList[key];
            arr.push(obj);
            linkList[key] = arr;
        });

        return linkList;
    }

    // useEffect(()=>{
    //     if(info){
    //         // areaRef.current.style.width = (Math.round(info['totalExt']))+"px";
    //     }
    // }, [info]);

    // useEffect(()=>{
    //     if(list){
    //         var lines = new Array();  
    //         var is_start = true;
    //         list.map((obj, inx)=>{
    //             if(obj['type']=='0104'){ // link
    //                 if(obj.hasOwnProperty(info['targetCol'])){
    //                     var targetVal = obj[info['targetCol']];
    //                     if(typeof targetVal == 'number'){
    //                         lines.push(<div key={`link${inx}`} className={`target${obj[info['targetCol']]} target-horizontal`}  style={{width:`${obj['sm_ext']}px`}}></div>)
    //                     }else if(typeof targetVal == 'string'){
    //                         lines.push(<div key={`link${inx}`} className="target1 target-horizontal" style={{width:`${obj['sm_ext']}px`}}></div>)
    //                     }else{
    //                         lines.push(<div key={`link${inx}`} className="default-color default-horizontal" style={{width:`${obj['sm_ext']}px`}}></div>)    
    //                     }
    //                 }else{
    //                     lines.push(<div key={`link${inx}`} className="default-color default-horizontal" style={{width:`${obj['sm_ext']}px`}}></div>)    
    //                 }
    //             }else{ // node
    //                 lines.push(<Tooltip title={obj['node_name']} key={`tooltip${inx}`}><div key={`node${inx}`} className="event-node"></div></Tooltip>);
    //                 if(is_start){
    //                     is_start = false;
    //                     lines.push(<div key={`node${inx}`} className="start-ext ">0m</div>);
    //                 }else if(inx==(list.length-1)){
    //                     // lines.push(<div key={`node${inx}`} className="end-ext">{`${Math.round(info['totalExt'])}m`}</div>);
    //                 }
    //             }
    //         });
            
    //         setLineList(lines);
    //     }
    // }, [list]);


    return (
        <div className="view-container" >
            <div ref={areaRef} className="view-area" > 
                {lineList}
            </div>
        </div>
    );
}

export default MakeView;