import React, { Component } from "react";
import OlMap from "ol/Map";
import OlView from "ol/View";
import OlLayerTile from "ol/layer/Tile";
import OlLayerVector from "ol/layer/Vector";
import Overlay from 'ol/Overlay';
import Feature from 'ol/Feature.js';
import Point from 'ol/geom/Point.js';
import VectorSource from 'ol/source/Vector';
import { Style, Icon } from 'ol/style';
import XYZ from 'ol/source/XYZ';
import { fromLonLat, toLonLat } from 'ol/proj';
import TileWMS from 'ol/source/TileWMS';
import GeoJSON from 'ol/format/GeoJSON';
import {connect} from 'react-redux';
import "./TpmsMap.css";
import Memo from './Memo';
import SavedMemo from './SavedMemo';
import { ButtonGroup } from 'react-bootstrap';
import ToggleButton from 'react-bootstrap/ToggleButton';
import RouteSectionInfo from "./RouteSectionInfo";
import ProgressLayout from "./ProgressLayout";
import serviceUrl from '../../request-url-list';
import { server_connect } from "../../tpms-common";
import memoIcon from "../../assets/images/marker_red.png";
import { Button } from "reactstrap";
import * as actionTypes from "../../store/actions";

class TpmsMap extends Component {
  baseMap = new OlLayerTile({
          visible: true,
          type: 'base',
          opacity: 1,
          source: new XYZ({
              url: 'http://xdworld.vworld.kr:8080/2d/Base/202002/{z}/{x}/{y}.png',
          }),
  });
  
  getConvertMediaCnt = async() => {
    var res = await server_connect(serviceUrl["pavement"]["getConvertMediaCnt"], {user_id : "test1"});
    if(res && res['status']===200){
      if(res['data']['res']>0) this.connectWebSocket();
    }
  }

  progressLayoutHidden = () => {
    document.getElementById("progress-container").style.display = 'none';
    document.getElementById("progress-hidden-container").style.display = 'none';
  }
  
  progressStateEvt = () => {
    this.progressLayoutHidden();
    document.getElementById("progress-container").style.display = 'block';
  }

  makeProgress = (dataList) => {
    if(!dataList) return;
    var completeCheck = this.allProgressCompleteCheck(dataList);
    if(completeCheck){
      this.progressLayoutHidden();
      this.state.webSocket.close();
      this.setState({progressLayout : ''});
      return;
    }
  
    var keys = Object.keys(dataList);
    var progressSet = keys.map((key)=> {
      return <ProgressLayout key={key} target_id={key} dataSet={dataList[key]} />
    });

    this.setState({progressLayout : progressSet});
  }

  allProgressCompleteCheck = (list) => {
    var keys = Object.keys(list);
    var flag = true;

    for(var k=0; k<keys.length; ++k){
      var target_id = keys[k];
      for(var i=0; i<list[target_id].length;++i){
        var obj = list[target_id][i];
        if(obj["progress"]!==100){
          flag = false;
          break;
        }
      }
      if(!flag) break;
    }
    return flag;
  }
  

  updateLayer() {

  }
  connectWebSocket = async() => {
    if(this.state.webSocket!=null) return;
    var _webSocket = new WebSocket(serviceUrl["etc"]["webSocket"]);
    this.setState({webSocket: _webSocket});
    _webSocket.onopen = (evt) => {
      _webSocket.send("test1");
    }

    _webSocket.onmessage = (evt) => {
      console.log('json parse : ', JSON.parse(evt.data));
      var receive = JSON.parse(evt.data);
      if(receive){
        if(receive.hasOwnProperty("target_id")){
          var dataList = this.state.progressDataList;
          if(dataList.hasOwnProperty(receive["target_id"])){
            var targetList = dataList[receive["target_id"]];
            var tempTargetList = targetList.map((obj)=>{
                if(obj["type"]===receive["type"]){
                  if(receive["type"]==="ai") obj["progress"] = receive[receive["type"]+"_progress"];
                  else obj["progress"] = receive[receive["type"]+"_video_progress"];
                  
                }
                return obj;
            });
            dataList[receive["target_id"]] = tempTargetList;
            this.setState({progressDataList: dataList});
            this.makeProgress(dataList);
          }
        }else{
          document.getElementById("progress-hidden-container").style.display = 'block';
          this.setState({progressDataList: receive});
          this.makeProgress(receive);
        }
      }
  
    }

    _webSocket.onclose = (evt) => {
      this.progressLayoutHidden();
      console.log('close evt : ', evt);
      _webSocket.close();
      this.setState({webSocket: null});
    }

    _webSocket.onerror = (evt) => {
      console.log('error : ', evt);
    }
  }

  

  searchLayer = new OlLayerTile({
                  visible: false,
                  source: null
                });

  nodeLayer = new OlLayerTile({
              visible: false,
              source: new TileWMS({
              url: 'http://192.168.0.104:8080/geoserver/tpms/wms',
              params: {
                          'LAYERS': 'tpms:tpms_sm_master',
                        'TILED': true,
                        STYLES: 'tpms:tpms_node'
                      },
              serverType: 'geoserver',
              transition: 0
              })
  });

  changeLayer = new OlLayerVector({
                  visible: false,
                  source: null,
                });

  changeLayer2 = new OlLayerTile({
                    visible: false,
                    source: null
                });

                
  memo_style = new Style({
    image: new Icon({
    anchor: [0.5, 20],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: memoIcon,
    scale: 1.2
    }),
  });
  // memo_style = new Style({
  //                 image: new Icon({
  //                 anchor: [0.5, 20],
  //                 anchorXUnits: 'fraction',
  //                 anchorYUnits: 'pixels',
  //                 src: 'http://map.vworld.kr/images/ol3/marker_blue.png',
  //                 scale: 1.2
  //                 }),
  //             });

  tileWmsSource = null;

  //지희추가
  grayMap = new OlLayerTile({
    visible: false,
    source: new XYZ({
    url: 'http://xdworld.vworld.kr:8080/2d/gray/202002/{z}/{x}/{y}.png'
    })
  });

  state = {
    center: fromLonLat([127.1575, 37.6093]),
    zoom: 12,
    layers: [
        this.baseMap,
        this.searchLayer,
        this.grayMap,
        this.changeLayer,
        this.changeLayer2,
        this.nodeLayer,
    ],
    cql : [],
    content: null,
    wmsSource: null,
    feature: null,
    markerLayer: null,
    dbCoord: null,
    default: 'colorBtn',
    sectionModal: null,
    // progressTitle: '',
    // progress: 0.0,
    // target_id: '',
    progressLayout: '',
    progressDataList: null,
    webSocket: null
  };


  olmap = new OlMap({
    target: null,
    layers: this.state.layers,
    view: new OlView({
      center: this.state.center,
      zoom: this.state.zoom, // 초기화면 zoom level
      minZoom: 6, // 최소 zoom level
      maxZoom: 19, // 최대 zoom level
    })
  }); 

  //지희추가
  radios = [
    { name: '컬러지도', value: 'colorBtn' },
    { name: '흑백지도', value: 'grayBtn' }
  ];

  toggle = () => { 
    this.setState({sectionModal: null});
  }
  
  updateLayer() {

  }

  updateMap() {
    // this.olmap.getView().setCenter(this.state.center);
    // this.olmap.getView().setZoom(this.state.zoom);
  }
  
  componentDidMount() {
    this.progressLayoutHidden();
    this.initMap();
    // this.connectWebSocket();
    this.getConvertMediaCnt();
  }

  
  
  initMap() {
    this.olmap.setTarget("map");
    // Listen to map changes
    this.olmap.on("moveend", () => {
      let center = this.olmap.getView().getCenter();
      let zoom = this.olmap.getView().getZoom();
      console.log('center : ', center, ', zoom : ', zoom);

    });

    this.olmap.on('singleclick', (evt)=>{
      if(!this.olmap || !this.tileWmsSource) return;
      var viewResolution = /** @type {number} */ (this.olmap.getView().getResolution());
      var url = this.tileWmsSource.getFeatureInfoUrl(
        evt.coordinate, viewResolution, this.olmap.getView().getProjection(), {'INFO_FORMAT':'application/json'}
      );

      if(url){
        fetch(url).then( (response) => { return response.text(); })
                  .then( (json) => { 
                      var res = JSON.parse(json);
                      if(res['features'].length<1) return;
                      console.log('features : ', res["features"]);
                      var geo = toLonLat(evt.coordinate);
                      this.geoXYMarker(geo[0], geo[1]);
                      // this.olmap.getView().setCenter(evt.coordinate);
                      this.setState({sectionModal : <RouteSectionInfo isOpen={true} toggle={this.toggle} info={res['features'][0]}/>});
                  });
      }
      
    });
    
    this.olmap.on('click', (evt)=>{
      var memo_feature = this.olmap.forEachFeatureAtPixel(evt.pixel, (feature)=> {
        return feature;
      })
      if(memo_feature){
        this.setState({dbCoord: null});
        this.setState({content: null});
        var popup2 = new Overlay({
          element: document.getElementById('memoContent'),
          // offset: [280, 500],
          positioning: 'top-left',
        });
        var coordinate = evt.coordinate;
        popup2.setPosition(coordinate);
        this.olmap.addOverlay(popup2);
        this.setState({feature: memo_feature.getProperties()});
        document.getElementById('savedMemo-modal').style.display='block';
      }

    });


    this.olmap.on('dblclick', (evt)=>{
      evt.preventDefault();
      if(this.state.feature ==null){
        this.setState({dbCoord: null});
        this.setState({content: null});

        var coordinate = evt.coordinate;
        // 마커 레이어 지우기
        this.olmap.removeLayer(this.state.markerLayer);
        this.setState({markerLayer: null});

        // 마커 feature
        var feature = new Feature({
          geometry: new Point([coordinate[0], coordinate[1]])
        });
        // 마커 스타일
        var style = new Style({
          image: new Icon({
            anchor: [0.5, 20],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: 'http://map.vworld.kr/images/ol3/marker_blue.png'
          }),
        });
        // feature에 스타일 설정
        feature.setStyle(style);
        // 마커 레이어에 들어갈 소스 생성
        var source = new VectorSource({
          features: [feature]
        });
        // 마커 레이어 생성
        var layer = new OlLayerVector({
          source: source,
          name: 'MARKER'
        });
      
        this.setState({markerLayer: layer});
        // layer의 ZIndex 설정(다른 레이어들 보다 더 앞에 보여주는 기능)
        // layer.setZIndex(6);
        // 지도에 마커추가
        this.olmap.addLayer(layer);
      
        var popup = new Overlay({
          element: document.getElementById('memoContent'),
          positioning: 'top-left',
        });

        popup.setPosition(coordinate);
        this.olmap.addOverlay(popup);
        this.setState({dbCoord: toLonLat(evt.coordinate)});
        this.setState({content: <Memo coord={toLonLat(evt.coordinate)} removeLayer={this.removeMarkerLayer} afterSaveLayer={this.saveMarker}/>})
        document.getElementById('memo-modal').style.display='block';
      }
    });
  }

  //지희추가
  changeColorBtn = (e) => {
    this.setState({ default: e.currentTarget.value });
    if (e.currentTarget.value == 'grayBtn') {
      this.grayMap.setVisible(true);
    } else {
      this.grayMap.setVisible(false);
    }
  }

  removeMarkerLayer = () =>{
    this.olmap.removeLayer(this.state.markerLayer);
    this.setState({markerLayer: null});
  }

  saveMarker = () => {
    this.olmap.removeLayer(this.state.markerlayer);
    this.refreshMemoSource();
  }

  closeEvt = () => {
    this.setState({feature: null});
    this.refreshMemoSource();
  }

  removeSavedMemo = () => {
    this.refreshMemoSource();
    this.setState({feature: null});
    this.olmap.removeLayer(this.state.markerLayer);
  }

  refreshMemoSource = () => {
    this.setState({wmsSource:null});
    this.changeLayer.setSource(null);
    this.olmap.removeLayer(this.changeLayer);


  var memo_source = new VectorSource({
    format: new GeoJSON(),
    url: function(extent){
      return 'http://192.168.0.104:8080/geoserver/tpms/ows?service=WFS&'+
      'version=1.0.0&'+
      'request=GetFeature&'+
      'typeName=tpms%3Atpms_memo&'+
      'outputFormat=application/json';
    },
  });

  this.changeLayer.setSource(memo_source);
  this.olmap.addLayer(this.changeLayer);
  this.setState({wmsSource: memo_source});
  }

  shouldComponentUpdate(nextProps, nextState) {
    let center = this.olmap.getView().getCenter();
    let zoom = this.olmap.getView().getZoom();
    if (center === nextState.center && zoom === nextState.zoom) return false;
    return true;
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps:',nextProps);
      this.collapseLayer();

    if (nextProps.hasOwnProperty('searchMapList') && nextProps.searchMapList) {

        var tsList = new Array();
        nextProps.searchMapList.map((obj) => {
        if (obj.hasOwnProperty("tsList")) tsList = tsList.concat(obj["tsList"]);
        else tsList.push(obj['ts_id']);
        });
        this.showSearchLayer(tsList.join());
        
    }

    if (nextProps.hasOwnProperty("changeLayer") && nextProps.changeLayer) {
        this.changeLayers(nextProps.changeLayer.layer,
                          nextProps.changeLayer.style,
                          nextProps.changeLayer.cqlFilter);
    }
    if (nextProps.hasOwnProperty("geoData") && nextProps.geoData) {
      this.geoXYMarker(nextProps.geoData.geo_x, nextProps.geoData.geo_y);
    }

    if (nextProps.hasOwnProperty("socketFlag") && nextProps.socketFlag){
      this.connectWebSocket();
      this.props.changeSocketFlag(false);
    }
    
  }

  //지희추가
  geoXYMarker(geo_x, geo_y) {
    if(geo_x==null && geo_y==null) return; 
    var marker = "";
    var pos = fromLonLat([geo_x, geo_y]);
    var markerObj = document.getElementById("search-marker");
    if (markerObj) {
      markerObj.remove();
    }
    var element = document.createElement('div');
    element.id = "search-marker";
    element.innerHTML = '<img style="width:27px; transform: translateY(-50%);" src="http://map.vworld.kr/images/ol3/marker_blue.png" />'
    marker = new Overlay({
      position: pos,
      positioning: 'center-center',
      element: element,
      // stopEvent: false,
    });
   
      this.olmap.getView().setCenter(pos);
      this.olmap.addOverlay(marker);
  }

  collapseLayer() {
    this.setState({feature: null});
    this.setState({wmsSource:null});
    this.setState({tileWmsSource:null});
    this.changeLayer.setSource(null);
    this.changeLayer.setVisible(false);
    this.changeLayer2.setVisible(false);
    this.searchLayer.setVisible(false);
    this.nodeLayer.setVisible(false);
  }


  changeLayers = (layer, style, cql)=> {
    if(cql.length>0){
      var filter;
      if (cql.length == 1 && cql[0] == null) filter = null;
      else filter = cql.join(" or ");

      if(cql[0]!="(1=1)") this.nodeLayer.setVisible(true);
   
      if(layer == 'tpms:tpms_memo'){
        var memo_source = new VectorSource({
          format: new GeoJSON(),
          url: function(extent){
            return 'http://192.168.0.104:8080/geoserver/tpms/ows?service=WFS&'+
            'version=1.0.0&'+
            'request=GetFeature&'+
            'typeName=tpms%3Atpms_memo&'+
            'outputFormat=application/json';
          },
        });

        this.changeLayer.setSource(memo_source);
        this.changeLayer.setStyle(this.memo_style);
        this.changeLayer.setVisible(true);
        this.olmap.removeLayer(this.state.markerLayer);
        this.setState({markerLayer: null});
      }else{
        this.tileWmsSource = new TileWMS({
          url: 'http://192.168.0.104:8080/geoserver/tpms/wms',
          params: {
            'LAYERS': layer,
            'TILED': true,
            CQL_FILTER: filter,
            STYLES: style
          },
          serverType: 'geoserver',
          transition: 0
        });
        this.changeLayer2.setSource(this.tileWmsSource);
        this.changeLayer2.setVisible(true);
      }
    } else this.collapseLayer();
  }

  showSearchLayer(tsList) {
    this.searchLayer.setVisible(true);

    if(tsList){
        this.searchLayer.setSource(new TileWMS({
            url: 'http://192.168.0.104:8080/geoserver/tpms/wms',
            params: {
                      'LAYERS': 'tpms:tpms_sm_master', 
                      'TILED': true,
                      CQL_FILTER : `id in (${tsList})`,
                      STYLES: 'tpms_default_layer'
                    },
            serverType: 'geoserver',
            transition: 0
        }));
    }else{ this.searchLayer.setSource(null); }
  }

  progressCloseEvt = () => {
    this.progressLayoutHidden();
    document.getElementById("progress-hidden-container").style.display = 'block';
  }
  
  render() {
    this.updateMap(); // Update map on render?
    return (
      <>
      <div id="map" style={{ width: "100%", height: "92.5vh" }}>
          {/* 지희추가 */}
          <div style={{ position: 'absolute', zIndex: 1, marginTop: 10, right: 30 }}>
            <ButtonGroup toggle size="sm">
              {this.radios.map((radio, inx) => (
                <ToggleButton
                  key={"mapToggleBtn"+inx}
                  type="radio"
                  variant="secondary"
                  name="equip1Radio"
                  value={radio.value}
                  checked={this.state.default === radio.value}
                  onChange={this.changeColorBtn}
                >
                  {radio.name}
                </ToggleButton>
              ))}
            </ButtonGroup>
            </div>
      </div>
      <div id="memoContent" >
        {this.state.content}
        <SavedMemo feature={this.state.feature} closeEvt={this.closeEvt} afterRemove={this.removeSavedMemo}/>
      </div>      
      {this.state.sectionModal}
      <div id="progress-hidden-container">
        <Button color="info" style={{backgroundColor: '#17A2BB', borderColor: '#17A2BB'}} onClick={this.progressStateEvt}>CRACK AI 분석 및 동영상 변환 상태확인</Button>
      </div>
      <div id="progress-container">
        <Button color="info" size="sm" className="progress-close-btn" onClick={this.progressCloseEvt}>X</Button>
        {this.state.progressLayout}
      </div>
      </>

    );
  }
}
const stateToProps = state => {
    return {
    searchMapList: state.searchMapList,
    changeLayer: state.changeLayer,
    geoData: state.geoData,
    socketFlag: state.socketFlag
    }
}

const dispatchToProps  = dispatch => {
  return {
      changeSocketFlag: (props) => dispatch({ type: actionTypes.CONNECT_WEB_SOCKET, flag: props }),
  }
}

export default connect(stateToProps, dispatchToProps)(TpmsMap);