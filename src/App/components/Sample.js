import React, { useState } from 'react';

const Sample = ({id, style}) => {

    return(
        <div style={style} id={id}></div>
    );
}

export default Sample;