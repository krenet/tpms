import React, { useEffect, useState} from 'react';
import { Button } from 'reactstrap';
import {AgGridReact} from 'ag-grid-react';
// import {ClientSideRowModelModule} from 'ag-grid-community';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import './GridComponent.css';

const GridComponent = ({id, columnDefs, rowData, onSelectionChanged, style, isDragAndSelect, rowClassRules, passStateEvt, gridTitle}) => {
    // const [modules, setModules] = useState(ClientSideRowModelModule);
    const [colDefs, setColDefs] = useState(columnDefs);
    const [isDAS, setIsDAS] = useState(isDragAndSelect);
    const [gridApi, setGridApi] = useState(null);
 
    const onGridReady = (params) => {
        setGridApi(params.api);
    };

    useEffect(()=>{
        if(gridApi) {
            if(passStateEvt) passStateEvt(gridApi);
        }
    }, [gridApi]);
    //const [defaultColDef, setDefaultColDef] = useState(options.defaultColDef);
    return (
        <>
        <div className="ag-theme-balham" style={style} id={id}>
        <div className="grid-title">{gridTitle}
        </div>
            {
                isDAS ? 
                (<AgGridReact 
                         columnDefs={colDefs} 
                         rowData={rowData} 
                         rowDragManaged={true}
                         enableMultiRowDragging={true}
                         rowSelection="multiple"
                         rowDeselection={true}
                         animateRows={true}
                         suppressMoveWhenRowDragging={true}
                         suppressMenu={true}
                         rowClassRules={rowClassRules}
                         //animateRows={true}
                        //defaultColDef={defaultColDef}
                        //  onGridReady={this.onGridReady}
                        //  rowData={this.state.rowData}
                         >
                </AgGridReact>)
                :
                (<AgGridReact 
                    // modules={modules}
                    defaultColDef={{
                        resizable: true
                    }}
                    suppressDragLeaveHidesColumns={true}
                    columnDefs={columnDefs} 
                    rowData={rowData} 
                    rowSelection="multiple" 
                    // onRowSelected={onRowSelected}
                    onSelectionChanged={onSelectionChanged}
                    onGridReady={onGridReady}
                    rowClassRules={rowClassRules}/>)
            }
            
        </div>
        </>
    );
}

export default GridComponent;

