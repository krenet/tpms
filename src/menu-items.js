export default {
    items: [
        {
            id: 'road-layer',
            value: 'road-layer',
            label: '도로',
            type: 'group',
            icon: 'icon-navigation',
            visible: true,
            children: [
                {
                    id: 'status',
                    value: 'status',
                    label: '상태현황',
                    type: 'collapse',
                    icon: 'feather icon-box',
                    visible : true,
                    layer: 'tpms:tpms_road_survey_master_tb',
                    style: 'tpms:tpms_default_line',
                    lastChildren: [
                        {
                            id: 'mpci',
                            value: 'mpci',
                            label: '인덱스(MPCI)',
                            type: 'collapse',
                            visible : false,
                            layer : 'tpms:tpms_road_survey_master_tb',
                            style : 'tpms:tpms_mpci', 
                            title : 'status',
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'status-mpci',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql: '(mpci is null or mpci=0) or (mpci<>0 and mpci<2) or (mpci<>0 and mpci>=2 and mpci<4) or (mpci<>0 and mpci>=4 and mpci<6) or (mpci<>0 and mpci>=6 and mpci<8) or (mpci<>0 and mpci>=8)'
                                },
                                {
                                    id: 0,
                                    category: 'status-mpci',
                                    label: '미평가', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: '(mpci is null or mpci=0)'
                                },
                                {
                                    id: 1,
                                    category: 'status-mpci',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: '(mpci<>0 and mpci>=8)'
                                },
                                {
                                    id: 2,
                                    category: 'status-mpci',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: '(mpci<>0 and mpci>=6 and mpci<8)'
                                },
                                {
                                    id: 3,
                                    category: 'status-mpci',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: '(mpci<>0 and mpci>=4 and mpci<6)'
                                },
                                {
                                    id: 4,
                                    category: 'status-mpci',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: '(mpci<>0 and mpci>=2 and mpci<4)'
                                },
                                {
                                    id: 5,
                                    category: 'status-mpci',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    color: 'tpms-red',
                                    cql: '(mpci<>0 and mpci<2)'
                                }
                            ]
                        },
                        {
                            id: 'spi',
                            value: 'spi',
                            label: '인덱스(SPI)',
                            type: 'collapse',
                            visible : false,
                            layer : 'tpms:tpms_road_survey_master_tb',
                            style : 'tpms:tpms_spi', 
                            title : 'status',
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'status-spi',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql: '(spi is null or spi=0) or (spi<>0 and spi<3) or (spi<>0 and spi>=3 and spi<5) or (spi<>0 and spi>=5 and spi<7) or (spi<>0 and spi>=7 and spi<8) or (spi<>0 and spi>=8)'
                                },
                                {
                                    id: 0,
                                    category: 'status-spi',
                                    label: '미평가', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: '(spi is null or spi=0)'
                                },
                                {
                                    id: 1,
                                    category: 'status-spi',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: '(spi<>0 and spi>=8)'
                                },
                                {
                                    id: 2,
                                    category: 'status-spi',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: '(spi<>0 and spi>=7 and spi<8)'
                                },
                                {
                                    id: 3,
                                    category: 'status-spi',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: '(spi<>0 and spi>=5 and spi<7)'
                                },
                                {
                                    id: 4,
                                    category: 'status-spi',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: '(spi<>0 and spi>=3 and spi<5)'
                                },
                                {
                                    id: 5,
                                    category: 'status-spi',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    color: 'tpms-red',
                                    cql: '(spi<>0 and spi<3)'
                                }
                            ]
                        },
                        {
                            id: 'nhpci',
                            value: 'nhpci',
                            label: '인덱스(NHPCI)',
                            type: 'collapse',
                            visible : false,
                            layer : 'tpms:tpms_road_survey_master_tb',
                            style : 'tpms:tpms_nhpci', 
                            title : 'status',
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'status-nhpci',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql: '(nhpci is null or nhpci=0) or (spi<>0 and spi<3) or (spi<>0 and spi>=3 and spi<4) or (spi<>0 and spi>=4 and spi<6) or (spi<>0 and spi>=6 and spi<7) or (spi<>0 and spi>=7)'
                                },
                                {
                                    id: 0,
                                    category: 'status-nhpci',
                                    label: '미평가', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: '(nhpci is null or nhpci=0)'
                                },
                                {
                                    id: 1,
                                    category: 'status-nhpci',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: '(nhpci<>0 and nhpci>=7)'
                                },
                                {
                                    id: 2,
                                    category: 'status-nhpci',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: '(nhpci<>0 and nhpci>=6 and nhpci<7)'
                                },
                                {
                                    id: 3,
                                    category: 'status-nhpci',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: '(nhpci<>0 and nhpci>=4 and nhpci<6)'
                                },
                                {
                                    id: 4,
                                    category: 'status-nhpci',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: '(nhpci<>0 and nhpci>=3 and nhpci<4)'
                                },
                                {
                                    id: 5,
                                    category: 'status-nhpci',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    color: 'tpms-red',
                                    cql: '(nhpci<>0 and nhpci<3)'
                                }
                            ]
                        },
                        {
                            id: 'cr',
                            value: 'cr',
                            label: '균열율',
                            type: 'collapse',
                            visible : true,
                            layer : 'tpms:tpms_road_survey_master_tb',
                            style : 'tpms:tpms_crack', 
                            title : 'status',
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'status-cr',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql: '(cr is null) or (cr>=0 and cr<5) or (cr>=5 and cr<10) or (cr>=10 and cr<15) or (cr>=15 and cr<25) or (cr>=25)'
                                },
                                {
                                    id: 0,
                                    category: 'status-cr',
                                    label: '미평가', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: '(cr is null)'
                                },
                                {
                                    id: 1,
                                    category: 'status-cr',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: '(cr>=0 and cr<5)'
                                },
                                {
                                    id: 2,
                                    category: 'status-cr',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: '(cr>=5 and cr<10)'
                                },
                                {
                                    id: 3,
                                    category: 'status-cr',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: '(cr>=10 and cr<15)'
                                },
                                {
                                    id: 4,
                                    category: 'status-cr',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: '(cr>=15 and cr<25)'
                                },
                                {
                                    id: 5,
                                    category: 'status-cr',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    cql: '(cr>=25)'
                                },
                            ]
                        },
                        {
                            id: 'rutt',
                            value: 'rutt',
                            label: '소성변형',
                            type: 'collapse',
                            visible : true,
                            layer : 'tpms:tpms_road_survey_master_tb',
                            style : 'tpms:tpms_rutt', 
                            title : 'status',
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'status-rutt',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql: '(rutting is null) or (rutting>=0 and rutting<4) or (rutting>=4 and rutting<8) or (rutting>=8 and rutting<12) or (rutting>=12 and rutting<16) or (rutting>=16)'
                                },
                                {
                                    id: 0,
                                    category: 'status-rutt',
                                    label: '미평가', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: '(rutting is null)'
                                },
                                {
                                    id: 1,
                                    category: 'status-rutt',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: '(rutting>=0 and rutting<4)'
                                },
                                {
                                    id: 2,
                                    category: 'status-rutt',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: '(rutting>=4 and rutting<8)'
                                },
                                {
                                    id: 3,
                                    category: 'status-rutt',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: '(rutting>=8 and rutting<12)'
                                },
                                {
                                    id: 4,
                                    category: 'status-rutt',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: '(rutting>=12 and rutting<16)'
                                },
                                {
                                    id: 5,
                                    category: 'status-rutt',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    cql: '(rutting>=16)'
                                },
                            ]
                        },
                        {
                            id: 'iri',
                            value: 'iri',
                            label: '평탄성',
                            type: 'collapse',
                            visible : true,
                            layer : 'tpms:tpms_road_survey_master_tb',
                            style : 'tpms:tpms_iri', 
                            title : 'status',
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'status-iri',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql : '(avg_iri is null) or (avg_iri>=0 and avg_iri<2) or (avg_iri>=2 and avg_iri<4) or (avg_iri>=4 and avg_iri<6) or (avg_iri>=6 and avg_iri<8) or (avg_iri>=8)'
                                },
                                {
                                    id: 0,
                                    category: 'status-iri',
                                    label: '미평가', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: '(avg_iri is null)'
                                },
                                {
                                    id: 1,
                                    category: 'status-iri',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: '(avg_iri>=0 and avg_iri<2)'
                                },
                                {
                                    id: 2,
                                    category: 'status-iri',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: '(avg_iri>=2 and avg_iri<4)'
                                },
                                {
                                    id: 3,
                                    category: 'status-iri',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: '(avg_iri>=4 and avg_iri<6)'
                                },
                                {
                                    id: 4,
                                    category: 'status-iri',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: '(avg_iri>=6 and avg_iri<8)'
                                },
                                {
                                    id: 5,
                                    category: 'status-iri',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    cql: '(avg_iri>=8)'
                                },
                            ]
                        }
                    ]
                },
                {
                    id: 'survey',
                    value: 'survey',
                    label: '이력',
                    type: 'collapse',
                    flag : true,
                    icon: 'feather icon-file-text',
                    visible : true,
                    layer: 'tpms:tpms_road_all_survey_repair_tb', 
                    style: 'tpms:tpms_default_line',
                    lastChildren: [
                        {
                            id: 'survey-year',
                            value: 'survey-year',
                            label: '연도',
                            type: 'collapse',
                            visible : true,
                            title : 'survey',
                            layer: 'tpms:tpms_road_all_survey_repair_tb',
                            style : '', 
                            lastChildren: [
                                // {
                                //     id: -1,
                                //     category: 'survey-year',
                                //     label: '전체', color: 'tpms-all',
                                //     type: 'radio',
                                //     disabled: true
                                // },
                                {
                                    id: 1,
                                    category: 'survey-year',
                                    label: new Date().getFullYear(),
                                    type: 'radio',
                                    disabled: true,
                                    cql: "ooo like '"+new Date().getFullYear()+"%'"
                                },
                                {
                                    id: 2,
                                    category: 'survey-year',
                                    label: new Date().getFullYear()-1,
                                    type: 'radio',
                                    disabled: true,
                                    cql: "ooo like '"+(new Date().getFullYear()-1)+"%'"
                                },
                                {
                                    id: 3,
                                    category: 'survey-year',
                                    label: new Date().getFullYear()-2,
                                    type: 'radio',
                                    disabled: true,
                                    cql: "ooo like '"+(new Date().getFullYear()-2)+"%'"
                                },
                                {
                                    id: 4,
                                    category: 'survey-year',
                                    label: new Date().getFullYear()-3,
                                    type: 'radio',
                                    disabled: true,
                                    cql: "ooo like '"+(new Date().getFullYear()-3)+"%'"
                                },
                                {
                                    id: 5,
                                    category: 'survey-year',
                                    label: new Date().getFullYear()-4,
                                    type: 'radio',
                                    disabled: true,
                                    cql: "ooo like '"+(new Date().getFullYear()-4)+"%'"
                                }
                            ]
                        },
                    ]
                
                },
                {
                    id: 'repair',
                    value: 'repair',
                    label: '보수현황',
                    type: 'collapse',
                    icon: 'feather icon-file-text',
                    visible : true,
                    layer: 'tpms:tpms_road_repair_history_tb',
                    style: 'tpms:tpms_default_line',
                    lastChildren: [
                        {
                            id: 'repair-method',
                            value: 'repair-method',
                            label: '보수공법',
                            type: 'collapse',
                            visible : true,
                            title : 'repair',
                            layer: 'tpms:tpms_road_repair_history_tb',
                            style : 'tpms:tpms_repair_method', 
                            lastChildren: [
                                {
                                    id: -1,
                                    category: 'repair-mathod',
                                    label: '전체', color: 'tpms-all',
                                    type: 'checkbox',
                                    cql: "(method_type is null or method_type='') or (method_type='일상보수') or (method_type='예방적 유지보수') or (method_type='덧씌우기') or (method_type='전단면보수') or (method_type='부분단면보수') or (method_type='균열보수') or (method_type='줄눈보수') or (method_type='미끄럼저항 향상') or (method_type='보강') or (method_type='기타')"
                                },
                                {
                                    id: 0,
                                    category: 'repair-mathod',
                                    label: '미보수', color: 'tpms-gray',
                                    type: 'checkbox',
                                    cql: "(method_type is null or method_type='')"
                                },
                                {
                                    id: 1,
                                    category: 'repair-mathod',
                                    label: '일상보수', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: "(method_type='일상보수')"
                                },
                                {
                                    id: 2,
                                    category: 'repair-mathod',
                                    label: '예방적 유지보수',
                                    type: 'checkbox', color: 'tpms-sky',
                                    cql: "(method_type='예방적 유지보수')"
                                },
                                {
                                    id: 3,
                                    category: 'repair-mathod',
                                    label: '덧씌우기', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: "(method_type='덧씌우기')"
                                },
                                {
                                    id: 4,
                                    category: 'repair-mathod',
                                    label: '전단면보수', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: "(method_type='전단면보수')"
                                },
                                {
                                    id: 5,
                                    category: 'repair-mathod',
                                    label: '부분단면보수', color: 'tpms-red',
                                    type: 'checkbox',
                                    cql: "(method_type='부분단면보수')"
                                },
                                {
                                    id: 6,
                                    category: 'repair-mathod',
                                    label: '균열보수', color: 'tpms-color1',
                                    type: 'checkbox',
                                    cql: "(method_type='균열보수')"
                                },
                                {
                                    id: 7,
                                    category: 'repair-mathod',
                                    label: '줄눈보수', color: 'tpms-color2',
                                    type: 'checkbox',
                                    cql: "(method_type='줄눈보수')"
                                },
                                {
                                    id: 8,
                                    category: 'repair-mathod',
                                    label: '미끄럼저항 향상', color: 'tpms-color3',
                                    type: 'checkbox',
                                    cql: "(method_type='미끄럼저항 향상')"
                                },
                                // {
                                //     id: 9,
                                //     category: 'repair-mathod',
                                //     label: '덧씌우기', color: 'tpms-color4',
                                //     type: 'radio',
                                //     cql: ''
                                // },
                                {
                                    id: 9,
                                    category: 'repair-mathod',
                                    label: '보강', color: 'tpms-color5',
                                    type: 'checkbox',
                                    cql: "(method_type='보강')"
                                },
                                {
                                    id: 10,
                                    category: 'repair-mathod',
                                    label: '기타', color: 'tpms-color6',
                                    type: 'checkbox',
                                    cql: "(method_type='기타')"
                                }
                            ]
                        },
                        // {
                        //     id: 'repair-year',
                        //     value: 'repair-year',
                        //     label: '보수이력',
                        //     type: 'collapse',
                        //     visible : true,
                        //     title : 'repair',
                        //     lastChildren: [
                        //         {
                        //             id: -1,
                        //             category: 'repair-year',
                        //             label: '전체', color: 'tpms-all',
                        //             type: 'radio'
                        //         },
                        //         {
                        //             id: 1,
                        //             category: 'repair-year',
                        //             label: '2020',
                        //             type: 'radio',
                        //             cql: ''
                        //         },
                        //         {
                        //             id: 2,
                        //             category: 'repair-year',
                        //             label: '2019',
                        //             type: 'radio',
                        //             cql: ''
                        //         },
                        //         {
                        //             id: 3,
                        //             category: 'repair-year',
                        //             label: '2018',
                        //             type: 'radio',
                        //             cql: ''
                        //         },
                        //         {
                        //             id: 4,
                        //             category: 'repair-year',
                        //             label: '2017',
                        //             type: 'radio',
                        //             cql: ''
                        //         },
                        //         {
                        //             id: 5,
                        //             category: 'repair-year',
                        //             label: '2016',
                        //             type: 'radio',
                        //             cql: ''
                        //         }
                        //     ]
                        // }
                    ]
                },
                // {
                //     id: 'etcRecord',
                //     value: 'etcRecord',
                //     label: '기타이력',
                //     type: 'collapse',
                //     icon: 'feather icon-file-text',
                //     visible : false,
                //     lastChildren: [
                //         {
                //             id: 'make-year',
                //             value: 'make-year',
                //             label: '준공연도',
                //             type: 'collapse',
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'make-year',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'make-year',
                //                     label: '2020',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'make-year',
                //                     label: '2019',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'make-year',
                //                     label: '2018',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'make-year',
                //                     label: '2017',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'make-year',
                //                     label: '2016',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         },
                //         {
                //             id: 'area-record',
                //             value: 'area-record',
                //             label: '행정구역 변화',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'area-record',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'area-record',
                //                     label: '2020',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'area-record',
                //                     label: '2019',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'area-record',
                //                     label: '2018',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'area-record',
                //                     label: '2017',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'area-record',
                //                     label: '2016',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         }
                //     ]
                
                // },
                {
                    id: 'budget',
                    value: 'budget',
                    label: '예산',
                    type: 'collapse',
                    icon: 'feather icon-map',
                    visible : true,
                    layer: 'tpms:tpms_road_maintain_target_view',
                    style: 'tpms:tpms_default_line',
                    lastChildren: [
                        {
                            id: 'maintain-area',
                            value: 'maintain-area',
                            label: '보수대상구간',
                            type: 'collapse',
                            visible : true,
                            title : 'budget',
                            layer: 'tpms:tpms_road_maintain_target_view',
                            style: 'tpms:tpms_default_line',
                            lastChildren: [
                                // {
                                //     id: -1,
                                //     category: 'budget-maintain-area',
                                //     label: '전체', color: 'tpms-all',
                                //     type: 'checkbox'
                                // },
                                {
                                    id: 1,
                                    category: 'budget-maintain-area',
                                    label: new Date().getFullYear(),
                                    type: 'radio',
                                    cql: '(year='+new Date().getFullYear()+')'
                                },
                                {
                                    id: 2,
                                    category: 'budget-maintain-area',
                                    label: new Date().getFullYear()-1,
                                    type: 'radio',
                                    cql: '(year='+(new Date().getFullYear()-1)+')'
                                },
                                {
                                    id: 3,
                                    category: 'budget-maintain-area',
                                    label: new Date().getFullYear()-2,
                                    type: 'radio',
                                    cql: '(year='+(new Date().getFullYear()-2)+')'
                                },
                                {
                                    id: 4,
                                    category: 'budget-maintain-area',
                                    label: new Date().getFullYear()-3,
                                    type: 'radio',
                                    cql: '(year='+(new Date().getFullYear()-3)+')'
                                },
                                {
                                    id: 5,
                                    category: 'budget-maintain-area',
                                    label: new Date().getFullYear()-4,
                                    type: 'radio',
                                    cql: '(year='+(new Date().getFullYear()-4)+')'
                                }
                            ]
                        },
                    ]
                },
                {
                    id: 'future',
                    value: 'future',
                    label: '미래예측',
                    type: 'collapse',
                    icon: 'feather icon-map',
                    visible : true,
                    layer: 'tpms:tpms_road_after_index_view',
                    style: 'tpms:tpms_future_mpic_index',
                    lastChildren: [
                        {
                            id: 'future-mpci',
                            value: 'future-mpci',
                            label: '도로상태예측(MPCI)',
                            type: 'collapse',
                            visible : false,
                            title : 'future',
                            layer: 'tpms:tpms_road_after_index_view',
                            style: 'tpms:tpms_future_mpic_index',
                            lastChildren: [
                                // {
                                //     id: -1,
                                //     category: 'budget-maintain-area',
                                //     label: '전체', color: 'tpms-all',
                                //     type: 'checkbox'
                                // },
                                {
                                    id: 1,
                                    category: 'future-mpci',
                                    label: new Date().getFullYear(),
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_mpic_index'
                                },
                                {
                                    id: 2,
                                    category: 'future-mpci',
                                    label: new Date().getFullYear()+1,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_mpic_index1'
                                },
                                {
                                    id: 3,
                                    category: 'future-mpci',
                                    label: new Date().getFullYear()+2,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_mpic_index2'
                                },
                                {
                                    id: 4,
                                    category: 'future-mpci',
                                    label: new Date().getFullYear()+3,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_mpic_index3'
                                },
                                {
                                    id: 5,
                                    category: 'future-mpci',
                                    label: new Date().getFullYear()+4,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_mpic_index4'
                                }
                            ]
                        },
                        {
                            id: 'future-spi',
                            value: 'future-spi',
                            label: '도로상태예측(SPI)',
                            type: 'collapse',
                            visible : false,
                            title : 'future',
                            layer: 'tpms:tpms_road_after_index_view',
                            style: 'tpms:tpms_future_spi_index',
                            lastChildren: [
                                // {
                                //     id: -1,
                                //     category: 'budget-maintain-area',
                                //     label: '전체', color: 'tpms-all',
                                //     type: 'checkbox'
                                // },
                                {
                                    id: 1,
                                    category: 'future-spi',
                                    label: new Date().getFullYear(),
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_spi_index'
                                },
                                {
                                    id: 2,
                                    category: 'future-spi',
                                    label: new Date().getFullYear()-1,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_spi_index1'
                                },
                                {
                                    id: 3,
                                    category: 'future-spi',
                                    label: new Date().getFullYear()-2,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_spi_index2'
                                },
                                {
                                    id: 4,
                                    category: 'future-spi',
                                    label: new Date().getFullYear()-3,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_spi_index3'
                                },
                                {
                                    id: 5,
                                    category: 'future-spi',
                                    label: new Date().getFullYear()-4,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_spi_index4'
                                }
                            ]
                        },
                        {
                            id: 'future-nhpci',
                            value: 'future-nhpci',
                            label: '도로상태예측(NHPCI)',
                            type: 'collapse',
                            visible : false,
                            title : 'future',
                            layer: 'tpms:tpms_road_after_index_view',
                            style: 'tpms:tpms_future_nhpci_index',
                            lastChildren: [
                                // {
                                //     id: -1,
                                //     category: 'budget-maintain-area',
                                //     label: '전체', color: 'tpms-all',
                                //     type: 'checkbox'
                                // },
                                {
                                    id: 1,
                                    category: 'future-nhpci',
                                    label: new Date().getFullYear(),
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_nhpci_index'
                                },
                                {
                                    id: 2,
                                    category: 'future-nhpci',
                                    label: new Date().getFullYear()-1,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_nhpci_index1'
                                },
                                {
                                    id: 3,
                                    category: 'future-nhpci',
                                    label: new Date().getFullYear()-2,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_nhpci_index2'
                                },
                                {
                                    id: 4,
                                    category: 'future-nhpci',
                                    label: new Date().getFullYear()-3,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_nhpci_index3'
                                },
                                {
                                    id: 5,
                                    category: 'future-nhpci',
                                    label: new Date().getFullYear()-4,
                                    type: 'radio',
                                    cql: null,
                                    style: 'tpms:tpms_future_nhpci_index4'
                                }
                            ]
                        },
                    ]
                },
                {
                    id: 'others',
                    value: 'others',
                    label: '기타',
                    type: 'collapse',
                    icon: 'feather icon-more-horizontal',
                    visible : true,
                    layer: 'tpms:tpms_memo',
                    style: 'tpms:tpms_memo_marker',
                    lastChildren: [
                        {
                            id: 'others-memo',
                            value: 'others-memo',
                            label: '공간메모불러오기',
                            type: 'collapse',
                            visible : true,
                            title : 'others',
                            layer: 'tpms:tpms_memo',
                            style: 'tpms:tpms_memo_marker',
                            lastChildren: [
                                {
                                  id: 1,
                                  category: 'others-memo',
                                  label: '공간메모', color: 'tpms-blue',
                                  type: 'checkbox',
                                  cql: '1=1'
                                }
                            ]
                        }
                    ]
                },
                // {
                //     id: 'safe',
                //     value: 'safe',
                //     label: '안전',
                //     type: 'collapse',
                //     icon: 'feather icon-lock',
                //     visible : true,
                //     lastChildren: [
                //         {
                //             id: 'geometry',
                //             value: 'geometry',
                //             label: '기하구조',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'geometry',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'geometry',
                //                     label: '좋음',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'geometry',
                //                     label: '보통',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'geometry',
                //                     label: '나쁨',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         },
                //         {
                //             id: 'luminance',
                //             value: 'luminance',
                //             label: '차선도색',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'luminance',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'luminance',
                //                     label: 'A', color: 'tpms-blue',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'luminance',
                //                     label: 'B', color: 'tpms-sky',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'luminance',
                //                     label: 'C', color: 'tpms-green',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'luminance',
                //                     label: 'D', color: 'tpms-yellow',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'luminance',
                //                     label: 'E', color: 'tpms-red',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         },
                //         {
                //             id: 'porthole',
                //             value: 'porthole',
                //             label: '포트홀',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'porthole',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'porthole',
                //                     label: 'A', color: 'tpms-blue',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'porthole',
                //                     label: 'B', color: 'tpms-sky',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'porthole',
                //                     label: 'C', color: 'tpms-green',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'porthole',
                //                     label: 'D', color: 'tpms-yellow',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'porthole',
                //                     label: 'E', color: 'tpms-red',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         },
                //         {
                //             id: 'pave-thin',
                //             value: 'pave-thin',
                //             label: '포장두께',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'pave-thin',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 0,
                //                     category: 'pave-thin',
                //                     label: '미평가', color: 'tpms-gray',
                //                     type: 'checkbox',
                //                     // cql : ''
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'pave-thin',
                //                     label: 'A', color: 'tpms-blue',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'pave-thin',
                //                     label: 'B', color: 'tpms-sky',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'pave-thin',
                //                     label: 'C', color: 'tpms-green',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'pave-thin',
                //                     label: 'D', color: 'tpms-yellow',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'pave-thin',
                //                     label: 'E', color: 'tpms-red',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         },
                //         {
                //             id: 'sag',
                //             value: 'sag',
                //             label: '처짐량',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'sag',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 0,
                //                     category: 'sag',
                //                     label: '미평가', color: 'tpms-gray',
                //                     type: 'checkbox',
                //                     // cql : ''
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'sag',
                //                     label: 'A', color: 'tpms-blue',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'sag',
                //                     label: 'B', color: 'tpms-sky',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'sag',
                //                     label: 'C', color: 'tpms-green',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'sag',
                //                     label: 'D', color: 'tpms-yellow',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'sag',
                //                     label: 'E', color: 'tpms-red',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         },
                //         {
                //             id: 'empty-place',
                //             value: 'empty-place',
                //             label: '지하공동',
                //             type: 'collapse',
                //             visible : true,
                //             lastChildren: [
                //                 {
                //                     id: -1,
                //                     category: 'empty-place',
                //                     label: '전체', color: 'tpms-all',
                //                     type: 'checkbox'
                //                 },
                //                 {
                //                     id: 0,
                //                     category: 'empty-place',
                //                     label: '미평가', color: 'tpms-gray',
                //                     type: 'checkbox',
                //                     // cql : ''
                //                 },
                //                 {
                //                     id: 1,
                //                     category: 'empty-place',
                //                     label: 'A', color: 'tpms-blue',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 2,
                //                     category: 'empty-place',
                //                     label: 'B', color: 'tpms-sky',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 3,
                //                     category: 'empty-place',
                //                     label: 'C', color: 'tpms-green',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 4,
                //                     category: 'empty-place',
                //                     label: 'D', color: 'tpms-yellow',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 },
                //                 {
                //                     id: 5,
                //                     category: 'empty-place',
                //                     label: 'E', color: 'tpms-red',
                //                     type: 'checkbox',
                //                     cql: ''
                //                 }
                //             ]
                //         }
                //     ]
                // }
            ]
        },

        {
            id: 'bridge-layer',
            value: 'bridge-layer',
            label: '교량',
            type: 'group',
            icon: 'icon-navigation',
            visible: false,
            children: [
                {
                    id: 'bridge-status',
                    value: 'bridge-status',
                    label: '현황',
                    type: 'collapse',
                    icon: 'feather icon-box',
                    visible : true,
                    lastChildren: [
                        {
                            id: 'bridge-index',
                            value: 'bridge-index',
                            label: '인덱스',
                            type: 'collapse',
                            visible : true,
                            lastChildren: [
                                {
                                    id: 1,
                                    category: 'index',
                                    label: 'A', color: 'tpms-blue',
                                    type: 'checkbox',
                                    cql: ''
                                },
                                {
                                    id: 2,
                                    category: 'index',
                                    label: 'B', color: 'tpms-sky',
                                    type: 'checkbox',
                                    cql: ''
                                },
                                {
                                    id: 3,
                                    category: 'index',
                                    label: 'C', color: 'tpms-green',
                                    type: 'checkbox',
                                    cql: ''
                                },
                                {
                                    id: 4,
                                    category: 'index',
                                    label: 'D', color: 'tpms-yellow',
                                    type: 'checkbox',
                                    cql: ''
                                },
                                {
                                    id: 5,
                                    category: 'index',
                                    label: 'E', color: 'tpms-red',
                                    type: 'checkbox',
                                    cql: ''
                                }
                            ]
                        },
                    ]
                },
            ]
        },
        
        {
            id: 'Bi-layer',
            value: 'Bi-layer',
            label: '비탈면',
            type: 'group',
            icon: 'icon-navigation',
            visible: false,
            
        },
        
        {
            id: 'gul-layer',
            value: 'gul-layer',
            label: '굴착복구',
            type: 'group',
            icon: 'icon-navigation',
            visible: false,
            
        },
        
        {
            id: 'ggung-layer',
            value: 'ggung-layer',
            label: '가로보안등',
            type: 'group',
            icon: 'icon-navigation',
            visible: false,
            
        },
        {
            id: 'etc1-layer',
            value: 'etc1-layer',
            label: '기타1',
            type: 'group',
            icon: 'icon-navigation',
            visible: false,
            
        }
        // {
            
        //     value: 'etc2-layer',
        //     label: '기타2',
        //     type: 'group',
        //     icon: 'icon-navigation',
            
        // }
    ]

}