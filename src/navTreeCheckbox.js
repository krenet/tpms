export default{
    navItems : [
        {
            id: 'road-layer',
            value:'도로',
            label:'도로',
            type: 'group',
            icon : "",
            //id:'road-layer',
            lastChildren:[
                { value: '현황', label: '현황', icon : "",},
                { value: '조사이력', label: '조사이력', icon : ""},
                { value: '보수이력', label: '보수이력', icon : "" },
                { value: '기타이력', label: '기타이력', icon : "" },
                { value: '예산', label: '예산', icon : "" },
                { value: '안전', label: '안전', icon : "" },
            ]
        },
        {
            id: 'Bi-layer',
            value: '비탈면',
            label:'비탈면',
            type: 'group',
            icon: 'icon-navigation',
            children:[
                { value: '비탈', label: '비탈', icon : ""},
            ]
        },
        {
            id: 'gul-layer',
            value: '굴착복구',
            label:'굴착복구',
            type: 'group',
            icon: 'icon-navigation',
            children:[
                { value: '굴착', label: '굴착', icon : ""},
            ]
        },
        {
            id: 'ggung-layer',
            value: '가로보안등',
            label:'가로보안등',
            type: 'group',
            icon: 'icon-navigation',
            children:[
                { value: '가로', label: '가로', icon : ""},
            ]
        },
        {
            id: 'etc1-layer',
            value: '기타1',
            label:'기타1',
            type: 'group',
            icon: 'icon-navigation',
            children:[
                { value: '기타', label: '기타', icon : ""},
            ]
        }
    ]
}