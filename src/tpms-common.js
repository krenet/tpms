import axios from "axios";
import 'url-search-params-polyfill';
import XMLParser from 'react-xml-parser';
/**
 * 사용 방법
 *   
  import connect from '../server/connect.js';
  
  const [db, setDb] = useState();
  useEffect(()=>{
    let ignore = false;
    async function fetchData(){
      const result = await connect("http://localhost:8080/tpms/tpms/selectTpmsCode.do");
      if(!ignore) { setDb(result);  }
    }
    fetchData();
    return () => { ignore = true; }
  }, []);

  useEffect(()=>{
    console.log('async result:', db);
  }, [db]);

 */
export const server_connect = async (url, param) => {
      var params = new URLSearchParams();
      if(param){
        var keys = Object.keys(param);
        keys.map((key)=>{
            params.append(key, param[key]);
        });
      }

      // 되는거
      const res = await axios.post(url, params).then((res)=>{
          return res;
      }).catch((err)=>{
          console.log('에러 내용 : ', err);
      });
       return res;
}

export const server_connect_withfile = async (url, data, header) => {

    // 되는거
    const res = await axios.post(url, data, {header}).then((res)=>{
        return res;
    }).catch((err)=>{
        console.log('에러 내용 : ', err);
    });
     return res;
}

/**
 * 사용 방법
 *   
    import RoadXML from '../../../../../../../../../setting/road-info.xml';
    import XmlParser from '../../../../../../../../components/XmlParser.js';
  
    const [metaData, setMetaData] = useState();
    const [columns, setColumns] = useState([]);
    useEffect(()=>{
        let ignore = false;
        async function fetchData(){
            const result = await XmlParser(RoadXML);
            if(!ignore) { setMetaData(result);  }
        }
        fetchData();
        return () => { ignore = true; }
    }, []);
    
    useEffect(()=>{
        if(metaData) setColumns(metaData.getElementsByTagName('Columns')[0]['children']);
    }, [metaData]);
    .
    .
    .
    {
        columns.map((obj)=>{
            var attributes = obj['attributes'];
            return attributes['visible']=='true'?<InputComponent label={attributes['title']} />:""
        })
    }

 */
export const XmlParser = async (xml) => {
    const res = await axios.get(xml, {"Content-Type": "application/xml; charset=utf-8;"})
    .then((res)=>{
        return res;
    });
    return new XMLParser().parseFromString(res['data'])
}
